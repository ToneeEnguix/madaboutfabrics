const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const settingsSchema = new Schema({
  name: { type: String, default: "" },
  title: { type: String, default: "" },
  fields: {
    type: Array,
    default: [
      {
        text: "",
        url: "",
      },
      {
        text: "",
        url: "",
      },
      {
        text: "",
        url: "",
      },
    ],
  },
  image: {
    pathname: { type: String, default: "" },
    filename: { type: String, default: "" },
  },
  bgColour: { type: String, default: "000" },
  useBgColour: { type: Boolean, default: false },
  colour: { type: String, default: "FFF" },
});

module.exports = mongoose.model("settings", settingsSchema);
