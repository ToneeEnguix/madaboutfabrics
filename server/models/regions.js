const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const regionsSchema = new Schema({

    name: {type:String,required:true},
    currency: {type:String,required:true}, 
    shippingRates:{type:Number, required:true}

});

module.exports =  mongoose.model('region', regionsSchema);