const mongoose = require('mongoose')
const Schema = mongoose.Schema

const variantsSchema = new Schema({
  realColor: [{ type: Schema.Types.ObjectId, ref: 'color', required: true }],
  color: { type: String, required: true },
  sku: { type: String, required: true },
  imageURLs: [],
  thumbnailURL: { type: String },
  availableForPickup: { type: Boolean, default: true },
  dateAdded: { type: Date, default: Date.now },
  saleDiscount: { type: Number, default: 0 },
  priceDrop: { type: Boolean, default: false },
  priceDropDate: { type: Date, default: null },
  favorited: { type: Number, default: 0 },
  metersSold: { type: Number, default: 0 },
})

const rangeSchema = new Schema({
  name: { type: String, required: true, default: '' },
  description: { type: String, default: '' },
  price: { type: Number, required: true, default: 0 },
  design: { type: Schema.Types.ObjectId, ref: 'design' },
  type: { type: Schema.Types.ObjectId, ref: 'type' },
  texture: { type: Schema.Types.ObjectId, ref: 'texture' },
  dateAdded: { type: Date, default: Date.now },
  usage: {
    upholstery: { type: Boolean, default: false },
    curtains: { type: Boolean, default: false },
    craft: { type: Boolean, default: false },
  },
  industry: {
    hospitality: { type: Boolean, default: false },
    healthcare: { type: Boolean, default: false },
    contract: { type: Boolean, default: false },
  },
  composition: { type: String, default: '' },
  fabricRepeat: { type: Number, default: 0 },
  fabricWidth: { type: Number, default: 138 },
  fabricCare: {
    machineWashable: { type: Boolean, default: false },
    dryClean: { type: Boolean, default: true },
    wipeClean: { type: Boolean, default: false },
    handWash: { type: Boolean, default: false },
    thirtyClean: { type: Boolean, default: false },
    nonDetergent: { type: Boolean, default: false },
    doNotIron: { type: Boolean, default: false },
  },
  variants: [variantsSchema],
  keyword01: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  keyword02: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  keyword03: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  keyword04: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  adminTag01: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  adminTag02: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  adminTag03: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  adminTag04: { type: Schema.Types.ObjectId, ref: 'tag', default: null },
  active: { type: Boolean, default: true },
  supplier: { type: String, default: '' },
  sample: { type: Boolean, default: true },
  inStock: { type: Boolean, default: true },
  inspired: { type: Boolean },
  pvc: { type: Boolean },
  accessoires: { type: Boolean },
  url: { type: String, default: '', unique: false },
})

const orderSchema = new Schema({
  orderNumber: { type: Number, required: true },
  user: {
    _id: { type: Schema.Types.ObjectId, ref: 'user' },
    email: { type: String, required: true },
    name: { type: String, required: true },
  },
  orderDate: { type: Date, default: Date.now },
  productsBought: [
    {
      range: rangeSchema,
      amount: { type: Number },
      variantIndex: { type: Number },
      discount: { type: Number, default: 0 },
    },
  ],
  shipmentCost: { type: Number, required: true },
  shipmentAddress: {
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    direction: { type: String, required: true },
    town: { type: String, required: true },
    postalCode: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    email: { type: String, required: true },
    region: {
      name: { type: String, required: true },
      coin: { type: String, required: true },
      shippingRate: { type: String, required: true },
      situation: { type: String, required: true },
    },
  },
  billingAddress: {
    name: { type: String, required: true },
    lastName: { type: String, required: true },
    direction: { type: String, required: true },
    town: { type: String, required: true },
    postalCode: { type: String, required: true },
    phoneNumber: { type: String, required: true },
    email: { type: String, required: true },
    region: {
      name: { type: String, required: true },
      coin: { type: String, required: true },
      shippingRate: { type: String, required: true },
      situation: { type: String, required: true },
    },
  },
  shippingMethod: { type: String, required: true },
  completed: { type: Boolean, default: false },
  newOrder: { type: Boolean, default: true },
  trackingNumber: { type: String, default: '' },
  paymentStatus: { type: String, default: '' },
})

module.exports = mongoose.model('orders', orderSchema)
