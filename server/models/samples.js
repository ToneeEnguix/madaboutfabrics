const mongoose = require('mongoose')
const Schema = mongoose.Schema

const variantsSchema = new Schema({
  realColor: [{ type: Schema.Types.ObjectId, ref: 'color', required: true }],
  color: { type: String, required: true },
  sku: { type: String, required: true },
  imageURLs: [],
})

const rangeSchema = new Schema({
  name: { type: String, required: true, default: '' },
  description: { type: String, default: '' },
  price: { type: Number, required: true, default: 0 },
  variants: [variantsSchema],
  active: { type: Boolean, default: true },
  supplier: { type: String, default: '' },
  url: { type: String, default: '', unique: false },
})

const sampleSchema = new Schema({
  user: {
    _id: { type: Schema.Types.ObjectId, ref: 'user', required: true },
    email: { type: String, required: true },
    name: { type: String, required: true },
  },
  orderDate: { type: Date, default: Date.now },
  requestedSamples: [
    {
      range: rangeSchema,
      variantIndex: { type: Number, required: true },
    },
  ],
  status: { type: Boolean, default: false },
  shipmentAddress: {
    name: { type: String },
    lastName: { type: String },
    direction: { type: String },
    town: { type: String },
    postalCode: { type: String },
    region: { type: String },
  },
  shippingMethod: { type: String },
  index: { type: Number },
  newSample: { type: Boolean, default: true },
})

module.exports = mongoose.model('samples', sampleSchema)
