const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_fuzzy_searching = require('mongoose-fuzzy-searching');

const colorsSchema = new Schema({

    name: { type: String, required: true },
});

colorsSchema.plugin(mongoose_fuzzy_searching, {
    fields: ["name"]
})
module.exports = mongoose.model('color', colorsSchema);
