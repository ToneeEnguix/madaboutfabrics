const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_fuzzy_searching = require('mongoose-fuzzy-searching');

const designSchema = new Schema({

    name: {type:String,required:true}, 
  
});

designSchema.plugin(mongoose_fuzzy_searching, {
    fields: ["name"]
})
module.exports =  mongoose.model('design', designSchema);