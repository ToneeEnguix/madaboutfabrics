const mongoose = require('mongoose')
const Schema = mongoose.Schema

const addressSchema = new Schema({
  name: { type: String },
  lastName: { type: String },
  direction: { type: String },
  extraDetail: { type: String },
  town: { type: String },
  postalCode: { type: String },
  phoneNumber: { type: String },
})

const userSchema = new Schema({
  email: { type: String, required: true },
  name: { type: String, required: true, unique: false },
  lastName: { type: String },
  region: {},
  password: { type: String, required: true },
  address: addressSchema,
  wishlist: [
    {
      range: { type: Schema.Types.ObjectId, ref: 'range' },
      variantIndex: Number,
    },
  ],
  cart: [
    {
      range: { type: Schema.Types.ObjectId, ref: 'range' },
      variantIndex: Number,
      amount: Number,
    },
  ],
  sampleCart: [
    {
      range: { type: Schema.Types.ObjectId, ref: 'range' },
      variantIndex: Number,
    },
  ],
  dateCreated: { type: Date, default: Date.now() },
  admin: { type: Boolean, default: false },
})

userSchema.methods.toJSON = function () {
  var obj = this.toObject()
  delete obj.password
  return obj
}
module.exports = mongoose.model('user', userSchema)
