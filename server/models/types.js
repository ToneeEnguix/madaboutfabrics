const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoose_fuzzy_searching = require('mongoose-fuzzy-searching');

const typeSchema = new Schema({

    name: {type:String,required:true}, 

});

typeSchema.plugin(mongoose_fuzzy_searching, {
    fields: ["name"]
})
module.exports =  mongoose.model('type', typeSchema);