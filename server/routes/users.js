const express = require('express'),
  router = express.Router(),
  controller = require('../controllers/users.js')
const protectedRoute = require('../security/protectedRoute.js')

router.get('/length', controller.getLength)

router.get('/some/:index/:order/:sort', controller.getSome)

router.get('/search/:search', controller.search)

router.get('/get_user_info/:user_id', controller.getUserInfo)

router.get('/count/:user_id', controller.count)

router.post('/signin', controller.signIn)

router.post('/signup', controller.signUp)

router.post('/save', protectedRoute, controller.save)

router.post('/newpassword', protectedRoute, controller.newPassword)

router.post('/adminsignup', controller.adminSignup)

router.post('/adminlogin', controller.adminLogin)

module.exports = router
