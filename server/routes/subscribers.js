const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/subscribers.js");

router.post("/subscribe", controller.subscribe);

module.exports = router;
