const express = require('express'),
  router = express.Router(),
  controller = require('../controllers/ranges.js')

router.post('/addrange', controller.addRange)

router.post('/newrange', controller.newRange)

router.post('/update', controller.update)

router.post('/delete', controller.delete)

router.get('/sales', controller.getSales)

router.get('/trending', controller.getTrending)

router.get('/new', controller.getNew)

router.get('/accessories', controller.getAccessories)

router.get('/ranges', controller.getRanges)

router.get('/pricedrop', controller.getPriceDrop)

router.get('/getdraft', controller.getDraft)

router.get('/all/:size/:page', controller.getAll)

router.get('/allranges', controller.getAllVariants)

router.get('/getrange/:id', controller.getRange)

router.get('/inspired', controller.inspired)

router.get('/sale', controller.sale)

router.get('/favorited/:range_id/:variant_index/:sign', controller.favorited)

router.get('/suggestions', controller.getSuggestions)

router.get('/suggestions/color/:color', controller.getSuggestionsByColor)

router.get('/suggestions/texture/:texture', controller.getSuggestionsByTexture)

module.exports = router
