const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/types.js");

router.get("/all", controller.getAll);

router.post("/delete", controller.delete);

router.post("/add", controller.addOne);

router.post("/edit", controller.edit);

module.exports = router;
