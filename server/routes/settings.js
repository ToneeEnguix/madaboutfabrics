const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/settings.js");

router.get("/all", controller.getAll);

router.get("/:setting", controller.getOne);

router.post("/update", controller.update);

module.exports = router;
