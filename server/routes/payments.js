const express = require ('express'),
  router = express.Router (),
  controller = require ('../controllers/payments.js');

router.post ('/create-payment-intent', controller.createPaymentIntent);

module.exports = router;
