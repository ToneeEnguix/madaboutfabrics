const express = require('express'),
  router = express.Router(),
  controller = require('../controllers/samples.js')

router.get('/get_some/:index', controller.getSome)

router.get('/count', controller.count)

router.get('/search/:search', controller.search)

router.get('/getnew', controller.getNew)

router.put('/purge', controller.purge)

router.get('/get_user_samples/:userid', controller.getUserSamples)

router.get('/get_sample_info/:sampleid', controller.getSampleInfo)

router.put('/changestatus/:sampleid', controller.changeStatus)

router.post('/request/:userid', controller.requestSamples)

router.get('/getlength/:userid', controller.getSamplesLength)

module.exports = router
