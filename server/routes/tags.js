const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/tags.js");

router.get("/all", controller.all);

router.post("/delete", controller.delete);

router.post("/add", controller.addOne);

router.get("/minislider", controller.miniSlider);

router.post("/tominislider", controller.toMiniSlider);

router.post("/edit", controller.edit);

module.exports = router;
