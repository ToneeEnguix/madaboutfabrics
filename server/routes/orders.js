const express = require('express'),
  router = express.Router(),
  controller = require('../controllers/orders.js')

router.get('/get_some/:count_index', controller.getSome)

router.get('/count', controller.count)

router.get('/search/:search', controller.search)

router.get('/getnew', controller.getNew)

router.put('/purge', controller.purge);

router.get('/get_user_orders/:user_id', controller.getUserOrders)

router.get('/get_order_info/:order_id', controller.getOrderInfo)

router.put('/changestatus/:order_id', controller.changeStatus)

router.post('/request/:user_id', controller.create)

router.get('/update/:order_id', controller.updateOrder)

module.exports = router
