const express = require("express"),
  router = express.Router(),
  controller = require("../controllers/photo.js");

router.post("/postphoto", controller.postphoto);

router.post("/deletephoto", controller.deletephoto);

module.exports = router;
