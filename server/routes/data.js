const express = require('express'),
    router = express.Router(),
    controller = require('../controllers/addData.js');


router.get('/add',controller.add);


module.exports = router;