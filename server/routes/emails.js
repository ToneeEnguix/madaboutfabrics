const router = require('express').Router(),
  controller = require('../controllers/emails.js')

router.post(
  '/client_sample_confirmation',
  controller.client_sample_confirmation
)

router.post('/client_order_confirmation', controller.client_order_confirmation)

router.post(
  '/client_sample_confirmation',
  controller.client_sample_confirmation
)

module.exports = router
