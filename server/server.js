require('dotenv').config()
const express = require('express')
const app = express()
const port = process.env.PORT || 7777

// SETTINGS
app.use(express.urlencoded({ extended: true }))
app.use(express.json())

// CONNECT TO MONGO
;(async function connecting() {
  try {
    await require('mongoose').connect(process.env.MONGO, {
      useUnifiedTopology: true,
      useNewUrlParser: true,
      useFindAndModify: false,
      useCreateIndex: true,
    })
    console.log('Connected to the DB')
  } catch (error) {
    console.log(
      'ERROR: Seems like your DB is not running, please start it up !!!'
    )
  }
})()

app.use(require('cors')())
// serving static files from public folder under the route /assets
app.use('/assets', express.static(__dirname + '/media'))

app.use('/security', require('./security/securityRoute.js'))
app.use('/orders', require('./routes/orders.js'))
app.use('/samples', require('./routes/samples.js'))
app.use('/ranges', require('./routes/ranges.js'))
app.use('/users', require('./routes/users.js'))
app.use('/regions', require('./routes/regions.js'))
app.use('/subscribers', require('./routes/subscribers.js'))
app.use('/data', require('./routes/data.js'))
app.use('/tags', require('./routes/tags.js'))
app.use('/designs', require('./routes/designs.js'))
app.use('/types', require('./routes/types.js'))
app.use('/textures', require('./routes/textures.js'))
app.use('/colours', require('./routes/colours.js'))
app.use('/settings', require('./routes/settings.js'))
app.use('/photo', require('./routes/photo.js'))
app.use('/emails', require('./routes/emails.js'))
app.use('/payments', require('./routes/payments.js'))

const path = require('path')
app.use(express.static(__dirname))
app.use(express.static(path.join(__dirname, '../build')))
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '../build', 'index.html'))
})

app.listen(port, () => console.log(`listening on port ${port}`))
