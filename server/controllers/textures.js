const textures = require("../models/textures.js");

class TexturesController {
  async getAll(req, res) {
    try {
      const data = await textures.find();
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async delete(req, res) {
    const { item } = req.body;
    try {
      const data = await textures.deleteOne({ _id: item._id });
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async addOne(req, res) {
    const { newItem } = req.body;
    try {
      const data = await textures.create({ name: newItem });
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async edit(req, res) {
    const { item } = req.body;
    try {
      const data = await textures.updateOne(
        { _id: item._id },
        { name: item.name }
      );
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send(err);
    }
  }
}

module.exports = new TexturesController();
