const settings = require("../models/settings.js");

class SettingsController {
  async getAll(req, res) {
    try {
      const data = await settings.find();
      if (data.length > 0) {
        res.status(200).send({ ok: true, data });
      } else {
        const created = await settings.insertMany([
          { name: "mini_slider" },
          { name: "hero_banner" },
          { name: "upholstery" },
          { name: "curtain" },
          { name: "craft" },
          { name: "featured" },
        ]);
        res.status(200).send({ ok: true, data: created });
      }
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async getOne(req, res) {
    const setting = req.params.setting;
    try {
      const data = await settings.findOne({ name: setting });
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async update(req, res) {
    const settingsReceived = req.body;
    try {
      settingsReceived.map(async (setting) => {
        await settings.updateOne(
          { _id: setting._id },
          {
            name: setting.name,
            title: setting.title,
            fields: setting.fields,
            image: setting.image,
            bgColour: setting.bgColour,
            useBgColour: setting.useBgColour,
            colour: setting.colour,
          }
        );
      });
      res.status(200).send({ ok: true });
    } catch (err) {
      res.status(500).send(err);
    }
  }
}

module.exports = new SettingsController();
