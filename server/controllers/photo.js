const multer = require('multer')
const fs = require('file-system')

// MULTER SETTINGS
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    // const date = new Date()
    // const month = `${date.getMonth().length > 1 ? '' : '0'}${
    //   date.getMonth() + 1
    // }`
    // const year = date.getFullYear()
    // cb(null, `./assets/${year}/${month}/`)
    cb(null, `./media`)
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  },
})
const upload = multer({
  storage: storage,
  limits: { fieldSize: '256mb' },
}).single('file')

class PhotoController {
  async postphoto(req, res) {
    try {
      upload(req, res, async function (err) {
        if (err instanceof multer.MulterError) {
          return res.status(500).json(err)
          // A Multer error occurred when uploading.
        } else if (err) {
          return res.status(500).json(err)
          // An unknown error occurred when uploading.
        }
        // here we can add filename or path to the DB
        const newImage = {
          pathname: req.file.path,
          filename: req.file.filename,
        }
        return res.status(200).send({ newImage })
      })
    } catch (error) {
      res.status(500).send({ msg: 'something went wrong' })
    }
  }

  async deletephoto(req, res) {
    const { filename, pathname } = req.body
    try {
      if (pathname.includes('media')) {
        fs.unlink(`./media/${filename}`, (err) => {
          err
            ? res.status(500).send({ ok: true, msg: "File doesn't exist" })
            : res
                .status(200)
                .send({ ok: true, msg: 'Photo deleted successfully!' })
        })
      } else {
        fs.unlink(`./${pathname.slice(0, -8) + filename}`, (err) => {
          err
            ? res.status(500).send({ ok: true, msg: "File doesn't exist" })
            : res
                .status(200)
                .send({ ok: true, msg: 'Photo deleted successfully!' })
        })
      }
    } catch (error) {
      res.status(500).send({ msg: 'Something went wrong' })
    }
  }
}

module.exports = new PhotoController()
