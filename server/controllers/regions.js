const regions = require("../models/regions.js");

class RegionController {
  async getAllRegions(req, res) {
    try {
      let regionsList = await regions.find().sort({ name: 1 });
      res.status(200).send(regionsList);
    } catch (error) {
      res.status(500).send(error);
    }
  }
}

module.exports = new RegionController();
