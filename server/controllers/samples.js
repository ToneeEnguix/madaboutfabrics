const Users = require('../models/users.js')
const Samples = require('../models/samples.js')
var mongoose = require('mongoose')

class SamplesController {
  async getSome(req, res) {
    let { index } = req.params
    try {
      const samples = await Samples.find({})
        .skip(Number(index) * 20)
        .limit(20)
        .populate('user')
        .populate('requestedSamples.range')
      res.status(200).send({ ok: true, samples })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async count(req, res) {
    try {
      const count = await Samples.estimatedDocumentCount()
      res.status(200).send({ ok: true, count })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async search(req, res) {
    const { search } = req.params
    try {
      let samples = await Samples.aggregate([
        {
          $search: {
            index: 'search_sample',
            text: {
              query: search,
              path: {
                wildcard: '*',
              },
            },
          },
        },
      ]).limit(10)
      res.status(200).send({ ok: true, samples })
    } catch (err) {
      console.error(err)
      res.status(500).send({ ok: false, err })
    }
  }

  async getNew(req, res) {
    try {
      const allSamples = await Samples.find({ newSample: true })
        .populate('user')
        .populate('requestedSamples.range')
      res.status(200).send({ ok: true, allSamples })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async purge(req, res) {
    try {
      await Samples.updateMany(
        { newSample: true, status: true },
        { newSample: false }
      )
      res.status(200).send({ ok: true })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async getUserSamples(req, res) {
    const { userid } = req.params
    try {
      const foundSamples = await Samples.find({ 'user._id': userid })
      let samplesArr = []
      foundSamples.forEach((sample) => {
        let newSample = {
          _id: sample._id,
          orderDate: sample.orderDate,
          requestedSamples: [],
          shipmentAddress: sample.shipmentAddress,
          shippingMethod: sample.shippingMethod,
        }
        sample.requestedSamples.forEach((request) => {
          let tempRequest = {
            _id: request.range._id,
            name: request.range.name,
            url: request.range.url,
            variant_name: request.range.variants[request.variantIndex].color,
            variantIndex: request.variantIndex,
            filename:
              request.range.variants[request.variantIndex].imageURLs[0]
                .filename,
          }
          newSample.requestedSamples.push(tempRequest)
        })
        samplesArr.push(newSample)
      })
      res.status(200).send({ ok: true, samples: samplesArr })
    } catch (err) {
      console.error(err)
      res.status(500).send({ ok: false, err })
    }
  }

  async getSampleInfo(req, res) {
    const { sampleid } = req.params
    try {
      if (sampleid.length !== 24) {
        res.status(200).send({ ok: false })
      }
      const sample = await Samples.findOne({ _id: sampleid })
        .populate('user')
        .populate('requestedSamples.range')
      res.status(200).send({ ok: true, sample })
    } catch (err) {
      res.status(500).send({ ok: false })
    }
  }

  async changeStatus(req, res) {
    const { sampleid } = req.params
    try {
      const found = await (
        await Samples.findOne({ _id: sampleid }).populate('user')
      ).populate('requestedSamples.range')
      found.status = !found.status
      !found.newSample && (found.newSample = !found.newSample)
      await found.save()
      res.status(200).send({ ok: true, sample: found })
    } catch (err) {
      res.status(500).send({ ok: false })
    }
  }

  async requestSamples(req, res) {
    const { userid } = req.params
    const { sampleCart, shipmentAddress, shippingMethod } = req.body
    try {
      const user = await Users.findById(userid)
      const count = await Samples.estimatedDocumentCount()
      const sample = await Samples.create({
        user: {
          _id: mongoose.Types.ObjectId(userid),
          name: user.name,
          email: user.email,
        },
        requestedSamples: sampleCart,
        shipmentAddress,
        shippingMethod,
        index: count + 1,
      })
      res.status(200).send({ ok: true, sample })
    } catch (err) {
      console.error(err)
      res.status(500).send({ ok: false, err })
    }
  }

  async getSamplesLength(req, res) {
    const { userid } = req.params
    try {
      const userSamples = await Samples.find({ 'user._id': userid })
      res.status(200).send({ ok: true, samplesLength: userSamples.length })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }
}

module.exports = new SamplesController()
