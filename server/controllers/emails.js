const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
var path = require('path')

const abc = [
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z',
]

// selecting mail service and authorazing with our credentials
let transport = nodemailer.createTransport({
  // you need to enable the less secure option on your gmail account
  // https://myaccount.google.com/lesssecureapps?pli=1
  service: 'gmail',
  auth: {
    user: process.env.NODEMAILER_EMAIL,
    pass: process.env.NODEMAILER_PASSWORD,
    // type: "OAuth2",
    // clientId: process.env.OAUTH_CLIENTID,
    // clientSecret: process.env.OAUTH_CLIENT_SECRET,
    // refreshToken: process.env.OAUTH_REFRESH_TOKEN,
  },
})

const options = {
  viewEngine: {
    extName: '.handlebars',
    partialsDir: path.resolve(__dirname, 'views'),
    defaultLayout: false,
  },
  viewPath: path.resolve(__dirname, 'views'),
  extName: '.handlebars',
}

transport.use('compile', hbs(options))

const client_order_confirmation = async (req, res) => {
  const {
    email,
    subject,
    message,
    orderNumber,
    productsBought,
    orderDate,
    eta,
    shippingAddress,
    billingAddress,
    shipped,
  } = req.body
  const products = productsBought.map((prod, i) => ({
    color: prod.range.variants[prod.variantIndex].color,
    image: `${process.env.URL}/assets/${
      prod.range.variants[prod.variantIndex].imageURLs[0].filename
    }`,
    url: `cid:${abc[i]}`,
    name: prod.range.name,
    price: prod.range.price,
    amount: prod.amount,
    total: (
      prod.range.price * prod.amount -
      (prod.discount > 0
        ? ((prod.range.price * prod.amount) / 100) * prod.discount
        : 0)
    ).toFixed(2),
    discount: prod.discount,
  }))
  const subtotal = calculateSubtotal(products).toFixed(2)
  const tempTotal = (
    Number(subtotal) +
    (shipped ? Number(shippingAddress.region.shippingRate) : 0)
  ).toFixed(2)
  const mailOptions = {
    from: 'Nodemailer <example@nodemailer.com>',
    to: 'Nodemailer <example@nodemailer.com>',

    from: `Mad About Fabrics ${process.env.NODEMAILER_EMAIL}`,
    replyTo: `Mad About Fabrics ${process.env.NODEMAILER_EMAIL}`,
    to: email,
    subject,
    template: 'order',
    context: {
      message,
      orderNumber,
      orderDate,
      s_name: shippingAddress.name,
      s_direction: shippingAddress.direction,
      s_town: shippingAddress.town,
      s_postalCode: shippingAddress.postalCode,
      s_email: shippingAddress.email,
      s_phoneNumber: shippingAddress.phoneNumber,
      shippingFee: shipped ? shippingAddress.region.shippingRate : 'FREE',
      b_name: billingAddress.name,
      b_direction: billingAddress.direction,
      b_town: billingAddress.town,
      b_postalCode: billingAddress.postalCode,
      b_email: billingAddress.email,
      b_phoneNumber: billingAddress.phoneNumber,
      eta,
      productsBought: products,
      subtotal: subtotal,
      shippingCosts: shipped
        ? Number(shippingAddress.region.shippingRate).toFixed(2)
        : '00.00',
      total: tempTotal,
      shipped: shipped,
    },
    attachments: products.map((prod, i) => ({
      filename: abc[i],
      path: prod.image.toString(),
      cid: abc[i],
    })),
  }
  try {
    transport.sendMail(mailOptions, function (err, data) {
      if (err) {
        console.error('err: ', err)
        return res.status(500).send({ ok: false, message: err })
      } else {
        return res.status(200).send({ ok: true, message: 'Email sent!' })
      }
    })
  } catch (err) {
    console.error('err: ', err)
    return res.status(500).send({ ok: false, message: err })
  }
}

const client_sample_confirmation = async (req, res) => {
  const {
    email,
    subject,
    message,
    sampleNumber,
    productsBought,
    orderDate,
    eta,
    shippingAddress,
    shipped,
  } = req.body
  const products = productsBought.map((prod, i) => ({
    color: prod.range.variants[prod.variantIndex].color,
    image: `${process.env.URL}/assets/${
      prod.range.variants[prod.variantIndex].imageURLs[0].filename
    }`,
    url: `cid:${abc[i]}`,
    name: prod.range.name,
  }))
  const mailOptions = {
    from: 'Nodemailer <example@nodemailer.com>',
    to: 'Nodemailer <example@nodemailer.com>',

    from: `Mad About Fabrics ${process.env.NODEMAILER_EMAIL}`,
    replyTo: `Mad About Fabrics ${process.env.NODEMAILER_EMAIL}`,
    to: email,
    subject,
    template: 'sample',
    context: {
      message,
      sampleNumber,
      orderDate,
      s_name: shippingAddress.name,
      s_direction: shippingAddress.direction,
      s_town: shippingAddress.town,
      s_postalCode: shippingAddress.postalCode,
      s_email: shippingAddress.email,
      s_phoneNumber: shippingAddress.phoneNumber,
      s_country: shippingAddress.region.name,
      shipped,
      eta,
      productsBought: products,
    },
    attachments: products.map((prod, i) => ({
      filename: abc[i],
      path: prod.image.toString(),
      cid: abc[i],
    })),
  }
  try {
    transport.sendMail(mailOptions, function (err, data) {
      if (err) {
        console.error('err: ', err)
        return res.status(500).send({ ok: false, message: err })
      } else {
        return res.status(200).send({ ok: true, message: 'Email sent!' })
      }
    })
  } catch (err) {
    console.error('err: ', err)
    return res.status(500).send({ ok: false, message: err })
  }
}

const calculateSubtotal = (cart) => {
  let tempTotal = 0

  cart.forEach((ele) => {
    tempTotal +=
      ele.price * ele.amount -
      (ele.discount > 0 ? ((ele.price * ele.amount) / 100) * ele.discount : 0)
  })
  return tempTotal
}

// const client_sample_confirmation = async (req, res) => {
//   const { name, email, subject, message } = req.body
//   const mailOptions = {
//     from: process.env.NODEMAILER_EMAIL,
//     replyTo: process.env.NODEMAILER_EMAIL,
//     to: email,
//     subject: subject,
//     html: '<p>Hello, ' + name + '</p><p>' + message + '</p>',
//   }
//   try {
//     transport.sendMail(mailOptions, function (err, data) {
//       if (err) {
//         res.status(500).send({ ok: false, message: err })
//       } else {
//         res.status(200).send({ ok: true, message: 'Email sent!' })
//       }
//     })
//   } catch (err) {
//     res.status(500).send({ ok: false, message: err })
//   }
// }

module.exports = {
  client_sample_confirmation,
  client_order_confirmation,
}
