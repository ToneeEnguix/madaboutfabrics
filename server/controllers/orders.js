const Users = require('../models/users.js')
const Orders = require('../models/orders.js')
var mongoose = require('mongoose')

class OrdersController {
  async getSome(req, res) {
    let { count_index: countIndex } = req.params
    try {
      const orders = await Orders.find({})
        .skip(Number(countIndex) * 20)
        .limit(20)
        .populate('user')
        .populate('productsBought.range')
      res.status(200).send({ ok: true, orders })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async count(req, res) {
    try {
      const count = await Orders.estimatedDocumentCount()
      res.status(200).send({ ok: true, count })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async search(req, res) {
    const { search } = req.params
    try {
      let orders = await Orders.aggregate([
        {
          $search: {
            index: 'search',
            text: {
              query: search,
              path: {
                wildcard: '*',
              },
            },
          },
        },
      ])
      res.status(200).send({ ok: true, orders })
    } catch (err) {
      console.error(err)
      res.status(500).send({ ok: false, err })
    }
  }

  async getNew(req, res) {
    try {
      const allOrders = await Orders.find({ newOrder: true })
        .populate('user')
        .populate('productsBought.range')
      res.status(200).send({ ok: true, allOrders })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async purge(req, res) {
    try {
      await Orders.updateMany(
        { newOrder: true, completed: true },
        { newOrder: false }
      )
      res.status(200).send({ ok: true })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async getUserOrders(req, res) {
    const { user_id } = req.params
    try {
      const foundOrders = await Orders.find({ 'user._id': user_id }).populate(
        'productsBought.range'
      )
      let ordersArr = []
      foundOrders.forEach((order) => {
        let newOrder = {
          _id: order._id,
          orderDate: order.orderDate,
          productsBought: [],
          shipmentAddress: order.shipmentAddress,
          shippingMethod: order.shippingMethod,
          completed: order.completed,
          newOrder: order.newOrder,
          paymentMethod: order.paymentMethod,
          trackingNumber: order.trackingNumber,
          orderNumber: order.orderNumber,
        }
        order.productsBought.forEach((request) => {
          let tempRequest = {
            _id: request.range._id,
            name: request.range.name,
            url: request.range.url,
            variant_name: request.range.variants[request.variantIndex].color,
            variantIndex: request.variantIndex,
            filename:
              request.range.variants[request.variantIndex].imageURLs[0]
                .filename,
            amount: request.amount,
            price: request.range.price,
            discount: request.discount,
          }
          newOrder.productsBought.push(tempRequest)
        })
        ordersArr.push(newOrder)
      })
      res.status(200).send({ ok: true, orders: ordersArr })
    } catch (err) {
      console.error(err)
      res.status(500).send({ ok: false, err })
    }
  }

  async getOrderInfo(req, res) {
    const { order_id } = req.params
    try {
      if (order_id.length !== 24) {
        res.status(200).send({ ok: false })
      }
      const order = await Orders.findOne({ _id: order_id })
        .populate('user')
        .populate('productsBought.range')
      res.status(200).send({ ok: true, order })
    } catch (err) {
      res.status(500).send({ ok: false })
    }
  }

  async changeStatus(req, res) {
    const { order_id } = req.params
    try {
      if (order_id.length !== 24) {
        res.status(200).send({ ok: false })
      }
      const order = await Orders.findById(order_id)
      order.completed = !order.completed
      !order.newOrder && (order.newOrder = !order.newOrder)
      await order.save()
      res.status(200).send({ ok: true, order })
    } catch (err) {
      console.error(err)
      res.status(500).send({ ok: false })
    }
  }

  async create(req, res) {
    const { user_id } = req.params
    const {
      cart,
      shipmentAddress,
      shippingMethod,
      billingAddress,
      shipmentCost,
    } = req.body
    try {
      let user = user_id !== 'undefined' ? await Users.findById(user_id) : ''
      const count = await Orders.estimatedDocumentCount()
      const tempCart = [...cart]
      tempCart.forEach((ele) => {
        const variant = ele.range.variants[ele.variantIndex]
        ele.discount =
          variant.saleDiscount > 0 &&
          variant.priceDrop &&
          new Date(variant.priceDropDate) > new Date(Date.now())
            ? variant.saleDiscount
            : 0
      })
      const order = await Orders.create({
        orderNumber: count + 1,
        user: {
          _id: user ? user._id : undefined,
          name: user ? user.name : 'No user',
          email: user ? user.email : 'No email',
        },
        productsBought: cart,
        shipmentCost,
        shipmentAddress,
        billingAddress,
        shippingMethod,
      })
      res.status(200).send({ ok: true, order })
    } catch (err) {
      console.info(err)
      res.status(500).send({ ok: false, err })
    }
  }

  async updateOrder(req, res) {
    const { order_id } = req.params
    try {
      if (order_id.length !== 24) {
        res.status(200).send({ ok: false })
      }
      const order = await Orders.findOneAndUpdate(
        { _id: order_id },
        { paymentStatus: 'completed' }
      )
      res.status(200).send({ ok: true, order })
    } catch (err) {
      res.status(500).send({ ok: false })
    }
  }
}

module.exports = new OrdersController()
