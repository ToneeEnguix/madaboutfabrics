const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY)

class TypesController {
  async createPaymentIntent(req, res) {
    try {
      const paymentIntent = await stripe.paymentIntents.create({
        amount: Number((req.body.amount * 100).toFixed(0)),
        currency: 'gbp',
        payment_method_types: ['card'],
      })

      res.status(200).send({ ok: true, paymentIntent })
    } catch (err) {
      console.error('err: ', err)
      res.status(500).send({ ok: false, err })
    }
  }
}

module.exports = new TypesController()
