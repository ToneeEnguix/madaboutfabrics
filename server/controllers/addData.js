const colors = require("../models/colors.js");
const designs = require("../models/designs.js");
const tags = require("../models/tags.js");
const textures = require("../models/textures.js");
const types = require("../models/types.js");
const ranges = require("../models/ranges.js");

const colorsArr = require("../../dataHandler/colors");
const designsArr = require("../../dataHandler/designs");
const texturesArr = require("../../dataHandler/texture");
const typesArr = require("../../dataHandler/type");
const tagsArr = require("../../dataHandler/tags");
const rangeArr = require("../../dataHandler/script");

class DataController {
  async add(req, res) {
    try {
      colorsArr.forEach((color) => {
        colors.create(color);
      });
      designsArr.forEach((design) => {
        designs.create(design);
      });
      tagsArr.forEach((tag) => {
        tags.create(tag);
      });
      texturesArr.forEach((texture) => {
        textures.create(texture);
      });
      typesArr.forEach((type) => {
        types.create(type);
      });
      rangeArr.forEach((range) => {
        ranges.create(range);
      });
      res.send("ok");
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = new DataController();
