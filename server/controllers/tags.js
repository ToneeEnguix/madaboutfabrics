const tags = require('../models/tags.js')

class TagsController {
  async all(req, res) {
    try {
      const data = await tags.find()
      res.status(200).send({ ok: true, data })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async delete(req, res) {
    const { item } = req.body
    try {
      const data = await tags.deleteOne({ _id: item._id })
      res.status(200).send({ ok: true, data })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async addOne(req, res) {
    const { newItem } = req.body
    try {
      const data = await tags.create({ name: newItem })
      res.status(200).send({ ok: true, data })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async miniSlider(req, res) {
    try {
      const miniSlider = await tags.find({ minislider: true })
      res.status(200).send(miniSlider)
    } catch (err) {
      res.status(500).send({ ok: false })
    }
  }

  async toMiniSlider(req, res) {
    const { toMiniSlider } = req.body
    try {
      const found = await tags.findOne({
        name: toMiniSlider,
      })
      found.minislider = !found.minislider
      await found.save()
      res.status(200).send({ ok: true, found })
    } catch (err) {
      res.status(500).send({ ok: false })
    }
  }

  async edit(req, res) {
    const { item } = req.body
    try {
      const data = await tags.updateOne(
        { _id: item._id },
        { name: item.name, url: item.url }
      )
      res.status(200).send({ ok: true, data })
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = new TagsController()
