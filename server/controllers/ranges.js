const ranges = require('../models/ranges.js')
const colors = require('../models/colors.js')
const designs = require('../models/designs.js')
const textures = require('../models/textures.js')
const types = require('../models/types.js')
const tags = require('../models/tags')
var mongoose = require('mongoose')
const cloudinary = require('cloudinary')

// const { RangesToVariants } = require('./Helpers/RangesToVariants.js')

cloudinary.config({
  cloud_name: 'ckellytv',
  api_key: '884113981828137',
  api_secret: 'lMAtVMwjbTz-mPc7udjQStS4I_E',
})

class RangeController {
  async getAll(req, res) {
    const { size, page } = { ...req.params }
    try {
      const allRanges = await ranges
        .find({ active: true })
        .populate(
          'design type texture adminTag01 adminTag02 adminTag03 adminTag04 keyword01 keyword02 keyword03 keyword04'
        )
        .sort({ dateAdded: -1 })
        .populate('variants.realColor')
        .skip(Number(page) * Number(size))
        .limit(Number(size))
      const count = await ranges.countDocuments()
      res.status(200).send({ data: allRanges, count })
    } catch (err) {
      console.error(err)
      res.status(500).send(err)
    }
  }

  async getAllVariants(req, res) {
    try {
      const allRanges = await ranges
        .find({ active: true })
        .populate(
          'design type texture adminTag01 adminTag02 adminTag03 adminTag04 keyword01 keyword02 keyword03 keyword04'
        )
        .populate('variants.realColor')
        .sort({ dateAdded: -1 })
      res.send({ data: allRanges })
    } catch (err) {
      console.error(err)
      res.status(500).send(err)
    }
  }

  async addRange(req, res) {
    const newRange = req.body.range
    try {
      const designID = await designs.findOne({ name: newRange.design })
      const textureID = await textures.findOne({ name: newRange.texture })
      const typeID = await types.findOne({ name: newRange.type })
      const keyword01ID = await tags.findOne({ name: newRange.keyword01 })
      const keyword02ID = await tags.findOne({ name: newRange.keyword02 })

      const variantsObject = [...newRange.variants]

      await Promise.all(
        newRange.variants.map(async (variant, index) => {
          const colorID = await colors.findOne({ name: variant.actualColor })
          variantsObject[index].realColor = colorID._id
        })
      )

      const range = await ranges.create({
        name: newRange.name,
        onPriceDrop: newRange.onPriceDrop,
        colors: newRange.colors,
        availableforPickup: newRange.availableforPickup,
        saleDiscount: newRange.saleDiscount,
        design: designID._id,
        type: typeID._id,
        imageURLs: newRange.imageURLS,
        thumbnailURL: newRange.thumnailURL,
        fabricCare: newRange.care,
        dateAdded: newRange.dateAdded,
        usage: newRange.usage,
        description: newRange.description,
        texture: textureID._id,
        keyword01: keyword01ID._id,
        keyword02: keyword02ID._id,
        industry: newRange.industry,
        composition: newRange.composition,
        fabricRepeat: newRange.fabricRepeat,
        fabricWidth: newRange.fabricWidth,
        variants: variantsObject,
      })
      res.status(200).send(range)
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async newRange(req, res) {
    const { product } = req.body
    try {
      const data = ranges.create(product)
      res.status(200).send({ ok: true, data })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async update(req, res) {
    let { product } = req.body
    try {
      if (product.variants[0].realColor[0]?.name) {
        product.variants.forEach((item) => {
          item.realColor.forEach((colour) => {
            colour = mongoose.mongo.ObjectId(item.realColor._id)
          })
        })
      }
      for (let i = 1; i < 5; i++) {
        const name = 'keyword0' + i
        const name2 = 'adminTag0' + i
        if (product[name]) {
          product[name] = mongoose.mongo.ObjectId(product[name]._id)
        } else if (product[name2]) {
          product[name2] = mongoose.mongo.ObjectId(product[name2]._id)
        } else {
          product[name] = null
          product[name2] = null
        }
      }
      const arr = ['design', 'type', 'texture']
      arr.map((item) => {
        if (!product[item]) {
          product[item] = ''
        }
        product[item] = mongoose.mongo.ObjectId(product[item]._id)
      })
      await ranges.updateOne(
        {
          _id: product._id,
        },
        {
          name: product.name,
          supplier: product.supplier,
          description: product.description,
          composition: product.composition,
          url: product.url,
          fabricRepeat: product.fabricRepeat,
          fabricWidth: product.fabricWidth,
          sample: product.sample,
          inspired: product.inspired,
          pvc: product.pvc,
          accessoires: product.accessoires,
          fabricCare: product.fabricCare,
          price: product.price,
          usage: product.usage,
          industry: product.industry,
          design: product.design,
          type: product.type,
          texture: product.texture,
          keyword01: product.keyword01,
          keyword02: product.keyword02,
          keyword03: product.keyword03,
          keyword04: product.keyword04,
          adminTag01: product.adminTag01,
          adminTag02: product.adminTag02,
          adminTag03: product.adminTag03,
          adminTag04: product.adminTag04,
          variants: product.variants,
          active: product.active,
        }
      )
      res.send({ ok: true })
    } catch (err) {
      console.error(err)
      res.status(500).send(err)
    }
  }

  async delete(req, res) {
    const { product } = req.body
    try {
      const found = await ranges.deleteOne({ _id: product._id })
      res.status(200).send({ ok: true, data: found })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async getSales(req, res) {
    try {
      const rangesOnSale = await ranges
        .find({ active: true })
        .sort({ dateAdded: -1 })
        .limit(10)
        .populate('design type texture keyword01 keyword02 keyword03 keyword04')

      rangesOnSale.forEach((range) => {
        range.variants.sort(function (x, y) {
          return x.saleDiscount > 0 ? -1 : y.saleDiscount > 0 ? 1 : 0
        })
      })

      res.status(200).send(rangesOnSale)
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async getNew(req, res) {
    try {
      const news = await ranges
        .find({ active: true })
        .sort({ dateAdded: -1 })
        .limit(50)
        .populate('design type texture keyword01 keyword02 keyword03 keyword04')

      res.status(200).send(news)
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async getAccessories(req, res) {
    try {
      const accessories = await ranges.find({ accessoires: true })
      res.status(200).send(accessories)
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async getRange(req, res) {
    const received_id = req.params.id
    try {
      let range
      range = await ranges
        .findOne({ url: received_id })
        .populate('variants.realColor')
      if (!range) {
        range = await ranges
          .findOne({ _id: received_id })
          .populate('variants.realColor')
      }
      res.status(200).send({ ok: true, range })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async getRanges(req, res) {
    try {
      const allRanges = await ranges
        .find({ active: true })
        .sort({ name: 1 })
        .limit(50)
        .populate(
          'design type texture adminTag01 adminTag02 adminTag03 adminTag04 keyword01 keyword02 keyword03 keyword04'
        )
        .populate('variants.realColor')
      res.status(200).send(allRanges)
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async getTrending(req, res) {
    try {
      const trending = await ranges
        .find({ active: true })
        .sort({ 'variants.meterSold': 1, 'variants.favorited': 1 })
        .limit(50)
        .populate('design type texture keyword01 keyword02 keyword03 keyword04')
      res.status(200).send(trending)
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async getPriceDrop(req, res) {
    try {
      const priceDrop = await ranges
        .find({
          'variants.priceDrop': true,
          'variants.priceDropDate': { $gt: Date.now() },
        })
        .limit(10)
        .populate('design type texture')
      priceDrop.forEach((range) => {
        range.variants.sort(function (x, y) {
          return x.priceDrop === true ? -1 : y.priceDrop === true ? 1 : 0
        })
      })

      res.status(200).send(priceDrop)
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async getDraft(req, res) {
    try {
      // if draft exists, send that
      const draft = await ranges.findOne({ active: false })
      if (draft) {
        res.status(200).send({ ok: true, product: draft })
      } else {
        // else create product
        // These 2 lines are for realColor
        const tempConverter = ''
        const tempRealColor = [mongoose.mongo.ObjectId(tempConverter.name)]
        // Declare product with content
        const product = {
          name: 'asd',
          price: 0,
          active: false,
          variants: [
            {
              color: 'asd',
              realColor: tempRealColor,
              sku: 'asd',
              imageURLs: [],
            },
          ],
        }
        // Create it on DB
        const newDraft = await ranges.create(product)
        // Remove content from product
        let tempVariant = { ...product.variants[0] }
        tempVariant.color = ''
        tempVariant.sku = ''
        // Save it to DB
        await ranges.updateOne(
          { _id: newDraft._id },
          { name: '', variants: [tempVariant] }
        )
        //
        const found = await ranges.findOne({ _id: newDraft._id })
        res.status(200).send({ ok: true, product: found })
      }
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async inspired(req, res) {
    try {
      const inspired = await ranges.find({ inspired: true })
      res.status(200).send({ ok: true, data: inspired })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async sale(req, res) {
    try {
      const sale = await ranges.find({
        'variants.priceDrop': true,
        'variants.priceDropDate': { $gt: Date.now() },
        'variants.saleDiscount': { $gt: 0 },
      })
      let soonSale
      let idx
      sale.forEach((range) => {
        range.variants.forEach((variant, i) => {
          if (
            variant.priceDrop &&
            variant.priceDropDate > Date.now() &&
            variant.saleDiscount > 0
          ) {
            if (!soonSale) {
              soonSale = range
              idx = i
            } else if (soonSale.priceDropDate > variant.priceDropDate) {
              soonSale = range
              idx = i
            }
          }
        })
      })
      res.status(200).send({ ok: true, data: soonSale, idx })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async favorited(req, res) {
    const { range_id, sign, variant_index } = req.params
    try {
      let range = await ranges.findOne({ _id: range_id })
      if (sign === '+') {
        range.variants[variant_index].favorited =
          range.variants[variant_index].favorited + 1
      } else {
        range.variants[variant_index].favorited =
          range.variants[variant_index].favorited - 1
      }
      await range.save()
      res.status(200).send({ ok: true })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async getSuggestions(req, res) {
    try {
      const count = ranges.estimatedDocumentCount()
      const random = Math.floor(Math.random() * count)
      const data = await ranges.find().skip(random).limit(10)
      res.status(200).send({ ok: true, data })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async getSuggestionsByColor(req, res) {
    const { color } = req.params
    try {
      const count = await ranges.estimatedDocumentCount()
      const random = Math.floor(Math.random() * count)
      const data = await ranges
        .find({ 'variants.realColor': color })
        .skip(random)
        .limit(5)
      if (data.length > 0) {
        res.status(200).send({ ok: true, data })
      } else {
        res.status(200).send({ ok: false })
      }
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async getSuggestionsByTexture(req, res) {
    const { texture } = req.params
    try {
      const data = await ranges.find({ texture })
      res.status(200).send({ ok: true, data })
    } catch (err) {
      res.status(500).send(err)
    }
  }
}
module.exports = new RangeController()
