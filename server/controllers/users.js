const Samples = require('../models/samples.js')
const Orders = require('../models/orders.js')
const users = require('../models/users.js')
const jwt = require('jsonwebtoken')
const config = require('../config/jwtConfig.js')
const express = require('express')
const app = express()
const bcrypt = require('bcrypt')
const saltRounds = 10
const _masterPassword = '1618&MAF'

app.set('key', config.key)

class UserController {
  async getLength(req, res) {
    try {
      let length = await users.estimatedDocumentCount()
      res.send({ length })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async getSome(req, res) {
    const { index, order, sort } = req.params
    const symbol = order === 'des' ? '-' : ''
    try {
      let data = await users
        .find()
        .sort(`${symbol}${sort}`)
        .skip(Number(index))
        .limit(10)
      res.status(200).send(data)
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async search(req, res) {
    const { search } = req.params
    try {
      let found = await users
        .aggregate([
          {
            $search: {
              index: 'searchUser',
              text: {
                query: search,
                path: ['name', 'email'],
              },
            },
          },
        ])
        .limit(10)
      res.send({ ok: true, users: found })
    } catch (err) {
      console.error(err)
      res.status(500).send(err)
    }
  }

  async getUserInfo(req, res) {
    const { user_id } = req.params
    try {
      const user = await users.findById(user_id)
      const userSamples = await Samples.find({ 'user._id': user_id })
      const userPurchases = await Orders.find({ 'user._id': user_id })
      res.send({ user, userSamples, userPurchases })
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async save(req, res) {
    const userToSave = req.body.user
    const _id = req.decoded._id
    try {
      let user = await users.findOneAndUpdate({ _id: _id }, userToSave)
      res.status(200).send(user)
    } catch (err) {
      res.status(500).send(err)
    }
  }

  async count(req, res) {
    const { user_id } = req.params
    try {
      const userSamples = await Samples.find({ 'user._id': user_id })
      const userPurchases = await Orders.find({ 'user._id': user_id })
      res.status(200).send({
        ok: true,
        userSamples: userSamples.length,
        userPurchases: userPurchases.length,
      })
    } catch (err) {
      res.status(500).send({ ok: false, err })
    }
  }

  async signIn(req, res) {
    const receivedPassword = req.body.password
    const receivedEmail = req.body.email
    try {
      const activeUser = await users
        .findOne({ email: receivedEmail })
        .populate('wishlist.range')
        .populate('sampleCart.range')
        .populate('cart.range')
        .populate('orders')
        .populate('cart.range.variants.color')
      if (activeUser === null) {
        return res.status(200).send({ ok: false })
      }
      if (activeUser.cart.range) {
        const filteredResult = activeUser.cart.filter((item) => {
          return item.range.inStock > 0
        })
        activeUser.cart = filteredResult
      }
      const match = await bcrypt.compare(receivedPassword, activeUser.password)
      if (match) {
        const payload = {
          check: true,
          _id: activeUser._id,
        }
        const token = jwt.sign(payload, app.get('key'), {
          expiresIn: Math.floor(Date.now() / 1000) + 60 * 60,
        })
        return res.status(200).send({
          ok: true,
          token: token,
          userData: activeUser,
        })
      } else {
        res.status(200).send({ ok: false })
      }
    } catch (err) {
      console.error(err)
      res.status(500).send(err)
    }
  }

  async signUp(req, res) {
    const receivedName = req.body.name
    const receivedEmail = req.body.email
    const receivedPassword = req.body.password
    try {
      const activeUser = await users.findOne({ email: receivedEmail })
      if (activeUser) {
        return res
          .status(200)
          .json({ ok: false, message: 'Email address already taken' })
      }
      const hashedPassword = await bcrypt.hash(receivedPassword, saltRounds)
      const newUser = await users.create({
        name: receivedName,
        email: receivedEmail,
        password: hashedPassword,
        region: {
          _id: 'NoRegion',
          name: 'Location',
          coin: 'GBP',
          conversionRate: 1,
          shippingRate: 'N/A',
          situation: 'N/A',
        },
      })
      const payload = {
        check: true,
        _id: newUser._id,
      }
      const token = jwt.sign(payload, app.get('key'), {
        expiresIn: Math.floor(Date.now() / 1000) + 60 * 60,
      })
      res.status(200).send({
        ok: true,
        token: token,
        userData: newUser,
      })
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async newPassword(req, res) {
    const receivedOldPassword = req.body.oldPassword
    const receivedNewPassword = req.body.newPassword
    const _id = req.decoded._id

    try {
      const activeUser = await users.findOne({ _id: _id })

      const match = await bcrypt.compare(
        receivedOldPassword,
        activeUser.password
      )

      if (match) {
        activeUser.password = await bcrypt.hash(receivedNewPassword, saltRounds)
        await activeUser.save()

        res.status(200).json({ message: 'Password Changed' })
      } else {
        res.status(401).json({ message: 'Wrong password' })
      }
    } catch (error) {
      res.status(500).send(error)
    }
  }

  async adminSignup(req, res) {
    const { name, email, password, confirmPassword, masterPassword } = req.body
    if (password !== confirmPassword) {
      return res.send({ ok: false, message: 'Passwords must match' })
    }
    try {
      const activeUser = await users.findOne({ email })
      if (activeUser) {
        return res.send({ ok: false, message: 'Email address already taken' })
      }
      if (masterPassword !== _masterPassword) {
        return res.send({
          ok: false,
          message:
            'Master password incorrect. Try again or contact administration',
        })
      }
      const hashedPassword = await bcrypt.hash(password, saltRounds)
      const newUser = await users.create({
        name,
        email,
        password: hashedPassword,
        admin: true,
      })
      const payload = {
        check: true,
        _id: newUser._id,
      }
      const token = jwt.sign(payload, app.get('key'), {
        expiresIn: Math.floor(Date.now() / 1000) + 60 * 60,
      })
      res.status(200).send({
        ok: true,
        token: token,
        userData: newUser,
      })
    } catch (err) {
      console.error(err)
      res.status(500).send(err)
    }
  }

  async adminLogin(req, res) {
    const receivedPassword = req.body.password
    const receivedEmail = req.body.email
    try {
      const activeUser = await users.findOne({
        email: receivedEmail,
        admin: true,
      })
      if (activeUser === null) {
        return res.send({ ok: false, message: 'No admin found' })
      }
      if (activeUser.cart.range) {
        const filteredResult = activeUser.cart.filter((item) => {
          return item.range.inStock > 0
        })
        activeUser.cart = filteredResult
      }
      const match = await bcrypt.compare(receivedPassword, activeUser.password)
      if (match) {
        const payload = {
          check: true,
          _id: activeUser._id,
        }
        const token = jwt.sign(payload, app.get('key'), {
          expiresIn: Math.floor(Date.now() / 1000) + 60 * 60,
        })
        return res.status(200).send({
          ok: true,
          token: token,
          userData: activeUser,
        })
      } else {
        res.send({ ok: false, message: 'Password incorrect' })
      }
    } catch (error) {
      res.status(500).send(error)
    }
  }
}

module.exports = new UserController()
