const colours = require("../models/colors.js");

class ColoursController {
  async getAll(req, res) {
    try {
      const data = await colours.find();
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async delete(req, res) {
    const { item } = req.body;
    try {
      const data = await colours.deleteOne({ _id: item._id });
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async addOne(req, res) {
    const { newItem } = req.body;
    try {
      const data = await colours.create({ name: newItem });
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send({ ok: false, err });
    }
  }

  async edit(req, res) {
    const { item } = req.body;
    try {
      const data = await colours.updateOne(
        { _id: item._id },
        { name: item.name }
      );
      res.status(200).send({ ok: true, data });
    } catch (err) {
      res.status(500).send(err);
    }
  }
}

module.exports = new ColoursController();
