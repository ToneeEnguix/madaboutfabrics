const subscribers = require("../models/subscribers.js");

class SubscriberController {
  async subscribe(req, res) {
    const newSubscriber = req.body.email;

    try {
      const alreadyThere = await subscribers.findOne({ email: newSubscriber });
      if (!alreadyThere) {
        const user = await subscribers.create({ email: newSubscriber });
        res.status(200).send(user);
      } else res.status(401).send({ message: "User already a subscriber" });
    } catch (error) {
      res.status(500).send(error);
    }
  }
}

module.exports = new SubscriberController();
