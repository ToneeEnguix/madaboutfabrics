const jwt = require('jsonwebtoken')
const express = require('express')
const app = express()
const config = require('../config/jwtConfig.js')
const users = require('../models/users.js')

app.set('key', config.key)

class SecurityController {
  async verifyToken(req, res) {
    const token = req.headers['access-token']
    try {
      if (token) {
        jwt.verify(token, app.get('key'), async (err, decoded) => {
          if (err) {
            return res.status(401).json({ message: 'Invalid Token' })
          } else {
            const _id = decoded._id
            const activeUser = await users
              .findOne({ _id })
              .populate('wishlist.range')
              .populate('sampleCart.range')
              .populate('cart.range')
              .populate('orders')
              .populate('cart.range.variants.color')
            if (activeUser) {
              const filteredResult = activeUser.cart.filter((item) => {
                return item.range.inStock > 0
              })
              activeUser.cart = filteredResult
            }
            res
              .status(200)
              .send({ message: 'Access Granted', userData: activeUser })
          }
        })
      } else {
        res.status(401).send({
          mensaje: 'Token not provided',
        })
      }
    } catch (e) {
      res.status(500).send(e)
    }
  }
}
module.exports = new SecurityController()
