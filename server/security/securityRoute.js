const express = require('express'),
    router = express.Router(),
    controller = require('./verifyToken.js');


router.get('/verifytoken', controller.verifyToken);



module.exports = router;