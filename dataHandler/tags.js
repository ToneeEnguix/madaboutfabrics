module.exports = [
  { name: "Floral" },
  { name: "Geometric" },
  { name: "Classic" },
  { name: "Fashionable" },
  { name: "Animals" },
  { name: "Subtle" },
  { name: "Elegant" },
  { name: "Trendy" },
  { name: "Urban" },
  { name: "Silly" },
];
