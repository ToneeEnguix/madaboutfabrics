module.exports = [
  {
    name: "Blue",
  },
  {
    name: "Black",
  },
  {
    name: "Brown",
  },
  {
    name: "Green",
  },
  {
    name: "Beige",
  },
  {
    name: "Grey",
  },
  {
    name: "Gold",
  },
  {
    name: "Duck egg",
  },
  {
    name: "Cream",
  },
  {
    name: "Lilac",
  },
  {
    name: "Mauve",
  },
  {
    name: "Natural",
  },
  {
    name: "Pink",
  },
  {
    name: "Purple",
  },
  {
    name: "Red",
  },
  {
    name: "Silver",
  },
  {
    name: "Teal",
  },
  {
    name: "Terracotta",
  },
  {
    name: "White",
  },
];
