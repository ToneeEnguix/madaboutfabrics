pepe = () => {
  const name = randomString();
  const onSale = randomBoolean();
  const onPriceDrop = randomBoolean();
  const design = randomDesign();
  const type = randomType();
  const description = randomString();
  const usage = {
    upholstery: randomBoolean(),
    curtains: randomBoolean(),
    craft: randomBoolean(),
  };
  const texture = randomTexture();
  const industry = {
    automotive: randomBoolean(),
    hospitality: randomBoolean(),
    film: randomBoolean(),
    healthcare: randomBoolean(),
  };
  const dateAdded = randomDate(new Date(2012, 0, 1), new Date());
  const composition = randomString();
  const fabricRepeat = randomNumber();
  const fabricWidth = randomNumber();
  const numberofVariants = Math.floor(Math.random() * 3) + 1;
  let variants = [];
  const keyword01 = randomTag();
  const keyword02 = randomTag();
  const care = {
    wipeClean: randomBoolean(),
    dryClean: randomBoolean(),
    handWash: randomBoolean(),
    thirtyClean: randomBoolean(),
    nonDetergent: randomBoolean(),
    doNotIron: randomBoolean(),
  };
  let colors = [];
  for (i = 0; i < numberofVariants; i++) {
    let variant = createVariant(onSale, onPriceDrop);
    variants.push(variant);
    colors.push(variant.color);
  }

  const range = {
    name,
    onSale,
    onPriceDrop,
    description,
    keyword01,
    keyword02,
    design,
    type,
    usage,
    texture,
    industry,
    dateAdded,
    composition,
    fabricRepeat,
    fabricWidth,
    colors,
    care,
    variants,
  };

  var myHeaders = new Headers();
  myHeaders.append("access-token", "");
  myHeaders.append("Content-Type", "application/x-www-form-urlencoded");

  var urlencoded = new URLSearchParams();
  urlencoded.append("range", JSON.stringify(range));

  var requestOptions = {
    mode: "no-cors",
    method: "POST",
    headers: myHeaders,
    body: urlencoded,
    redirect: "follow",
  };

  fetch("http://localhost:4000/ranges/addrange", requestOptions)
    .then((response) => response.json())
    .then((result) => console.error(result))
    .catch((error) => console.error("error", error));
};

createVariant = (onSale, onPriceDrop) => {
  const actualColor = randomColor();
  const color = randomString();

  const sku = randomString();
  const price = randomNumber();
  const thumbnailURL = "https://picsum.photos/200";
  const imageURLs = [
    "https://picsum.photos/200/400",
    "https://picsum.photos/200/400",
    "https://picsum.photos/400/200",
    "https://picsum.photos/400/200",
  ];
  const availableForPickup = randomBoolean();
  const dateAdded = randomDate(new Date(2012, 0, 1), new Date());
  let saleDiscount = 0;
  if (onSale) {
    const variantOnSale = randomBoolean();
    if (variantOnSale) saleDiscount = randomNumber();
    if (saleDiscount === 0) saleDiscount = 10;
  }
  let priceDrop = false;
  if (onPriceDrop) priceDrop = randomBoolean();
  let priceDropDate = new Date();
  if (priceDrop) priceDropDate.setDate(new Date().getDate() + 13);
  const stock = randomNumber();
  const favorited = randomNumber();
  const metersSold = randomNumber();

  return {
    actualColor,
    thumbnailURL,
    imageURLs,
    sku,
    price,
    dateAdded,
    availableForPickup,
    saleDiscount,
    priceDrop,
    priceDropDate,
    stock,
    favorited,
    metersSold,
    color,
  };
};

randomDate = (start, end) => {
  return new Date(
    start.getTime() + Math.random() * (end.getTime() - start.getTime())
  );
};

randomNumber = () => {
  return Math.floor(Math.random() * 100) + 1;
};

randomString = () => {
  return (
    Math.random().toString(36).substring(2, 5) +
    Math.random().toString(36).substring(2, 3)
  );
};

randomBoolean = () => {
  const val = Math.round(Math.random() * 1);

  if (val === 0) return true;
  else return false;
};

randomColor = () => {
  const colors = [
    "Beige",
    "Black",
    "Blue",
    "Brown",
    "Cream",
    "Duck egg",
    "Gold",
    "Green",
    "Grey",
    "Lilac",
    "Mauve",
    "Natural",
    "Pink",
    "Purple",
    "Red",
    "Silver",
    "Teal",
    "Terracotta",
    "White",
  ];

  const value = Math.floor(Math.random() * (colors.length - 1));

  return colors[value];
};

randomDesign = () => {
  const designs = [
    "Checks",
    "Circles",
    "Florals",
    "Geometrics",
    "Plains",
    "Semi plains",
    "Striped",
    "Themed",
  ];

  const value = Math.floor(Math.random() * 7);

  return designs[value];
};

randomType = () => {
  const types = [
    "Chenille",
    "Digital prints",
    "Jacquards",
    "Natural",
    "Organzas",
    "Satins",
    "Seasonal",
    "Velvets",
    "Voiles",
    "Wool",
  ];

  const value = Math.floor(Math.random() * 9);

  return types[value];
};

randomTexture = () => {
  const categories = [
    "Silk",
    "Faux fur",
    "Knitting wool",
    "Linen",
    "Cotton",
    "Velvet",
    "Satin",
    "Fleece",
    "Tweed",
    "Leatherettes",
    "Pure wools",
    "Sheepskin",
    "Waterproof",
    "Outdoor",
  ];

  const value = Math.floor(Math.random() * 13);

  return categories[value];
};

randomTag = () => {
  const tags = [
    "Floral",
    "Geometric",
    "Classic",
    "Fashionable",
    "Animals",
    "Subtle",
    "Elegant",
    "Trendy",
    "Urban",
    "Silly",
  ];

  const value = Math.floor(Math.random() * 10);

  return tags[value];
};
