import axios from 'axios'
import { URL } from '../config'

export const sendSampleConfirmationEmail = async (data) => {
  try {
    const res = await axios.post(
      `${URL}/emails/client_sample_confirmation`,
      data
    )
    if (!res.data.ok) {
      return res.data.ok
    }
    const res2 = await axios.post(`${URL}/emails/client_sample_confirmation`, {
      ...data,
      email: ['hello@madaboutfabrics.com', 'roisin@madaboutfabrics.com'],
    })
    return res2.data.ok
  } catch (err) {
    console.error(err)
  }
}

export const sendConfirmationEmail = async (data) => {
  try {
    // to client
    const res = await axios.post(
      `${URL}/emails/client_order_confirmation`,
      data
    )
    if (!res.data.ok) {
      return res.data.ok
    }
    // to administration
    const res2 = await axios.post(`${URL}/emails/client_order_confirmation`, {
      ...data,
      email: ['hello@madaboutfabrics.com', 'roisin@madaboutfabrics.com'],
    })
    return res2.data.ok
  } catch (err) {
    console.error(err)
    return false
  }
}
