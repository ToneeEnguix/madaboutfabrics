/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'
import './App.css'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom'
import { createBrowserHistory } from 'history'
import axios from 'axios'

import UserContext from './contexts/userContext'
import { URL } from './config'

import PrivateRoute from './components/router/PrivateRoute'
import PublicRoute from './components/router/PublicRoute'
import Home from './components/public/home/Home'
import Locations from './components/public/locations/Locations'
import Product from './components/public/product/Product'
import UserDashboard from './components/public/userDashboard/UserDashboard'
import UKLocations from './components/public/locations/UKLocations'
import Checkout from './components/public/checkout/Checkout'
import AdminLogin from './components/admin/adminapp'
import Megamenu from './components/public/views/Megamenu/Megamenu'
import Inspired from './components/public/views/Inspired'
import NewArrivals from './components/public/views/New'
import Accessories from './components/public/views/Accessories'
import Catalogue from './components/public/views/Catalogue/Catalogue.js'
import OnSale from './components/public/views/Sale'
import SampleCheckout from './components/public/sampleCheckout/SampleCheckout.js'
import Ranges from './components/public/views/Ranges/Ranges'
import More from './components/public/More/MoreApp.js'
import NavigationWrapper from './components/router/NavigationWrapper'

const customHistory = createBrowserHistory()

class App extends React.Component {
  constructor(props) {
    super(props)

    const abort = new AbortController()
    this.signal = abort.signal
    this.state = {
      user: {
        _id: undefined,
        name: 'Sign In',
        cart: [],
        wishlist: [],
        sampleCart: [],
        address: {
          name: '',
          lastName: '',
          direction: '',
          postalCode: '',
          town: '',
          region: '',
          email: '',
          phoneNumber: '',
        },
      },

      toggleRegion: (region) => {
        let tempUser = this.state.user
        tempUser.region = region
        this.setState({ user: tempUser })
        this.saveUser(undefined, tempUser)
      },

      toggleUser: (user) => {
        this.setState({ user: user })
      },

      saveAddress: (address) => {
        let tempUser = this.state.user
        tempUser.address = address
        this.setState({ user: tempUser })
        this.saveUser(undefined, tempUser)
      },

      modifyUsername: (newData) => {
        let tempUser = this.state.user
        tempUser.name = newData.name
        tempUser.lastName = newData.lastName
        tempUser.email = newData.email
        this.setState({ user: { tempUser } })
        this.saveUser(undefined, tempUser)
      },

      logOut: () => {
        this.saveUser(undefined, this.state.user)
        const emptyUser = {
          cart: [],
          wishlist: [],
          sampleCart: [],
          name: 'Sign In',
          region: {
            _id: 'NoRegion',
            name: 'Location',
            coin: 'GBP',
            conversionRate: 1,
            shippingRate: 'N/A',
            situation: 'N/A',
          },
        }
        localStorage.clear()
        this.setState({ user: emptyUser })
        this.forceUpdate()
      },

      removeFromCart: (range, variantIndex) => {
        const toRemove = this.state.user.cart.findIndex(
          (productOnCart) =>
            productOnCart.range._id === range._id &&
            variantIndex === productOnCart.variantIndex
        )
        let cartCopy = [...this.state.user.cart]
        cartCopy.splice(toRemove, 1)
        this.setState({ user: { ...this.state.user, cart: cartCopy } })
      },

      addToCart: (range, variantIndex, amount) => {
        const alreadyOnCart = this.state.user.cart.findIndex(
          (productOnCart) => {
            return (
              productOnCart.range._id === range._id &&
              productOnCart.variantIndex === Number(variantIndex)
            )
          }
        )
        let cartCopy = [...this.state.user.cart]
        if (alreadyOnCart > -1) {
          cartCopy[alreadyOnCart] = {
            ...cartCopy[alreadyOnCart],
            amount: cartCopy[alreadyOnCart].amount + amount,
          }
        } else {
          cartCopy.push({ range, amount, variantIndex })
        }
        let tempUser = { ...this.state.user }
        tempUser.cart = cartCopy
        this.setState({ user: tempUser })
        this.saveUser(undefined, tempUser)
        return true
      },

      addToWishlist: async (range, variantIndex) => {
        const alreadyOnWishlist = this.state.user.wishlist.findIndex(
          (productOnCart) => productOnCart._id === range._id
        )
        let wishlistCopy = [...this.state.user.wishlist]
        if (alreadyOnWishlist === -1) {
          wishlistCopy.push({ range, variantIndex })
          let tempUser = this.state.user
          tempUser.wishlist = wishlistCopy
          this.setState({ user: tempUser })
          this.saveUser(undefined, tempUser)
          this.favorited(range._id, variantIndex, '+')
        }
      },

      emptyWishlist: () => {
        let tempUser = this.state.user
        tempUser.wishlist = []
        this.setState({ user: tempUser })
        this.saveUser(undefined, tempUser)
      },

      removeFromWishlist: (range, variantIndex) => {
        const indexToRemove = this.state.user.wishlist.findIndex(
          (productOnCart) => productOnCart.range._id === range._id
        )
        let wishlistCopy = [...this.state.user.wishlist]
        wishlistCopy.splice(indexToRemove, 1)
        let tempUser = this.state.user
        tempUser.wishlist = wishlistCopy
        this.setState({ user: tempUser })
        this.saveUser(undefined, tempUser)
        this.favorited(range._id, variantIndex, '-')
      },

      addToSampleCart: (range, variantIndex) => {
        const alreadyOnSampleCart = this.state.user.sampleCart.findIndex(
          (productOnCart) => productOnCart._id === range._id
        )
        let sampleCartCopy = [...this.state.user.sampleCart]
        if (alreadyOnSampleCart === -1 && sampleCartCopy.length !== 6) {
          sampleCartCopy.push({ range, variantIndex })
          let tempUser = this.state.user
          tempUser.sampleCart = sampleCartCopy
          this.setState({ user: tempUser })
          this.saveUser(undefined, tempUser)
        } else if (sampleCartCopy.length === 6) {
          alert('Max samples already taken')
        }
      },

      removeFromSampleCart: (range) => {
        const indexToRemove = this.state.user.sampleCart.findIndex(
          (productOnCart) => productOnCart._id === range._id
        )
        let sampleCartCopy = [...this.state.user.sampleCart]
        sampleCartCopy.splice(indexToRemove, 1)
        let tempUser = this.state.user
        tempUser.sampleCart = sampleCartCopy
        this.setState({ user: tempUser })
        this.saveUser(undefined, tempUser)
      },

      updateCart: (range, variantIndex, newAmount) => {
        const alreadyOnCart = this.state.user.cart.findIndex(
          (productOnCart) =>
            productOnCart.range._id === range._id &&
            productOnCart.variantIndex === Number(variantIndex)
        )
        let cartCopy = [...this.state.user.cart]
        cartCopy[alreadyOnCart] = {
          ...cartCopy[alreadyOnCart],
          amount: parseInt(newAmount),
        }
        let tempUser = this.state.user
        tempUser.cart = cartCopy
        this.setState({ ...this.state, user: tempUser })
        this.saveUser(undefined, tempUser)
      },
    }
  }

  favorited = async (range_id, variantIndex, sign) => {
    try {
      await axios.get(
        `${URL}/ranges/favorited/${range_id}/${variantIndex}/${sign}`
      )
    } catch (err) {
      console.error(err)
    }
  }

  componentDidMount() {
    this.getLoggedUser()
    window.addEventListener('beforeunload', (e) =>
      this.saveUser(e, this.state.user)
    )
  }

  saveUser = async (e, user) => {
    const token = localStorage.getItem('token')
    e && e.preventDefault()
    if (user._id !== undefined && token) {
      try {
        await axios.post(
          `${URL}/users/save`,
          { user },
          {
            headers: {
              'access-token': token,
            },
          }
        )
        this.getLoggedUser()
      } catch (err) {
        console.error(err)
      }
    }
  }

  getLoggedUser = async () => {
    const token = localStorage.getItem('token')

    if (token) {
      var myHeaders = new Headers()
      myHeaders.append('access-token', token)
      myHeaders.append('Content-Type', 'application/x-www-form-urlencoded')

      var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow',
        signal: this.signal,
      }

      fetch(`${URL}/security/verifytoken`, requestOptions)
        .then((response) => response.json())
        .then((result) => {
          if (result.message === 'Access Granted') {
            this.setState({ user: result.userData })
            if (result.userData.region) {
              result.userData.region.name !== this.state.user.region.name &&
                this.state.toggleRegion(result.userData.region)
            }
          } else {
            localStorage.clear()
          }
        })
        .catch((error) => console.error(error))
    }
  }

  componentWillUnmount() {
    this.abort.abort()
  }

  render() {
    return (
      <div className='App'>
        <UserContext.Provider value={this.state}>
          <Router>
            <Switch>
              <NavigationWrapper>
                <PublicRoute restricted={false} component={Home} path='/home' />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={UKLocations}
                  path='/uklocations'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={Locations}
                  path='/locations'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={Ranges}
                  path='/ranges'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  exact
                  component={Product}
                  path='/productdetails/:id/:variant_index'
                />
                <PrivateRoute
                  restricted={true}
                  component={UserDashboard}
                  path='/userdashboard'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={Checkout}
                  path='/checkout'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={SampleCheckout}
                  path='/samplecheckout'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={Catalogue}
                  path='/catalogue'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={Megamenu}
                  path='/searchterms'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={Inspired}
                  path='/inspired'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={NewArrivals}
                  path='/new'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={Accessories}
                  path='/accessories'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={OnSale}
                  path='/sale'
                />
                <PublicRoute
                  restricted={false}
                  history={customHistory}
                  component={More}
                  path='/more'
                />
                <PublicRoute component={AdminLogin} path='/adminlogin' />
                <Route exact path='/'>
                  <Redirect to='/home' />
                </Route>
              </NavigationWrapper>
            </Switch>
          </Router>
        </UserContext.Provider>
      </div>
    )
  }
}

export default App

// DONE:
// link to 'my orders' from sider bar => re-check ✅
// remove 'menu' from siderbar ✅
// profile icon same size as cart icon ✅
// underline out on siderbar and name ✅
// on categories no margin right nor left <-- 16px --> ✅
// sign in in mobile from wishlist! ✅
// /more section ✅

// HOME:
// - little space on the left ✅
// - no space on the right ✅
// - less view of the next product ✅
// - arrows out ✅
// - slider line centered or out ✅
// - Carousels => horizontal scroll on small size ✅
// - removed scrollbar for carousels ✅
// SECTIONS:
// - categories ✅
// - new arrivals ✅
// - trending ✅
// - price drop ✅
// - ranges ✅

// Horizontal Scroll on catgories ✅
// Horizontal Scroll on navbar ✅

// - navbar on desktop big view STRAGE BEHAVIOUR ✅

// changed something about ranges changing in single range view ✅

// - categories title smaller ✅

// - abstract navigation bars ✅
// - for adminlogin no problem ✅
// - for difference between checkout and not checkout => have boolean for show navbars or not ✅
// - note: navbar on checkout is different. boolean can be checkout or not and show something specific for each one ✅

// - single product view => add to cart button loading color NOT red ✅

// MENU:
// - when click and change route => close menu ✅

// CHRIS' TO DOS:
// - megamenu final version ✅
// - checkouts ✅
// - /more ✅
// - on sale view mobile ✅
// - user dashboard ✅

// PRODUCT:
// - single product view ✅
// - suggestions carousel ✅
// - add space between suggestions ( vertical ) ✅
// - when click on a variant:
//   - carousel will stay on same place, even if just 1 image ✅
//   - and go back => ERROR ( BUT not for idx 0 ) ✅
// - mobile mobile not working ✅
// - changed images mobile carousel for normal Y scroll with css ✅
// - suggestions not changing when changing product ( must trigger update ) ✅

// VIEWS ON MOBILE
// - new ✅
// - accessories ✅
// - inspired ✅
// - sale ✅

// Remove ALL loading2.gif ✅
// - ranges ✅
// - user dashboard orders ✅
// - user dashboard samples ✅

// USER DASHBOARD
// - not accessible through url?? SOLVED added exact to route redirecting home ✅
// - cart => error ✅
// - address => error ✅
// - responsiveness:
//   - orders ✅
//   - cart ✅
//   - samples ✅
//   - wishlist ✅
//   - profile ✅
//   - address ✅
//   - password ✅

// PAYMENT including region change
// - when you change location => shipping price doesn't update bc paymentIntent is already craeted ✅
// - when click on 'continue' in shippinh => shippingRate gets set to N/A ✅

// ORDERS PRICE
// - Issue: order doesn't show discounted price but real!!! ✅
// - Real issue: discount not being saved in the order ✅
// - Solution: add discount to order when being saved ✅

// SAFARI FORMAT
// - display: ✅
//    - opening hours ✅
//    - phone ✅
// - catalogue: sort select ✅
// - ranges: discount box ✅

// Responsiveness:
// - more ✅
// - megamenu ✅
// - checkouts ✅
// - copy modal ✅
// - product single view price drop component ✅
// - views on mobile => sale ✅
// - user dashboard ✅
// - ranges ✅
// - catalogue ✅
// - footer ❎

// - responsiveness filters catalogue ✅
//    - checked get icon ✅
//    - wrapper fixed position + scrollable on the inside ✅
//    - filters button before applied filters ✅
//    - sortby box too small ✅
//    - test that filters are working, all of them ✅
//    - open filter => select filter => immediately added ✅
//    - apply or close would the same ✅
//    - delete will remove all filters and filters stay open ✅
//    - floating buttons ✅
//    - sortby box safari format??? ✅
//    - add all filters ✅
//    - scrollbar in filters? ✅
//    - double check filters sort by newest ✅
//    - add filters icon ✅
//    - 5 pages shown in pagination ONLY in desktop ✅
// - when click on add to favourite => not go up ✅

// When open menu: ✅
// - close wishlist ✅
// - close cart ✅
// - close samples ✅
// in order to achieve this... lift state... you know 🙃 ✅

// CHECKOUTS
// - I can click on 'Continue' without the billing being there ✅
// - click on 'billing address is the same' should toggle the view ✅
// - pressing enter in billing form should submit ✅
// - sample checkout ERRROR ( not working bc location was changed ) ✅
// - freezing!! ERROR ( maybe only on sample now ) ✅
// - phone number should be type number? ✅
// - size of react-loading when placing sample order => make bigger ✅
// - confirmation ✅
// - when selecting PICKUP => shiiping price should NOT be added ✅
// - no user => BUG ✅
// - order confirm: hide 'view orders' if not logged in ✅
// - sampleCheckout => return policy ( inside terms use id? no entenc això, estava apuntat ) ✅
// - no user + no products you shouldn't be able to be in checkout ✅
// - check if all products are available for pickup in order to show that radio button ✅
// - on mobile: NOT zoom when click ✅

// DETAILS
// - checkout payment remove padding for mobile ✅
// - cart and checkout have different shipping price!!! ✅
// - cart in user dashboard not showing total if no location ✅
// - if change phone number from 'profile' it should appear in checkout ✅ ( it was type number )
// - suggestions loading very strangely + loading +150 times ✅
// - close sidebar menu when selecting location ✅
// - total in orders NOT REAL PRICE ✅
// - when you clear filter in catalogue => also remove props.state ✅
// - in ranges 2 decimals always in price ✅
// - if products per page change => move to first page ✅
// - craft upholstery craft filter not working?? ✅
// - categories background color ✅
// - visible and invisible terms not being saved ✅
// - change tags to terms ✅
// - product suggestions not loading => endless loading ✅
// - HIDE ORDER SAMPLE IF NOT OFFER SAMPLE ✅
// - if adding more products of the same => increase amount instead of continue to add different products ✅
// - new arrivals algorithm!!! ✅
// - extra images ✅
// - change cart icon ✅
// - change favicon ✅

// ADMIN LOGIN
// - not possible to add photo ✅
// - deleting a picture that's not there will not work ✅
// - active ranges search ONLY BY RANGE NAME not variants ✅
// - can't update alessia range ✅
// - add inStock clickable ❎
// - new orders mark as complete => bugged ✅
// - new samples mark as complete => bugged ✅
// - in customers => 1 customer => amount spent => limit to 2 decimals ✅
// - in products search => go to page 1 ✅
// - logging in should be securer ✅
// - all orders => date received === date order?? ✅
// - pressing enter not working => add button ✅

// FIX!:
// - emails ✅
// - stripe ✅

// publish front-end to netlify to check mobile responsiveness SOLVED by using IP ✅
// try to publish server free on heroku ( + get practice on it ) NOT NECESSARY, TIME IS PRECIOUS ❎

// FUCKING LIST
// - justify solo elements to left on ranges and catalogue ✅
// - fabric care change wording ✅
// - navbar sticky ✅
// - doubled arrows on dropdown ✅
// - format contact us ✅
// - sliders 'not working' xd ✅
// - navbar bug 'related to margin' ✅

// MOBILE MENU:
// - navbar icons too close to edges ✅
// - align X to close menu with burguer to open menu ✅
// - align user icon with cart icon ✅
// - align searchbar with navbar line ✅
// - 80% opacity on background ✅
// - decrease sign in fontSize ✅
// - transition .4s to enter ✅

// NOT SO BIG ONES
// - slider go 5 ranges ✅
// - add fade to mobile filters button wrapper? ✅
// - tag carousel infinite rolling + stop on hover ✅
// - mini slider LINKS ✅

// TO DOs:

// BIG ONES
// - when click on username in menu it goes to orders and BUG ✅
// - user dashboard wishlist bug ✅
// - format order email ✅
// - format sample email ✅

//

// !$$$

//

// ABORT FUNCTIONS
// - getting suggestions function when unloading component!

// PENDING BUT... YOU KNOW $$$
// - calendar in price drop date ✅
// - lazy loading on EVERYTHING
// - outlines for accessibility?
// - Abstract regions to separate file to maintain consistency
// - remove home from navbar
// - Subscribers thing NOT working
// - settings => go to type: geometric in catalogue. possible?
// - require discount + droprate + countdown in order to update OR
//   discount value + date MEANS there IS a discount.
//   This is major change.
// - create 'seession' for user not logged in
// - forgot pw
// - user shipping zones? not active
// - customers => sort by samples/orders made
