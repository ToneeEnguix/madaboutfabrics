import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './resources/fonts/Avallon_Alt.ttf'

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
)
