const localIP = '192.168.1.104'
const onlineIP = '46.101.24.185'
const URL =
  window.location.hostname === `localhost`
    ? `http://${localIP}:7777`
    : `http://${onlineIP}`

const clientURL =
  window.location.hostname === `localhost`
    ? `http://${localIP}:3000`
    : `http://${onlineIP}`

const NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME = 'ckellytv'
const NEXT_PUBLIC_CLOUDINARY_UPLOAD_PRESET = 'dzxujvsk'
const NEXT_PUBLIC_CLOUDINARY_KEY = '884113981828137'
const CLOUDINARY_SECRET = 'lMAtVMwjbTz-mPc7udjQStS4I_E'

const STRIPE_PUBLIC_KEY =
  'pk_test_51JtWznI8WJHCF0z6otBqX5DSy2EsxCA9ePP3yczJfkAEiKO9vH46S4FCO5fNQ3acvXUVzjmkDV7on1Dlohvg8qp400jqjl2fW1'

export {
  URL,
  clientURL,
  NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME,
  NEXT_PUBLIC_CLOUDINARY_UPLOAD_PRESET,
  NEXT_PUBLIC_CLOUDINARY_KEY,
  CLOUDINARY_SECRET,
  STRIPE_PUBLIC_KEY,
}
