import React from 'react'

const UserContext = React.createContext({
  user: {
    name: 'Sign in',
    cart: [],
    region: {
      _id: 'NoRegion',
      name: 'Location',
      coin: 'GBP',
      conversionRate: 1,
      shippingRate: 'N/A',
      situation: 'N/A',
    },
  },
})

export default UserContext
