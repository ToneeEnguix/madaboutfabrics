/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState } from 'react'
import { URL } from '../../config'
import { Route, Switch } from 'react-router'
import { Link } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify'
import axios from 'axios'

const authWrapper = {
  backgroundColor: 'gray',
  height: '50vh',
  minHeight: '500px',
  width: '50vw',
  minWidth: '500px',
  margin: '5rem auto 0',
  a: {
    color: '#222',
    textDecoration: 'inherit',
    margin: '1rem 0',
    ':hover': {
      textDecoration: 'underline',
      cursor: 'pointer',
    },
  },
  input: {
    margin: '0.25rem 0 1rem',
    padding: '0.25rem 1rem',
  },
}

export default function Authentication(props) {
  const login = (token) => props.login(token)

  return (
    <Switch>
      <Route
        exact
        path={`${props.match.path}/`}
        render={(props) => <Login {...props} login={login} />}
      />
      <Route
        exact
        path={`${props.match.path}/signup`}
        render={(props) => <Signup {...props} login={login} />}
      />
    </Switch>
  )
}

const Login = (props) => {
  const [form, setForm] = useState(null)

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/users/adminlogin`, {
        email: form.email,
        password: form.password,
      })
      if (res.data.ok) {
        props.login(res.data.token)
      } else toast.error(res.data.message)
    } catch (err) {
      console.error(err)
    }
  }

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  return (
    <div className='page2'>
      <h1>Auth</h1>
      <form
        css={authWrapper}
        className='flexColumn'
        onSubmit={handleSubmit}
        onChange={handleChange}
      >
        <p>Email:</p>
        <input name='email' type='email' />
        <p>Password;</p>
        <input name='password' type='password' />
        <button css={submitBtn} className='pointer'>
          Submit
        </button>
        <Link to={`${props.match.path}signup`}>I don't have an admin user</Link>
      </form>
      <ToastContainer autoClose={2000} />
    </div>
  )
}

const Signup = (props) => {
  const [form, setForm] = useState(null)

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/users/adminsignup`, {
        name: form.name,
        email: form.email,
        password: form.password,
        confirmPassword: form.confirmPassword,
        masterPassword: form.masterPassword,
      })
      if (res.data.ok) {
        props.login(res.data.token)
      }
    } catch (err) {
      console.error(err)
    }
  }

  const handleChange = (e) => {
    setForm({ ...form, [e.target.name]: e.target.value })
  }

  return (
    <div className='page2'>
      <h1>Auth</h1>
      <form
        css={authWrapper}
        className='flexColumn'
        onSubmit={handleSubmit}
        onChange={handleChange}
      >
        <p>Name:</p>
        <input name='name' type='text' />
        <p>Email:</p>
        <input name='email' type='email' />
        <p>Password;</p>
        <input name='password' type='password' />
        <p>Confirm Password;</p>
        <input name='confirmPassword' type='password' />
        <p>Master Password;</p>
        <input name='masterPassword' type='password' />
        <button css={submitBtn} className='pointer'>
          Submit
        </button>
        <Link to='/adminlogin'>I have an admin user</Link>
      </form>
    </div>
  )
}

const submitBtn = {
  padding: '.5rem 1rem',
  fontFamily: 'Raleway, sans-serif',
}
