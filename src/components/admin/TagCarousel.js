/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import ReactModal from 'react-modal'
import { ToastContainer, toast } from 'react-toastify'

import { URL } from '../../config'
import cross from './pictures/cross.svg'

export default function MiniSlider() {
  // Terms to map & render
  const [allTerms, setAllTerms] = useState({})
  const [terms, setTerms] = useState({})
  // Modal Open
  const [openModal, setOpenModal] = useState(false)
  // Selected item for modal
  const [item, setItem] = useState({})
  // Update content
  // Add this new term
  const [newItem, setNewItem] = useState('')
  // to edit
  const [editable, setEditable] = useState(false)

  useEffect(() => {
    getTags()
  }, [])

  const getTags = async () => {
    try {
      const res = await axios.get(`${URL}/tags/all`)
      setAllTerms(res.data.data)
      let tempMinislider = []
      res.data.data.forEach((term) => {
        term.minislider && tempMinislider.push(term)
      })
      setTerms(tempMinislider)
    } catch (err) {
      console.error(err)
    }
  }

  const toMiniSlider = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/tags/tominislider`, {
        toMiniSlider: newItem === '' ? item.name : newItem,
      })
      if (res.data.ok) {
        toast('Term updated ✅')
        setEditable(false)
        setOpenModal(false)
        getTags()
        setNewItem('')
      }
    } catch (err) {
      console.error(err)
    }
  }

  const editTerm = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/tags/edit`, { item })
      if (res.data.ok) {
        toast('Term updated ✅')
        getTags()
      }
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div className='page'>
      <h1>Mini Slider Terms</h1>
      <div className='ec_grid2'>
        <div className='ec_fullTable'>
          <form className='ec_inputCont' onSubmit={(e) => toMiniSlider(e)}>
            <input
              placeholder='Write term'
              value={newItem}
              onChange={(e) => setNewItem(e.target.value)}
              list='termsList'
            />
          </form>
          <datalist id='termsList'>
            {Object.keys(allTerms).map((term) => {
              let values = Object.values(allTerms)
              // check if values to map already on miniSlider table
              if (
                terms.length > 0 &&
                !terms.some((element) => element._id === values[term]._id)
              ) {
                return <option key={term}>{values[term].name}</option>
              } else {
                return null
              }
            })}
          </datalist>
          <div className='ec_itemsCont'>
            {terms.length !== 0 ? (
              Object.keys(terms).map((term) => {
                let values = Object.values(terms)
                return (
                  <div
                    className='ec_item_name flexCenter pointer'
                    key={term}
                    onClick={() => {
                      setItem(terms[term])
                      if (!item.name) {
                        setEditable(true)
                      } else if (item.name === terms[term].name) {
                        setEditable(!editable)
                      } else {
                        setEditable(true)
                      }
                    }}
                  >
                    <p>{values[term].name}</p>
                    <img
                      src={cross}
                      className='ec_cross'
                      alt='cross'
                      onClick={(e) => {
                        setOpenModal(true)
                        setItem(terms[term])
                        e.stopPropagation()
                      }}
                    />
                  </div>
                )
              })
            ) : (
              <div style={{ padding: '1rem' }}>No terms yet!</div>
            )}
          </div>
        </div>
        {editable && (
          <div className='ec_right_section'>
            <form className='ec_R_box' onSubmit={(e) => editTerm(e)}>
              <p>Name:</p>
              <input
                value={item?.name}
                onChange={(e) => {
                  setItem({ ...item, name: e.target.value })
                }}
              />
              <p>URL:</p>
              <input
                value={item?.url || ''}
                placeholder='www.madaboutfabrics.com/belice/0'
                onChange={(e) => {
                  setItem({ ...item, url: e.target.value })
                }}
              />
              <hr className='ep_hr'></hr>
              <div className='flexCenter'>
                <button className='pointer' onClick={(e) => editTerm(e)}>
                  Save
                </button>
              </div>
            </form>
          </div>
        )}
      </div>
      <ToastContainer autoClose={2000} />
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className='modal'
        style={{
          overlay: {
            backgroundColor: '#2626266d',
          },
        }}
      >
        <div className='dm_modalQuestionCont'>
          <div className='flexCenter dm_modalQuestion'>
            <p className='inline'>
              Do you want to delete tag <i>{item.name}</i> in the Mini Slider??
            </p>
          </div>
          <div className='flexCenter'>
            <button
              className='raleway dm_modalBtn dm_modalBtn1 pointer'
              onClick={() => {
                setOpenModal(false)
              }}
            >
              No
            </button>

            <button
              className='raleway dm_modalBtn dm_modalBtn2 pointer'
              onClick={(e) => {
                toMiniSlider(e)
              }}
            >
              Yes
            </button>
          </div>
        </div>
      </ReactModal>
    </div>
  )
}
