/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'

import './header.css'

import plus from '../pictures/plus.svg'

const Header = (props) => {
  return (
    <div className='header flexCenter'>
      <Link to='/home' style={{ textDecoration: 'none', color: 'inherit' }}>
        <h2 className='flexCenter h_left'>Mad About Fabrics</h2>
      </Link>
      <div className='flexCenter h_right'>
        <Link
          className='flexCenter h_rightPlus'
          to={`${props.match.path}/addnewrange`}
        >
          <img src={plus} className='h_rightImg' alt='add symbol' />
        </Link>
      </div>
    </div>
  )
}

export default Header
