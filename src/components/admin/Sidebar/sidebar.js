/** @jsx jsx */
import { jsx } from '@emotion/react'
import './sidebar.css'
import { Link } from 'react-router-dom'

const Sidebar = (props) => {
  return (
    <div className='sidebar flexColumn'>
      <Link
        to={`${props.match.path}`}
        className={`sd_content ${
          props.location.pathname === '/adminlogin' && 'selected'
        }`}
      >
        Home
      </Link>
      <Link
        to={`${props.match.path}/neworders`}
        className={`sd_content ${
          props.location.pathname.includes('/neworders') && 'selected'
        }`}
      >
        New Orders
      </Link>
      <Link
        to={`${props.match.path}/newsamples`}
        className={`sd_content ${
          props.location.pathname.includes('/newsamples') && 'selected'
        }`}
      >
        New Samples
      </Link>
      <Link
        to={`${props.match.path}/allorders`}
        className={`sd_content ${
          props.location.pathname.includes('/allorders') && 'selected'
        }`}
      >
        All Orders
      </Link>
      <Link
        to={`${props.match.path}/allsamples`}
        className={`sd_content ${
          props.location.pathname.includes('/allsamples') && 'selected'
        }`}
      >
        All Samples
      </Link>
      <Link
        to={`${props.match.path}/customers`}
        className={`sd_content ${
          props.location.pathname.includes('/customers') && 'selected'
        }`}
      >
        Customers
      </Link>
      <Link
        to={`${props.match.path}/products`}
        className={`sd_content ${
          props.location.pathname.includes('/products') && 'selected'
        }`}
      >
        Active Ranges
      </Link>
      <Link
        to={`${props.match.path}/addnewrange`}
        className={`sd_content ${
          props.location.pathname.includes('addnewrange') && 'selected'
        }`}
      >
        Add New Range
      </Link>
      <Link
        to={`${props.match.path}/categories`}
        className={`sd_content ${
          props.location.pathname.includes('categories') && 'selected'
        }`}
      >
        Categories
      </Link>
      <Link
        to={`${props.match.path}/tagcarousel`}
        className={`sd_content ${
          props.location.pathname === '/adminlogin/tagcarousel' && 'selected'
        }`}
      >
        Tag Carousel
      </Link>
      <Link
        to={`${props.match.path}/settings`}
        className={`sd_content ${
          props.location.pathname === '/adminlogin/settings' && 'selected'
        }`}
      >
        Settings
      </Link>
      <p onClick={() => props.logout()} style={{ cursor: 'pointer' }}>
        Log Out
      </p>
    </div>
  )
}
export default Sidebar
