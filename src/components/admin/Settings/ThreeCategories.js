/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import ImagePicker from '../utils/imagePicker'
import close from '../../admin/pictures/close.svg'
import tick from '../../admin/pictures/tick.svg'
import { URL } from '../../../config'

export default function ThreeCategories(props) {
  // Inititalize state
  const [upholstery, setUpholstery] = useState({
    colour: '',
    title: '',
    image: { pathname: '' },
  })
  const [curtain, setCurtain] = useState({
    colour: '',
    title: '',
    image: { pathname: '' },
  })
  const [craft, setCraft] = useState({
    colour: '',
    title: '',
    image: { pathname: '' },
  })

  // Get data from parent
  useEffect(() => {
    if (props.settings && props.settings[0].name && !upholstery.name) {
      let found1 = props.settings.find(
        (setting) => setting.name === 'upholstery'
      )
      setUpholstery(found1)
      let found2 = props.settings.find((setting) => setting.name === 'curtain')
      setCurtain(found2)
      let found3 = props.settings.find((setting) => setting.name === 'craft')
      setCraft(found3)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  // For upholstery inputs
  const handleChangeUpholstery = (e, setting) => {
    let tempSetting = { ...upholstery }
    if (e.target.value === '#') {
      tempSetting[setting] = null
    }
    if (e.target.value[0] !== '#') {
      tempSetting[setting] = e.target.value
    } else {
      tempSetting[setting] = e.target.value.slice(1)
    }
    props.setSettings(tempSetting, 'upholstery')
    setUpholstery(tempSetting)
  }

  // For Curtain inputs
  const handleChangeCurtain = (e, setting) => {
    let tempSetting = { ...curtain }
    if (e.target.value === '#') {
      tempSetting[setting] = null
    }
    if (e.target.value[0] !== '#') {
      tempSetting[setting] = e.target.value
    } else {
      tempSetting[setting] = e.target.value.slice(1)
    }
    props.setSettings(tempSetting, 'curtain')
    setCurtain(tempSetting)
  }

  // For Curtain inputs
  const handleChangeCraft = (e, setting) => {
    let tempSetting = { ...craft }
    if (e.target.value === '#') {
      tempSetting[setting] = null
    }
    if (e.target.value[0] !== '#') {
      tempSetting[setting] = e.target.value
    } else {
      tempSetting[setting] = e.target.value.slice(1)
    }
    props.setSettings(tempSetting, 'craft')
    setCraft(tempSetting)
  }

  // For checkbox
  const handleClickUpholstery = (e) => {
    e.preventDefault()
    let tempUpholstery = { ...upholstery }
    tempUpholstery.useBgColour = !upholstery.useBgColour
    props.setSettings(tempUpholstery, 'upholstery')
    setUpholstery(tempUpholstery)
  }

  // For checkbox
  const handleClickCurtain = (e) => {
    e.preventDefault()
    let tempCurtain = { ...curtain }
    tempCurtain.useBgColour = !curtain.useBgColour
    props.setSettings(tempCurtain, 'curtain')
    setCurtain(tempCurtain)
  }

  // For checkbox
  const handleClickCraft = (e) => {
    e.preventDefault()
    let tempCraft = { ...craft }
    tempCraft.useBgColour = !craft.useBgColour
    props.setSettings(tempCraft, 'craft')
    setCraft(tempCraft)
  }

  // For Upholstery Image Upload
  const handleImageUploadUpholstery = (uploadedFile) => {
    let tempUpholstery = { ...upholstery }
    tempUpholstery.image = uploadedFile
    props.setSettings(tempUpholstery, 'upholstery')
    setUpholstery(tempUpholstery)
  }

  // For Curtain Image Upload
  const handleImageUploadCurtain = (uploadedFile) => {
    let tempCurtain = { ...curtain }
    tempCurtain.image = uploadedFile
    props.setSettings(tempCurtain, 'curtain')
    setCurtain(tempCurtain)
  }

  // For Craft Image Upload
  const handleImageUploadCraft = (uploadedFile) => {
    let tempCraft = { ...craft }
    tempCraft.image = uploadedFile
    props.setSettings(tempCraft, 'craft')
    setCraft(tempCraft)
  }

  return (
    <form onSubmit={(e) => props.save(e)}>
      <h3 css={subtitle}>Upholstery</h3>
      <div className='st_inputCont'>
        <p>Text</p>
        <input
          className='input'
          placeholder='Check Out The Latest In Upholstery Fabric'
          type='text'
          value={upholstery.title ? upholstery.title : ''}
          onChange={(e) => handleChangeUpholstery(e, 'title')}
        />
      </div>
      <div className='st_inputCont'>
        <p>Colour</p>
        <input
          className='input'
          placeholder='#2D2D2D'
          type='text'
          value={upholstery.colour ? '#' + upholstery.colour : ''}
          onChange={(e) => handleChangeUpholstery(e, 'colour')}
        />
      </div>
      <div className='st_inputCont2' style={{ margin: '0.7rem 0' }}>
        <div className='st_inputCont3'>
          <p>BG Colour</p>
          <input
            className='input'
            placeholder='#000000'
            value={upholstery.bgColour ? '#' + upholstery.bgColour : ''}
            onChange={(e) => handleChangeUpholstery(e, 'bgColour')}
          />
        </div>
        <div className='st_inputCont4'>
          <p>Use BG</p>
          <div
            className='checkbox checkbox_with_border flexCenter pointer'
            id='priceDrop'
            onClick={(e) => handleClickUpholstery(e)}
          >
            <img
              src={tick}
              className={`ps_tick ${!upholstery.useBgColour && 'nope'}`}
              alt='tick'
              id='priceDrop'
            />
          </div>
        </div>
      </div>
      <h3 css={subtitle}>Curtain</h3>
      <div className='st_inputCont'>
        <p>Text</p>
        <input
          className='input'
          placeholder='Check Out The Latest In Curtain Fabric'
          type='text'
          value={curtain.title ? curtain.title : ''}
          onChange={(e) => handleChangeCurtain(e, 'title')}
        />
      </div>
      <div className='st_inputCont'>
        <p>Colour</p>
        <input
          className='input'
          placeholder='#2D2D2D'
          type='text'
          value={curtain.colour ? '#' + curtain.colour : ''}
          onChange={(e) => handleChangeCurtain(e, 'colour')}
        />
      </div>
      <div className='st_inputCont2' style={{ margin: '0.7rem 0' }}>
        <div className='st_inputCont3'>
          <p>BG Colour</p>
          <input
            className='input'
            placeholder='#000000'
            value={curtain.bgColour ? '#' + curtain.bgColour : ''}
            onChange={(e) => handleChangeCurtain(e, 'bgColour')}
          />
        </div>
        <div className='st_inputCont4'>
          <p>Use BG</p>
          <div
            className='checkbox checkbox_with_border flexCenter pointer'
            id='priceDrop'
            onClick={(e) => handleClickCurtain(e)}
          >
            <img
              src={tick}
              className={`ps_tick ${!curtain.useBgColour && 'nope'}`}
              alt='tick'
              id='priceDrop'
            />
          </div>
        </div>
      </div>
      <h3 css={subtitle}>Craft</h3>
      <div className='st_inputCont'>
        <p>Text</p>
        <input
          className='input'
          placeholder='Check Out The Latest In Craft Fabric'
          type='text'
          value={craft.title ? craft.title : ''}
          onChange={(e) => handleChangeCraft(e, 'title')}
        />
      </div>
      <div className='st_inputCont'>
        <p>Colour</p>
        <input
          className='input'
          placeholder='#2D2D2D'
          type='text'
          value={craft.colour ? '#' + craft.colour : ''}
          onChange={(e) => handleChangeCraft(e, 'colour')}
        />
      </div>
      <div className='st_inputCont2' style={{ margin: '0.7rem 0' }}>
        <div className='st_inputCont3'>
          <p>BG Colour</p>
          <input
            className='input'
            placeholder='#000000'
            value={craft.bgColour ? '#' + craft.bgColour : ''}
            onChange={(e) => handleChangeCraft(e, 'bgColour')}
          />
        </div>
        <div className='st_inputCont4'>
          <p>Use BG</p>
          <div
            className='checkbox checkbox_with_border flexCenter pointer'
            id='priceDrop'
            onClick={(e) => handleClickCraft(e)}
          >
            <img
              src={tick}
              className={`ps_tick ${!craft.useBgColour && 'nope'}`}
              alt='tick'
              id='priceDrop'
            />
          </div>
        </div>
      </div>
      <div className='flex' css={ThreeCatCont}>
        <div css={EachCatCont} className='flexColumn'>
          <h3>Upholstery Background</h3>
          {upholstery.image.pathname === '' ? (
            <ImagePicker
              setState={(uploadedFile) =>
                handleImageUploadUpholstery(uploadedFile)
              }
            />
          ) : (
            <div className='photoContStyle'>
              {!props.openModal && (
                <img
                  alt='close'
                  src={close}
                  className='pointer imageEraser'
                  onClick={() => {
                    props.manageModal('upholstery')
                  }}
                />
              )}
              <img
                src={`${URL}/assets/${upholstery.image.filename}`}
                className='imgStyle'
                alt='variant examples'
              />
            </div>
          )}
        </div>
        <div css={EachCatCont}>
          <h3>Curtain Background</h3>
          {curtain.image.pathname === '' ? (
            <ImagePicker
              setState={(uploadedFile) =>
                handleImageUploadCurtain(uploadedFile)
              }
            />
          ) : (
            <div className='photoContStyle'>
              {!props.openModal && (
                <img
                  alt='close'
                  src={close}
                  className='pointer imageEraser'
                  onClick={() => {
                    props.manageModal('curtain')
                  }}
                />
              )}
              <img
                src={`${URL}/assets/${curtain.image.filename}`}
                className='imgStyle'
                alt='variant examples'
              />
            </div>
          )}
        </div>
        <div css={EachCatCont}>
          <h3>Craft Background</h3>
          {craft.image.pathname === '' ? (
            <ImagePicker
              setState={(uploadedFile) => handleImageUploadCraft(uploadedFile)}
            />
          ) : (
            <div className='photoContStyle'>
              {!props.openModal && (
                <img
                  alt='close'
                  src={close}
                  className='pointer imageEraser'
                  onClick={() => {
                    props.manageModal('craft')
                  }}
                />
              )}
              <img
                src={`${URL}/assets/${craft.image.filename}`}
                className='imgStyle'
                alt='variant examples'
              />
            </div>
          )}
        </div>
      </div>
      <button className='none'></button>
    </form>
  )
}

const ThreeCatCont = {
    marginTop: '2rem',
  },
  EachCatCont = {
    justifyContent: 'center',
    marginRight: '1rem',
    h3: {
      margin: '0 0.9rem 1rem 0',
      textAlign: 'center',
    },
  },
  subtitle = {
    margin: '.3rem 0 -.3rem',
  }
