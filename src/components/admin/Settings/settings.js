/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import axios from 'axios'
import { URL } from '../../../config'

import MiniSlider from './MiniSlider'
import HeroBanner from './HeroBanner'
import ThreeCategories from './ThreeCategories'
import Featured from './Featured'
import ReactModal from 'react-modal'

export default function Settings() {
  const [settings, setSettings] = useState(null)
  const [message, setMessage] = useState(null)

  // Modal Open
  const [openModal, setOpenModal] = useState(false)

  // For Deleting specific picture
  const [remove, setRemove] = useState(null)

  useEffect(() => {
    const getSettings = async () => {
      try {
        const res = await axios.get(`${URL}/settings/all`)
        setSettings(res.data.data)
      } catch (err) {
        console.error(err)
      }
    }
    getSettings()
  }, [])

  const updateSettings = (setting, name) => {
    const idx = settings.findIndex((search) => search.name === name)
    const tempSettings = [...settings]
    tempSettings[idx] = { ...setting }
    setSettings(tempSettings)
  }

  const save = async (e) => {
    e && e.preventDefault()
    try {
      const res = await axios.post(`${URL}/settings/update`, settings)
      if (res.data.ok) {
        setMessage('Range saved correctly!')
        setTimeout(() => {
          setMessage(null)
        }, [1000])
      }
    } catch (err) {
      console.error(err)
    }
  }

  const deleteImage = async () => {
    let idx = settings.findIndex((setting) => setting.name === remove)
    let tempSettings = [...settings]
    try {
      const res = await axios.post(`${URL}/photo/deletephoto`, {
        filename: settings[idx].image.filename,
        pathname: settings[idx].image.pathname,
      })
      if (res.data.ok) {
        tempSettings[idx].image.pathname = ''
        tempSettings[idx].image.filename = ''
        setSettings(tempSettings)
        save()
        setOpenModal(false)
      }
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div className='page'>
      <h1 css={title}>Home Page Settings</h1>
      <div className='st_Cont'>
        <main>
          <section css={section}>
            <h2 css={subTitle}>Mini Slider</h2>
            <MiniSlider
              settings={settings}
              save={(e) => save(e)}
              setSettings={(setting, name) => updateSettings(setting, name)}
              openModal={openModal}
              manageModal={(what) => {
                setRemove(what)
                setOpenModal(true)
              }}
            />
          </section>
          <section css={section}>
            <h2 css={subTitle}>Hero Banner</h2>
            <HeroBanner
              settings={settings}
              save={(e) => save(e)}
              setSettings={(setting, name) => updateSettings(setting, name)}
              openModal={openModal}
              manageModal={(what) => {
                setRemove(what)
                setOpenModal(true)
              }}
            />
          </section>
          <section css={section}>
            <h2 css={subTitle2}>Three Categories</h2>
            <ThreeCategories
              settings={settings}
              save={(e) => save(e)}
              setSettings={(setting, name) => updateSettings(setting, name)}
              openModal={openModal}
              manageModal={(what) => {
                setRemove(what)
                setOpenModal(true)
              }}
            />
          </section>
          <section css={section}>
            <h2 css={subTitle}>Featured Product</h2>
            <Featured
              settings={settings}
              save={(e) => save(e)}
              setSettings={(setting, name) => updateSettings(setting, name)}
              openModal={openModal}
              manageModal={(what) => {
                setRemove(what)
                setOpenModal(true)
              }}
            />
          </section>
        </main>
        <div className='st_btnCont'>
          <button className='ep_right_btn pointer' onClick={(e) => save(e)}>
            Save Changes
          </button>
          {message ? <p className='green'>{message}</p> : null}
        </div>
      </div>
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className='modal'
        style={{
          overlay: {
            backgroundColor: '#2626266d',
          },
        }}
      >
        <div className='dm_modalQuestionCont'>
          <div className='flexCenter dm_modalQuestion'>
            <p className='inline'>
              Do you want to delete image from{' '}
              <i>
                {remove
                  ? remove[0].toUpperCase() + remove.slice(1).replace('_', ' ')
                  : null}
              </i>
              ?
            </p>
          </div>
          <div className='flexCenter'>
            <button
              className='raleway dm_modalBtn dm_modalBtn1 pointer'
              onClick={() => {
                setOpenModal(false)
              }}
            >
              No
            </button>
            <button
              className='raleway dm_modalBtn dm_modalBtn2 pointer'
              onClick={() => {
                deleteImage()
              }}
            >
              Yes
            </button>
          </div>
        </div>
      </ReactModal>
    </div>
  )
}
const section = {
    marginBottom: '3rem',
  },
  title = {
    marginBottom: '30px',
  },
  subTitle = {
    marginBottom: '10px',
  },
  subTitle2 = {
    marginBottom: '20px',
  }
