/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import ImagePicker from '../utils/imagePicker'
import close from '../../admin/pictures/close.svg'
import tick from '../../admin/pictures/tick.svg'
import { URL } from '../../../config'

export default function HeroBanner(props) {
  // Inititalize state
  const [heroBanner, setHeroBanner] = useState({
    colour: '',
    title: '',
    fields: [
      { text: '', url: '' },
      { text: '', url: '' },
      { text: '', url: '' },
    ],
    image: {
      filename: '',
      pathname: '',
    },
  })

  // Get data from parent and put it to state
  useEffect(() => {
    if (props.settings && props.settings[0].name && !heroBanner.name) {
      let found = props.settings.find(
        (setting) => setting.name === 'hero_banner'
      )
      setHeroBanner(found)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  // For inputs
  const handleChange = (e, setting, part) => {
    let tempHeroBanner = { ...heroBanner }
    if (e.target.value === '#') {
      tempHeroBanner[setting] = null
    }
    if (part) {
      tempHeroBanner.fields[setting][part] = e.target.value
    } else {
      if (e.target.value[0] !== '#') {
        tempHeroBanner[setting] = e.target.value
      } else {
        tempHeroBanner[setting] = e.target.value.slice(1)
      }
    }
    props.setSettings(tempHeroBanner, 'hero_banner')
    setHeroBanner(tempHeroBanner)
  }

  // For checkbox
  const handleClick = (e) => {
    e.preventDefault()
    let tempHeroBanner = { ...heroBanner }
    tempHeroBanner.useBgColour = !heroBanner.useBgColour
    props.setSettings(tempHeroBanner, 'hero_banner')
    setHeroBanner(tempHeroBanner)
  }

  // For image upload
  const handleImageUpload = (uploadedFile) => {
    let tempHeroBanner = { ...heroBanner }
    tempHeroBanner.image = uploadedFile
    props.setSettings(tempHeroBanner, 'hero_banner')
    setHeroBanner(tempHeroBanner)
  }

  return (
    <form onSubmit={(e) => props.save(e)}>
      <div className='st_minisliderCont'>
        <div>
          <div className='st_inputCont'>
            <p>Colour</p>
            <input
              className='input'
              placeholder='#2D2D2D'
              value={heroBanner.colour ? '#' + heroBanner.colour : ''}
              type='text'
              onChange={(e) => handleChange(e, 'colour')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Main Text</p>
            <input
              className='input'
              placeholder='Check Out The Latest Christmas Fabric'
              value={heroBanner.title ? heroBanner.title : ''}
              type='text'
              onChange={(e) => handleChange(e, 'title')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Text</p>
            <input
              className='input'
              placeholder='Christmas PVC'
              type='text'
              value={heroBanner.fields[0].text ? heroBanner.fields[0].text : ''}
              onChange={(e) => handleChange(e, 0, 'text')}
            />
          </div>
          <div className='st_inputCont'>
            <p>url</p>
            <input
              className='input'
              placeholder='madaboutfabrics.com/christmaspvc'
              type='text'
              value={heroBanner.fields[0].url ? heroBanner.fields[0].url : ''}
              onChange={(e) => handleChange(e, 0, 'url')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Text</p>
            <input
              className='input'
              placeholder='Christmas Craft'
              type='text'
              value={heroBanner.fields[1].text ? heroBanner.fields[1].text : ''}
              onChange={(e) => handleChange(e, 1, 'text')}
            />
          </div>
          <div className='st_inputCont'>
            <p>url</p>
            <input
              className='input'
              placeholder='madaboutfabrics.com/christmascraft'
              type='text'
              value={heroBanner.fields[1].url ? heroBanner.fields[1].url : ''}
              onChange={(e) => handleChange(e, 1, 'url')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Text</p>
            <input
              className='input'
              placeholder='Christmas Dress'
              type='text'
              value={heroBanner.fields[2].text ? heroBanner.fields[2].text : ''}
              onChange={(e) => handleChange(e, 2, 'text')}
            />
          </div>
          <div className='st_inputCont'>
            <p>url</p>
            <input
              className='input'
              placeholder='madaboutfabrics.com/christmasdress'
              type='text'
              value={heroBanner.fields[2].url ? heroBanner.fields[2].url : ''}
              onChange={(e) => handleChange(e, 2, 'url')}
            />
          </div>
          <div className='st_inputCont2'>
            <div className='st_inputCont3'>
              <p>BG Colour</p>
              <input
                className='input'
                placeholder='#000000'
                value={heroBanner.bgColour ? '#' + heroBanner.bgColour : ''}
                onChange={(e) => handleChange(e, 'bgColour')}
              />
            </div>
            <div className='st_inputCont4'>
              <p>Use BG</p>
              <div
                className='checkbox checkbox_with_border flexCenter pointer'
                id='priceDrop'
                onClick={(e) => handleClick(e)}
              >
                <img
                  src={tick}
                  className={`ps_tick ${!heroBanner.useBgColour && 'nope'}`}
                  alt='tick'
                  id='priceDrop'
                />
              </div>
            </div>
          </div>
        </div>
        <div className='st_miniSlider_R flexCenter'>
          {heroBanner.image.pathname === '' ? (
            <ImagePicker
              setState={(uploadedFile) => handleImageUpload(uploadedFile)}
            />
          ) : (
            <div className='photoContStyle'>
              {!props.openModal && (
                <img
                  alt='close'
                  src={close}
                  className='pointer imageEraser'
                  onClick={() => {
                    props.manageModal('hero_banner')
                  }}
                />
              )}
              <img
                src={`${URL}/assets/${heroBanner.image.filename}`}
                className='imgStyle'
                alt='variant examples'
              />
            </div>
          )}
        </div>
      </div>
      <button className='none'></button>
    </form>
  )
}
