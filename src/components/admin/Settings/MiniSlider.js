/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import ImagePicker from '../utils/imagePicker'
import tick from '../../admin/pictures/tick.svg'
import close from '../../admin/pictures/close.svg'
import { URL } from '../../../config'

export default function MiniSlider(props) {
  // Inititalize state
  const [miniSlider, setMiniSlider] = useState({
    colour: '',
    fields: [{ text: '' }, { text: '' }, { text: '' }],
    image: {
      filename: '',
      pathname: '',
    },
  })

  // Get data from parent and put it to state
  useEffect(() => {
    if (props.settings && props.settings[0].name && !miniSlider.name) {
      let found = props.settings.find(
        (setting) => setting.name === 'mini_slider'
      )
      setMiniSlider(found)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  // For inputs
  const handleChange = (e, setting, part) => {
    let tempMiniSlider = { ...miniSlider }
    if (e.target.value === '#') {
      tempMiniSlider[setting] = null
    }
    if (part) {
      tempMiniSlider.fields[setting][part] = e.target.value
    } else {
      if (e.target.value[0] !== '#') {
        tempMiniSlider[setting] = e.target.value
      } else {
        tempMiniSlider[setting] = e.target.value.slice(1)
      }
    }
    props.setSettings(tempMiniSlider, 'mini_slider')
    setMiniSlider(tempMiniSlider)
  }

  // For checkbox
  const handleClick = (e) => {
    e.preventDefault()
    let tempMiniSlider = { ...miniSlider }
    tempMiniSlider.useBgColour = !miniSlider.useBgColour
    props.setSettings(tempMiniSlider, 'mini_slider')
    setMiniSlider(tempMiniSlider)
  }

  // For image upload
  const handleImageUpload = (uploadedFile) => {
    let tempMiniSlider = { ...miniSlider }
    tempMiniSlider.image = uploadedFile
    props.setSettings(tempMiniSlider, 'mini_slider')
    setMiniSlider(tempMiniSlider)
  }

  return (
    <form onSubmit={(e) => props.save(e)}>
      <div className='st_minisliderCont'>
        <div>
          <div className='st_inputCont'>
            <p>Colour</p>
            <input
              className='input'
              placeholder='#2D2D2D'
              type='text'
              value={miniSlider.colour ? '#' + miniSlider.colour : ''}
              onChange={(e) => handleChange(e, 'colour')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Slide 1</p>
            <input
              className='input'
              placeholder='Covid Restrictions Apply'
              type='text'
              value={miniSlider.fields[0].text ? miniSlider.fields[0].text : ''}
              onChange={(e) => handleChange(e, 0, 'text')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Slide 2</p>
            <input
              className='input'
              placeholder='Free Delivery On Selected Products'
              value={miniSlider.fields[1].text ? miniSlider.fields[1].text : ''}
              onChange={(e) => handleChange(e, 1, 'text')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Slide 3</p>
            <input
              className='input'
              placeholder='Pick Up In-Store Available'
              value={miniSlider.fields[2].text ? miniSlider.fields[2].text : ''}
              onChange={(e) => handleChange(e, 2, 'text')}
            />
          </div>
          <div className='st_inputCont2'>
            <div className='st_inputCont3'>
              <p>BG Colour</p>
              <input
                className='input'
                placeholder='#000000'
                value={miniSlider.bgColour ? '#' + miniSlider.bgColour : ''}
                onChange={(e) => handleChange(e, 'bgColour')}
              />
            </div>
            <div className='st_inputCont4'>
              <p>Use BG</p>
              <div
                className='checkbox checkbox_with_border flexCenter pointer'
                id='priceDrop'
                onClick={(e) => handleClick(e)}
              >
                <img
                  src={tick}
                  className={`ps_tick ${!miniSlider.useBgColour && 'nope'}`}
                  alt='tick'
                  id='priceDrop'
                />
              </div>
            </div>
          </div>
        </div>
        <div className='st_miniSlider_R flexCenter'>
          {miniSlider.image.pathname === '' ? (
            <ImagePicker
              setState={(uploadedFile) => handleImageUpload(uploadedFile)}
            />
          ) : (
            <div className='photoContStyle'>
              {!props.openModal && (
                <img
                  alt='close'
                  src={close}
                  className='pointer imageEraser'
                  onClick={() => {
                    props.manageModal('mini_slider')
                  }}
                />
              )}
              <img
                src={`${URL}/assets/${miniSlider.image.filename}`}
                className='imgStyle'
                alt='variant examples'
              />
            </div>
          )}
        </div>
      </div>
      <button className='none'></button>
    </form>
  )
}
