/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import ImagePicker from '../utils/imagePicker'
import close from '../../admin/pictures/close.svg'
import { URL } from '../../../config'
import tick from '../../admin/pictures/tick.svg'

export default function Featured(props) {
  // Inititalize state
  const [featured, setFeatured] = useState({
    colour: '',
    fields: [{ text: '', url: '' }],
    image: {
      filename: '',
      pathname: '',
    },
  })

  // Get data from parent and put it to state
  useEffect(() => {
    if (props.settings && props.settings[0].name && !featured.name) {
      let found = props.settings.find((setting) => setting.name === 'featured')
      setFeatured(found)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  // For inputs
  const handleChange = (e, setting, part) => {
    let tempFeatured = { ...featured }
    if (e.target.value === '#') {
      tempFeatured[setting] = null
    }
    if (part) {
      tempFeatured.fields[setting][part] = e.target.value
    } else {
      if (e.target.value[0] !== '#') {
        tempFeatured[setting] = e.target.value
      } else {
        tempFeatured[setting] = e.target.value.slice(1)
      }
    }
    props.setSettings(tempFeatured, 'featured')
    setFeatured(tempFeatured)
  }

  // For checkbox
  const handleClick = (e) => {
    e.preventDefault()
    let tempFeatured = { ...featured }
    tempFeatured.useBgColour = !featured.useBgColour
    props.setSettings(tempFeatured, 'featured')
    setFeatured(tempFeatured)
  }

  // For image upload
  const handleImageUpload = (uploadedFile) => {
    let tempFeatured = { ...featured }
    tempFeatured.image = uploadedFile
    props.setSettings(tempFeatured, 'featured')
    setFeatured(tempFeatured)
  }

  return (
    <form onSubmit={(e) => props.save(e)}>
      <div className='st_minisliderCont'>
        <div>
          <div className='st_inputCont'>
            <p>Colour</p>
            <input
              className='input'
              placeholder='#2D2D2D'
              type='text'
              value={featured.colour ? '#' + featured.colour : ''}
              onChange={(e) => handleChange(e, 'colour')}
            />
          </div>
          <div className='st_inputCont'>
            <p>Text</p>
            <input
              className='input'
              placeholder='Covid Restrictions Apply'
              type='text'
              value={featured.fields[0].text ? featured.fields[0].text : ''}
              onChange={(e) => handleChange(e, 0, 'text')}
            />
          </div>
          <div className='st_inputCont'>
            <p>url</p>
            <input
              className='input'
              placeholder='Free Delivery On Selected Products'
              type='text'
              value={featured.fields[0].url ? featured.fields[0].url : ''}
              onChange={(e) => handleChange(e, 0, 'url')}
            />
          </div>
          <div className='st_inputCont2'>
            <div className='st_inputCont3'>
              <p>BG Colour</p>
              <input
                className='input'
                placeholder='#000000'
                value={featured.bgColour ? '#' + featured.bgColour : ''}
                onChange={(e) => handleChange(e, 'bgColour')}
              />
            </div>
            <div className='st_inputCont4'>
              <p>Use BG</p>
              <div
                className='checkbox checkbox_with_border flexCenter pointer'
                id='priceDrop'
                onClick={(e) => handleClick(e)}
              >
                <img
                  src={tick}
                  className={`ps_tick ${!featured.useBgColour && 'nope'}`}
                  alt='tick'
                  id='priceDrop'
                />
              </div>
            </div>
          </div>
        </div>
        <div className='st_miniSlider_R flexCenter'>
          {featured.image.pathname === '' ? (
            <ImagePicker
              setState={(uploadedFile) => handleImageUpload(uploadedFile)}
            />
          ) : (
            <div className='photoContStyle'>
              {!props.openModal && (
                <img
                  alt='close'
                  src={close}
                  className='pointer imageEraser'
                  onClick={() => {
                    props.manageModal('featured')
                  }}
                />
              )}
              <img
                src={`${URL}/assets/${featured.image.filename}`}
                className='imgStyle'
                alt='variant examples'
              />
            </div>
          )}
        </div>
      </div>
      <button className='none'></button>
    </form>
  )
}
