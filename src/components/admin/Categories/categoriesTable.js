/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

export default function CategoriesTable(props) {
  const [categories, setCategories] = useState({
    colours: [],
    design: [],
    types: [],
    tags: [],
    textures: [],
  })

  useEffect(() => {
    if (props.categories) {
      setCategories(props.categories)
    }
  }, [props.categories])

  return (
    <div className='ct_fullTable default'>
      <div className='cl_table_title'>
        <p>Category</p>
        <p>Items</p>
        <p>#</p>
      </div>
      {Object.keys(categories).map((category, i) => {
        let values = Object.values(categories)
        return (
          <Link
            to={`${props.match.path}/${category}`}
            className='cl_table_section pointer'
            key={i}
          >
            <p>
              {category.charAt(0).toUpperCase() + category.slice(1) === 'Tags'
                ? 'Terms'
                : category.charAt(0).toUpperCase() + category.slice(1)}
            </p>
            <p>
              {values[i].length > 0
                ? values[i]?.map((array, i) => {
                    return `${i === 0 ? '' : ', '} ${array.name}`
                  })
                : 'No terms yet!'}
            </p>
            <p>{values[i].length}</p>
          </Link>
        )
      })}
    </div>
  )
}
