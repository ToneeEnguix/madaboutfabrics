/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import { URL } from '../../../config'
import axios from 'axios'
import { Switch, Route, Link } from 'react-router-dom'
import CategoriesTable from './categoriesTable'
import EditCategory from './editCategory'

export default function Categories(props) {
  const [update, setUpdate] = useState(false)
  const [categories, setCategories] = useState({
    colours: [],
    design: [],
    types: [],
    tags: [],
    textures: [],
  })

  useEffect(() => {
    const get_categories = async () => {
      try {
        Promise.all([
          axios.get(`${URL}/colours/all`),
          axios.get(`${URL}/designs/all`),
          axios.get(`${URL}/types/all`),
          axios.get(`${URL}/tags/all`),
          axios.get(`${URL}/textures/all`),
        ]).then((values) => {
          setCategories({
            ...categories,
            colours: values[0].data.data,
            design: values[1].data.data,
            types: values[2].data.data,
            tags: values[3].data.data,
            textures: values[4].data.data,
          })
        })
      } catch (err) {
        console.error(err)
      }
    }
    get_categories()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [update])

  return (
    <div className='page'>
      <Link className='cat_title' to={props.match.path}>
        <h1>Categories</h1>
      </Link>
      <Switch>
        <Route
          exact
          path={`${props.match.path}`}
          render={(props) => (
            <CategoriesTable {...props} categories={categories} />
          )}
        />
        <Route
          exact
          path={`${props.match.path}/:category`}
          render={(props) => (
            <EditCategory
              {...props}
              categories={categories}
              update={() => setUpdate(!update)}
            />
          )}
        />
      </Switch>
    </div>
  )
}
