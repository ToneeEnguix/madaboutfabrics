/** @jsx jsx */
import { jsx } from '@emotion/react'
// REACT IS NOT BEING USED BUT NECESSARY. DO NOT DELETE
// eslint-disable-next-line no-unused-vars
import React, { useState, useEffect, useCallback } from 'react'
import arrow from '../pictures/arrow.svg'
import cross from '../pictures/cross.svg'
import ReactModal from 'react-modal'
import { URL } from '../../../config'
import axios from 'axios'
import { Link } from 'react-router-dom'

export default function EditCategory(props) {
  // Category to map & render
  const [category, setCategory] = useState([])
  const categories = ['Colours', 'Design', 'Types', 'Tags', 'Textures']

  // Title of category
  const [title, setTitle] = useState('')

  // Modal Open
  const [openModal, setOpenModal] = useState(false)

  // Selected item for modal
  const [item, setItem] = useState({})

  // Add this new item
  const [newItem, setNewItem] = useState('')

  // Show edit box
  const [editable, setEditable] = useState(false)

  const escFunction = useCallback((event) => {
    if (event.keyCode === 27) {
      setOpenModal(false)
    }
  }, [])

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)
    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  // Get data from parent and put it to State to render
  useEffect(() => {
    if (props.categories) {
      Object.values(props.categories).forEach((category, i) => {
        let keys = Object.keys(props.categories)
        if (keys[i] === props.match.params.category) {
          setCategory(category)
          setTitle(keys[i])
        }
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  const deleteItem = async () => {
    try {
      const res = await axios.post(
        `${URL}/${
          title[title.length - 1] !== 's' ? `${title}s` : title
        }/delete`,
        { item }
      )
      if (res.data.ok) {
        setOpenModal(false)
        setEditable(false)
        props.update()
      }
    } catch (err) {
      console.error(err)
    }
  }

  const addNewItem = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.post(
        `${URL}/${title[title.length - 1] !== 's' ? `${title}s` : title}/add`,
        { newItem }
      )
      if (res.data.ok) {
        props.update()
        setNewItem('')
      }
    } catch (err) {
      console.error(err)
    }
  }

  const editTerm = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.post(
        `${URL}/${title[title.length - 1] !== 's' ? `${title}s` : title}/edit`,
        { item }
      )
      if (res.data.ok) {
        props.update()
        setEditable(!editable)
        setItem({})
      }
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <>
      <div className='ec_grid2'>
        <div className='ec_left_section'>
          <div className='ep_titleCont_left'>
            <img
              alt='back'
              className='arrow1 pointer'
              src={arrow}
              onClick={() => {
                props.history.goBack()
              }}
            />
            <h2>
              {title.charAt(0).toUpperCase() + title.slice(1) === 'Tags'
                ? 'Terms'
                : title.charAt(0).toUpperCase() + title.slice(1)}
            </h2>
            {categories.map((category, i) => {
              if (category !== title.charAt(0).toUpperCase() + title.slice(1)) {
                return (
                  <Link
                    to={
                      props.match.url.slice(0, -title.length) +
                      category.charAt(0).toLowerCase() +
                      category.slice(1)
                    }
                    key={i}
                    className='ec_otherCtgs'
                  >
                    {category === 'Tags' ? 'Terms' : category}
                  </Link>
                )
              } else {
                return null
              }
            })}
          </div>
          <div className='ec_fullTable'>
            <form className='ec_inputCont' onSubmit={(e) => addNewItem(e)}>
              <input
                placeholder='Write property'
                value={newItem}
                onChange={(e) => setNewItem(e.target.value)}
              />
            </form>
            <div className='ec_itemsCont'>
              {category.length !== 0 ? (
                category.map((term, i) => {
                  return (
                    <div
                      className='ec_item_name flexCenter pointer'
                      key={i}
                      onClick={() => {
                        item === term ? setItem({}) : setItem(term)
                        if (item === term || Object.keys(item).length === 0) {
                          setEditable(!editable)
                        }
                      }}
                    >
                      <p>{term.name}</p>
                      <img
                        src={cross}
                        className='ec_cross'
                        alt='cross'
                        onClick={(e) => {
                          setItem(term)
                          setOpenModal(true)
                          e.stopPropagation()
                        }}
                      />
                    </div>
                  )
                })
              ) : (
                <div style={{ padding: '1rem' }}>No terms yet!</div>
              )}
            </div>
          </div>
        </div>
        {editable && (
          <div className='ec_right_section'>
            <form className='ec_R_box' onSubmit={(e) => editTerm(e)}>
              <input
                value={item.name}
                onChange={(e) => {
                  setItem({ ...item, name: e.target.value })
                }}
              />
              <hr className='ep_hr'></hr>
              <div className='flexCenter'>
                <button className='pointer' onClick={(e) => editTerm(e)}>
                  Save
                </button>
              </div>
            </form>
          </div>
        )}
      </div>
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className='modal'
        style={{
          overlay: {
            backgroundColor: '#2626266d',
          },
        }}
      >
        <div className='dm_modalQuestionCont'>
          <div className='flexCenter dm_modalQuestion'>
            <p className='inline'>
              Do you want to delete{' '}
              {title[title.length - 1] === 's' ? title.slice(0, -1) : title}{' '}
              <i>{item.name}</i>?
            </p>
          </div>
          <div className='flexCenter'>
            <button
              className='raleway dm_modalBtn dm_modalBtn1 pointer'
              onClick={() => {
                setOpenModal(false)
              }}
            >
              No
            </button>

            <button
              className='raleway dm_modalBtn dm_modalBtn2 pointer'
              onClick={() => {
                deleteItem()
              }}
            >
              Yes
            </button>
          </div>
        </div>
      </ReactModal>
    </>
  )
}
