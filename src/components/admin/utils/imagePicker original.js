/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState, useCallback } from 'react'
import {
  NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME,
  NEXT_PUBLIC_CLOUDINARY_UPLOAD_PRESET,
} from '../../../config'
import { useDropzone } from 'react-dropzone'
import insert from '../pictures/insert.svg'
import loading from '../pictures/loading.gif'

const ImagePicker = (props) => {
  const [uploadedFile, setUploadedFile] = useState('')

  useEffect(() => {
    uploadedFile !== '' && props.setState(uploadedFile)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [uploadedFile])

  const onDrop = useCallback((acceptedFiles) => {
    props.setIdx2 && props.setIdx2(props.i)
    props.setUploading && props.setUploading(true)
    const url = `https://api.cloudinary.com/v1_1/${NEXT_PUBLIC_CLOUDINARY_CLOUD_NAME}/upload`

    acceptedFiles.forEach(async (acceptedFile) => {
      const formData = new FormData()
      formData.append('file', acceptedFile)
      formData.append('upload_preset', NEXT_PUBLIC_CLOUDINARY_UPLOAD_PRESET)

      const res = await fetch(url, {
        method: 'post',
        body: formData,
      })
      const data = await res.json()
      setUploadedFile(data)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accepts: 'image/*',
    multiple: false,
  })

  return (
    <>
      {props.uploading && props.match ? (
        <div className='photoContStyle flexColumn pointer'>
          <img src={loading} alt='insert' className='loadingImgStyle' />
        </div>
      ) : (
        <div {...getRootProps()} className='photoContStyle flexColumn pointer'>
          <input {...getInputProps()} />
          <img src={insert} alt='insert' className='noImgStyle' />
          <p
            className='gray'
            style={{ fontWeight: '300', letterSpacing: '.01rem' }}
          >
            Click to select
          </p>
          <p
            className='gray'
            style={{ fontWeight: '300', letterSpacing: '.01rem' }}
          >
            or drag and drop
          </p>
        </div>
      )}
    </>
  )
}

export default ImagePicker
