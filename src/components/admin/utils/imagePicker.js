/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState, useCallback } from 'react'
import { useDropzone } from 'react-dropzone'
import insert from '../pictures/insert.svg'
import loading from '../pictures/loading.gif'
import axios from 'axios'
import { URL } from '../../../config'

const ImagePicker = (props) => {
  const [uploadedFile, setUploadedFile] = useState(null)

  useEffect(() => {
    if (uploadedFile) {
      props.setState(uploadedFile)
      setUploadedFile(null)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [uploadedFile])

  // sending file to the server onClick
  const onDrop = useCallback(async (acceptedFiles) => {
    var selectedFile = acceptedFiles
    const data = new FormData()
    data.append('file', selectedFile[0])
    try {
      const res = await axios.post(`${URL}/photo/postphoto`, data)
      // then print response status
      setUploadedFile(res.data.newImage)
    } catch (err) {
      console.error(err)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const { getRootProps, getInputProps } = useDropzone({
    onDrop,
    accepts: 'image/*',
    multiple: false,
  })

  return (
    <div>
      {props.uploading && props.match ? (
        <div className='photoContStyle flexColumn pointer'>
          <img src={loading} alt='insert' className='loadingImgStyle' />
        </div>
      ) : (
        <div {...getRootProps()} className='photoContStyle flexColumn pointer'>
          <input {...getInputProps()} />
          <img src={insert} alt='insert' className='noImgStyle' />
          <p
            className='gray'
            style={{ fontWeight: '300', letterSpacing: '.01rem' }}
          >
            Click to select
          </p>
          <p
            className='gray'
            style={{ fontWeight: '300', letterSpacing: '.01rem' }}
          >
            or drag and drop
          </p>
        </div>
      )}
    </div>
  )
}

export default ImagePicker
