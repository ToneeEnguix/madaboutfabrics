/** @jsx jsx */
import { jsx } from '@emotion/react'

export default function header() {
  return (
    <div style={{ paddingBottom: '1rem' }} className='flexBetween'>
      <h1 className='inline'>New Orders</h1>
    </div>
  )
}
