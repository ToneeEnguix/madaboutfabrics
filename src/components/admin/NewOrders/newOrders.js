/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, useEffect } from 'react'
import Header from './header'
import NewOrdersTable from './newOrdersTable'
import { Route } from 'react-router-dom'
import axios from 'axios'
import { URL } from '../../../config'

export default function AllOrders(props) {
  const [newOrders, setNewOrders] = useState(null)
  const [loaded, setLoaded] = useState(false)
  const [update, setUpdate] = useState(false)

  useEffect(() => {
    const getOrders = async () => {
      try {
        const res = await axios.get(`${URL}/orders/getnew`)
        setNewOrders(res.data.allOrders)
      } catch (err) {
        console.error(err)
      } finally {
        !loaded && setLoaded(true)
      }
    }
    getOrders()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props, update])

  return (
    <React.Fragment>
      <Route
        exact
        path={`${props.match.path}`}
        render={() => (
          <div className='page'>
            <Header />
            <NewOrdersTable
              {...props}
              newOrders={newOrders}
              loaded={loaded}
              update={() => setUpdate(!update)}
              setLoaded={(what) => setLoaded(what)}
            />
          </div>
        )}
      />
    </React.Fragment>
  )
}
