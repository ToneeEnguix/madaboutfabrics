/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'

import loading from '../../../resources/loading.gif'
import axios from 'axios'
import { URL } from '../../../config'
import { getTime } from '../../public/helpers/getTime'

const header = {
  boxShadow: 'none',
  p: {
    marginRight: '1rem',
    ':hover': {
      color: '#2680EB',
      cursor: 'pointer',
    },
  },
}

const newOrdersWrapper = {
  display: 'flex',
  flexWrap: 'wrap',
}

const newOrderDetails = {
  margin: '2rem 2rem 0 0',
  backgroundColor: '#262626',
  padding: '2rem 1rem 2rem 2rem',
  width: '100%',
  maxWidth: '350px',
}

export default function NewOrdersTable(props) {
  const [newOrders, setNewOrders] = useState(null)
  const [filter, setFilter] = useState('all')

  useEffect(() => {
    setNewOrders(props.newOrders)
  }, [props])

  const changeStatus = async (order_id) => {
    try {
      await axios.put(`${URL}/orders/changestatus/${order_id}`)
      props.update()
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    const filterNewOrders = () => {
      if (filter === 'non-completed') {
        let tempOrders = props.newOrders.filter(
          (order) => order.completed === false
        )
        setNewOrders(tempOrders)
      } else if (filter === 'all') {
        setNewOrders(props.newOrders)
      } else if (filter === 'last') {
        let tempOrders = props.newOrders.filter(
          (order) =>
            Date.now() - new Date(order.orderDate).getTime() <= 86400000
        )
        setNewOrders(tempOrders)
      }
    }
    filterNewOrders()
  }, [filter, props])

  const purge = async () => {
    props.setLoaded(false)
    try {
      await axios.put(`${URL}/orders/purge`)
      props.update()
    } catch (err) {
      console.error(err)
    }
  }

  if (!props.loaded) {
    return (
      <div className='loadingStyle'>
        <img alt='loading' src={loading} />
      </div>
    )
  } else {
    return (
      <div>
        <div className='flex' css={header}>
          <p
            style={{ color: filter === 'all' && '#2680EB' }}
            onClick={() => setFilter('all')}
          >
            View All New
          </p>
          <p
            style={{ color: filter === 'last' && '#2680EB' }}
            onClick={() => setFilter('last')}
          >
            View Last 24h
          </p>
          <p
            style={{ color: filter === 'non-completed' && '#2680EB' }}
            onClick={() => setFilter('non-completed')}
          >
            View Incomplete
          </p>
          <p onClick={() => purge()}>Purge List</p>
        </div>
        <div css={newOrdersWrapper}>
          {newOrders?.map((newOrder) => {
            return (
              <div css={newOrderDetails} key={newOrder._id}>
                <div className='flexBetween'>
                  <h2>
                    Order #
                    {newOrder.orderNumber.toString().length === 1
                      ? '00'
                      : newOrder.orderNumber.toString().length === 2 && '0'}
                    {newOrder.orderNumber + 1}
                  </h2>
                  {newOrder.completed ? (
                    <div
                      onClick={() => {
                        changeStatus(newOrder._id)
                      }}
                    >
                      <p className='complete pointer black'>Completed</p>
                    </div>
                  ) : (
                    <div
                      onClick={() => {
                        changeStatus(newOrder._id)
                      }}
                    >
                      <p className='incomplete pointer'>Incomplete</p>
                    </div>
                  )}
                </div>
                <h2
                  className={`sample_details_name ${
                    newOrder.completed ? 'this_green' : 'this_red'
                  }`}
                >
                  {newOrder.user.name}
                </h2>
                <p>Request made - {getTime(newOrder.orderDate)}</p>
                <div className='sample_details_table'>
                  <div className='sample_details_section'>
                    <p>
                      {newOrder.shippingMethod === 'Ship'
                        ? 'Shipping'
                        : 'Collect'}
                      :
                    </p>
                    <p>{newOrder.shipmentAddress.name}</p>
                    <p>{newOrder.shipmentAddress.direction}</p>
                    <p>{newOrder.shipmentAddress.town}</p>
                    <p>{newOrder.shipmentAddress.region.name}</p>
                    <p>{newOrder.shipmentAddress.postalCode}</p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Billing:</p>
                    <p>N/A</p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Email address</p>
                    <p
                      className={`${
                        newOrder.completed ? 'this_green' : 'this_red'
                      }`}
                    >
                      {newOrder.user.email}
                    </p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Phone:</p>
                    <p
                      className={`${
                        newOrder.completed ? 'this_green' : 'this_red'
                      }`}
                    >
                      {newOrder.user.phone || '-'}
                    </p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Orders:</p>
                    {newOrder.productsBought.map((order, i) => {
                      return (
                        <p key={order._id}>
                          {i +
                            1 +
                            '. ' +
                            order.range.name +
                            ' | ' +
                            order.range.variants[order.variantIndex].color}
                        </p>
                      )
                    })}
                  </div>
                  <div className='sample_details_section'>
                    <p>SKU:</p>
                    {newOrder.productsBought.map((order) => {
                      return (
                        <p key={order._id}>
                          {order.range.variants[order.variantIndex].sku}
                        </p>
                      )
                    })}
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}
