/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import insert from '../pictures/insert.svg'
import { Link } from 'react-router-dom'
import { URL } from '../../../config'
import axios from 'axios'
import ReactModal from 'react-modal'

export default function ProductsTable(props) {
  const [loading, setLoading] = useState(true)
  // For commas in usage
  let truth = []
  // All products
  const [products, setProducts] = useState(null)
  // Product _id to delete
  const [product, setProduct] = useState('')
  // Modal Open
  const [openModal, setOpenModal] = useState(false)

  // Get products from parent and put it to state
  useEffect(() => {
    if (props.products) {
      setProducts(props.products.slice(props.counter, props.counter + 50))
      setLoading(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  // Delete Range
  const deleteRange = async () => {
    props.products &&
      product.variants.forEach((variant) => {
        variant.imageURLs.forEach((image) => deleteImage(image))
      })
    try {
      let res = await axios.post(`${URL}/ranges/delete`, { product })
      if (res.data.ok) {
        setOpenModal(false)
        props.update()
      }
    } catch (err) {
      console.error(err)
    }
  }

  const deleteImage = async (image) => {
    try {
      await axios.post(`${URL}/photo/deletephoto`, {
        filename: image?.filename,
        pathname: image?.pathname,
      })
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div className='ordersTable'>
      <div className='productsTable_grid1 productsTable_grid'>
        <img
          src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Small-dark-grey-circle.svg/1024px-Small-dark-grey-circle.svg.png'
          className='align_self_center products_table_lil_circle justify_self_center'
          alt='circle'
        />
        <img
          src={insert}
          alt='product'
          className='align_self_center justify_self_center'
        />
        <p>Range</p>
        <p>Colours</p>
        <p>Supplier (hidden)</p>
        <p>Price</p>
        <p>Usage</p>
        <p>Sample</p>
        <p>Sale</p>
        <p>Pricedrop</p>
      </div>
      {loading ? (
        [1, 2, 3, 4, 5].map((num) => {
          return (
            <div key={num} className='productsTable_grid light default'>
              <img
                alt='circle'
                src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Small-dark-grey-circle.svg/1024px-Small-dark-grey-circle.svg.png'
                className='products_table_lil_circle justify_self_center'
              />
              <div className='align_self_center product_imgCont justify_self_center' />
              <p style={{ padding: '0.3rem 0 0' }}>Loading...</p>
              <div className='pt_coloursNameCont' />
              <p />
              <p />
              <p />
              <p />
              <p />
              <p />
            </div>
          )
        })
      ) : products.length > 0 ? (
        products.map((product, i) => {
          return (
            <div key={i} className='productsTable_grid light default'>
              <img
                alt='cirlce'
                src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Small-dark-grey-circle.svg/1024px-Small-dark-grey-circle.svg.png'
                className='products_table_lil_circle justify_self_center'
              />
              <Link
                to={`${props.match.path}/${product._id}`}
                className='product_imgCont justify_self_center'
              >
                <img
                  alt='product'
                  src={`${URL}/assets/${product.variants[0].imageURLs[0]?.filename}`}
                  className='product_img'
                />
              </Link>
              <div>
                <Link
                  to={`${props.match.path}/${product._id}`}
                  className='pt_name'
                >
                  {product.name}
                </Link>
                <div className='show_options pointer'>
                  <p
                    onClick={() => {
                      setProduct(product)
                      setOpenModal(true)
                    }}
                  >
                    Trash
                  </p>
                  <p className='pt_line_betweem_trash_and_edit'>|</p>
                  <p
                    onClick={() =>
                      props.history.push(
                        `/adminlogin/products/${product._id}/edit`
                      )
                    }
                  >
                    Edit
                  </p>
                </div>
              </div>
              <div className='pt_coloursNameCont'>
                {product.variants.slice(0, 3).map((variant, i) => {
                  return (
                    <div className='pt_name2' key={i}>
                      <Link
                        to={`${props.match.path}/${product._id}/${variant.color}`}
                      >
                        {variant.color.slice(0, 20) +
                          (variant.color.length > 20 ? '...' : '')}
                      </Link>
                      <p>{i === product.variants.length - 1 ? '' : ', '}</p>
                    </div>
                  )
                })}
              </div>
              <p>{product.supplier}</p>
              <p>£{product.price}</p>
              <p>
                {Object.values(product.usage).map((usage, i) => {
                  let keys = Object.keys(product.usage)
                  i === 0 && (truth = [])
                  usage && truth.push(keys[i])
                  return (
                    usage &&
                    `${keys[i] !== truth[0] ? ', ' : ''} ${
                      keys[i].charAt(0).toUpperCase() + keys[i].slice(1)
                    }`
                  )
                })}
              </p>

              <p>{product.sample ? 'Yes' : 'No'}</p>
              <p>{product.sale ? 'Yes' : 'No'}</p>
              <p>
                {product.variants.some(
                  (prod) =>
                    prod.priceDrop &&
                    prod.priceDropDate &&
                    prod.saleDiscount > 0
                )
                  ? 'Yes'
                  : 'No'}
              </p>
            </div>
          )
        })
      ) : (
        <p className='noProdsYet'>No products yet!</p>
      )}
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className='modal'
        style={{
          overlay: {
            backgroundColor: '#2626266d',
          },
        }}
      >
        <div className='dm_modalQuestionCont'>
          <div className='flexCenter dm_modalQuestion'>
            <p className='inline'>
              Do you want to delete range <i>{product.name}</i>?
            </p>
          </div>
          <div className='flexCenter'>
            <button
              className='raleway dm_modalBtn dm_modalBtn1 pointer'
              onClick={() => {
                setOpenModal(false)
              }}
            >
              No
            </button>

            <button
              className='raleway dm_modalBtn dm_modalBtn2 pointer'
              onClick={() => {
                deleteRange()
              }}
            >
              Yes
            </button>
          </div>
        </div>
      </ReactModal>
    </div>
  )
}
