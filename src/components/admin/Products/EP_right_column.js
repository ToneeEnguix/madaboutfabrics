/** @jsx jsx */
import { jsx } from '@emotion/react'

export default function EP_right_column(props) {
  // Radio buttons designs
  const lil_circle =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Small-dark-grey-circle.svg/1024px-Small-dark-grey-circle.svg.png',
    selected_circle =
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd6%2FLACMTA_Circle_Blue_Line.svg%2F1024px-LACMTA_Circle_Blue_Line.svg.png&f=1&nofb=1'

  const sections = ['design', 'type', 'texture']

  return (
    <div className='ep_grid2_right'>
      <div className='flexColumn ep_right_btnCont'>
        <button
          className='ep_right_btn pointer'
          onClick={(e) => props.saveChanges(e)}
        >
          Save Changes
        </button>
        <button
          className='ep_right_btn pointer'
          onClick={() => props.setOpenModal(true)}
        >
          Delete Range
        </button>
        <p
          className={`ep_message ${props.color} ${
            props.message ? 'ep_message' : 'ep_nope'
          }`}
        >
          {props.message}
        </p>
      </div>
      <div className='ep_right_box'>
        <p>Usage</p>
        <hr className='ep_hr'></hr>
        <div className='ep_right_box_content'>
          {props.product.usage &&
            Object.values(props.product.usage).map((usage, i) => {
              let keys = Object.keys(props.product.usage)
              return (
                <div
                  className='ep_range_section pointer'
                  key={i}
                  onClick={(e) => props.handleClick2(e)}
                  id='usage'
                  name={keys[i]}
                >
                  <img
                    alt='circle'
                    src={usage ? selected_circle : lil_circle}
                    className={`justify_self_center ${
                      usage ? 'ep_blue_circle' : 'ep_lil_circle'
                    }`}
                    id='usage'
                    name={keys[i]}
                  />
                  <p>{keys[i].charAt(0).toUpperCase() + keys[i].slice(1)}</p>
                </div>
              )
            })}
        </div>
      </div>
      {props.right_boxes_content.map((section, i) => {
        return (
          <div className='ep_right_box' key={i}>
            <p>{sections[i].charAt(0).toUpperCase() + sections[i].slice(1)}</p>
            <hr className='ep_hr'></hr>
            <div className='ep_right_box_content'>
              {section.length > 0 ? (
                section.map((string, j) => {
                  return (
                    <div
                      className='ep_range_section'
                      onClick={(e) => props.handleClick3(e, string)}
                      id={sections[i]}
                      name={string.name}
                      key={i + j}
                    >
                      <img
                        alt='circle'
                        src={
                          props.product[sections[i]]?.name === string.name
                            ? selected_circle
                            : lil_circle
                        }
                        className='ep_lil_circle justify_self_center pointer'
                        id={sections[i]}
                        name={string.name}
                      />
                      <p>{string.name}</p>
                    </div>
                  )
                })
              ) : (
                <p>No terms yet!</p>
              )}
            </div>
          </div>
        )
      })}
      <div className='ep_right_box'>
        <p>Industry</p>
        <hr className='ep_hr'></hr>
        <div className='ep_right_box_content'>
          {props.product.industry &&
            Object.values(props.product.industry).map((industry, i) => {
              let keys = Object.keys(props.product.industry)
              if (
                keys[i] !== 'hospitality' &&
                keys[i] !== 'healthcare' &&
                keys[i] !== 'contract'
              ) {
                return null
              }
              return (
                <div
                  className='ep_range_section pointer'
                  key={i}
                  onClick={(e) => props.handleClick2(e)}
                  id='industry'
                  name={keys[i]}
                >
                  <img
                    alt='circle'
                    src={industry ? selected_circle : lil_circle}
                    className={`justify_self_center ${
                      industry ? 'ep_blue_circle' : 'ep_lil_circle'
                    }`}
                    id='industry'
                    name={keys[i]}
                  />
                  <p>{keys[i].charAt(0).toUpperCase() + keys[i].slice(1)}</p>
                </div>
              )
            })}
        </div>
      </div>
    </div>
  )
}
