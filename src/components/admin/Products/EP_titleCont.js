/** @jsx jsx */
import { jsx } from '@emotion/react'
import arrow from '../pictures/arrow.svg'

export default function TitleCont(props) {
  return (
    <div className='ep_titleCont'>
      <div className='ep_titleCont_left'>
        <img
          alt='back'
          className='arrow1 pointer'
          src={arrow}
          onClick={() => {
            props.history.goBack()
          }}
        />
        <h1>{props.product?.name || '    '} Edit Page</h1>
      </div>
      <div className='ep_titleCont_right'>
        <p>Permalink: https://madaboutfabrics.com/</p>
        <input
          className={`ep_titleCont_url ep_input ${
            props.editable && 'ep_editable_url'
          }`}
          name='url'
          value={props.product.url}
          onChange={(e) => props.handleChange(e)}
          readOnly={props.editable ? false : true}
        />
        <div
          className='ep_editBtn pointer flexCenter'
          onClick={() => props.setEditable()}
        >
          <p>{props.editable ? 'Save' : 'Edit'}</p>
        </div>
      </div>
    </div>
  )
}
