/** @jsx jsx */
import { jsx } from '@emotion/react'
import tick from '../pictures/tick.svg'
import { Link } from 'react-router-dom'
import plus from '../pictures/plus.svg'

export default function EP_left_column(props) {
  const loopMe = [1, 2, 3, 4]

  // For tags
  const handleChange = (e) => {
    const tempItem = props.tags.findIndex((tag) => tag.name === e.target.value)
    props.setProduct(e, tempItem)
  }

  return (
    <form
      className='ep_grid2_left'
      onSubmit={(e) => {
        props.saveChanges(e)
      }}
    >
      <div className='inputCont'>
        <p>Range Name</p>
        <input
          className='input'
          value={props.product.name}
          name='name'
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      <div className='inputCont'>
        <p>Supplier (hidden)</p>
        <input
          className='input'
          value={props.product.supplier}
          name='supplier'
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      <div className='inputCont'>
        <p>Description</p>
        <textarea
          value={props.product.description}
          name='description'
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      <div className='inputCont'>
        <p>Regular Price (£)</p>
        <input
          type='number'
          className='input'
          value={props.product.price}
          name='price'
          step='.01'
          min='0'
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      <div className='inputCont2'>
        <p>Fabric Care</p>
        <select
          value={props.selectedCare}
          className='select'
          onChange={(e) => props.handleChange_select(e)}
        >
          <option value='machineWashable'>Machine Washable</option>
          <option value='wipeClean'>Wipe Clean</option>
          <option value='dryClean'>Dry Clean</option>
          <option value='handWash'>Hand Wash</option>
          <option value='thirtyClean'>Thirty Clean</option>
          <option value='nonDetergent'>Non Detergent</option>
          <option value='doNotIron'>Do Not Iron</option>
        </select>
        <p></p>
        <p></p>
        <p>Fabric Repeat</p>
        <input
          className='input'
          type='number'
          name='fabricRepeat'
          value={props.product.fabricRepeat}
          onChange={(e) => props.handleChange(e)}
          step='any'
        />
        <p style={{ marginLeft: '20px' }}>Fabric Width</p>
        <input
          className='input'
          style={{ marginLeft: '20px' }}
          type='number'
          name='fabricWidth'
          value={props.product.fabricWidth}
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      <div className='inputCont'>
        <p>Fabric Composition</p>
        <input
          className='input'
          value={props.product.composition}
          name='composition'
          onChange={(e) => props.handleChange(e)}
        />
      </div>
      <div className='ep_grid4'>
        <div className='checkboxCont flexCenter'>
          <p>Offer Sample</p>
          <div
            className='checkbox checkbox_with_border flexCenter'
            id='sample'
            onClick={(e) => props.handleClick(e)}
          >
            <img
              src={tick}
              className={`ps_tick ${!props.product.sample && 'nope'}`}
              alt='tick'
              id='sample'
            />
          </div>
        </div>
        <div className='checkboxCont flexCenter'>
          <p>Be Inspired</p>
          <div
            className='checkbox checkbox_with_border flexCenter'
            id='inspired'
            onClick={(e) => props.handleClick(e)}
          >
            <img
              src={tick}
              className={`ps_tick ${!props.product.inspired && 'nope'}`}
              alt='tick'
              id='inspired'
            />
          </div>
        </div>
        <div className='checkboxCont flexCenter'>
          <p>This is a PVC Tablecloth</p>
          <div
            className='checkbox checkbox_with_border flexCenter'
            id='pvc'
            onClick={(e) => props.handleClick(e)}
          >
            <img
              src={tick}
              className={`ps_tick ${!props.product.pvc && 'nope'}`}
              alt='tick'
              id='pvc'
            />
          </div>
        </div>
        <div className='checkboxCont flexCenter'>
          <p>In Accessoires</p>
          <div
            className='checkbox checkbox_with_border flexCenter'
            id='accessoires'
            onClick={(e) => props.handleClick(e)}
          >
            <img
              id='accessoires'
              src={tick}
              className={`ps_tick ${!props.product.accessoires && 'nope'}`}
              alt='tick'
            />
          </div>
        </div>
      </div>
      <div className='ep_range_coloursCont'>
        <p className='ep_range_colours_title'>Range Colours:</p>
        <div className='ep_coloursCont'>
          {props.product.variants?.map((colour, i) => {
            return (
              <Link
                to={`${props.match.url.slice(0, -5)}/${colour.color}`}
                className='ep_colour_item pointer'
                key={i}
              >
                {colour.color}
              </Link>
            )
          })}
          <Link
            to={`${props.match.url.slice(0, -5)}/newcolour`}
            className='pointer ep_L_addColour_plusCont flexCenter'
          >
            <img src={plus} alt='plus' />
          </Link>
        </div>
      </div>
      <div className='ep_L_tagsCont'>
        {loopMe.map((item) => {
          const keyword = 'keyword0' + item
          return (
            <div className='inputCont' key={item}>
              <p>Visible Term</p>
              <input
                autoComplete='off'
                className='input'
                value={
                  props.product[keyword]?.name || props.product[keyword] || ''
                }
                name={keyword}
                list='tagsList'
                onChange={handleChange}
              />
              <button className='none'></button>
            </div>
          )
        })}
        {loopMe.map((item) => {
          const adminTag = 'adminTag0' + item
          return (
            <div className='inputCont' key={item}>
              <p>Invisible Term</p>
              <input
                autoComplete='off'
                className='input'
                value={
                  props.product[adminTag]?.name || props.product[adminTag] || ''
                }
                name={adminTag}
                list='tagsList'
                onChange={handleChange}
              />
            </div>
          )
        })}
        <datalist id='tagsList'>
          {Object.keys(props.tags).map((tag) => {
            let values = Object.values(props.tags)
            return <option key={tag}>{values[tag].name}</option>
          })}
        </datalist>
      </div>
      <button className='none'></button>
    </form>
  )
}
