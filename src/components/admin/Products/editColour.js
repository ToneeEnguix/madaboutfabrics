/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import arrow from '../pictures/arrow.svg'
import { URL } from '../../../config'
import axios from 'axios'
import ReactModal from 'react-modal'
import tick from '../pictures/tick.svg'
import { Link } from 'react-router-dom'
import ImagePicker from '../utils/imagePicker'
import close from '../pictures/close.svg'
import plus from '../pictures/plus.svg'

export default function EditColour(props) {
  // Radio buttons designs
  const lil_circle =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Small-dark-grey-circle.svg/1024px-Small-dark-grey-circle.svg.png',
    selected_circle =
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd6%2FLACMTA_Circle_Blue_Line.svg%2F1024px-LACMTA_Circle_Blue_Line.svg.png&f=1&nofb=1'

  // Whole Product
  const [product, setProduct] = useState({
    variants: [],
  })

  // Selected variant
  const [idx, setIdx] = useState(null)
  const [variant, setVariant] = useState({
    color: '',
    sku: '',
    saleDiscount: 0,
    imageURLs: [
      { pathname: '', filename: '' },
      { pathname: '', filename: '' },
      { pathname: '', filename: '' },
      { pathname: '', filename: '' },
    ],
  })

  // All colours on DB with initial empty values for space when loading
  const [colours, setColours] = useState([
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
    { name: '' },
  ])

  // Modal Open
  const [openModal, setOpenModal] = useState(false)

  // Message settings
  const [message, setMessage] = useState('')
  const [color, setColor] = useState('')

  // Delete range or image
  const [remove, setRemove] = useState(true)
  // Which image
  const [idx2, setIdx2] = useState(0)

  // For loading image
  const [uploading, setUploading] = useState(false)

  // For updating info
  const [update, setUpdate] = useState(true)

  // Get products and product variants from parent element
  useEffect(() => {
    if (props.products) {
      props.products.forEach((product) => {
        if (product._id === props.match.params.productid) {
          setProduct(product)
          product.variants.forEach((variant) => {
            if (variant.color === props.match.params.variant) {
              while (variant.imageURLs.length < 4) {
                variant.imageURLs.push({ pathname: '' })
              }
              setVariant(variant)
            }
          })
        }
      })
    }
    if (colours[0].name === '') {
      getColours()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.products, props.match, update])

  // Fetch all colours on DB and put them on state
  const getColours = async () => {
    try {
      const res = await axios.get(`${URL}/colours/all`)
      if (res.data.ok) {
        setColours(res.data.data)
      }
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    if (idx === null && variant && product.variants[0]) {
      let tempIdx = product.variants.findIndex(
        (item) => item.color === variant.color
      )
      setIdx(tempIdx)
    }
  }, [idx, product, variant])

  // For handling simple text/number inputs
  const handleChange = (e) => {
    let tempValue =
      e.target.name === 'priceDropDate'
        ? new Date(e.target.value).toISOString()
        : e.target.value
    setVariant({ ...variant, [e.target.name]: tempValue })
  }

  // Save Range
  const saveChanges = async (e) => {
    e && e.preventDefault()
    const num = variant.imageURLs.filter((image) => image.pathname !== '')
    if (
      !variant.realColor ||
      !variant.sku ||
      !variant.color ||
      num.length === 0
    ) {
      setColor('yellow')
      setMessage(`Fields "Name", "Sku", "Colour" and 1 image required.`)
      setTimeout(() => {
        setColor('')
        setMessage('')
      }, 2000)
      return false
    }
    let tempProduct = product
    let tempVariant = { ...variant }
    tempVariant.imageURLs = num
    tempProduct.variants[idx] = tempVariant
    try {
      // return false
      let res = await axios.post(`${URL}/ranges/update`, {
        product: tempProduct,
      })
      if (res.data.ok) {
        // update info
        setUpdate(!update)
        setColor('green')
        setMessage('Range saved correctly')
      } else {
        setColor('red')
        setMessage('Oops... Something went wrong')
      }
      setTimeout(() => {
        setColor('')
        setMessage('')
      }, 1500)
      return true
    } catch (err) {
      console.error(err)
    }
  }

  // Delete Colour
  const deleteColour = async () => {
    // filter empty images (boxes for uploading)
    let tempImageURLs = variant.imageURLs.filter((item) => item.pathname !== '')
    // remove current variant
    let tempVariants = product.variants.filter((item) => item !== variant)
    let tempProduct = product
    tempProduct.variants = tempVariants
    tempImageURLs.forEach((image) => deleteImage(image))
    try {
      setOpenModal(false)
      if (tempProduct.variants.length === 0) {
        let res = await axios.post(`${URL}/ranges/delete`, {
          product: tempProduct,
        })
        res.data.ok && props.history.push(`/adminlogin/products`)
      } else {
        let res = await axios.post(`${URL}/ranges/update`, {
          product: tempProduct,
        })
        res.data.ok && props.history.push(`/adminlogin/products/${product._id}`)
      }
    } catch (err) {
      console.error(err)
    }
  }

  // String ? handle colours : handle pickup/countdown
  const handleClick = async (e, string) => {
    if (string) {
      const tempVariant = { ...variant }
      // if color already picked
      let index = tempVariant.realColor.findIndex((color) => {
        if (color.name === string.name) {
          return true
        } else {
          return false
        }
      })
      index >= 0
        ? // remove it
          tempVariant.realColor.splice(index, 1)
        : // else add it
          tempVariant.realColor.push(string)
      setVariant(tempVariant)
    } else {
      setVariant({ ...variant, [e.target.id]: !variant[e.target.id] })
    }
  }

  const handleImageUpload = async (uploadedFile, i) => {
    let tempVariant = { ...variant }
    tempVariant.imageURLs[i] = uploadedFile
    setVariant(tempVariant)
    let tempVariants = [...product.variants]
    tempVariants[idx] = tempVariant
    setProduct({ ...product, variants: tempVariants })
    setUploading(false)
  }

  const deleteImage = async (image) => {
    try {
      const res = await axios.post(`${URL}/photo/deletephoto`, {
        filename: image ? image.filename : variant.imageURLs[idx2].filename,
        pathname: image ? image.pathname : variant.imageURLs[idx2].pathname,
      })
      if (res.data.ok && !image) {
        let tempVariant = { ...variant }
        tempVariant.imageURLs[idx2].pathname = ''
        let tempVariants = [...product.variants]
        tempVariants[idx] = tempVariant
        setOpenModal(false)
        setProduct({ ...product, variants: tempVariants })
        setUploading(false)
        saveChanges()
      }
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div>
      <div className='ecl_titleCont'>
        <img
          alt='back'
          className='arrow1 pointer'
          src={arrow}
          onClick={() => {
            if (message === '') {
              props.history.goBack()
            }
          }}
        />
        <div className='flexCenter'>
          <Link
            className='noLink'
            to={`/adminlogin/products/${props.match.params.productid}/edit`}
          >
            <h1>
              <i>{product.name}</i>
            </h1>
          </Link>
          <pre> {'  ==>'} </pre>
          <h1>
            {' '}
            <i>{variant.color}</i>
          </h1>
        </div>
      </div>
      <div className='ecl_grid2'>
        <form onSubmit={(e) => saveChanges(e)}>
          <div className='inputCont'>
            <p>Colour Name*</p>
            <input
              className='input'
              value={variant.color}
              name='color'
              onChange={handleChange}
            />
          </div>
          <div className='inputCont'>
            <p>Real Name (hidden)*</p>
            <input
              className='input'
              value={variant.sku}
              name='sku'
              onChange={handleChange}
            />
          </div>
          <div className='ecl_true_coloursCont'>
            <p>True Colours*</p>
            <div className='ecl_true_colours'>
              {colours.map((colour, i) => {
                return (
                  <div
                    className='ecl_true_colours_item pointer'
                    key={i}
                    onClick={(e) => handleClick(e, colour)}
                  >
                    <img
                      alt='product'
                      src={
                        variant.realColor?.some(
                          (realColor) => realColor.name === colour.name
                        )
                          ? selected_circle
                          : lil_circle
                      }
                      className={`justify_self_center ${
                        variant.realColor?.some(
                          (realColor) => realColor.name === colour.name
                        )
                          ? 'ep_blue_circle'
                          : 'ep_lil_circle'
                      }`}
                    />
                    <p>{colour.name}</p>
                  </div>
                )
              })}
            </div>
          </div>
          <div className='inputCont'>
            <p>On Sale (%)</p>
            <input
              className='input'
              value={variant.saleDiscount || 0}
              name='saleDiscount'
              type='number'
              min='0'
              max='100'
              onChange={handleChange}
            />
          </div>
          <div className='inputCont'>
            <p>Countdown</p>
            <div
              className='checkbox checkbox_with_border flexCenter pointer'
              id='priceDrop'
              onClick={(e) => handleClick(e)}
            >
              <img
                src={tick}
                className={`ps_tick ${!variant.priceDrop && 'nope'}`}
                alt='tick'
                id='priceDrop'
              />
            </div>
          </div>
          <div className='inputCont'>
            <p>Countdown End</p>
            <input
              className='input'
              value={variant.priceDropDate?.substring(0, 10) || ''}
              name='priceDropDate'
              onChange={handleChange}
              type='date'
            />
          </div>
          <div className='inputCont'>
            <p>Available for Pick Up</p>
            <div
              className='checkbox checkbox_with_border flexCenter pointer'
              id='availableForPickup'
              onClick={(e) => handleClick(e)}
            >
              <img
                src={tick}
                className={`ps_tick ${!variant.availableForPickup && 'nope'}`}
                alt='tick'
                id='availableForPickup'
              />
            </div>
          </div>
          <div>
            <p>Pictures</p>
            <div className='ecl_img_sectionCont'>
              {variant.imageURLs?.map((image, i) => {
                if (image.pathname === '') {
                  let match = i === idx2 ? true : false
                  return (
                    <ImagePicker
                      key={i}
                      setState={(uploadedFile) => {
                        handleImageUpload(uploadedFile, i)
                      }}
                      match={match}
                      uploading={uploading}
                      setUploading={(what) => setUploading(what)}
                      i={i}
                      setIdx2={(j) => setIdx2(j)}
                    />
                  )
                } else {
                  return (
                    <div className='photoContStyle' key={i}>
                      {!openModal && (
                        <img
                          alt='close'
                          src={close}
                          className='pointer imageEraser'
                          onClick={() => {
                            setRemove(false)
                            setOpenModal(true)
                            setIdx2(i)
                          }}
                        />
                      )}
                      <img
                        src={`${URL}/assets/${image.filename}`}
                        key={i}
                        className='imgStyle'
                        alt='variant examples'
                      />
                    </div>
                  )
                }
              })}
            </div>
          </div>
          <button className='none'></button>
        </form>
        <div className='ecl_right_column'>
          <div className='flexColumn ep_right_btnCont'>
            <button
              className='ep_right_btn pointer'
              onClick={(e) => saveChanges(e)}
            >
              Save Changes
            </button>
            {product.variants.length > 1 ? (
              <button
                className='ep_right_btn pointer'
                onClick={() => setOpenModal(true)}
              >
                Delete Colour
              </button>
            ) : null}
            <p
              className={`ep_message ${color} ${
                message ? 'ep_message' : 'ep_nope'
              }`}
            >
              {message}
            </p>
          </div>
          <div className='ecl_table'>
            <div className='ecl_table_title'>
              <p>Range: {product.name}</p>
            </div>
            <hr className='ep_hr'></hr>
            <div className='ecl_itemsCont'>
              {product.variants.map((item, i) => {
                if (item._id !== variant._id) {
                  return (
                    <Link
                      className='ec_item_name flexCenter pointer'
                      key={i}
                      to={`${props.match.url.slice(
                        0,
                        -[variant.color.length]
                      )}${item.color}`}
                    >
                      <p>{item.color}</p>
                    </Link>
                  )
                } else {
                  return null
                }
              })}
              <Link
                className='ec_item_name flexCenter pointer'
                to={`${props.match.url.slice(
                  0,
                  -[variant.color.length]
                )}newcolour`}
              >
                <img alt='plus' src={plus} />
              </Link>
            </div>
          </div>
        </div>
      </div>
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className='modal'
        style={{
          overlay: {
            backgroundColor: '#2626266d',
          },
        }}
      >
        <div className='dm_modalQuestionCont'>
          <div className='flexCenter dm_modalQuestion'>
            {remove ? (
              <p className='inline'>
                Do you want to delete colour <i>{variant.color}</i> from{' '}
                <i>{product.name}</i>?
              </p>
            ) : (
              <p className='inline'>
                Do you want to delete image from <i>{variant.color}</i> from{' '}
                <i>{product.name}</i>?
              </p>
            )}
          </div>
          <div className='flexCenter'>
            <button
              className='raleway dm_modalBtn dm_modalBtn1 pointer'
              onClick={() => {
                setOpenModal(false)
                !remove && setRemove(true)
              }}
            >
              No
            </button>
            <button
              className='raleway dm_modalBtn dm_modalBtn2 pointer'
              onClick={() => {
                setUploading(true)
                remove ? deleteColour() : deleteImage()
              }}
            >
              Yes
            </button>
          </div>
        </div>
      </ReactModal>
    </div>
  )
}
