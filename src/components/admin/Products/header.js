/** @jsx jsx */
import { jsx } from '@emotion/react'
import arrow from '../pictures/arrow.svg'

export default function header(props) {
  return (
    <div style={{ paddingBottom: '1.5rem' }} className='flexBetween'>
      <h1 css={title}>Products</h1>
      {
        <div className='flexBetween ao_headerRight'>
          <div>
            <input
              className='input'
              value={props.searchbar}
              onChange={props.handleChange}
              placeholder='Search...'
              type='text'
            />
          </div>
          <div className='flexCenter'>
            <img
              src={arrow}
              alt='arrow'
              className='arrow1 pointer'
              onClick={() => props.counter !== 0 && props.setCounter(-50)}
            />
            {props.products ? (
              <div css={pageIndicator}>
                <span>{props.page}</span>
                <span> of </span>
                <span> {props.totalPages}</span>
              </div>
            ) : (
              <div css={pageIndicator}>loading</div>
            )}
            <img
              src={arrow}
              alt='arrow'
              className='arrow2 pointer'
              onClick={() => props.setCounter(50)}
            />
          </div>
          <p>{props.products?.length} items</p>
        </div>
      }
    </div>
  )
}

const pageIndicator = {
    fontSize: '.7rem',
  },
  title = {
    lineHeight: '40px',
  }
