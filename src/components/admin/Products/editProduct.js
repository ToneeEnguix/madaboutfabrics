/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { URL } from '../../../config'
import axios from 'axios'
import ReactModal from 'react-modal'
import TitleCont from './EP_titleCont'
import EpLeftColumn from './EP_left_column'
import EpRightColumn from './EP_right_column'

export default function EditProduct(props) {
  // URL Edit
  const [editable, setEditable] = useState(false)

  // Message settings
  const [message, setMessage] = useState(false)
  const [color, setColor] = useState('')

  // Select 'Fabric Care'
  const [selectedCare, setSelectedCare] = useState('')

  // Modal Open
  const [openModal, setOpenModal] = useState(false)

  // Product Object
  const [product, setProduct] = useState({
    name: '',
    sample: false,
    inspired: false,
    pvc: false,
    accessoires: false,
    supplier: '',
    fabricRepeat: 0,
    fabricWidth: 0,
    composition: '',
    url: '',
    price: 0,
    keyword01: { name: '' },
    keyword02: { name: '' },
    keyword03: { name: '' },
    keyword04: { name: '' },
    adminTag01: { name: '' },
    adminTag02: { name: '' },
    adminTag03: { name: '' },
    adminTag04: { name: '' },
  })

  // Right gray boxes content
  const [design, setDesign] = useState([])
  const [type, setType] = useState([])
  const [texture, setTexture] = useState([])
  const right_boxes_content = [design, type, texture]

  // Tags for product
  const [tags, setTags] = useState([])

  // Get data from parent and put it to State to render
  useEffect(() => {
    if (props.products) {
      props.products.forEach((product) => {
        if (product._id === props.match.params.productid) {
          setProduct(product)
          Object.values(product.fabricCare).map((item, i) => {
            let keys = Object.keys(product.fabricCare)
            if (item === true) {
              setSelectedCare(keys[i])
            }
            return false
          })
        }
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.products])

  // Fetch data for design, type and texture
  useEffect(() => {
    const get_design_type__texture = async () => {
      try {
        Promise.all([
          axios.get(`${URL}/designs/all`),
          axios.get(`${URL}/types/all`),
          axios.get(`${URL}/textures/all`),
          axios.get(`${URL}/tags/all`),
        ]).then((values) => {
          setDesign(values[0].data.data)
          setType(values[1].data.data)
          setTexture(values[2].data.data)
          setTags(values[3].data.data)
        })
      } catch (err) {
        console.error(err)
      }
    }
    get_design_type__texture()
  }, [])

  // For simple inputs
  const handleChange = (e) => {
    setProduct({ ...product, [e.target.name]: e.target.value })
  }

  // For checkboxes
  const handleClick = (e) => {
    setProduct({ ...product, [e.target.id]: !product[e.target.id] })
  }

  // For select
  const handleChange_select = (e) => {
    let tempCare = product.fabricCare
    for (const item in tempCare) {
      e.target.value === item
        ? (tempCare[e.target.value] = true)
        : (tempCare[item] = false)
    }
    setSelectedCare(e.target.value)
    setProduct({ ...product, fabricCare: tempCare })
  }

  // For right boxes => MULTIPLE choice (usage and industry)
  const handleClick2 = (e) => {
    if (e.target.name && e.target.id) {
      let tempProduct = product
      tempProduct[e.target.id][e.target.name] =
        !tempProduct[e.target.id][e.target.name]
      setProduct({ ...product, [e.target.id]: tempProduct[e.target.id] })
    }
  }

  // For right boxes => SIMPLE choice (design, type and texture)
  const handleClick3 = async (e, string) => {
    if (e.target.name && e.target.id) {
      if (e.target.name === product[e.target.id]?.name) {
        setProduct({ ...product, [e.target.id]: null })
      } else {
        setProduct({ ...product, [e.target.id]: string })
      }
    }
  }

  // For saving changes to DB
  const saveChanges = async (e) => {
    e && e.preventDefault()
    if (!product.name) {
      setColor('yellow')
      setMessage('A name is required')
      setTimeout(() => {
        setColor('')
        setMessage('')
      }, 2000)
      return false
    }
    try {
      let res = await axios.post(`${URL}/ranges/update`, { product })
      if (res.data.ok) {
        setColor('green')
        setMessage('Range saved correctly')
      } else {
        setColor('red')
        setMessage('Oops... Something went wrong')
      }
      setTimeout(() => {
        setColor('')
        setMessage('')
      }, 2000)
    } catch (err) {
      console.error(err)
    }
  }

  // Delete Range
  const deleteRange = async () => {
    product.variants.forEach((variant) => {
      variant.imageURLs.forEach((image) => deleteImage(image))
    })
    try {
      let res = await axios.post(`${URL}/ranges/delete`, { product })
      props.update()
      res.data.ok && props.history.push('/adminlogin/products')
    } catch (err) {
      console.error(err)
    }
  }

  const deleteImage = async (image) => {
    try {
      await axios.post(`${URL}/photo/deletephoto`, {
        public_id: image.public_id,
      })
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div>
      <TitleCont
        {...props}
        product={product}
        editable={editable}
        setEditable={() => setEditable(!editable)}
        handleChange={(e) => handleChange(e)}
      />
      <div className='ep_grid2'>
        <EpLeftColumn
          {...props}
          product={product}
          handleChange={(e) => handleChange(e)}
          selectedCare={selectedCare}
          handleChange_select={(e) => handleChange_select(e)}
          handleClick={(e) => handleClick(e)}
          saveChanges={(e) => saveChanges(e)}
          tags={tags}
          setProduct={(e, tempItem) => {
            tempItem !== -1
              ? setProduct({ ...product, [e.target.name]: tags[tempItem] })
              : setProduct({ ...product, [e.target.name]: e.target.value })
          }}
        />
        <EpRightColumn
          saveChanges={(e) => saveChanges(e)}
          setOpenModal={(what) => setOpenModal(what)}
          color={color}
          message={message}
          product={product}
          handleClick2={(e) => handleClick2(e)}
          handleClick3={(e, string) => handleClick3(e, string)}
          right_boxes_content={right_boxes_content}
        />
      </div>
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className='modal'
        style={{
          overlay: {
            backgroundColor: '#2626266d',
          },
        }}
      >
        <div className='dm_modalQuestionCont'>
          <div className='flexCenter dm_modalQuestion'>
            <p className='inline'>
              Do you want to delete range <i>{product.name}</i>?
            </p>
          </div>
          <div className='flexCenter'>
            <button
              className='raleway dm_modalBtn dm_modalBtn1 pointer'
              onClick={() => {
                setOpenModal(false)
              }}
            >
              No
            </button>

            <button
              className='raleway dm_modalBtn dm_modalBtn2 pointer'
              onClick={() => {
                deleteRange()
              }}
            >
              Yes
            </button>
          </div>
        </div>
      </ReactModal>
    </div>
  )
}
