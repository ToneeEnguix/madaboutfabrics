/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import Header from './header'
import ProductsTable from './productsTable'
import ProductSummary from './productSummary'
import EditProduct from './editProduct'
import EditColour from './editColour'
import { URL } from '../../../config'
import axios from 'axios'
import { Route, Switch } from 'react-router-dom'
import NewColour from './newColour'

export default function AllProducts(props) {
  const [initialProducts, setInitialProducts] = useState(null)
  const [products, setProducts] = useState(null)
  const [counter, setCounter] = useState(0)
  const [page, setPage] = useState(1)
  const [sortedByName, setSortedByName] = useState(null)
  const [update, setUpdate] = useState(false)
  const [whatIsSorted, setWhatIsSorted] = useState('')
  const [searchbar, setSearchbar] = useState('')
  const [totalPages, setTotalPages] = useState(0)

  useEffect(() => {
    setTotalPages(products ? Math.ceil(products.length / 50) : 0)
    setPage(1)
    setCounter(0)
  }, [products])

  useEffect(() => {
    const getProductsInfo = async () => {
      try {
        let res = await axios.get(`${URL}/ranges/allranges`)
        setInitialProducts(res.data.data)
        setProducts(res.data.data)
      } catch (err) {
        console.error(err)
      }
    }
    getProductsInfo()
  }, [])

  const reInitialiseProducts = () => {
    setProducts(initialProducts)
  }

  const sort = (order, what) => {
    setSortedByName(order)
    setWhatIsSorted(what)
    if (!order) {
      setUpdate(!update)
    }
    const sortedProducts = products.sort(function (a, b) {
      var nameA = a[what].toUpperCase() // ignore upper and lowercase
      var nameB = b[what].toUpperCase() // ignore upper and lowercase
      if (order === 'sorted') {
        if (nameA < nameB) {
          return -1 //nameA comes first
        }
        if (nameA > nameB) {
          return 1 // nameB comes first
        }
        return 0 // names must be equal
      } else if (order === 'reverse') {
        if (nameA > nameB) {
          return -1 //nameA comes first
        }
        if (nameA < nameB) {
          return 1 // nameB comes first
        }
        return 0 // names must be equal
      } else {
        return false // must return something
      }
    })
    setProducts(sortedProducts)
  }

  const handleChange = (e) => {
    if (e.target.value === '') {
      reInitialiseProducts()
    } else {
      setProducts(
        initialProducts?.filter((value, i) => {
          if (
            value.name.toLowerCase().includes(e.target.value.toLowerCase()) ||
            value.supplier.toLowerCase().includes(e.target.value.toLowerCase())
          ) {
            return value
          } else {
            return null
          }
        })
      )
    }
    setSearchbar(e.target.value)
  }

  return (
    <Switch>
      <Route
        exact
        path={`${props.match.path}`}
        render={(props) => (
          <div className='page'>
            <Header
              {...props}
              products={products}
              counter={counter}
              setCounter={(num) => {
                if (num > 0 && totalPages !== page) {
                  setPage(page + 1)
                  setCounter(counter + num)
                } else if (num < 0 && page !== 0) {
                  setCounter(counter + num)
                  setPage(page - 1)
                }
              }}
              page={page}
              totalPages={totalPages}
              sortedByName={sortedByName}
              searchbar={searchbar}
              handleChange={(e) => handleChange(e)}
            />
            <ProductsTable
              products={products}
              {...props}
              update={() => setUpdate(!update)}
              counter={counter}
              sort={(what) =>
                sort(
                  !sortedByName
                    ? 'sorted'
                    : sortedByName === 'sorted'
                    ? 'reverse'
                    : sortedByName === 'reverse' && null,
                  what
                )
              }
              sortedByName={sortedByName}
              whatIsSorted={whatIsSorted}
              searchbar={searchbar}
            />
          </div>
        )}
      />
      <Route
        exact
        path={`${props.match.path}/:productid`}
        render={(props) => (
          <div className='page'>
            <ProductSummary {...props} products={products} />
          </div>
        )}
      />
      <Route
        exact
        path={`${props.match.path}/:productid/edit`}
        render={(props) => (
          <div className='page'>
            <EditProduct
              {...props}
              products={products}
              update={() => setUpdate(!update)}
            />
          </div>
        )}
      />
      <Route
        exact
        path={`${props.match.path}/:productid/newcolour`}
        render={(props) => (
          <div className='page'>
            <NewColour
              {...props}
              products={products}
              update={() => setUpdate(!update)}
            />
          </div>
        )}
      />
      <Route
        exact
        path={`${props.match.path}/:productid/:variant`}
        render={(props) => (
          <div className='page'>
            <EditColour
              {...props}
              products={products}
              update={() => setUpdate(!update)}
            />
          </div>
        )}
      />
    </Switch>
  )
}
