/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import arrow from '../pictures/arrow.svg'
import { URL } from '../../../config'
import axios from 'axios'
import ReactModal from 'react-modal'
import tick from '../pictures/tick.svg'
import { Link } from 'react-router-dom'
import ImagePicker from '../utils/imagePicker'
import close from '../pictures/close.svg'

export default function EditColour(props) {
  // Radio buttons designs
  const lil_circle =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Small-dark-grey-circle.svg/1024px-Small-dark-grey-circle.svg.png',
    selected_circle =
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd6%2FLACMTA_Circle_Blue_Line.svg%2F1024px-LACMTA_Circle_Blue_Line.svg.png&f=1&nofb=1'

  // Whole Product
  const [product, setProduct] = useState({
    variants: [],
  })

  // Selected variant
  const [idx, setIdx] = useState(0)
  const [variant, setVariant] = useState({
    color: '',
    sku: '',
    saleDiscount: 0,
    imageURLs: [
      { pathname: '', filename: '' },
      { pathname: '', filename: '' },
      { pathname: '', filename: '' },
      { pathname: '', filename: '' },
    ],
  })

  // All the colours on DB
  const [colours, setColours] = useState([])

  // Modal Open
  const [openModal, setOpenModal] = useState(false)

  // Message settings
  const [message, setMessage] = useState('')
  const [color, setColor] = useState('')

  // Delete range or image
  const [remove, setRemove] = useState(true)
  // Which image
  const [idx2, setIdx2] = useState(0)

  // Get products and product variants from parent element
  useEffect(() => {
    if (props.products && props.products[0] && colours[0]) {
      props.products.forEach((product) => {
        if (product._id === props.match.params.productid) {
          let tempVariant = {
            imageURLs: [
              {
                pathname: '',
              },
              {
                pathname: '',
              },
              {
                pathname: '',
              },
              {
                pathname: '',
              },
            ],
            availableForPickup: true,
            saleDiscount: 0,
            priceDrop: false,
            stock: 0,
            favorited: 0,
            metersSold: 0,
            dateAdded: Date.now(),
            realColor: [colours[0]],
            color: '',
            sku: '',
            thumbnailURL: 'https://picsum.photos/20/30',
          }
          setVariant(tempVariant)
          product.variants.push(tempVariant)
          setProduct(product)
        }
      })
    }
    if (!colours[0]) {
      getColours()
    }
  }, [props, colours])

  // Fetch all colours on DB and put them on state
  const getColours = async () => {
    try {
      const res = await axios.get(`${URL}/colours/all`)
      if (res.data.ok) {
        setColours(res.data.data)
      }
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    if (variant && product.variants[0]) {
      let tempIdx = product.variants.findIndex((item) => {
        if (item._id === variant._id) {
          return true
        } else {
          return false
        }
      })
      setIdx(tempIdx)
    }
  }, [product, variant])

  // For handling simple text/number inputs
  const handleChange = (e) => {
    let tempValue =
      e.target.name === 'priceDropDate'
        ? new Date(e.target.value).toISOString()
        : e.target.value
    setVariant({ ...variant, [e.target.name]: tempValue })
  }

  // Save Range
  const saveChanges = async (e) => {
    e && e.preventDefault()
    const num = variant.imageURLs.filter((image) => image.pathname !== '')
    if (
      !variant.realColor ||
      !variant.sku ||
      !variant.color ||
      num.length === 0
    ) {
      setColor('yellow')
      setMessage(`Fields "Name", "Sku", "Colour" and 1 image required.`)
      setTimeout(() => {
        setColor('')
        setMessage('')
      }, 2000)
      return false
    }
    let tempProduct = { ...product }
    let tempVariant = { ...variant }
    tempVariant.imageURLs = tempVariant.imageURLs.filter(
      (image) => image.pathname !== ''
    )
    tempProduct.variants[idx] = tempVariant
    try {
      let res = await axios.post(`${URL}/ranges/update`, {
        product: tempProduct,
      })
      if (res.data.ok) {
        setColor('green')
        setMessage('Range saved correctly. Redirecting to products')
      } else {
        setColor('red')
        setMessage('Oops... Something went wrong')
      }
      setTimeout(() => {
        setColor('')
        setMessage('')
        props.history.push('/adminlogin/products')
      }, 1500)
    } catch (err) {
      console.error(err)
    }
  }

  // String ? handle colours : handle pickup/countdown
  const handleClick = async (e, string) => {
    if (string) {
      const tempVariant = { ...variant }
      // if color already picked
      let index = tempVariant.realColor.findIndex((color) => {
        if (color.name === string.name) {
          return true
        } else {
          return false
        }
      })
      index >= 0
        ? // remove it
          tempVariant.realColor.splice(index, 1)
        : // else add it
          tempVariant.realColor.push(string)
      setVariant(tempVariant)
    } else {
      setVariant({ ...variant, [e.target.id]: !variant[e.target.id] })
    }
  }

  const handleImageUpload = async (uploadedFile, i) => {
    let tempVariant = { ...variant }
    tempVariant.imageURLs[i] = uploadedFile
    setVariant(tempVariant)
    let tempVariants = [...product.variants]
    tempVariants[idx] = tempVariant
    setProduct({ ...product, variants: tempVariants })
  }

  const deleteImage = async (image) => {
    try {
      const res = await axios.post(`${URL}/photo/deletephoto`, {
        filename: image ? image.filename : variant.imageURLs[idx2].filename,
        pathname: image ? image.pathname : variant.imageURLs[idx2].pathname,
      })
      if (res.data.ok && !image) {
        let tempVariant = { ...variant }
        tempVariant.imageURLs[idx2].pathname = ''
        tempVariant.imageURLs[idx2].filename = ''
        let tempVariants = [...product.variants]
        tempVariants[idx] = tempVariant
        setProduct({ ...product, variants: tempVariants })
        setOpenModal(false)
        saveChanges()
      }
    } catch (err) {
      console.error(err)
    }
  }

  const deleteColour = async () => {
    let tempImageURLs = variant.imageURLs.filter(
      (image) => image.pathname !== ''
    )
    tempImageURLs.length > 0 ??
      tempImageURLs.forEach((image) => deleteImage(image))
    props.update()
    setOpenModal(false)
    setTimeout(() => {
      props.history.push('/adminlogin/products')
    }, 1000)
  }

  return (
    <div>
      <div className='ecl_titleCont'>
        <img
          alt='arrow'
          className='arrow1 pointer'
          src={arrow}
          onClick={() => {
            if (message === '') {
              props.history.goBack()
            }
          }}
        />
        <div className='flexCenter'>
          <Link
            className='noLink'
            to={`/adminlogin/products/${props.match.params.productid}/edit`}
          >
            <h1>
              <i>{product.name}</i>
            </h1>
          </Link>
          <pre> {'  ==>'} </pre>
          <h1>
            {' '}
            <i>{variant.color}</i>
          </h1>
        </div>
      </div>
      <div className='ecl_grid2'>
        <form onSubmit={(e) => saveChanges(e)}>
          <div className='inputCont'>
            <p>Colour Name</p>
            <input
              className='input'
              value={variant.color}
              name='color'
              onChange={handleChange}
            />
          </div>
          <div className='inputCont'>
            <p>Real Name (hidden)</p>
            <input
              className='input'
              value={variant.sku}
              name='sku'
              onChange={handleChange}
            />
          </div>
          <div className='ecl_true_coloursCont'>
            <p>True Colours*</p>
            <div className='ecl_true_colours'>
              {colours.map((colour, i) => {
                return (
                  <div
                    className='ecl_true_colours_item pointer'
                    key={i}
                    onClick={(e) => handleClick(e, colour)}
                  >
                    <img
                      alt='circle'
                      src={
                        variant.realColor?.some(
                          (realColor) => realColor.name === colour.name
                        )
                          ? selected_circle
                          : lil_circle
                      }
                      className={`justify_self_center ${
                        variant.realColor?.some(
                          (realColor) => realColor.name === colour.name
                        )
                          ? 'ep_blue_circle'
                          : 'ep_lil_circle'
                      }`}
                    />
                    <p>{colour.name}</p>
                  </div>
                )
              })}
            </div>
          </div>
          <div className='inputCont'>
            <p>On Sale (%)</p>
            <input
              className='input'
              value={variant.saleDiscount}
              name='saleDiscount'
              type='number'
              min='0'
              max='100'
              onChange={handleChange}
            />
          </div>
          <div className='inputCont'>
            <p>Countdown</p>
            <div
              className='checkbox checkbox_with_border flexCenter pointer'
              id='priceDrop'
              onClick={(e) => handleClick(e)}
            >
              <img
                src={tick}
                className={`ps_tick ${!variant.priceDrop && 'nope'}`}
                alt='tick'
                id='priceDrop'
              />
            </div>
          </div>
          <div className='inputCont'>
            <p>Countdown End</p>
            <input
              className='input'
              value={variant.priceDropDate?.substring(0, 10) || ''}
              name='priceDropDate'
              onChange={handleChange}
              type='date'
            />
          </div>
          <div className='inputCont'>
            <p>Available for Pick Up</p>
            <div
              className='checkbox checkbox_with_border flexCenter pointer'
              id='availableForPickup'
              onClick={(e) => handleClick(e)}
            >
              <img
                src={tick}
                className={`ps_tick ${!variant.availableForPickup && 'nope'}`}
                alt='tick'
                id='availableForPickup'
              />
            </div>
          </div>
          <div>
            <p>Pictures</p>
            <div className='ecl_img_sectionCont'>
              {variant.imageURLs?.map((image, i) => {
                if (image.pathname === '') {
                  return (
                    <ImagePicker
                      key={i}
                      setState={(uploadedFile) =>
                        handleImageUpload(uploadedFile, i)
                      }
                    />
                  )
                } else {
                  return (
                    <div className='photoContStyle' key={i}>
                      {!openModal && (
                        <img
                          alt='close'
                          src={close}
                          className='pointer imageEraser'
                          onClick={() => {
                            setRemove(false)
                            setOpenModal(true)
                            setIdx2(i)
                          }}
                        />
                      )}
                      <img
                        src={`${URL}/assets/${image.filename}`}
                        key={i}
                        className='imgStyle'
                        alt='variant examples'
                      />
                    </div>
                  )
                }
              })}
            </div>
          </div>
          <button className='none'></button>
        </form>
        <div className='ecl_right_column'>
          <div className='flexColumn ep_right_btnCont'>
            <button
              className='ep_right_btn pointer'
              onClick={(e) => saveChanges(e)}
            >
              Create
            </button>
            <button
              className='ep_right_btn pointer'
              onClick={() => setOpenModal(true)}
            >
              Cancel
            </button>
            <p
              className={`ep_message ${color} ${
                message ? 'ep_message' : 'ep_nope'
              }`}
            >
              {message}
            </p>
          </div>
        </div>
      </div>
      <ReactModal
        ariaHideApp={false}
        isOpen={openModal}
        className='modal'
        style={{
          overlay: {
            backgroundColor: '#2626266d',
          },
        }}
      >
        <div className='dm_modalQuestionCont'>
          <div className='flexCenter dm_modalQuestion'>
            {remove ? (
              <p className='inline'>
                Do you want to cancel colour <i>{variant.color}</i> from{' '}
                <i>{product.name}</i>?
              </p>
            ) : (
              <p className='inline'>
                Do you want to delete image from <i>{variant.color}</i> from{' '}
                <i>{product.name}</i>?
              </p>
            )}
          </div>
          <div className='flexCenter'>
            <button
              className='raleway dm_modalBtn dm_modalBtn1 pointer'
              onClick={() => {
                setOpenModal(false)
                !remove && setRemove(true)
              }}
            >
              No
            </button>
            <button
              className='raleway dm_modalBtn dm_modalBtn2 pointer'
              onClick={() => {
                remove ? deleteColour() : deleteImage()
              }}
            >
              Yes
            </button>
          </div>
        </div>
      </ReactModal>
    </div>
  )
}
