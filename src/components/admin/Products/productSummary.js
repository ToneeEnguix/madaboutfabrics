/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import arrow from '../pictures/arrow.svg'
import tick from '../pictures/tick.svg'
import { Link } from 'react-router-dom'

export default function ProductSummary(props) {
  // Product structure
  const [product, setProduct] = useState({
    sample: false,
    inspired: false,
    pvc: false,
    accessoires: false,
  })

  // For commas in usage
  let truth = []

  // Get products from parent and put selected product to state
  useEffect(() => {
    if (props.products?.[0]) {
      props.products.forEach((product) => {
        if (product._id === props.match.params.productid) {
          setProduct(product)
        }
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.products])

  return (
    <div className='productSummary'>
      <div className='ps_titleCont'>
        <img
          className='arrow1 pointer'
          src={arrow}
          onClick={() => {
            props.history.goBack()
          }}
          alt='back arrow'
        />
        <h1>{product?.name || '    '} Summary Page</h1>
        <Link to={`${props.match.url}/edit`} className='ps_editBtn pointer'>
          Edit
        </Link>
        {/* <div className="ps_favorited_sold">
          <p>Favourited</p>
          <div className="flexCenter">
            {product.variants?.map((variant, i) => {
              return (
                <p key={i}>
                  {variant.favorited}
                  {i === 0 && product.variants.length > 1 && ", "}
                </p>
              );
            })}
          </div>
        </div>
        <div className="ps_favorited_sold">
          <p>Meters Sold</p>
          <div className="flexCenter">
            {product.variants?.map((variant, i) => {
              return (
                <p key={i}>
                  {variant.metersSold}
                  {i === 0 && product.variants.length > 1 && ", "}
                </p>
              );
            })}
          </div>
        </div> */}
      </div>
      <div className='ps_main'>
        <div className='ps_grid2'>
          <p>Range Name</p>
          <p className='gray'>{product.name}</p>
          <p>Supplier (hidden)</p>
          <p className='gray'>{product.supplier}</p>
          <p>Description</p>
          <p className='gray'>{product.description}</p>
        </div>
        <div className='ps_grid2'>
          <p>Regular Price</p>
          <p className='gray'>{product.price}</p>
          <p>Fabric Care</p>
          <p className='gray'>
            {product.fabricCare &&
              Object.values(product.fabricCare).map((fabricCare, i) => {
                let keys = Object.keys(product.fabricCare)
                return (
                  fabricCare &&
                  `${keys[i].charAt(0).toUpperCase() + keys[i].slice(1)}`
                )
              })}
          </p>
          <p>Fabric Width</p>
          <p className='gray'>{product.fabricWidth}</p>
          <p>Fabric Repeat</p>
          <p className='gray'>{product.fabricRepeat}</p>
          <p>Product Composition</p>
          <p className='gray'>{product.composition}</p>
        </div>
        <div className='ps_grid4'>
          <div className='checkboxCont flexCenter'>
            <p>Offer Sample</p>
            <div className='checkbox flexCenter'>
              <img
                src={tick}
                className={`ps_tick ${!product.sample && 'nope'}`}
                alt='tick'
              />
            </div>
          </div>
          <div className='checkboxCont flexCenter'>
            <p>Be Inspired</p>
            <div className='checkbox flexCenter'>
              <img
                src={tick}
                className={`ps_tick ${!product.inspired && 'nope'}`}
                alt='tick'
              />
            </div>
          </div>
          <div className='checkboxCont flexCenter'>
            <p>This is a PVC Tablecloth</p>
            <div className='checkbox flexCenter'>
              <img
                src={tick}
                className={`ps_tick ${!product.pvc && 'nope'}`}
                alt='tick'
              />
            </div>
          </div>
          <div className='checkboxCont flexCenter'>
            <p>In Accessoires</p>
            <div className='checkbox flexCenter'>
              <img
                src={tick}
                className={`ps_tick ${!product.accessoires && 'nope'}`}
                alt='tick'
              />
            </div>
          </div>
        </div>
        <div className='ps_grid2'>
          <p>Usage</p>
          <p className='gray'>
            {product.usage &&
              Object.values(product.usage).map((usage, i) => {
                let keys = Object.keys(product.usage)
                i === 0 && (truth = [])
                usage && truth.push(keys[i])
                return (
                  usage &&
                  `${keys[i] !== truth[0] ? ', ' : ''} ${
                    keys[i].charAt(0).toUpperCase() + keys[i].slice(1)
                  }`
                )
              })}
          </p>
          <p>Design</p>
          <p className='gray'>{product.design?.name}</p>
          <p>Type</p>
          <p className='gray'>{product.type?.name}</p>
          <p>Texture</p>
          <p className='gray'>{product.texture?.name}</p>
          <p>Industry</p>
          <p className='gray'>
            {product.industry &&
              Object.values(product.industry).map((industry, i) => {
                let keys = Object.keys(product.industry)
                i === 0 && (truth = [])
                industry && truth.push(keys[i])
                return (
                  industry &&
                  `${keys[i] !== truth[0] ? ', ' : ''} ${
                    keys[i].charAt(0).toUpperCase() + keys[i].slice(1)
                  }`
                )
              })}
          </p>
          <p>Colours</p>
          <p className='gray'>
            {product.variants &&
              product.variants.map(
                (variant, i) => `${i !== 0 ? ', ' : ''} ${variant.color}`
              )}
          </p>
        </div>
      </div>
    </div>
  )
}
