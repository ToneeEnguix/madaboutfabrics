/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import { Route, Switch } from 'react-router-dom'

import './admin.css'

import Header from './Header/header'
import Sidebar from './Sidebar/sidebar'
import Home from './home'
import NewOrders from './NewOrders/newOrders'
import NewSamples from './New Samples/newSamples'
import AllOrders from './All Orders/allOrders'
import AllSamples from './All Samples/allSamples'
import Customers from './Customers/customers'
import Customer from './Customers/customer'
import Products from './Products/products'
import AddNewRange from './addNewRange/addNewRange'
import Categories from './Categories/categories'
import TagCarousel from './TagCarousel'
import Settings from './Settings/settings'
import Authentication from './Authentication'

const Adminapp = (props) => {
  const [isLoggedIn, setIsLoggedIn] = useState(null)

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [props])

  useEffect(() => {
    const checkCredentials = async () => {
      const token = localStorage.getItem('admintoken')
      if (token) setIsLoggedIn(true)
      else setIsLoggedIn(false)
    }
    checkCredentials()
  }, [])

  const login = (token) => {
    setIsLoggedIn(true)
    localStorage.setItem('admintoken', token)
  }

  const logout = () => {
    localStorage.removeItem('admintoken')
    window.location.href = '/'
  }

  if (isLoggedIn === false) {
    return <Authentication {...props} login={(token) => login(token)} />
  } else if (!isLoggedIn) {
    return <div>Loading...</div>
  }
  return (
    <div>
      <Header {...props} />
      <Sidebar {...props} logout={logout} />
      <Switch>
        <Route
          exact
          path={`${props.match.path}`}
          render={(props) => <Home {...props} />}
        />
        <Route
          path={`${props.match.path}/neworders`}
          render={(props) => <NewOrders {...props} />}
        />
        <Route
          path={`${props.match.path}/newsamples`}
          render={(props) => <NewSamples {...props} />}
        />
        <Route
          path={`${props.match.path}/allorders`}
          render={(props) => <AllOrders {...props} />}
        />
        <Route
          path={`${props.match.path}/allsamples`}
          render={(props) => <AllSamples {...props} />}
        />
        <Route
          path={`${props.match.path}/customers/:customer_id`}
          render={(props) => <Customer {...props} />}
        />
        <Route
          path={`${props.match.path}/customers`}
          render={() => <Customers />}
        />
        <Route
          path={`${props.match.path}/products`}
          render={(props) => <Products {...props} />}
        />
        <Route
          path={`${props.match.path}/addnewrange`}
          render={(props) => <AddNewRange {...props} />}
        />
        <Route
          path={`${props.match.path}/categories`}
          render={(props) => <Categories {...props} />}
        />
        <Route
          path={`${props.match.path}/tagcarousel`}
          render={() => <TagCarousel />}
        />
        <Route
          path={`${props.match.path}/settings`}
          render={() => <Settings />}
        />
      </Switch>
    </div>
  )
}

export default Adminapp
