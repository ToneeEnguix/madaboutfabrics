/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { v4 as uuid } from 'uuid'
import { ToastContainer, toast } from 'react-toastify'

import { getTime } from '../../public/helpers/getTime'

import arrow from '../pictures/arrow.svg'
import { URL } from '../../../config'

export default function SampleDetails(props) {
  const [sample, setSample] = useState(null)

  const sample_id = props.location.pathname.slice(-24)

  // get sample info from backend
  useEffect(() => {
    const getSampleInfo = async () => {
      try {
        const res = await axios.get(
          `${URL}/samples/get_sample_info/${sample_id}`
        )
        let tempSample = { ...res.data.sample }
        tempSample.requestedSamples.forEach((item) => (item.id = uuid()))
        setSample(res.data.sample)
      } catch (err) {
        console.error(err)
      }
    }
    getSampleInfo()
  }, [sample_id])

  // change status in the DB
  const changeStatus = async () => {
    try {
      await axios.put(`${URL}/samples/changestatus/${sample._id}`)
      setSample({ ...sample, status: !sample.status })
      toast('Order status updated successfully')
    } catch (err) {
      toast.error(err)
      console.error(err)
    }
  }

  if (sample) {
    return (
      <div className='sampleDetails'>
        <Link to={props.match.path} className='flex'>
          <img alt='arrow back' src={arrow} className='arrow1' />
          <p className='sampleDetails_back'>Back</p>
        </Link>
        <div className='sample_details_full_content'>
          <div className='flexBetween'>
            <h2>
              Sample #
              {sample.index.toString().length === 1
                ? '00'
                : sample.index.toString().length === 2 && '0'}
              {sample.index + 1}
            </h2>
            {sample.status ? (
              <div
                onClick={() => {
                  changeStatus()
                }}
              >
                <p className='complete pointer black'>Complete</p>
              </div>
            ) : (
              <div
                onClick={() => {
                  changeStatus()
                }}
              >
                <p className='incomplete pointer'>Mark as complete</p>
              </div>
            )}
          </div>
          <h2
            className={`sample_details_name ${
              sample.status ? 'this_green' : 'this_red'
            }`}
          >
            {sample.user.name}
          </h2>
          <p>Request made - {getTime(sample.orderDate)}</p>
          <div className='sample_details_table'>
            <div className='sample_details_section'>
              <p>
                {sample.shippingMethod === 'Ship' ? 'Shipping' : 'Collect'}:
              </p>
              <p>{sample.shipmentAddress.name}</p>
              <p>{sample.shipmentAddress.direction}</p>
              <p>{sample.shipmentAddress.town}</p>
              <p>{sample.shipmentAddress.region}</p>
              <p>{sample.shipmentAddress.postalCode}</p>
            </div>
            <div className='sample_details_section'>
              <p>Billing:</p>
              <p>N/A</p>
            </div>
            <div className='sample_details_section'>
              <p>Email address</p>
              <p className={`${sample.status ? 'this_green' : 'this_red'}`}>
                {sample.user.email}
              </p>
            </div>
            <div className='sample_details_section'>
              <p>Phone:</p>
              <p className={`${sample.complete ? 'this_green' : 'this_red'}`}>
                {sample.user.phone || '-'}
              </p>
            </div>
            <div className='sample_details_section'>
              <p>Samples:</p>
              {sample.requestedSamples.map((requestedSample, i) => {
                return (
                  <p key={requestedSample.id}>
                    {i +
                      1 +
                      '. ' +
                      requestedSample.range.name +
                      ' | ' +
                      requestedSample.range.variants[
                        requestedSample.variantIndex
                      ].color}
                  </p>
                )
              })}
            </div>
            <div className='sample_details_section'>
              <p>SKU:</p>
              {sample.requestedSamples.map((requestedSample, i) => {
                return (
                  <p key={requestedSample.id}>
                    {
                      requestedSample.range.variants[
                        requestedSample.variantIndex
                      ].sku
                    }
                  </p>
                )
              })}
            </div>
          </div>
        </div>
        <ToastContainer autoClose={2000} />
      </div>
    )
  } else {
    return <div>loading</div>
  }
}
