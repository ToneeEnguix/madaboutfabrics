/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import loading from '../../../resources/loading.gif'

export default function allSamplesTable(props) {
  if (!props.loaded) {
    return (
      <div className='flexCenter loadingStyle'>
        <img alt='loading' src={loading} />
      </div>
    )
  } else {
    return (
      <div className='ordersTable'>
        <div className='samplesTable_grid'>
          <p>Customer</p>
          <p>Date Received</p>
          <p className='justify_items_left'>Samples</p>
          <p>Status</p>
          <p>Quantity</p>
        </div>
        {props.samples?.length > 0 ? (
          props.samples.map((sample, i) => {
            return (
              <Link
                to={`${props.match.path}/${sample._id}`}
                className='samplesTable_grid light'
                key={i}
              >
                <p>{`${sample.user.name} ${
                  sample.user.lastname?.length > 0 ? sample.user.lastname : ''
                }`}</p>
                <p>{`${new Date(
                  sample.orderDate
                ).toLocaleDateString()} @${new Date(
                  sample.orderDate
                ).getHours()}:${new Date(sample.orderDate).getMinutes()}`}</p>
                <p className='justify_items_left'>
                  {sample.requestedSamples.map((sample, i) => {
                    return `${i !== 0 ? ', ' : ''} ${sample.range.name} ${
                      sample.range.variants[sample.variantIndex].color
                    }`
                  })}
                </p>
                <p>{sample.status ? 'Complete' : 'Incomplete'}</p>
                <p>{sample.requestedSamples.length}</p>
              </Link>
            )
          })
        ) : (
          <div className='no_samples'>No samples yet!</div>
        )}
      </div>
    )
  }
}
