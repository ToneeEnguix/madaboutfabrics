/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, useEffect } from 'react'
import Header from './header'
import { Route } from 'react-router-dom'

import AllSamplesTable from './allSamplesTable'
import SampleDetails from './sampleDetails'
import axios from 'axios'

import { URL } from '../../../config'

export default function AllSamples(props) {
  const [count, setCount] = useState(1)
  const [countIndex, setCountIndex] = useState(0)
  const [samples, setSamples] = useState(null)
  const [loaded, setLoaded] = useState(false)
  const [search, setSearch] = useState('')
  const [initial, setInitial] = useState([])

  useEffect(() => {
    const getSamples = async () => {
      setLoaded(false)
      try {
        const res = await axios.get(`${URL}/samples/get_some/${countIndex}`)
        setSamples(res.data.samples)
        setInitial(res.data.samples)
      } catch (err) {
        console.error(err)
      }
      setLoaded(true)
    }
    getSamples()
  }, [props, countIndex])

  // get documents count
  useEffect(() => {
    const getOrdersCount = async () => {
      try {
        const res = await axios.get(`${URL}/samples/count`)
        setCount(res.data.count)
      } catch (err) {
        console.error(err)
      }
    }
    getOrdersCount()
  }, [])

  const searchSamples = async () => {
    setLoaded(false)
    try {
      const res = await axios.get(
        `${URL}/samples/search/${search.toLowerCase()}`
      )
      let tempSamples = [...res.data.samples]
      setSamples(tempSamples)
      setCount(res.data.samples.length)
    } catch (err) {
      console.error(err)
    } finally {
      setLoaded(true)
    }
  }

  return (
    <React.Fragment>
      <Route
        exact
        path={`${props.match.path}`}
        render={() => (
          <div className='page'>
            <Header
              count={count}
              countIndex={countIndex}
              setCountIndex={setCountIndex}
              search={search}
              setSearch={(text) => {
                text === '' && setSamples(initial)
                setSearch(text)
              }}
              searchSamples={searchSamples}
            />
            <AllSamplesTable {...props} samples={samples} loaded={loaded} />
          </div>
        )}
      />
      <Route
        path='/adminlogin/allsamples/:sampleid'
        render={() => (
          <div className='page'>
            <SampleDetails {...props} />
          </div>
        )}
      />
    </React.Fragment>
  )
}
