/** @jsx jsx */
import { jsx } from '@emotion/react'
import arrow from '../pictures/arrow.svg'

export default function Header({
  count,
  countIndex,
  setCountIndex,
  search,
  setSearch,
  searchOrders,
}) {
  const decreasePage = () => {
    if (countIndex !== 0) setCountIndex(countIndex - 1)
  }

  const increasePage = () => {
    if (countIndex + 1 < Math.ceil(count / 10)) setCountIndex(countIndex + 1)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    searchOrders()
  }

  return (
    <div style={{ paddingBottom: '1.5rem' }} className='flexBetween'>
      <h1 css={title}>Customers</h1>
      {
        <div className='flexBetween ao_headerRight'>
          <form onSubmit={handleSubmit}>
            <input
              className='input'
              value={search}
              onChange={setSearch}
              placeholder='Search...'
              type='text'
            />
            <button style={{ display: 'none' }} />
          </form>
          <div className='flexCenter'>
            <img
              src={arrow}
              alt='arrow'
              className='arrow1 pointer'
              onClick={decreasePage}
            />
            {count ? (
              <div css={pageIndicator}>
                {countIndex + 1} of {Math.ceil(count / 10) || 1}
              </div>
            ) : (
              <div css={pageIndicator}>loading</div>
            )}
            <img
              src={arrow}
              alt='arrow'
              className='arrow2 pointer'
              onClick={increasePage}
            />
          </div>
          <p>{count || '-'} items</p>
        </div>
      }
    </div>
  )
}

const pageIndicator = {
    fontSize: '.7rem',
  },
  title = {
    lineHeight: '40px',
  }
