/** @jsx jsx */
import { jsx } from '@emotion/react'
import lilArrow from '../pictures/lilArrow.svg'
import { Link } from 'react-router-dom'

export default function OrdersTable({ customers, sort, sorting, loaded }) {
  return (
    <div className='ordersTable'>
      {/* HEADER */}
      <div className='customersTable_grid1'>
        <div className='flexCenter'>
          <p onClick={() => sort('name')}>Customer name</p>
          <img
            src={lilArrow}
            alt='arrow'
            className={`${
              sorting.by === 'name' && sorting.order === 'asc'
                ? 'lilArrow'
                : sorting.by === 'name' && sorting.order === 'des'
                ? 'lilArrowReverse'
                : 'none'
            }`}
          />
        </div>
        <p>Total Samples Ordered</p>
        <p>Total Purchases Made</p>
        <div className='flexCenter'>
          <p onClick={() => sort('email')}>Email</p>
          <img
            src={lilArrow}
            alt='little arrow'
            className={`${
              sorting.by === 'email' && sorting.order === 'asc'
                ? 'lilArrow'
                : sorting.by === 'email' && sorting.order === 'des'
                ? 'lilArrowReverse'
                : 'none'
            }`}
          />
        </div>
        <p>Shipping Zone</p>
        <p></p>
      </div>
      {/* IT'S LOADED AND THERE ARE CUSTOMERS */}
      {(customers?.length && loaded) > 0 ? (
        customers.map((user, i) => {
          return (
            <Link
              to={`/adminlogin/customers/${user._id}`}
              className='customersTable_grid light'
              key={user._id}
            >
              <p>{user.name}</p>
              <p>{user.userSamples}</p>
              <p>{user.userPurchases}</p>
              <p>{user.email}</p>
              <p>Zone {user.shipping_zone}</p>
              <p></p>
            </Link>
          )
        })
      ) : !loaded ? (
        // IT'S NOT LOADED
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((num, i) => {
          return (
            <div className='customersTable_grid light' key={i}>
              <p>-</p>
              <p>-</p>
              <p>-</p>
              <p>-</p>
              <p>-</p>
              <p>-</p>
            </div>
          )
        })
      ) : (
        // THERE ARE NO CUSTOMERS
        <div className='no_samples'>No customers yet!</div>
      )}
    </div>
  )
}
