/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import { URL } from '../../../config'
import axios from 'axios'
import arrow from '../pictures/arrow.svg'
import { getTime } from '../../public/helpers/getTime'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [1000]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function Customer(props) {
  const { customer_id } = props.match.params

  const [user, setUser] = useState(null)
  const [samples, setSamples] = useState(null)
  const [orders, setOrders] = useState(null)

  useEffect(() => {
    const getUserInfo = async () => {
      try {
        const res = await axios.get(`${URL}/users/get_user_info/${customer_id}`)
        setUser(res.data.user)
        setSamples(res.data.userSamples)
        setOrders(res.data.userPurchases)
      } catch (err) {
        console.error(err)
      }
    }
    getUserInfo()
  }, [customer_id])

  return (
    <div className='page'>
      <div className='flex'>
        <img
          className='arrow1'
          src={arrow}
          alt='arrow'
          onClick={() => props.history.goBack()}
        />
        <h2>Customer</h2>
      </div>
      {user ? (
        <div css={wrapper}>
          <div className='leftColumn'>
            <div className='topBox'>
              <h3 className='boxTitle'>{user.name}</h3>
              <div className='grid'>
                <div>
                  <p>Email:</p>
                  <p className='this_green'>{user.email}</p>
                </div>
                <div>
                  <p>User created on:</p>
                  <p className='this_green'>
                    {getTime(user.dateCreated).slice(0, 10)}
                  </p>
                </div>
              </div>
            </div>
            <div className='centerBox'>
              <p className='boxTitle'>
                Amount Spent by {user.name} {user.lastName || ''} in Total:
              </p>
              <p className='this_green boxTitle'>
                £
                {orders &&
                  orders
                    .reduce(
                      (acc, order) =>
                        acc +
                        order.productsBought.reduce(
                          (acc, prod) => acc + prod.amount * prod.range.price,
                          0
                        ) +
                        order.shipmentCost,
                      0
                    )
                    .toFixed(2)}
              </p>
              {orders?.map((order, i) => {
                return (
                  <div key={order._id} style={{ marginTop: '2rem' }}>
                    <p className='boxTitle'>
                      Order received on {getTime(order.orderDate).slice(0, 10)}
                    </p>
                    <p className='this_green'>
                      {order.productsBought.length} Items in Order
                    </p>
                    <p>
                      {order.productsBought.map((prod) => (
                        <span key={prod._id}>
                          {prod.range.variants[prod.variantIndex].color} x
                          {prod.amount}
                          {i === order.productsBought.length - 1 ? '' : ', '}
                        </span>
                      ))}
                    </p>
                  </div>
                )
              })}
            </div>
            <div className='bottomBox'>
              <p className='boxTitle'>
                Total Samples Ordered by {user.name} {user.lastName || ''}
              </p>
              <p className='this_green boxTitle'>{samples?.length}</p>
              {samples?.map((sample) => {
                return (
                  <div style={{ marginTop: '2rem' }} key={sample._id}>
                    <p className='boxTitle'>
                      Samples Sent on {getTime(sample.orderDate).slice(0, 10)}
                    </p>
                    <p className='this_green'>
                      {sample.requestedSamples.length} Items in Order
                    </p>
                    <div>
                      {sample.requestedSamples.map((sample, i) => {
                        return (
                          <div className='grid' key={sample._id}>
                            <p>
                              {i + 1}.{'  '}
                              {sample.range.variants[sample.variantIndex].color}
                            </p>
                            <p>
                              {sample.range.variants[sample.variantIndex].sku}
                            </p>
                          </div>
                        )
                      })}
                    </div>
                  </div>
                )
              })}
            </div>
          </div>
          <div className='rightColumn'>
            {orders?.map((order, i) => {
              return (
                <div className='orderWrapper' key={order._id}>
                  <p className='orderWhen'>
                    Order Recevied on {getTime(order.orderDate)}
                  </p>
                  <div className='order'>
                    <div className='grid7 gridHeader'>
                      <p>Items</p>
                      <p>Range</p>
                      <p>Colour</p>
                      <p>SKU</p>
                      <p>Supplier</p>
                      <p>Quantity</p>
                      <p>Total</p>
                    </div>
                    <div>
                      {order.productsBought.map((prod, i) => {
                        const variant = prod.range.variants[prod.variantIndex]
                        return (
                          <div className='grid7' key={prod._id}>
                            {/* <img
                              alt="product"
                              src={`${URL}/assets/${variant.imageURLs[0].filename}`}
                            /> */}
                            <img
                              alt='product'
                              src='https://picsum.photos/200/300'
                            />
                            <p>{prod.range.name}</p>
                            <p>{variant.color}</p>
                            <p>{variant.sku}</p>
                            <p>{prod.range.supplier}</p>
                            <p>{prod.amount}</p>
                            <p>£{prod.amount * prod.range.price}</p>
                          </div>
                        )
                      })}
                      <div className='grid7 shipping'>
                        <p>Shipping</p>
                        <p />
                        <p />
                        <p />
                        <p />
                        <p />
                        <p>£{order.shipmentCost.toFixed(2)}</p>
                      </div>
                      <div className='billing'>
                        <div>
                          <p>Billing</p>
                        </div>
                        <div className='billing_right'>
                          <div>
                            <p>Item Subtotal:</p>
                            <p>
                              £
                              {order.productsBought
                                .reduce(
                                  (acc, prod) =>
                                    acc + prod.amount * prod.range.price,
                                  0
                                )
                                .toFixed(2)}
                            </p>
                          </div>
                          <div>
                            <p>Shipping:</p>
                            <p>£{order.shipmentCost.toFixed(2)}</p>
                          </div>
                          <div
                            style={{
                              borderBottom: '1px solid grey',
                              paddingBottom: '.5rem',
                            }}
                          >
                            <p>Order Total:</p>
                            <p>
                              £
                              {(
                                Number(
                                  order.productsBought.reduce(
                                    (acc, prod) =>
                                      acc + prod.amount * prod.range.price,
                                    0
                                  )
                                ) + Number(order.shipmentCost)
                              ).toFixed(2)}
                            </p>
                          </div>
                          <div style={{ padding: '.5rem 0 .5rem 30%' }}>
                            <p>Paid:</p>
                            <p className='this_green'>
                              £
                              {(
                                Number(
                                  order.productsBought.reduce(
                                    (acc, prod) =>
                                      acc + prod.amount * prod.range.price,
                                    0
                                  )
                                ) + Number(order.shipmentCost)
                              ).toFixed(2)}
                            </p>
                          </div>
                          <p>Paid - {getTime(user.dateCreated).slice(0, 10)}</p>
                          <p>Payment: Credit Card</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </div>
      ) : (
        <div>loading</div>
      )}
    </div>
  )
}

const wrapper = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr 2fr'],
  gap: '1rem',
  marginTop: '1.5rem',
  '.boxTitle': {
    fontSize: '1rem',
    marginBottom: '.3rem',
    fontWeight: 300,
  },
  '.leftColumn': {
    '.topBox': {
      marginBottom: '1rem',
      backgroundColor: '#262626',
      padding: '2rem 0 2.5rem 1.3rem',
      '.grid': {
        marginTop: '1.5rem',
        display: 'grid',
        gridTemplateColumns: '1fr 1fr',
      },
    },
    '.centerBox': {
      marginBottom: '1rem',
      backgroundColor: '#262626',
      padding: '2rem 1.3rem 2.5rem',
    },
    '.bottomBox': {
      backgroundColor: '#262626',
      padding: '2rem 1.3rem 2.5rem',
      '.grid': {
        display: 'grid',
        gridTemplateColumns: '1fr 1fr',
      },
    },
  },
  '.rightColumn': {
    '.orderWrapper': {
      '.orderWhen': {
        fontSize: '1rem',
        lineHeight: '3rem',
      },
    },
    '.order': {
      backgroundColor: '#262626',
      '.gridHeader': {
        boxShadow: '0px 3px 6px #00000029',
      },
      '.grid7': {
        padding: '1.5rem 0 1.5rem 2rem',
        display: 'grid',
        gridTemplateColumns: 'repeat(7, 1fr)',
        img: {
          width: '40px',
          height: '40px',
        },
        p: {
          display: 'flex',
          alignItems: 'center',
        },
      },
      '.shipping': {
        backgroundColor: '#212121',
      },
    },
    '.billing': {
      padding: '1.5rem 9.3% 1.5rem 2rem',
      display: 'grid',
      gridTemplateColumns: '1fr 2fr',
      '.billing_right': {
        div: {
          paddingLeft: '30%',
          display: 'grid',
          gridTemplateColumns: '2fr 1fr',
        },
        'div > p': {
          textAlign: 'left',
        },
        'p + p': {
          textAlign: 'right',
        },
        p: {
          textAlign: 'right',
          margin: '.2rem 0',
        },
      },
    },
  },
})
