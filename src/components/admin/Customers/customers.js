/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import Header from './header'
import CustomersTable from './customersTable'
import { URL } from '../../../config'
import axios from 'axios'

export default function AllSamples(props) {
  // loading
  const [loaded, setLoaded] = useState(false)
  // customers
  const [customers, setCustomers] = useState(null)
  // sorting
  const [sorting, setSorting] = useState({
    by: 'dateCreated',
    order: 'asc',
  })
  // searching
  const [search, setSearch] = useState('')
  // pagination
  const [count, setCount] = useState(0)
  const [countIndex, setCountIndex] = useState(0)

  useEffect(() => {
    const getCustomersLength = async () => {
      try {
        const res = await axios.get(`${URL}/users/length`)
        setCount(res.data.length)
      } catch (err) {
        console.error(err)
      }
    }
    const getCustomers = async () => {
      try {
        let res = await axios.get(
          `${URL}/users/some/${countIndex * 10}/${sorting.order}/${sorting.by}`
        )
        setCustomers([...res.data])
        res.data.length > 0 && getUsersCount(res.data)
      } catch (err) {
        console.error(err)
      } finally {
        setLoaded(true)
      }
    }
    !loaded && setLoaded(false)
    !count && getCustomersLength()
    getCustomers()
  }, [sorting, countIndex, loaded, count])

  const getUsersCount = async (users) => {
    let tempCustomers = [...users]
    try {
      let promises = tempCustomers.map((customer) =>
        axios.get(`${URL}/users/count/${customer._id}`)
      )
      Promise.all(promises).then((values) => {
        values.forEach(
          (value, i) =>
            (tempCustomers[i] = {
              ...tempCustomers[i],
              userPurchases: value.data.userPurchases,
              userSamples: value.data.userSamples,
            })
        )
        setCustomers(tempCustomers)
      })
    } catch (err) {
      console.error(err)
    }
  }

  const sort = (receivedOrder) => {
    setSorting({
      by:
        receivedOrder === sorting.by && sorting.order === 'des'
          ? 'dateCreated'
          : receivedOrder,
      order:
        receivedOrder === sorting.by && sorting.order === 'asc' ? 'des' : 'asc',
    })
  }

  const searchOrders = async () => {
    try {
      const res = await axios.get(`${URL}/users/search/${search}`)
      setCustomers(res.data.users)
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div className='page'>
      <Header
        {...props}
        count={count}
        countIndex={countIndex}
        setCountIndex={setCountIndex}
        search={search}
        setSearch={(e) => {
          setSearch(e.target.value)
          if (e.target.value === '') {
            setSorting({ ...sorting })
          }
        }}
        searchOrders={searchOrders}
      />
      <CustomersTable
        customers={customers}
        countIndex={countIndex}
        loaded={loaded}
        sorting={sorting}
        sort={sort}
      />
    </div>
  )
}
