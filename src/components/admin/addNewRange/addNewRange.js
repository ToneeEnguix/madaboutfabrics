/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Route, Switch } from 'react-router-dom'
import AnrProduct from './aNR_product'
import AnrColour from './aNR_colour'
import { URL } from '../../../config'
import axios from 'axios'

export default function AddNewRange(props) {
  // Categories
  const [design, setDesign] = useState([])
  const [type, setType] = useState([])
  const [texture, setTexture] = useState([])
  const [tags, setTags] = useState([])
  // Select 'Fabric Care'
  const [selectedCare, setSelectedCare] = useState('')
  // Product Object
  const [product, setProduct] = useState({
    name: '',
    supplier: '',
    description: '',
    composition: '',
    url: '',
    fabricRepeat: 0,
    fabricWidth: 0,
    sample: false,
    inspired: false,
    pvc: false,
    accessoires: false,
    price: 0,
    usage: {
      upholstery: false,
      curtains: false,
      craft: false,
    },
    industry: {
      hospitality: false,
      healthcare: false,
      contract: false,
    },
    design: design[0],
    type: type[0],
    texture: texture[0],
    keyword01: '',
    keyword02: '',
    keyword03: '',
    keyword04: '',
    adminTag01: '',
    adminTag02: '',
    adminTag03: '',
    adminTag04: '',
  })

  useEffect(() => {
    const getDraft = async () => {
      try {
        const res = await axios.get(`${URL}/ranges/getdraft`)
        setProduct(res.data.product)
      } catch (err) {
        console.error(err)
      }
    }
    getDraft()
  }, [])

  // Fetch data for design, type and texture
  useEffect(() => {
    const get_design_type_texture_tags_colours = async () => {
      try {
        Promise.all([
          axios.get(`${URL}/designs/all`),
          axios.get(`${URL}/types/all`),
          axios.get(`${URL}/textures/all`),
          axios.get(`${URL}/tags/all`),
        ]).then((values) => {
          setDesign(values[0].data.data)
          setType(values[1].data.data)
          setTexture(values[2].data.data)
          setTags(values[3].data.data)
        })
      } catch (err) {
        console.error(err)
      }
    }
    get_design_type_texture_tags_colours()
  }, [])

  // For simple inputs
  const handleChange = (e) => {
    setProduct({ ...product, [e.target.name]: e.target.value })
  }

  // For checkboxes
  const handleClick = (e) => {
    setProduct({ ...product, [e.target.id]: !product[e.target.id] })
  }

  // For select
  const handleChange_select = (e) => {
    let tempCare = product.fabricCare
    for (const item in tempCare) {
      e.target.value === item
        ? (tempCare[e.target.value] = true)
        : (tempCare[item] = false)
    }
    setSelectedCare(e.target.value)
    setProduct({ ...product, fabricCare: tempCare })
  }

  // For right boxes => MULTIPLE choice (usage and industry)
  const handleClick2 = (e) => {
    if (e.target.name && e.target.id) {
      let tempProduct = product
      tempProduct[e.target.id][e.target.name] =
        !tempProduct[e.target.id][e.target.name]
      setProduct({ ...product, [e.target.id]: tempProduct[e.target.id] })
    }
  }

  // For right boxes => SIMPLE choice (design, type and texture)
  const handleClick3 = async (e, string) => {
    if (e.target.name && e.target.id) {
      if (e.target.name === product[e.target.id]?.name) {
        setProduct({ ...product, [e.target.id]: null })
      } else {
        setProduct({ ...product, [e.target.id]: string })
      }
    }
  }

  return (
    <Switch>
      <Route
        exact
        path={`${props.match.path}`}
        render={(props) => (
          <AnrProduct
            {...props}
            product={product}
            design={design}
            type={type}
            texture={texture}
            tags={tags}
            handleChange={(e) => handleChange(e)}
            handleClick={(e) => handleClick(e)}
            handleClick2={(e) => handleClick2(e)}
            handleClick3={(e, string) => handleClick3(e, string)}
            handleChange_select={(e) => handleChange_select(e)}
            selectedCare={selectedCare}
            setProduct={(e, tempItem) => {
              tempItem !== -1
                ? setProduct({ ...product, [e.target.name]: tags[tempItem] })
                : setProduct({ ...product, [e.target.name]: e.target.value })
            }}
          />
        )}
      />
      <Route
        exact
        path={`${props.match.path}/newcolour`}
        render={(props) => (
          <AnrColour
            {...props}
            product={product}
            setProduct={(variant, active) => {
              let tempProduct = product
              tempProduct.variants = [variant]
              tempProduct.active = active ? true : false
              setProduct(tempProduct)
            }}
          />
        )}
      />
    </Switch>
  )
}
