/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import tick from '../pictures/tick.svg'
import ReactModal from 'react-modal'
import axios from 'axios'
import { URL } from '../../../config'

export default function ANR_product(props) {
  // Radio buttons designs
  const lil_circle =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/5/55/Small-dark-grey-circle.svg/1024px-Small-dark-grey-circle.svg.png',
    selected_circle =
      'https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2Fd%2Fd6%2FLACMTA_Circle_Blue_Line.svg%2F1024px-LACMTA_Circle_Blue_Line.svg.png&f=1&nofb=1'

  // URL Edit
  const [editable, setEditable] = useState(false)

  // For looping through adminTag and keyword
  const loopMe = [1, 2, 3, 4]

  // For right boxes
  const right_boxes_content = [props.design, props.type, props.texture]
  const sections = ['design', 'type', 'texture']

  // Message settings
  const [message, setMessage] = useState(false)
  const [color, setColor] = useState('')

  // Modal Open
  const [openModal, setOpenModal] = useState(false)

  const [loading, setLoading] = useState(true)

  useEffect(() => {
    if (props.product?.variants && props.product?.variants[0]) {
      setLoading(false)
    }
  }, [props])

  // For tags
  const handleChange = (e) => {
    const tempItem = props.tags.findIndex((tag) => {
      if (tag.name === e.target.value) {
        return true
      }
      return false
    })
    props.setProduct(e, tempItem)
  }

  // For saving changes to localStorage and Redirect to newColour
  const saveChanges = async (e) => {
    e.preventDefault()
    if (props.product.name === '') {
      setColor('yellow')
      setMessage('Please, write name for new range.')
      setTimeout(() => {
        setColor('')
        setMessage('')
      }, 2000)
      return false
    }
    try {
      await axios.post(`${URL}/ranges/update`, { product: props.product })
      props.history.push(`${props.match.url}/newcolour`)
    } catch (err) {
      console.error(err)
    }
  }

  // Delete Range ==> Re-do (to localStorage)
  const cancelRange = async () => {
    setOpenModal(false)
    try {
      await axios.post(`${URL}/ranges/delete`, { product: props.product })
    } catch (err) {
      console.error(err)
    }
    props.history.push('/adminlogin/products')
  }

  if (loading) {
    return (
      <div className='page'>
        <h3>Creating new range or retrieving existing draft...</h3>
      </div>
    )
  } else {
    return (
      <div className='page'>
        <div className='ep_titleCont'>
          <div className='ep_titleCont_left'>
            <h1>Add New Range</h1>
          </div>
          <div className='ep_titleCont_right'>
            <p>Permalink: https://madaboutfabrics.com/</p>
            <input
              className={`ep_titleCont_url ep_input ${
                editable && 'ep_editable_url'
              }`}
              name='url'
              value={props.product.url || ''}
              onChange={(e) => props.handleChange(e)}
              readOnly={editable ? false : true}
            />
            <div
              className='ep_editBtn pointer flexCenter'
              onClick={() => setEditable(!editable)}
            >
              <p>{editable ? 'Save' : 'Edit'}</p>
            </div>
          </div>
        </div>
        <div className='ep_grid2'>
          <div className='ep_grid2_left'>
            <div className='inputCont'>
              <p>Range Name*</p>
              <input
                className='input'
                value={props.product.name}
                name='name'
                onChange={(e) => props.handleChange(e)}
              />
            </div>
            <div className='inputCont'>
              <p>Supplier (hidden)</p>
              <input
                className='input'
                value={props.product.supplier || ''}
                name='supplier'
                onChange={(e) => props.handleChange(e)}
              />
            </div>
            <div className='inputCont'>
              <p>Description</p>
              <textarea
                value={props.product.description || ''}
                name='description'
                onChange={(e) => props.handleChange(e)}
              />
            </div>
            <div className='inputCont'>
              <p>Regular Price (£)*</p>
              <input
                type='number'
                className='input'
                value={props.product.price}
                name='price'
                min='0'
                step='.01'
                onChange={(e) => props.handleChange(e)}
              />
            </div>
            <div className='inputCont2'>
              <p>Fabric Care</p>
              <select
                value={props.selectedCare}
                className='select'
                onChange={(e) => props.handleChange_select(e)}
              >
                {Object.keys(props.product.fabricCare).map((care, idx) => {
                  return (
                    <option value={care} key={idx}>
                      {care}
                    </option>
                  )
                })}
              </select>
              <p></p>
              <p></p>
              <p>Fabric Repeat</p>
              <input
                className='input'
                type='number'
                name='fabricRepeat'
                min='0'
                value={props.product.fabricRepeat}
                onChange={(e) => props.handleChange(e)}
                step='any'
              />
              <p style={{ marginLeft: '20px' }}>Fabric Width</p>
              <input
                className='input'
                style={{ marginLeft: '20px' }}
                type='number'
                min='0'
                name='fabricWidth'
                value={props.product.fabricWidth || 0}
                onChange={(e) => props.handleChange(e)}
              />
            </div>
            <div className='inputCont'>
              <p>Fabric Composition</p>
              <input
                className='input'
                value={props.product.composition || ''}
                name='composition'
                onChange={(e) => props.handleChange(e)}
              />
            </div>
            <div className='ep_grid4'>
              <div className='checkboxCont flexCenter'>
                <p>Offer Sample</p>
                <div
                  className='checkbox checkbox_with_border flexCenter'
                  id='sample'
                  onClick={(e) => props.handleClick(e)}
                >
                  <img
                    src={tick}
                    className={`ps_tick ${!props.product.sample && 'nope'}`}
                    alt='tick'
                    id='sample'
                  />
                </div>
              </div>
              <div className='checkboxCont flexCenter'>
                <p>Be Inspired</p>
                <div
                  className='checkbox checkbox_with_border flexCenter'
                  id='inspired'
                  onClick={(e) => props.handleClick(e)}
                >
                  <img
                    src={tick}
                    className={`ps_tick ${!props.product.inspired && 'nope'}`}
                    alt='tick'
                    id='inspired'
                  />
                </div>
              </div>
              <div className='checkboxCont flexCenter'>
                <p>This is a PVC Tablecloth</p>
                <div
                  className='checkbox checkbox_with_border flexCenter'
                  id='pvc'
                  onClick={(e) => props.handleClick(e)}
                >
                  <img
                    src={tick}
                    className={`ps_tick ${!props.product.pvc && 'nope'}`}
                    alt='tick'
                    id='pvc'
                  />
                </div>
              </div>
              <div className='checkboxCont flexCenter'>
                <p>In Accessoires</p>
                <div
                  className='checkbox checkbox_with_border flexCenter'
                  id='accessoires'
                  onClick={(e) => props.handleClick(e)}
                >
                  <img
                    id='accessoires'
                    src={tick}
                    className={`ps_tick ${
                      !props.product.accessoires && 'nope'
                    }`}
                    alt='tick'
                  />
                </div>
              </div>
            </div>
            <datalist id='tagsList'>
              {Object.keys(props.tags).map((tag) => {
                let values = Object.values(props.tags)
                return <option key={tag}>{values[tag].name}</option>
              })}
            </datalist>
            <div className='ep_L_tagsCont'>
              {loopMe.map((item) => {
                const keyword = 'keyword0' + item
                return (
                  <div className='inputCont' key={item}>
                    <p>Visible Term</p>
                    <input
                      autoComplete='off'
                      className='input'
                      value={
                        props.product[keyword]?.name ||
                        props.product[keyword] ||
                        ''
                      }
                      name={keyword}
                      list='tagsList'
                      onChange={handleChange}
                    />
                    <button className='none'></button>
                  </div>
                )
              })}
              {loopMe.map((item) => {
                const adminTag = 'adminTag0' + item
                return (
                  <div className='inputCont' key={item}>
                    <p>Invisible Term</p>
                    <input
                      autoComplete='off'
                      className='input'
                      value={
                        props.product[adminTag]?.name ||
                        props.product[adminTag] ||
                        ''
                      }
                      name={adminTag}
                      list='tagsList'
                      onChange={handleChange}
                    />
                  </div>
                )
              })}
            </div>
          </div>

          <div className='ep_grid2_right'>
            <div className='flexColumn ep_right_btnCont'>
              <button
                className='ep_right_btn pointer'
                onClick={(e) => saveChanges(e)}
              >
                Continue to colour
              </button>
              <button
                className='ep_right_btn pointer'
                onClick={() => setOpenModal(true)}
              >
                Cancel Range
              </button>
              <p
                className={`ep_message ${color} ${
                  message ? 'ep_message' : 'ep_nope'
                }`}
              >
                {message}
              </p>
            </div>
            <div className='ep_right_box'>
              <p>Usage</p>
              <hr className='ep_hr'></hr>
              <div className='ep_right_box_content'>
                {props.product.usage &&
                  Object.values(props.product.usage).map((usage, i) => {
                    let keys = Object.keys(props.product.usage)
                    return (
                      <div
                        className='ep_range_section pointer'
                        key={i}
                        onClick={(e) => props.handleClick2(e)}
                        id='usage'
                        name={keys[i]}
                      >
                        <img
                          src={usage ? selected_circle : lil_circle}
                          className={`justify_self_center ${
                            usage ? 'ep_blue_circle' : 'ep_lil_circle'
                          }`}
                          alt='circle'
                          id='usage'
                          name={keys[i]}
                        />
                        <p>
                          {keys[i].charAt(0).toUpperCase() + keys[i].slice(1)}
                        </p>
                      </div>
                    )
                  })}
              </div>
            </div>
            {right_boxes_content.map((section, i) => {
              return (
                <div className='ep_right_box' key={i}>
                  <p>
                    {sections[i].charAt(0).toUpperCase() + sections[i].slice(1)}
                  </p>
                  <hr className='ep_hr'></hr>
                  <div className='ep_right_box_content'>
                    {section.length > 0 ? (
                      section.map((string, j) => {
                        return (
                          <div
                            className='ep_range_section'
                            onClick={(e) => props.handleClick3(e, string)}
                            id={sections[i]}
                            name={string.name}
                            key={i + j}
                          >
                            <img
                              src={
                                props.product[sections[i]]?.name === string.name
                                  ? selected_circle
                                  : lil_circle
                              }
                              alt='circle'
                              className='ep_lil_circle justify_self_center pointer'
                              id={sections[i]}
                              name={string.name}
                            />
                            <p>{string.name}</p>
                          </div>
                        )
                      })
                    ) : (
                      <p>No terms yet!</p>
                    )}
                  </div>
                </div>
              )
            })}
            <div className='ep_right_box'>
              <p>Industry</p>
              <hr className='ep_hr'></hr>
              <div className='ep_right_box_content'>
                {props.product.industry &&
                  Object.values(props.product.industry).map((industry, i) => {
                    let keys = Object.keys(props.product.industry)
                    return (
                      <div
                        className='ep_range_section pointer'
                        key={i}
                        onClick={(e) => props.handleClick2(e)}
                        id='industry'
                        name={keys[i]}
                      >
                        <img
                          src={industry ? selected_circle : lil_circle}
                          className={`justify_self_center ${
                            industry ? 'ep_blue_circle' : 'ep_lil_circle'
                          }`}
                          alt='circle'
                          id='industry'
                          name={keys[i]}
                        />
                        <p>
                          {keys[i].charAt(0).toUpperCase() + keys[i].slice(1)}
                        </p>
                      </div>
                    )
                  })}
              </div>
            </div>
          </div>
        </div>
        <ReactModal
          ariaHideApp={false}
          isOpen={openModal}
          className='modal'
          style={{
            overlay: {
              backgroundColor: '#2626266d',
            },
          }}
        >
          <div className='dm_modalQuestionCont'>
            <div className='flexCenter dm_modalQuestion'>
              <p className='inline'>
                Do you want to delete range <i>{props.product.name}</i>?
              </p>
            </div>
            <div className='flexCenter'>
              <button
                className='raleway dm_modalBtn dm_modalBtn1 pointer'
                onClick={() => {
                  setOpenModal(false)
                }}
              >
                No
              </button>

              <button
                className='raleway dm_modalBtn dm_modalBtn2 pointer'
                onClick={() => {
                  cancelRange()
                }}
              >
                Yes
              </button>
            </div>
          </div>
        </ReactModal>
      </div>
    )
  }
}
