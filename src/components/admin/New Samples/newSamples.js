/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, useEffect } from 'react'
import Header from './header'
import NewSamplesTable from './newSamplesTable'
import { Route } from 'react-router-dom'
import axios from 'axios'
import { URL } from '../../../config'

export default function AllSamples(props) {
  const [newSamples, setNewSamples] = useState(null)
  const [loaded, setLoaded] = useState(true)
  const [update, setUpdate] = useState(false)

  useEffect(() => {
    const getSamples = async () => {
      try {
        const res = await axios.get(`${URL}/samples/getnew`)
        setNewSamples(res.data.allSamples)
      } catch (err) {
        console.error(err)
      } finally {
        !loaded && setLoaded(true)
      }
    }
    getSamples()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props, update])

  return (
    <React.Fragment>
      <Route
        exact
        path={`${props.match.path}`}
        render={() => (
          <div className='page'>
            <Header />
            <NewSamplesTable
              {...props}
              newSamples={newSamples}
              loaded={loaded}
              update={() => setUpdate(!update)}
              setLoaded={(what) => setLoaded(what)}
            />
          </div>
        )}
      />
    </React.Fragment>
  )
}
