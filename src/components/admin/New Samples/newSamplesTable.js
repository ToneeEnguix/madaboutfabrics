/** @jsx jsx */
import { jsx } from '@emotion/react'
import axios from 'axios'
import { useState, useEffect } from 'react'

import { URL } from '../../../config'
import { getTime } from '../../public/helpers/getTime'

import loading from '../../../resources/loading.gif'

const header = {
  boxShadow: 'none',
  p: {
    marginRight: '1rem',
    ':hover': {
      color: '#2680EB',
      cursor: 'pointer',
    },
  },
}

const newSamplesWrapper = {
  display: 'flex',
  flexWrap: 'wrap',
}

const newSampleDetails = {
  margin: '2rem 2rem 0 0',
  backgroundColor: '#262626',
  padding: '2rem 1rem 2rem 2rem',
  width: '100%',
  maxWidth: '350px',
}

export default function NewSamplesTable(props) {
  const [newSamples, setNewSamples] = useState(null)
  const [filter, setFilter] = useState('all')

  useEffect(() => {
    setNewSamples(props.newSamples)
  }, [props])

  const changeStatus = async (sample_id) => {
    try {
      await axios.put(`${URL}/samples/changestatus/${sample_id}`)
      props.update()
    } catch (err) {
      console.error(err)
    }
  }

  useEffect(() => {
    const filterNewSamples = () => {
      if (filter === 'non-completed') {
        let tempSamples = props.newSamples.filter(
          (sample) => sample.status === false
        )
        setNewSamples(tempSamples)
      } else if (filter === 'all') {
        setNewSamples(props.newSamples)
      } else if (filter === 'last') {
        let tempSamples = props.newSamples.filter(
          (sample) =>
            Date.now() - new Date(sample.orderDate).getTime() <= 86400000
        )
        setNewSamples(tempSamples)
      }
    }
    filterNewSamples()
  }, [filter, props])

  const purge = async () => {
    props.setLoaded(false)
    try {
      await axios.put(`${URL}/samples/purge`)
      props.update()
    } catch (err) {
      console.error(err)
    }
  }

  if (!props.loaded) {
    return (
      <div className='loadingStyle'>
        <img alt='loading' src={loading} />
      </div>
    )
  } else {
    return (
      <div>
        <div className='flex' css={header}>
          <p
            style={{ color: filter === 'all' && '#2680EB' }}
            onClick={() => setFilter('all')}
          >
            View All New
          </p>
          <p
            style={{ color: filter === 'last' && '#2680EB' }}
            onClick={() => setFilter('last')}
          >
            View Last 24h
          </p>
          <p
            style={{ color: filter === 'non-completed' && '#2680EB' }}
            onClick={() => setFilter('non-completed')}
          >
            View Incomplete
          </p>
          <p onClick={() => purge()}>Purge List</p>
        </div>
        <div css={newSamplesWrapper}>
          {newSamples?.map((newSample) => {
            return (
              <div css={newSampleDetails} key={newSample._id}>
                <div className='flexBetween'>
                  <h2>
                    Sample #
                    {newSample.index.toString().length === 1
                      ? '00'
                      : newSample.index.toString().length === 2 && '0'}
                    {newSample.index + 1}
                  </h2>
                  {newSample.status ? (
                    <div
                      onClick={() => {
                        changeStatus(newSample._id)
                      }}
                    >
                      <p className='complete pointer black'>Completed</p>
                    </div>
                  ) : (
                    <div
                      onClick={() => {
                        changeStatus(newSample._id)
                      }}
                    >
                      <p className='incomplete pointer'>Incomplete</p>
                    </div>
                  )}
                </div>
                <h2
                  className={`sample_details_name ${
                    newSample.status ? 'this_green' : 'this_red'
                  }`}
                >
                  {newSample.user.name}
                </h2>
                <p>Request made - {getTime(newSample.orderDate)}</p>
                <div className='sample_details_table'>
                  <div className='sample_details_section'>
                    <p>
                      {newSample.shippingMethod === 'Ship'
                        ? 'Shipping'
                        : 'Collect'}
                      :
                    </p>
                    <p>{newSample.shipmentAddress.name}</p>
                    <p>{newSample.shipmentAddress.direction}</p>
                    <p>{newSample.shipmentAddress.town}</p>
                    <p>{newSample.shipmentAddress.region}</p>
                    <p>{newSample.shipmentAddress.postalCode}</p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Billing:</p>
                    <p>N/A</p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Email address</p>
                    <p
                      className={`${
                        newSample.status ? 'this_green' : 'this_red'
                      }`}
                    >
                      {newSample.user.email}
                    </p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Phone:</p>
                    <p
                      className={`${
                        newSample.complete ? 'this_green' : 'this_red'
                      }`}
                    >
                      {newSample.user.phone || '-'}
                    </p>
                  </div>
                  <div className='sample_details_section'>
                    <p>Samples:</p>
                    {newSample.requestedSamples.map((requestedSample, i) => {
                      return (
                        <p key={requestedSample._id}>
                          {i +
                            1 +
                            '. ' +
                            requestedSample.range.name +
                            ' | ' +
                            requestedSample.range.variants[
                              requestedSample.variantIndex
                            ].color}
                        </p>
                      )
                    })}
                  </div>
                  <div className='sample_details_section'>
                    <p>SKU:</p>
                    {newSample.requestedSamples.map((requestedSample) => {
                      return (
                        <p key={requestedSample._id}>
                          {
                            requestedSample.range.variants[
                              requestedSample.variantIndex
                            ].sku
                          }
                        </p>
                      )
                    })}
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    )
  }
}
