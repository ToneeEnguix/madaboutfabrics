/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { v4 as uuid } from 'uuid'
import { ToastContainer, toast } from 'react-toastify'

import { getTime } from '../../public/helpers/getTime'

import arrow from '../pictures/arrow.svg'
import { URL } from '../../../config'

export default function OrderDetails(props) {
  const [order, setOrder] = useState(null)

  const order_id = props.location.pathname.slice(-24)

  // get order info from backend
  useEffect(() => {
    const getOrderInfo = async () => {
      try {
        const res = await axios.get(`${URL}/orders/get_order_info/${order_id}`)
        let tempOrder = { ...res.data.order }
        tempOrder.productsBought.forEach((item) => (item.id = uuid()))
        setOrder(res.data.order)
      } catch (err) {
        console.error(err)
      }
    }
    getOrderInfo()
  }, [order_id])

  // change status in the DB
  const changeStatus = async () => {
    try {
      await axios.put(`${URL}/orders/changestatus/${order._id}`)
      setOrder({ ...order, completed: !order.completed })
      toast('Order status updated successfully')
    } catch (err) {
      toast.error(err)
      console.error(err)
    }
  }

  if (order) {
    return (
      <div className='sampleDetails'>
        <Link to={props.match.path} className='flex'>
          <img alt='arrow back' src={arrow} className='arrow1' />
          <p className='sampleDetails_back'>Back</p>
        </Link>
        <div className='sample_details_full_content'>
          <div className='flexBetween'>
            <h2>
              Order #
              {order.orderNumber.toString().length === 1
                ? '00'
                : order.orderNumber.toString().length === 2 && '0'}
              {order.orderNumber + 1}
            </h2>
            <div onClick={() => changeStatus()}>
              {order.completed ? (
                <p className='complete pointer black'>Complete</p>
              ) : (
                <p className='incomplete pointer'>Mark as complete</p>
              )}
            </div>
          </div>
          <h2
            className={`sample_details_name ${
              order.completed ? 'this_green' : 'this_red'
            }`}
          >
            {order.user.name}
          </h2>
          <p>Order made - {getTime(order.orderDate)}</p>
          <div className='sample_details_table'>
            <div className='sample_details_section'>
              <p>{order.shippingMethod === 'Ship' ? 'Shipping' : 'Collect'}:</p>
              <p>{order.shipmentAddress.name}</p>
              <p>{order.shipmentAddress.direction}</p>
              <p>{order.shipmentAddress.town}</p>
              <p>{order.shipmentAddress.region.name}</p>
              <p>{order.shipmentAddress.postalCode}</p>
            </div>
            <div className='sample_details_section'>
              <p>Billing:</p>
              <p>N/A</p>
            </div>
            <div className='sample_details_section'>
              <p>Email address</p>
              <p className={`${order.completed ? 'this_green' : 'this_red'}`}>
                {order.user.email}
              </p>
            </div>
            <div className='sample_details_section'>
              <p>Phone:</p>
              <p className={`${order.completed ? 'this_green' : 'this_red'}`}>
                {order.user.phone || '-'}
              </p>
            </div>
            <div className='sample_details_section'>
              <p>Orders:</p>
              {order.productsBought.map((requestedSample, i) => {
                return (
                  <p key={requestedSample.id}>
                    {i +
                      1 +
                      '. ' +
                      requestedSample.range.name +
                      ' | ' +
                      requestedSample.range.variants[
                        requestedSample.variantIndex
                      ].color}
                  </p>
                )
              })}
            </div>
            <div className='sample_details_section'>
              <p>SKU:</p>
              {order.productsBought.map((requestedSample, i) => {
                return (
                  <p key={requestedSample.id}>
                    {
                      requestedSample.range.variants[
                        requestedSample.variantIndex
                      ].sku
                    }
                  </p>
                )
              })}
            </div>
          </div>
        </div>
        <ToastContainer autoClose={2000} />
      </div>
    )
  } else {
    return <div>loading</div>
  }
}
