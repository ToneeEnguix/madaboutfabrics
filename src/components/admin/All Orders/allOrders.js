/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Route } from 'react-router-dom'

import Header from './header'
import OrdersTable from './ordersTable'
import OrderDetails from './orderDetails'

import { URL } from '../../../config'

export default function AllOrders(props) {
  const [count, setCount] = useState(0)
  const [countIndex, setCountIndex] = useState(0)
  const [orders, setOrders] = useState([])
  const [loaded, setLoaded] = useState(false)
  const [search, setSearch] = useState('')

  useEffect(() => {
    const getOrders = async () => {
      setLoaded(false)
      try {
        const res = await axios.get(`${URL}/orders/get_some/${countIndex}`)
        setOrders(res.data.orders)
      } catch (err) {
        console.error(err)
      }
      setLoaded(true)
    }
    getOrders()
  }, [props, countIndex])

  // get documents count
  useEffect(() => {
    const getOrdersCount = async () => {
      try {
        const res = await axios.get(`${URL}/orders/count`)
        setCount(res.data.count)
      } catch (err) {
        console.error(err)
      }
    }
    getOrdersCount()
  }, [])

  const searchOrders = async () => {
    setLoaded(false)
    try {
      const res = await axios.get(
        `${URL}/orders/search/${search.toLowerCase()}`
      )
      let tempOrders = [...res.data.orders]
      setOrders(tempOrders)
      setCount(res.data.orders.length)
    } catch (err) {
      console.error(2, err)
    } finally {
      setTimeout(() => setLoaded(true), 1000)
    }
  }

  return (
    <React.Fragment>
      <Route
        exact
        path={`${props.match.path}`}
        render={() => (
          <div className='page'>
            <Header
              count={count}
              countIndex={countIndex}
              setCountIndex={setCountIndex}
              search={search}
              setSearch={setSearch}
              searchOrders={searchOrders}
            />
            <OrdersTable {...props} orders={orders} loaded={loaded} />
          </div>
        )}
      />
      <Route
        path='/adminlogin/allorders/:orderid'
        render={() => (
          <div className='page'>
            <OrderDetails {...props} order={orders && orders[0]} />
          </div>
        )}
      />
    </React.Fragment>
  )
}
