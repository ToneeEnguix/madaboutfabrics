/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import ReactLoading from 'react-loading'

export default function ordersTable(props) {
  return (
    <div className='ordersTable'>
      <div className='ordersTable_grid'>
        <p>Customer</p>
        <p>Spent</p>
        <p>Date of Order</p>
        <p>Order ID</p>
        <p>In-store Pick-up</p>
        <p>Status</p>
      </div>
      {!props.loaded ? (
        <ReactLoading type={'spin'} color='#030303' />
      ) : props.orders?.length > 0 ? (
        props.orders.map((order, i) => {
          return (
            <Link
              to={`${props.match.path}/${order._id}`}
              className='ordersTable_grid light'
              key={i}
            >
              <p>{order.user.name}</p>
              <p>
                £
                {order.shipmentCost +
                  order.productsBought
                    .reduce((acc, val) => acc + val.range.price * val.amount, 0)
                    .toFixed(2)}
              </p>
              <p>
                {new Date(order.orderDate).toLocaleDateString('en-us', {
                  weekday: 'long',
                  year: 'numeric',
                  month: 'short',
                  day: 'numeric',
                })}
              </p>
              <p>#{order._id}</p>
              <p>{order.shippingMethod === 'Collect' ? 'Yes' : 'No'}</p>
              <p>{order.completed ? 'Complete' : 'Incomplete'}</p>
            </Link>
          )
        })
      ) : (
        <div className='no_samples'>No orders yet!</div>
      )}
    </div>
  )
}
