/** @jsx jsx */
import { jsx } from '@emotion/react'
import arrow from '../pictures/arrow.svg'

export default function header({
  count,
  countIndex,
  setCountIndex,
  search,
  setSearch,
  searchOrders,
}) {
  const decreasePage = () => {
    if (countIndex !== 0) setCountIndex(countIndex - 1)
  }

  const increasePage = () => {
    if (countIndex + 1 < Math.ceil(count / 20)) setCountIndex(countIndex + 1)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    searchOrders()
  }

  return (
    <div style={{ paddingBottom: '2.5rem' }} className='flexBetween'>
      <h1 className='inline'>All Orders</h1>
      <div className='flexBetween header_items'>
        <form
          onSubmit={handleSubmit}
          style={{ marginRight: '10%' }}
          className='flexBetween'
        >
          <input
            style={{ marginRight: '1rem' }}
            className='input'
            onChange={(e) => setSearch(e.target.value)}
            value={search}
          />
          <button className='search_btn'>Search</button>
        </form>
        <p>{count} items</p>
        <div className='flexCenter'>
          <img
            src={arrow}
            alt='arrow'
            className='arrowL'
            onClick={decreasePage}
          />
          <p>
            {countIndex + 1} of {Math.ceil(count / 20) || 1}
          </p>
          <img
            src={arrow}
            alt='arrow'
            className='arrowR'
            onClick={increasePage}
          />
        </div>
      </div>
    </div>
  )
}
