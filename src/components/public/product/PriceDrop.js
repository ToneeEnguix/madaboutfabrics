/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import Countdown from 'react-countdown'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 950, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const priceDrop = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr', '1fr', '1fr', '1fr 1.5fr'],
  backgroundColor: 'white',
  boxShadow: ['none', '0px 3px 6px 0px rgba(235,235,235,1)'],
  borderRadius: '8px',
  margin: [0, '0 2rem 1.2rem', '0 0 1.2rem 3rem'],
  padding: ['1.5rem 0', '2rem 0'],
  fontFamily: 'Montserrat',
  div: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    textTransform: 'uppercase',
  },
  '.countdown': {
    fontSize: ['2rem', '2rem', '2rem', '3rem'],
    fontWeight: '800',
    letterSpacing: '0.25em',
    textShadow: '0px 1px #00000',
  },
})

const leftSide = mq({
  h2: {
    fontSize: '1.4rem',
    fontWeight: '600',
  },
  h3: {
    fontSize: ['2rem', '2rem', '2rem', '3rem'],
  },
})

export default function PriceDrop(props) {
  return (
    <div css={priceDrop}>
      <div css={leftSide}>
        <h2>Time limited offer</h2>
        <h3>{props.discount}% off</h3>
      </div>
      <div className='countdown'>
        <Countdown date={props.limitDate} />
      </div>
    </div>
  )
}
