/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function MobileProductInfo({
  addMetre,
  substractMetre,
  amount,
  usage,
  addToCart,
  inStock,
  sample,
  addToSampleCart,
}) {
  return (
    <div css={main}>
      <div css={quantityStyle}>
        <span className='a'>Choose Quantity </span>
        <span className='b'>| {'  '} Metres</span>
      </div>
      <div className='buttonWrapper flexCenter'>
        <div
          onClick={() => substractMetre()}
          className='circles flexCenter symbols pointer'
        >
          -
        </div>
        <div
          onClick={() => addMetre()}
          className='circles flexCenter symbols pointer'
        >
          +
        </div>
        <div className='circles flexCenter number default'>
          {amount.length > 1 ? '' : '0' + amount}
        </div>
      </div>
      <div className='suitable'>
        <div className='flex'>
          {usage.curtains ? (
            <i className='material-icons green'>done</i>
          ) : (
            <i className='material-icons red'>clear</i>
          )}
          <span>Suitable for curtains</span>
        </div>
        <div className='flex'>
          {usage.upholstery ? (
            <i className='material-icons green'>done</i>
          ) : (
            <i className='material-icons red'>clear</i>
          )}
          <span>Suitable for upholstery</span>
        </div>
        <div className='flex'>
          {usage.craft ? (
            <i className='material-icons green'>done</i>
          ) : (
            <i className='material-icons red'>clear</i>
          )}
          <span>Suitable for craft</span>
        </div>
      </div>
      {inStock ? (
        <div onClick={() => addToCart()} className='buy_now uppercase pointer'>
          Add To Bag
        </div>
      ) : inStock === false ? (
        <div className='out'>OUT OF STOCK</div>
      ) : null}
      {sample && (
        <div
          className='order_sample uppercase pointer'
          onClick={addToSampleCart}
        >
          ORDER SAMPLE
        </div>
      )}
      <div className='pickup flex'>
        <i className='material-icons'>assignment_turned_in</i>
        <span>Available for store pickup</span>
      </div>
    </div>
  )
}

const main = mq({
  display: ['block', 'block', 'none'],
  '.buttonWrapper': {
    '.circles': {
      backgroundColor: 'white',
      borderRadius: '100px',
      height: '80px',
      width: '80px',
      margin: '0 10px',
      // UNSELECTABLE ELEMENT
      userSelect: 'none',
      MozUserSelect: 'none',
      KhtmlUserSelect: 'none',
      WebkitUserSelect: 'none',
      OUserSelect: 'none',
    },
    '.symbols': {
      fontWeight: 700,
      fontSize: '3rem',
      boxShadow: '0px 3px 6px #00000029',
    },
    '.number': {
      fontSize: '1.8rem',
      fontWeight: 500,
    },
  },
  '.suitable': {
    marginTop: '3rem',
    '.flex': {
      marginTop: '.5rem',
      alignItems: 'center',
      i: { marginRight: '.5rem', fontSize: '1.5rem' },
    },
  },
  '.buy_now': {
    marginTop: '2rem',
    backgroundColor: 'black',
    color: 'white',
    padding: '.75rem 0',
    textAlign: 'center',
    borderRadius: '6px',
    fontWeight: 500,
    letterSpacing: '1px',
    transition: 'all 200ms linear',
    ':hover': {
      backgroundColor: 'white',
      color: 'black',
      boxShadow: '0px 3px 6px #00000029',
    },
  },
  '.order_sample': {
    color: 'black',
    backgroundColor: 'white',
    padding: '.75rem 0',
    textAlign: 'center',
    borderRadius: '6px',
    fontWeight: 500,
    letterSpacing: '1px',
    transition: 'all 200ms linear',
    boxShadow: '0px 3px 6px #00000029',
    ':hover': {
      color: 'white',
      backgroundColor: 'black',
      boxShadow: '0px 3px 6px #00000029',
    },
    marginTop: '1rem',
  },
  '.pickup': {
    marginTop: '2rem',
    alignItems: 'center',
    color: 'green',
    i: { marginRight: '.5rem' },
  },
})

const quantityStyle = {
  fontFamily: 'Roboto, sans-serif',
  paddingLeft: '0.7rem',
  margin: '2rem 0 1.5rem',
  '.a': {
    fontWeight: 500,
  },
  '.b': {
    fontWeight: 300,
  },
}
