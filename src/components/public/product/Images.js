/** @jsx jsx */
import { jsx } from '@emotion/react'
import { URL } from '../../../config'
import facepaint from 'facepaint'

import ReactLoading from 'react-loading'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const loadingStyle = {
  backgroundColor: 'white',
}

const allImages = mq({
  display: ['none', 'flex'],
  boxSizing: 'border-box',
  flexWrap: 'wrap',
  margin: [0, '0 0 0 3rem'],
  justifyContent: 'space-between',
  div: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    height: '480px',
    boxSizing: 'border-box',
    margin: '0 0 20px',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '10px',
  },
  img: {
    flexShrink: '0',
    minWidth: '100%',
    minHeight: '100%',
  },
})

const singleImageCont = mq({
  minWidth: '100%',
})

const doubleImageCont = mq({
  width: ['100%', '100%', '49%'],
})

export default function Images(props) {
  if (!props.loaded) {
    return (
      <div className='flexCenter' css={loadingStyle}>
        <ReactLoading type={'spin'} color='#030303' />
      </div>
    )
  } else if (props.variant?.imageURLs?.length === 1) {
    return (
      <div css={allImages}>
        <div css={singleImageCont}>
          <img
            loading='lazy'
            alt='product'
            src={`${URL}/assets/${props.variant.imageURLs[0].filename}`}
          />
        </div>
      </div>
    )
  } else if (props.variant?.imageURLs?.length === 2) {
    return (
      <div css={allImages}>
        {props.variant.imageURLs.map((image, i) => (
          <div css={doubleImageCont} key={i}>
            <img
              loading='lazy'
              alt='product'
              src={`${URL}/assets/${image.filename}`}
            />
          </div>
        ))}
      </div>
    )
  } else if (props.variant?.imageURLs?.length === 3) {
    return (
      <div css={allImages}>
        {props.variant.imageURLs.map((image, i) => {
          if (i !== 2) {
            return (
              <div css={doubleImageCont} key={i}>
                <img
                  loading='lazy'
                  alt='product'
                  src={`${URL}/assets/${image.filename}`}
                />
              </div>
            )
          } else {
            return (
              <div css={singleImageCont} key={i}>
                <img
                  loading='lazy'
                  alt='product'
                  src={`${URL}/assets/${image.filename}`}
                />
              </div>
            )
          }
        })}
      </div>
    )
  } else if (props.variant?.imageURLs?.length === 4) {
    return (
      <div css={allImages}>
        {props.variant.imageURLs.map((image, i) => (
          <div css={doubleImageCont} key={i}>
            <img
              loading='lazy'
              alt='product'
              src={`${URL}/assets/${image.filename}`}
            />
          </div>
        ))}
      </div>
    )
  } else {
    return null
  }
}
