/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import facepaint from 'facepaint'

import { URL } from '../../../config'

import CarouselWithScrollBarBig from '../home/carouselWithScrollBar/CarouselWithScrollBarBig'
import CarouselMobile from '../home/carouselWithScrollBar/CarouselMobile'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const suggestionsWrapper = {
  width: '100%',
  paddingBottom: '10rem',
}

export default function Suggestions({ texture, realColor }) {
  const [colorSuggestions, setColorSuggestions] = useState(null)
  const [textureSuggestions, setTextureSuggestions] = useState(null)

  useEffect(() => {
    const getColorSuggestions = async () => {
      try {
        const res = await axios.get(
          `${URL}/ranges/suggestions/color/${realColor._id || realColor}`
        )
        res.data.data && setColorSuggestions(res.data.data)
      } catch (err) {
        console.error(err)
      }
    }
    !colorSuggestions && getColorSuggestions()
  }, [colorSuggestions, realColor])

  useEffect(() => {
    const getTextureSuggestions = async () => {
      try {
        const res = await axios.get(
          `${URL}/ranges/suggestions/texture/${texture._id || texture}`
        )
        res.data.data && setTextureSuggestions(res.data.data)
      } catch (err) {
        console.error(err)
      }
    }
    !textureSuggestions && getTextureSuggestions()
  }, [textureSuggestions, texture])

  return (
    <div css={suggestionsWrapper}>
      {colorSuggestions?.length > 0 && (
        <div>
          <h2 className='title'>Similar in color</h2>
          <div css={desktop}>
            <CarouselWithScrollBarBig items={colorSuggestions} />
          </div>
          <div css={mobile}>
            <CarouselMobile items={colorSuggestions} />
          </div>
        </div>
      )}
      {textureSuggestions?.length > 0 && (
        <div style={{ marginTop: '2rem' }}>
          <h2 className='title'>Similar in pattern</h2>
          <div css={desktop}>
            <CarouselWithScrollBarBig items={textureSuggestions} />
          </div>
          <div css={mobile}>
            <CarouselMobile items={textureSuggestions} />
          </div>
        </div>
      )}
    </div>
  )
}

const desktop = mq({
  display: ['none', 'block'],
})

const mobile = mq({
  display: ['block', 'none'],
})
