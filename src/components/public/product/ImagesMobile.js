/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { isMobile } from 'react-device-detect'
import HorizontalScroll from 'react-scroll-horizontal'

import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function ImagesMobile({ product, loaded }) {
  return (
    <div css={main}>
      {loaded &&
        product?.realColor &&
        (isMobile ? (
          <div className='categories_slider'>
            {product.imageURLs.map((img, i) => {
              return (
                <div
                  key={i}
                  css={itemStyle}
                  style={{
                    marginLeft: i === 0 ? '32px' : '8px',
                    marginRight: i === product.imageURLs.length - 1 && 0,
                  }}
                >
                  <img alt='product' src={`${URL}/assets/${img.filename}`} />
                </div>
              )
            })}
          </div>
        ) : (
          <HorizontalScroll reverseScroll={true}>
            {product.imageURLs.map((img, i) => {
              return (
                <div
                  key={i}
                  css={itemStyle}
                  style={{
                    marginLeft: i === 0 ? '32px' : '8px',
                    marginRight: i === product.imageURLs.length - 1 && 0,
                  }}
                >
                  <img alt='product' src={`${URL}/assets/${img.filename}`} />
                </div>
              )
            })}
          </HorizontalScroll>
        ))}
    </div>
  )
}

const main = mq({
  display: ['block', 'block', 'none'],
  width: '100%',
  marginTop: '1rem',
  height: 'calc(100vw - 64px)',
})

const itemStyle = {
  display: 'block',
  margin: '0 8px',
  width: 'calc(100vw - 64px)',
  minWidth: 'calc(100vw - 64px)',
  height: 'calc(100vw - 64px)',
  fontFamily: 'Montserrat, sans-serif',
  textDecoration: 'none',
  color: 'inherit',
  img: {
    width: '100%',
    height: 'calc(100vw - 64px)',
    marginBottom: '1rem',
    borderRadius: '10px',
  },
  '.upper': {
    fontSize: '1.5rem',
    marginBottom: '.8rem',
  },
}
