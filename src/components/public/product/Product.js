/** @jsx jsx */
import { jsx, keyframes } from '@emotion/react'
import axios from 'axios'
import facepaint from 'facepaint'
import React, { useState, useContext, useEffect, useCallback } from 'react'
import ReactModal from 'react-modal'
import { Link } from 'react-router-dom'

import UserContext from '../../../contexts/userContext'
import { URL } from '../../../config'

import PriceDrop from './PriceDrop'
import TagCarousel from '../home/TagCarousel'
import Suggestions from './Suggestions.js'
import MobileProductInfo from './MobileProductInfo.js'

import CustomAlert from '../generalPurpose/modals/CustomAlert'
import Copied from '../generalPurpose/modals/Copied'

import Images from './Images'
import ImagesMobile from './ImagesMobile'
import arrow from '../../admin/pictures/arrow.svg'
import close from '../../../resources/closeBlack.svg'
import WipeCleanOnly from '../../../resources/fabric_care_icons/wipecleanonly.svg'
import DryCleanOnly from '../../../resources/fabric_care_icons/drycleanonly.svg'
import HandWash from '../../../resources/fabric_care_icons/handwash30.svg'
import MachineWashable30º from '../../../resources/fabric_care_icons/machinewash30.svg'
import NonDetergentWash from '../../../resources/fabric_care_icons/nodetergent.svg'
import DoNotIron from '../../../resources/fabric_care_icons/donotiron.svg'
import machineWash from '../../../resources/fabric_care_icons/machinewash.svg'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const mainContent = mq({
  display: ['flex', 'flex', 'grid'],
  gridTemplateColumns: ['50% 50%', '50% 50%', '50% 50%', '63% 37%'],
  maxWidth: '100vw',
  justifyContent: 'center',
  marginTop: '1rem',
  flexDirection: 'column',
  boxSizing: 'border-box',
  padding: '0 0 5rem 0',
  '.desktop': {
    display: ['none', 'none', 'block'],
  },
  '.mobile': {
    display: ['block', 'block', 'none'],
  },
})

const leftColumn = mq({
  display: 'flex',
  flexDirection: 'column',
  wrap: 'flex-wrap',
  padding: 0,
  width: '100%',
  boxSizing: 'border-box',
})

const goBackStyle = mq({
  fontFamily: 'Montserrat',
  fontWeight: '500',
  margin: ['0 0 0 1.7rem', '0 0 1rem 1.7rem', '0 0 1rem 2.7rem'],
  cursor: 'pointer',
  letterSpacing: '0.2',
  display: 'flex',
  alignItems: 'center',
  width: 'fit-content',
  img: {
    marginRight: '.5rem',
  },
  transition: 'all linear 150ms',
  ':hover': {
    filter:
      'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(100%) contrast(97%)',
  },
})

const rightColumn = mq({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: [0, 0, '.6rem 1rem 5rem 0'],
  width: '100%',
})

const productDescription = mq({
  width: '90%',
  padding: ['1rem 0 0 0', '1rem 0 0 0', '1rem', '2rem 0 3rem 1.6rem'],
  boxShadow: [0, 0, '0px 3px 6px #00000029'],
  borderRadius: '8px',
  fontFamily: 'Montserrat, sans-serif',
})

const firstBox = {
  marginTop: '1.2rem',
  marginBottom: '1rem',
}

const titleAndPrice = {
  margin: '0 2.3rem .6rem 12px',
  letterSpacing: '0.56px',
  fontFamily: 'Montserrat, sans-serif',
  '.this_div': {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: '0.2rem',
    'h3, span': {
      fontSize: '1.3rem',
      fontWeight: '300',
    },
  },
  h1: {
    textTransform: 'uppercase',
    fontSize: '2rem',
    fontWeight: '600',
  },
}

const flex = {
  span: {
    fontWeight: ['200 !important', '200 !important', '300 !important'],
    fontSize: '1.1rem !important',
  },
}

const oldPrice = {
  textDecoration: 'line-through',
  fontFamily: 'Montserrat,sans-serif',
  color: '#CECECE',
  fontSize: '1.2rem',
  fontWeight: '100',
  marginRight: '1rem',
}

const colorTitle = {
  fontWeight: '500',
  fontSize: '0.85rem',
  padding: '0.2rem',
  fontFamily: 'Roboto',
}

const colorVariant = {
  fontFamily: 'Roboto',
  fontWeight: '300',
  fontSize: '0.8rem',
  padding: '0.2rem',
}

const colorContainer = {
  paddingRight: '1rem',
  display: 'flex',
  flexWrap: 'wrap',
  margin: '1rem 0 1.5rem',
  paddingLeft: '.7rem',
  a: {
    color: 'inherit',
    textDecoration: 'inherit',
  },
}

const variantWrapper = {
  margin: '0.5rem 1.2rem 0 0',
  display: 'flex-column',
  justifyContent: 'center',
  alignItems: 'center',
  maxWidth: '88px !important',
  textAlign: 'center',
  div: {
    marginTop: '7px',
  },
}

const variantImg = {
  margin: '0 !important',
  cursor: 'pointer',
  height: '80px',
  width: '80px',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  boxSizing: 'border-box',
  borderRadius: '15px',
  border: '2px solid white',
  ':hover': {
    border: '2px solid #cecece',
  },
}

const circleImageFitter = {
  flexShrink: '0',
  minWidth: '100%',
  minHeight: '100%',
  overflow: 'hidden',
  boxShadow: '0px 3px 6px 0px rgba(235,235,235,1)',
}

const textAlign = {
  textAlign: 'center',
  span: {
    fontFamily: 'Montserrat, sans-serif',
    marginTop: '1.3rem',
    fontSize: '0.85rem',
    fontWeight: '300',
  },
}

const addStyle = {
  marginTop: '1rem',
}

const cartInteractor = mq({
  margin: [0, '1rem 0 0', '1rem'],
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  span: {
    letterSpacing: '0.2',
    fontSize: '0.9rem',
  },
  'input::-webkit-outer-spin-button,input::-webkit-inner-spin-button': {
    appearance: 'none',
    margin: 0,
  },
  button: {
    backgroundColor: 'white',
    minWidth: ['85px', '75px', '75px', '85px'],
    minHeight: ['85px', '75px', '75px', '85px'],
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '100px',
    margin: '0 0.5rem',
    border: 'none',
    fontWeight: 'bold',
    cursor: 'pointer',
    transition: 'all linear 150ms',
    ':hover': {
      boxShadow: '2px 8px 15px #00001029',
    },
  },
  '*': { color: 'black' },
})

const amountInput = {
  width: '85px',
  height: '85px',
  border: '3px solid #FFFFFF',
  padding: '0.4rem 1rem',
  fontFamily: 'Montserrat,sans-serif',
  borderRadius: '12px',
  margin: '0 0.7rem',
  fontWeight: '800',
  fontSize: '1.5rem',
  textAlign: 'center',
  appearance: 'none',
  outline: 'none',
  backgroundColor: 'transparent',
  boxShadow: '0px 3px 6px #00000029, inset 0 0 4px #00000029',
}

const moreButton = {
  fontSize: '1.5rem',
  fontWeight: '100',
}

const buttonWrapper = {
  display: 'flex',
  flexDirection: 'column',
  marginTop: '3rem',
  '.add': {
    transition: 'all linear 150ms',
    backgroundColor: '#000',
    color: 'white',
    border: 'none',
    fontWeight: 'bold',
    fontFamily: 'Montserrat,sans-serif',
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    span: {
      margin: '-.1rem 0 0 .7rem',
    },
    ':hover': {
      boxShadow: '0px -0px 11px #000009',
    },
  },
  '.out': {
    backgroundColor: 'red',
    border: 'none',
    fontWeight: 'bold',
    fontFamily: 'Montserrat,sans-serif',
    color: 'white',
  },
  '.sample': {
    transition: 'all linear 150ms',
    color: '#00263E',
    marginTop: '1.2rem',
    border: 'none',
    fontWeight: 'bold',
    fontFamily: 'Montserrat,sans-serif',
    backgroundColor: 'white',
    cursor: 'pointer',
    ':hover': {
      boxShadow: '2px 8px 15px #00001029',
    },
  },
  button: {
    borderRadius: '100px',
    boxShadow: '0px 3px 6px #00000029',
    padding: '1.2rem 1.2rem',
    width: '100%',
  },
}

const wishlistShareIcons = {
  display: 'flex !important',
  alignItems: 'center',
  marginTop: '1.5rem',
  '.shareIcon': {
    width: '60px',
    height: '60px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '100px',
    cursor: 'pointer !important',
    transition: 'all linear 150ms',
    marginLeft: '1rem',
    ':hover': {
      color: '#FF8E8E !important',
    },
  },
}

const available = mq({
  display: ['none', 'none', 'flex !important'],
  alignItems: 'center',
  marginTop: '2.2rem',
  color: 'green',
  // color: '#0ec14c',
  span: {
    marginLeft: '0.8rem',
    fontSize: '0.9rem',
    fontFamily: 'Montserrat,sans-serif',
    fontWeight: '600',
    textTransform: 'uppercase',
  },
})

const pickupIcon = {
  fontSize: '1.7rem',
  paddingBottom: '.1rem',
  color: '#108B00',
}

const usageWrapper = mq({
  margin: '1.5rem 0 0',
  fontSize: '0.8rem',
  fontWeight: '300',
  display: ['none', 'none', 'flex !important'],
  flexWrap: 'wrap',
  i: {
    margin: '0 0.8rem 0 0',
    fontSize: '1.2rem',
  },
  p: {
    width: 'fit-content',
    borderRadius: '100px',
    padding: '0.7rem 1.2rem',
    textTransform: 'uppercase',
    border: '1px solid gray',
    fontFamily: 'Roboto, sans-serif',
    marginRight: '1rem',
  },
  '.green': {
    color: 'green',
  },
  '.red': {
    color: 'red',
  },
})

const usageProp = {
  display: 'flex',
  alignItems: 'center',
  padding: '0.1rem 0.5rem 0.1rem 0',
  marginBottom: '.7rem',
}

const icon = {
  padding: '0 0.3rem',
  '.pink': {
    color: '#FBA586',
  },
}

const valueStyle = {
  display: 'flex',
  alignItems: 'center',
  fontWeight: '500',
  fontSize: '0.9rem',
  lineHeight: '1.5rem',
  img: {
    height: '1.5rem',
    marginRight: '.5rem',
  },
}

export default function Product(props) {
  const [activeIndex, setActiveIndex] = useState(0)
  const [variant, setVariant] = useState({})
  const [loaded, setLoaded] = useState(false)

  const [amount, setAmount] = useState(1)
  const [showConfirm, setShowConfirm] = useState(false)

  const [openModal, setOpenModal] = useState(false)

  const contextType = useContext(UserContext)

  const [item, setItem] = useState({
    variants: [{ imageURLs: [{ filename: '' }] }],
    usage: {
      curtains: false,
    },
    price: 0,
  })

  // if coming from any section => get info from state. else from server
  useEffect(() => {
    props.match.params.variant_index
      ? setActiveIndex(props.match.params.variant_index)
      : setActiveIndex(props.location.state?.index)
    const getRange = async () => {
      const { id } = props.match.params
      try {
        const res = await axios.get(`${URL}/ranges/getrange/${id}`)
        if (res.data.ok) {
          const tempItem = removeNonExistingImgs(res.data.range)
          setItem(tempItem)
        }
      } catch (err) {
        console.error(err)
      }
    }
    const removeNonExistingImgs = (prod) => {
      let indexes = []
      let tempProd = { ...prod }
      tempProd.variants.forEach((vari, i) =>
        vari.imageURLs.forEach(
          (img, j) => !img.pathname && indexes.push([i, j])
        )
      )
      for (let x = indexes.length - 1; x > -1; x--)
        tempProd.variants[indexes[x][0]].imageURLs.splice(indexes[x][1], 1)
      return tempProd
    }
    // only call to backend if item is not coming from state
    if (props.location.state?.item) {
      let tempItem = removeNonExistingImgs(props.location.state.item)
      setItem(tempItem)
    } else getRange()
  }, [props])

  // THIS FIXED BOTH SUGGESTIONS AND CAROUSEL PROBLEM OMFG FY
  useEffect(() => {
    setLoaded(false)
  }, [props.location.pathname])

  const changeVariant = (idx) => {
    setLoaded(false)
    setActiveIndex(idx)
  }

  // separate variant from range
  useEffect(() => {
    setVariant(item.variants[activeIndex])
    setLoaded(true)
  }, [activeIndex, item])

  // scroll to top
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [activeIndex])

  const goBack = () => {
    props.history.goBack()
  }

  const substractMetre = () => {
    if (amount === '') {
      setAmount(1)
    } else if (amount !== 1) {
      setAmount(amount - 1)
    }
  }

  const addMetre = () => {
    if (!amount) {
      return setAmount(1)
    }
    setAmount(amount + 1)
  }

  const handleInputChange = (e) => {
    let toAdd = e.target.value
    if (e.target.value !== '') {
      toAdd = parseFloat(e.target.value)
    }
    setAmount(toAdd)
  }

  const addToCart = () => {
    contextType.addToCart(item, activeIndex, amount)
    showAlert()
  }

  const addToSampleCart = () => {
    contextType.addToSampleCart(item, activeIndex)
    showAlert()
  }

  const showAlert = () => {
    if (!showConfirm) {
      setShowConfirm(true)
      setTimeout(() => {
        setShowConfirm(false)
      }, 2000)
    }
  }

  return (
    <React.Fragment>
      <TagCarousel />
      <section css={mainContent}>
        <div css={leftColumn}>
          <div css={goBackStyle} onClick={() => goBack()}>
            <img alt='arrow' src={arrow} />
            <p>BACK</p>
          </div>
          {item.variants[activeIndex] &&
          item.variants[activeIndex]?.priceDrop &&
          new Date(item.variants[activeIndex]?.priceDropDate).getTime() >
            Date.now() &&
          item.variants[activeIndex]?.saleDiscount > 0 ? (
            <PriceDrop
              limitDate={item.variants[activeIndex]?.priceDropDate}
              discount={item.variants[activeIndex]?.saleDiscount}
            />
          ) : null}
          <Images loaded={loaded} variant={variant} />
          <ImagesMobile product={variant} loaded={loaded} />
        </div>
        <div css={rightColumn}>
          <div css={productDescription}>
            <div css={firstBox}>
              <div css={titleAndPrice}>
                <div className='this_div'>
                  <h3>{item.name}</h3>
                  {item.variants[activeIndex] && (
                    <div css={desktop}>
                      <PriceCalculator
                        permetre={false}
                        price={item.price}
                        priceDrop={item.variants[activeIndex]?.priceDrop}
                        priceDropDate={
                          item.variants[activeIndex]?.priceDropDate
                        }
                        saleDiscount={item.variants[activeIndex]?.saleDiscount}
                      />
                    </div>
                  )}
                </div>
                <h1>
                  {item.variants[activeIndex] &&
                    item.variants[activeIndex].color}
                </h1>
                <div css={mobile}>
                  <PriceCalculator
                    permetre={true}
                    price={item.price}
                    priceDrop={item.variants[activeIndex]?.priceDrop}
                    priceDropDate={item.variants[activeIndex]?.priceDropDate}
                    saleDiscount={item.variants[activeIndex]?.saleDiscount}
                  />
                </div>
              </div>
            </div>
            <div>
              <div css={colorContainer}>
                {item.variants.map((variant, i) => {
                  if (i !== Number(activeIndex)) {
                    return (
                      <Link
                        key={i}
                        css={variantWrapper}
                        to={{
                          pathname: item.url
                            ? `/productdetails/${item.url}/${i}`
                            : `/productdetails/${item._id}/${i}`,
                          state: {
                            item: item,
                            index: i,
                          },
                        }}
                      >
                        <div css={variantImg}>
                          <img
                            alt='color thumbnail'
                            css={circleImageFitter}
                            src={`${URL}/assets/${variant.imageURLs[0].filename}`}
                          />
                        </div>
                        <div css={textAlign}>
                          <span>{variant.color}</span>
                        </div>
                      </Link>
                    )
                  } else {
                    return (
                      <div
                        key={i}
                        css={variantWrapper}
                        onClick={() => {
                          changeVariant(i)
                        }}
                      >
                        <div
                          css={variantImg}
                          style={{ borderColor: '#FFA3C3' }}
                        >
                          <img
                            alt='color thumbnail'
                            css={circleImageFitter}
                            src={`${URL}/assets/${variant.imageURLs[0].filename}`}
                          />
                        </div>
                        <div css={textAlign}>
                          <span> {variant.color} </span>
                        </div>
                      </div>
                    )
                  }
                })}
              </div>
            </div>
            <MobileProductInfo
              addMetre={addMetre}
              substractMetre={substractMetre}
              amount={amount}
              usage={item.usage}
              addToCart={addToCart}
              addToSampleCart={addToSampleCart}
              inStock={item.inStock}
              sample={item.sample}
            />
            <div css={addStyle} className='desktop'>
              <span css={colorTitle}>Choose Quantity</span>
              <span css={colorVariant}>| {'  '} Metres</span>
              <div css={cartInteractor}>
                <button onClick={substractMetre}>—</button>
                <input
                  css={amountInput}
                  onChange={handleInputChange}
                  type='number'
                  value={amount}
                  name='amount'
                />
                <button css={moreButton} onClick={addMetre}>
                  +
                </button>
              </div>
            </div>
            <div css={buttonWrapper} className='desktop'>
              {item.inStock ? (
                <button className='add' onClick={addToCart}>
                  ADD TO SHOPPING BAG
                  <i className='material-icons' style={{ marginLeft: '.5rem' }}>
                    shopping_bag
                  </i>
                </button>
              ) : item.inStock === false ? (
                <button className='out'>OUT OF STOCK</button>
              ) : null}
              {item.sample && (
                <button className='sample' onClick={addToSampleCart}>
                  ORDER SAMPLE
                </button>
              )}
            </div>
            <div css={wishlistShareIcons} className='desktop'>
              <Wishlist
                id={item?._id}
                index={activeIndex}
                addToWishlist={() => {
                  contextType.addToWishlist(item, Number(activeIndex))
                  showAlert()
                }}
                removeFromWishlist={() =>
                  contextType.removeFromWishlist(item, activeIndex)
                }
                wishlist={contextType.user.wishlist}
              />
              <div className='shareIcon' onClick={() => setOpenModal(true)}>
                <i css={icon} className='material-icons'>
                  share
                </i>
              </div>
            </div>
            {item.variants[activeIndex] &&
            item.variants[activeIndex].availableForPickup ? (
              <Available />
            ) : null}
            <div css={usageWrapper} className='desktop'>
              <p css={usageProp}>
                {item.usage.curtains ? (
                  <i className='material-icons green'>done</i>
                ) : (
                  <i className='material-icons red'>clear</i>
                )}
                Curtains
              </p>
              <p css={usageProp}>
                {item.usage.upholstery ? (
                  <i className='material-icons green'>done</i>
                ) : (
                  <i className='material-icons red'>clear</i>
                )}{' '}
                Upholstery
              </p>
              <p css={usageProp}>
                {item.usage.craft ? (
                  <i className='material-icons green'>done</i>
                ) : (
                  <i className='material-icons red'>clear</i>
                )}{' '}
                Craft
              </p>
            </div>
            <div className='desktop'>
              {item.composition ? (
                <DropDown title={'Composition'} value={[item.composition]} />
              ) : null}
              <DropDown title={'Fabric Repeat'} value={[item.fabricRepeat]} />
              <DropDown title={'Fabric Width'} value={[item.fabricWidth]} />
              <DropDown title={'Fabric Care'} value={[item.fabricCare]} />
              {item.keyword01 ? (
                <KeywordHandler
                  kw01={item.keyword01}
                  kw02={item.keyword02}
                  kw03={item.keyword03}
                  kw04={item.keyword04}
                />
              ) : null}
            </div>
          </div>
        </div>
      </section>
      {loaded && variant?.realColor?.[0] && item.texture && (
        <Suggestions
          {...props}
          realColor={variant.realColor[0]}
          texture={item.texture}
        />
      )}
      {showConfirm && <CustomAlert color={'green'} content={'PRODUCT ADDED'} />}
      <Modal
        openModal={openModal}
        setOpenModal={(what) => setOpenModal(what)}
        {...props}
      />
    </React.Fragment>
  )
}

const Wishlist = (props) => {
  const heartIcon = {
    width: '60px',
    height: '60px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '100px',
    cursor: 'pointer !important',
    transition: 'all linear 150ms',
    ':hover': {
      color: '#FF8E8E !important',
    },
  }

  const [animation, setAnimation] = useState('none')
  const bounce = keyframes`
    from, 20%, 53%, 80%, to {
      transform: translate3d(0,0,0);
    }
  
    40%, 43% {
      transform: translate3d(0, -15px, 0);
    }
  
    70% {
      transform: translate3d(0, -7px, 0);
    }
  
    90% {
      transform: translate3d(0,-2px,0);
    }`

  const removeItem = () => {
    props.removeFromWishlist()
  }

  const addItem = () => {
    setAnimation(`${bounce} 1s ease 1`)
    props.addToWishlist()
  }

  const alreadyFav = props.wishlist?.some(
    (productFavorited) =>
      productFavorited.range?._id === props.id &&
      productFavorited.variantIndex === Number(props.index)
  )

  if (alreadyFav) {
    return (
      <div
        css={heartIcon}
        onClick={() => {
          removeItem()
        }}
      >
        <i
          style={{
            animation: animation,
            color: '#FBA586',
          }}
          className='material-icons'
        >
          favorite
        </i>
      </div>
    )
  } else {
    return (
      <i
        css={heartIcon}
        style={{
          color: 'black',
        }}
        onClick={(e) => {
          addItem()
        }}
        className='material-icons'
      >
        favorite_border
      </i>
    )
  }
}

const PriceCalculator = (props) => {
  if (
    props?.priceDrop === true &&
    new Date(props?.priceDropDate).getTime() > Date.now() &&
    props?.saleDiscount > 0
  ) {
    return (
      <div css={flex}>
        <span css={oldPrice}>£{props.price.toFixed(2)}</span>
        <span>
          £
          {(props.price - props.price * (props?.saleDiscount / 100)).toFixed(2)}{' '}
          {props.permetre ? 'per metre' : ''}
        </span>
      </div>
    )
  } else {
    return (
      <div css={flex}>
        <span>
          £ {props.price.toFixed(2)} {props.permetre ? 'per metre' : ''}
        </span>
      </div>
    )
  }
}

const Available = () => {
  return (
    <div css={available} className='desktop'>
      <i css={pickupIcon} className='material-icons'>
        assignment_turned_in
      </i>
      <span>Available for Store Pickup</span>
    </div>
  )
}

const DropDown = (props) => {
  const [height, setHeight] = useState('2.5rem')
  const [rotate, setRotate] = useState('rotate(0deg)')

  useEffect(() => {
    props.title !== 'Keywords' && calculateHeight()
    props.title !== 'Keywords' && expandDropDown()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const calculateHeight = () => {
    let height = props.value.length * 2 + 2
    setHeight(`${height}rem`)
    setRotate('rotate(90deg)')
  }

  const expandDropDown = () => {
    if (props.title === 'Keywords') {
      if (height !== '2.5rem') {
        setHeight('2.5rem')
        setRotate('rotate(0deg')
      } else {
        calculateHeight()
      }
    } else if (height === '2.5rem') {
      setHeight('4.5rem')
      setRotate('rotate(90deg)')
    } else {
      setHeight('2.5rem')
      setRotate('rotate(0deg)')
    }
  }

  const dropDown = {
    height: height,
    width: '100%',
    display: 'flex',
    padding: '0.5rem 1rem 0',
    alignItems: 'center',
    boxShadow: '0px 3px 6px #00000010',
    borderRadius: '8px',
    flexDirection: 'column',
    fontSize: '0.8rem',
    color: 'grey',
    fontWeight: '400',
    cursor: 'pointer',
    overflow: 'hidden',
    transition: 'height 0.5s',
    margin: '1rem 0',
    fontFamily: 'Montserrat,sans-serif',
    'span:': {
      margin: '0 0 0 1rem',
    },
    i: {
      color: 'black',
      fontSize: '1.2rem',
      transform: rotate,
      transition: 'transform 0.5s',
    },
  }

  return (
    <div
      onClick={() => {
        expandDropDown()
      }}
      css={dropDown}
    >
      <div
        css={{
          width: '100%',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          color: 'black',
          fontWeight: '300',
        }}
      >
        <span>{props.title}</span>
        <i className='material-icons'>keyboard_arrow_right</i>
      </div>
      <div
        css={{
          marginBottom: '1rem',
          color: 'black',
          width: '100%',
          display: 'flex',
          marginTop: '1rem',
          justifyContent: 'flex-start',
          flexDirection: 'column',
          alignItems: 'flex-start',
        }}
      >
        {props.title === 'Fabric Care' ? (
          <FabricCare
            values={props.value}
            calculateHeight={calculateHeight}
            height={height}
          />
        ) : (
          <span>
            {props.value.map((value, index) => {
              return (
                <span css={valueStyle} key={index}>
                  {value}
                  {props.title.includes('Fabric') && ' cm'}
                </span>
              )
            })}
          </span>
        )}
      </div>
    </div>
  )
}

const FabricCare = (props) => {
  const texts = [
    'Machine Washable',
    'Dry Clean',
    'Wipe Clean',
    'Hand Wash',
    'Thirt Clean',
    'Non Detergent',
    'Do Not Iron',
  ]
  const icons = [
    machineWash,
    DryCleanOnly,
    WipeCleanOnly,
    HandWash,
    MachineWashable30º,
    NonDetergentWash,
    DoNotIron,
  ]

  let mapping = props.values[Object.keys(props.values)[0]]

  if (typeof mapping === 'object') {
    return Object.keys(mapping).map((term, i) => {
      let values = Object.values(mapping)
      if (term && values[i]) {
        return (
          <div css={valueStyle} key={i}>
            <img src={icons[i]} alt='icon' />
            <span>{texts[i]}</span>
          </div>
        )
      } else return null
    })
  } else return null
}

const KeywordHandler = (props) => {
  const keywords = []

  if (props.kw01) keywords.push(props.kw01.name)
  if (props.kw02) keywords.push(props.kw02.name)
  if (props.kw03) keywords.push(props.kw03.name)
  if (props.kw04) keywords.push(props.kw04.name)

  return (
    <React.Fragment>
      <DropDown
        title={'Keywords'}
        style={{ fontSize: '1rem', textDecoration: 'underline' }}
        value={keywords}
      />
    </React.Fragment>
  )
}

const Modal = (props) => {
  const [showCopied, setShowCopied] = useState(false)

  const modalTop = {
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
    h3: { fontSize: '1.2rem', fontWeight: '400 !important' },
    img: {
      width: '30px',
      filter: 'invert(100%) grayscale(100%)',
    },
  }

  const textToCopy = mq({
    width: 'fit-content',
    alignSelf: 'center',
    display: 'flex',
    span: {
      padding: '1rem',
      border: '1px solid white',
    },
    '.url': {
      borderRadius: '10px 0 0 10px',
    },
    '.url1': {
      display: ['none', 'none', 'block'],
    },
    '.url2': {
      display: ['none', 'block', 'none'],
    },
    '.url3': {
      display: ['block', 'none'],
    },
    '.copy_text': {
      borderRadius: '0 10px 10px 0',
    },
  })

  const escFunction = useCallback(
    (event) => {
      if (event.keyCode === 27) {
        props.setOpenModal(false)
      }
    },
    [props]
  )

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)
    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  const copyText = () => {
    navigator.clipboard.writeText(window.location.href)
    showCopiedFunc()
  }

  const showCopiedFunc = () => {
    if (!showCopied) {
      setShowCopied(true)
      setTimeout(() => {
        setShowCopied(false)
      }, 1200)
    }
  }

  return (
    <ReactModal
      ariaHideApp={false}
      isOpen={props.openModal}
      style={{
        overlay: {
          backgroundColor: '#2626266d',
        },
        content: {
          position: 'relative',
          top: '50%',
          left: '50%',
          right: 'auto',
          bottom: 'auto',
          transform: 'translate(-50%, -50%)',
          backgroundColor: '#00263E',
          color: 'white',
          fontFamily: 'Montserrat, sans-serif',
          border: 'none',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-around',
          height: '180px',
          maxWidth: '550px',
          width: '95vw',
          boxShadow: '0px 3px 6px #00000029',
          borderRadius: '14px',
          textAlign: 'center',
        },
      }}
    >
      <div css={modalTop}>
        <h3>Paste this link anywhere you want to share!</h3>
        <img
          alt='close'
          src={close}
          onClick={() => props.setOpenModal(false)}
          className='pointer'
        />
      </div>
      <div css={textToCopy} className='pointer' onClick={copyText}>
        <span className='url url1'>
          {window.location.href.slice(0, 40) + '...'}
        </span>
        <span className='url url2'>
          {window.location.href.slice(0, 25) + '...'}
        </span>
        <span className='url url3'>
          {window.location.href.slice(0, 20) + '...'}
        </span>
        <span className='copy_text'>COPY</span>
        {showCopied && <Copied />}
      </div>
    </ReactModal>
  )
}

const desktop = mq({
  display: ['none', 'none', 'block'],
})

const mobile = mq({
  display: ['block', 'block', 'none'],
})
