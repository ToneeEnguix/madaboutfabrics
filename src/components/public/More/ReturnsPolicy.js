/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '70%'],
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.2rem',
    letterSpacing: 0,
    fontWeight: 600,
    marginBottom: '0.5em',
  },
  p: {
    fontWeight: 300,
    lineHeight: '1.3rem',
  },
  '.space': {
    marginBottom: '1rem',
  },
})

export default function ReturnsPolicy() {
  return (
    <div css={show}>
      <h1>Returns Policy</h1>
      <p className='space'>
        All our products are cut to your specific measurement and quality
        checked before dispatch.
      </p>
      <p className='space'>
        Upon receipt of your order, and before cutting into the fabric or
        altering it in any way, please ensure that you are totally satisfied
        with the quality, colour, measurement and design etc. We allow you 7
        days after receipt of your goods, for you to be entirely satisfied. If
        you do have a complaint please notify us within this period as no
        complaints will be entertained thereafter.
      </p>
      <p className='space'>
        Because your fabric has been cut to your specific measurements,
        unfortunately and with regret, no returns can be entertained unless, of
        course, the order is defective in any way.
      </p>
      <p className='space'>
        In the unlikely event that a fault is discovered by you, please email us
        immediately at admin@madaboutfabrics.com It is important that you
        include your email address along with your Order Reference and Payment
        Reference numbers , the description of the problem, your name, address
        and contact number. We will respond to you quickly and efficiently to
        arrange the return of your purchase for our examination.
      </p>
      <p className='space'>
        Should any fault on our part be established, a replacement or full
        refund will be offered. In this instance your postage cost will also be
        reimbursed.
      </p>
      <p className='space'>
        All return items should be sent in their original condition to:
      </p>
      <p>Mad About Fabrics Ltd</p>
      <p>16-18 Dargan Crescent</p>
      <p>Duncrue Industrial Estate</p>
      <p>Belfast</p>
      <p>BT3 9JP</p>
      <p className='space'>Co Antrim</p>
      <p className='space'>Telephone: 02890 370390</p>
      <p className='space'>Fax: 02890 778610</p>
      <p className='space'>
        We advise you to obtain proof of return from your Carrier or from the
        Post Office if using recorded delivery, as we cannot accept
        responsibility for items lost in transit.
      </p>
      <p>Force Majuere</p>
      <p className='space'>
        Neither party shall be liable for any default due to fire, flood,
        explosion, accident, adverse weather conditions, riot, governmental act,
        act of God, terrorism, war or from any industrial dispute whatsoever.
      </p>
    </div>
  )
}
