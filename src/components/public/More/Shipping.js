/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '70%'],
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.2rem',
    letterSpacing: 0,
    fontWeight: 600,
    marginBottom: '0.5em',
  },
  p: {
    fontWeight: 300,
    lineHeight: '1.3rem',
  },
  '.space': {
    marginBottom: '1rem',
  },
})

export default function Shipping() {
  return (
    <div css={show}>
      <h1>Shipping</h1>
      <p className='space'>Delivery Service</p>
      <p className='space'>
        Upon receipt of your order we will email confirmation immediately. All
        of the products on our site are available from our own extensive stocks
        (except Moon Wools) and you should receive your order within 7 working
        days.
      </p>
      <p className='space'>
        Occasionally, like every other company, we run out of stock. In this
        circumstance, we will advise you of the expected delivery date and of
        course, if this is not acceptable, we will offer you an immediate
        refund.
      </p>
      <p className='space'>
        If your package arrives damaged in any way, please sign the delivery
        note ‘PACKAGED DAMAGED’ otherwise we will have no redress against the
        courier.
      </p>
      <p className='space'>
        Please be aware that when you sign for goods you are usually stating
        that these goods have been delivered in perfect condition.
      </p>
      <p className='space'>
        The goods shall be delivered to your nominated address and remain at our
        risk until such delivery is made.
      </p>
      <p>Delivery Charges</p>
      <p className='space'>
        We use fast and cost effective methods to ensure efficient delivery of
        your order. In order to achieve the highest standard we have partnered
        with DPD Ireland delivery services. We constantly review our carriers
        and our delivery rates to ensure we can offer a great delivery service
        at a competitive price. All prices below include postage and packing.
      </p>
      <p className='space'>We no longer ship to the Channel Islands</p>
      <p>Destination</p>
      <p>Great Britain and Northern Ireland £10.00</p>
      <p>Republic of Ireland £7.50</p>
      <p>Scottish Highlands only £10.00</p>
      <p>
        UK Off Shore including Scottish Islands, Isle of Man, Isle of Wight,
        Isles of Scilly £30.00
      </p>
      <p>ZONE 1: Belgium, France, Germany, Luxembourg, Netherlands £25.00</p>
      <p>ZONE 2: Austria, Czech Republic, Denmark, Poland £25.00</p>
      <p>ZONE 3: Hungary, Lithuania, Slovakia, Slovenia £27.50</p>
      <p>ZONE 4: Estonia, Italy, Latvia, Spain, Sweden £27.50</p>
      <p>ZONE 5: Finland, Portugal, £30.00</p>
      <p>ZONE 6: (non-EU) Switzerland £40.00</p>
      <p>
        ZONE 7: (non-EU) Bosnia & Herzegovina, Croatia, Liechtenstein, Norway,
        Serbia & Montenegro £45.00
      </p>
      <p>
        Unfortunately we do not deliver outside these destinations at this
        present time.
      </p>
      <p>Export/Import Duties</p>
      <p className='space'>
        If your delivery address is outside the EU you may be subject to import
        duties and taxes, which are levied once a shipment reaches the
        destination country. Any such additional charges for customs clearance
        must be borne by the customer.
      </p>
      <p className='space'>
        The customer agrees to comply with all applicable laws and regulations
        of the country in which the goods are to be received.
      </p>
      <p>
        Please note that when shipping products internationally, cross-border
        shipments are subject to opening and inspection by customs authorities.
      </p>
    </div>
  )
}
