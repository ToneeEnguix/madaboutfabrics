/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: '100%',
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.2rem',
    letterSpacing: 0,
    fontWeight: 600,
    marginBottom: '0.5em',
  },
  p: {
    fontWeight: 300,
  },
  '.space': {
    marginBottom: '1rem',
  },
  '.grid': {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    width: 'fit-content',
  },
})

export default function Contact() {
  return (
    <div css={show}>
      <h1>Contact Us</h1>
      <p className='space'>A family run business since 1957</p>
      <p className='space'>Location</p>
      <p>Mad About Fabrics Limited</p>
      <p>16-18 Dargan Crescent</p>
      <p>Duncrue Industrial Estate</p>
      <p>Belfast</p>
      <p>BT3 9JP</p>
      <p className='space'>Co No. NI606630</p>
      <p className='space'>Email</p>
      <p className='space'>hello@madaboutfabrics.com</p>
      <p>Telephone</p>
      <p className='space'>02890 370390</p>
      <p className='space'>Opening Hours</p>
      <div className='space grid'>
        <div>
          <p>Monday</p>
          <p>Tuesday</p>
          <p>Wednesday</p>
          <p>Thursday</p>
          <p>Friday</p>
          <p>Saturday</p>
          <p>Sunday</p>
        </div>
        <div>
          <p>9.00 am - 4.45 pm</p>
          <p>9.00 am - 4.45 pm</p>
          <p>9.00 am - 4.45 pm</p>
          <p>9.00 am - 8.45 pm</p>
          <p>9.00 am - 4.45 pm</p>
          <p>9.00 am - 4.45 pm</p>
          <p>Closed</p>
        </div>
      </div>
      <p>Social Media</p>

      <p>Find us on the map</p>
    </div>
  )
}
