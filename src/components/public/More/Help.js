/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '70%'],
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.2rem',
    letterSpacing: 0,
    fontWeight: 600,
    marginBottom: '0.5em',
  },
  p: {
    fontWeight: 300,
    lineHeight: '1.3rem',
  },
  '.space': {
    marginBottom: '1rem',
  },
})

export default function Help() {
  return (
    <div css={show}>
      <h1>Help</h1>
      <p className='space'>
        If you require special assistance or help with anything at all, our
        operators are standing by. Please call:
      </p>
      <p>Call:</p>
      <p className='space'>02890 370390</p>
      <p>Optionally you can email us here:</p>
      <p>hello@madaboutfabrics.com</p>
    </div>
  )
}
