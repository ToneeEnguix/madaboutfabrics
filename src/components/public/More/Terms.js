/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '70%'],
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.2rem',
    letterSpacing: 0,
    fontWeight: 600,
    marginBottom: '0.5em',
  },
  p: {
    fontWeight: 300,
    lineHeight: '1.3rem',
  },
  '.space': {
    marginBottom: '1rem',
  },
})

export default function Terms() {
  return (
    <div css={show}>
      <h1>Terms</h1>
      <p>Mad About Fabrics Terms & Conditions</p>
      <p className='space'>Please read these Terms and Conditions carefully.</p>
      <p>
        This website is owned and operated by Mad About Fabrics Limted, a
        subsidiary of Talbot Textiles Upholstery & Supply Co Ltd.
        www.talbottextiles.co.uk of which Mad About Fabrics Ltd is a trading
        division.
      </p>
      <p className='space'>You can contact us by:</p>
      <p className='space'>Email: hello@madaboutfabrics.com</p>
      <p className='space'>Telephone: 02890 370390</p>
      <p className='space'>
        Shop – opening times – Mon, Tues, Wed, Fri, Sat 9am-4.45pm, Thurs
        9am-8.45pm, Closed Sunday
      </p>
      <p className='space'>madaboutfabrics Admin Office Mon-Thur 9am – 5pm</p>
      <p className='space'>Fax: 02890 778610</p>
      <p className='space'>Address:</p>
      <p>Mad About Fabrics Ltd.,</p>
      <p>16-18 Dargan Crescent,</p>
      <p>Duncrue Industrial Estate,</p>
      <p className='space'>Belfast, BT3 9JP</p>
      <p className='space'>www.madaboutfabrics.co.uk</p>
      <p className='space'>www.madaboutfabrics.com</p>
      <p className='space'>Customer Service</p>
      <p className='space'>
        Our aim is to provide a fast, friendly and efficient service for our
        customers. All enquiries are welcome so please feel free to contact us.
      </p>
      <p className='space'>Please feel free to contact us at any time.</p>
      <p>Product</p>
      <p className='space'>
        Natural fabrics such as silks, cottons and linens can sometimes have
        natural blemishes which should be regarded as features rather than
        faults.
      </p>
      <p className='space'>
        We reserve the right to amend, change or withdraw any product at any
        time without notice.
      </p>
      <p>VAT</p>
      <p className='space'>
        The current UK rate of 20% is included in our prices to all UK and EU
        destinations.
      </p>
      <p className='space'>
        Customers outside the EU enjoy 0% rated sales tax. A current VAT number
        must be provided.
      </p>
      <p className='space'>Our VAT Registration No: see Legal Provision</p>
      <p>General</p>
      <p className='space'>
        Once an order has been received it shall be deemed to be an offer by the
        customer to purchase fabrics according to these Terms and Conditions.
      </p>
      <p className='space'>
        A contract has been entered into once we have accepted your order and a
        confirmation email has been sent to you, according to these Terms and
        Conditions. We cannot accept responsibility for non-receipt of emails.
      </p>
      <p className='space'>
        We reserve the right to change prices and availability information
        without notice.
      </p>
      <p className='space'>
        Mad About Fabric Ltd Services mentioned on this website are either
        trademarks or registered trademarks of UK and the jurisdiction of
        Northern Ireland. Other product and company names mentioned on this
        website may be the trademarks or registered trademarks of their
        respective owners.
      </p>
      <p>Making a Purchase</p>
      <p className='space'>
        Please ensure that your measurements are correct at time of ordering.
      </p>
      <p className='space'>
        The customer is responsible for her/his own choice of fabric in terms of
        measurements, quality, colour, design and suitability. (see Returns and
        Refunds policy)
      </p>
      <p className='space'>Please take time to consider your requirements.</p>
      <p className='space'>
        We recommend that you order all fabric required at the same time as
        there may be slight colour variations between differently dated
        consignments.
      </p>
      <p>Credit Card Security</p>
      <p className='space'>
        We treat security of your information very seriously. For this reason we
        have chosen to use the e-payment system provided by Realex. When you
        choose to pay online you will be navigated directly into the secured
        Realex online payment system of encryption which processes your payment
        details on our behalf. Once you have completed the payment you will be
        immediately navigated back to our website page to confirm your order. We
        never receive your credit card details. We are simply provided with a
        confirmation of your payment. For maximum security all credit card
        payments are handled by Realex.
      </p>
      <p>Cancellations</p>
      <p className='space'>
        It is our aim to dispatch your order as quickly as possible.
        Consequently, your order will be cut to your specific measurement within
        a couple of hours and for these reasons we cannot accept a cancellation.
      </p>
      <p>Ownership and Liability</p>
      <p className='space'>
        The customer is responsible for the goods at delivery, which must be
        accepted and signed for at the stated delivery address.
      </p>
      <p className='space'>
        The customer is responsible for her/his own choice of fabric in terms of
        measurements, quality, colour, design and suitability. Please take time
        to consider your requirements and again, we strongly advise you to avail
        of our free sample service.
      </p>
      <p>Returns/Refunds Policy</p>
      <p className='space'>
        All our products are cut to your specific measurement and quality
        checked before dispatch.
      </p>
      <p className='space'>
        Upon receipt of your order, and before cutting into the fabric or
        altering it in any way, please ensure that you are totally satisfied
        with the quality, colour, measurement and design etc. We allow you 7
        days after receipt of your goods, for you to be entirely satisfied. If
        you do have a complaint please notify us within this period as no
        complaints will be entertained thereafter.
      </p>
      <p className='space'>
        Because your fabric has been cut to your specific measurements,
        unfortunately and with regret, no returns can be entertained unless, of
        course, the order is defective in any way.
      </p>
      <p className='space'>
        In the unlikely event that a fault is discovered by you, please email us
        immediately at admin@madaboutfabrics.com It is important that you
        include your email address along with your Order Reference and Payment
        Reference numbers , the description of the problem, your name, address
        and contact number. We will respond to you quickly and efficiently to
        arrange the return of your purchase for our examination.
      </p>
      <p className='space'>
        Should any fault on our part be established, a replacement or full
        refund will be offered. In this instance your postage cost will also be
        reimbursed.
      </p>
      <p className='space'>
        All return items should be sent in their original condition to:
      </p>
      <p>Mad About Fabrics Ltd</p>
      <p>16-18 Dargan Crescent</p>
      <p>Duncrue Industrial Estate</p>
      <p>Belfast</p>
      <p>BT3 9JP</p>
      <p className='space'>Co Antrim</p>
      <p className='space'>Telephone: 02890 370390</p>
      <p className='space'>Fax: 02890 778610</p>
      <p className='space'>
        We advise you to obtain proof of return from your Carrier or from the
        Post Office if using recorded delivery, as we cannot accept
        responsibility for items lost in transit.
      </p>
      <p>Force Majuere</p>
      <p className='space'>
        Neither party shall be liable for any default due to fire, flood,
        explosion, accident, adverse weather conditions, riot, governmental act,
        act of God, terrorism, war or from any industrial dispute whatsoever.
      </p>
      <p>Legal Provision</p>
      <p>
        Madaboutfabrics is a trading division of subsiduary of Talbot Textiles &
        Upholstery Supply Co Ltd.
      </p>
      <p className='space'>www.talbottextiles.co.uk</p>
      <p className='space'>VAT Registration No: 216 681 015</p>
      <p>
        The construction, validity and performance of this agreement is governed
        by the laws of the jurisdiction of Northern Ireland.
      </p>
    </div>
  )
}
