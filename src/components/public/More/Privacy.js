/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '70%'],
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.2rem',
    letterSpacing: 0,
    fontWeight: 600,
    marginBottom: '0.5em',
  },
  p: {
    fontWeight: 300,
    lineHeight: '1.3rem',
  },
  '.space': {
    marginBottom: '1rem',
  },
})

export default function Privacy() {
  return (
    <div css={show}>
      <h1>Privacy</h1>
      <p className='space'>
        IN ACCORDANCE WITH UK LAW AND GDPR RULES PERTAINING TO YOUR INFORMATION
      </p>
      <p className='space'>
        We do not share your information in accordance with the law governing
        the UK, and in accordance with the law of GDPR. This website Mad About
        Fabrics, hosted on a server registered with Namesco does not retain
        details of visitors through cookies, phishing software, tracking
        applications or any other firmware.
      </p>
      <p className='space'>
        We do not disseminate any information, nor harvest information about
        you; the visitor, in any format.
      </p>
      <p className='space'>
        IF YOU CHOOSE TO SUBMIT A FORM ON THE CONTACT US PAGE
      </p>
      <p className='space'>
        The only place where information about a visitor is recorded is if a
        website visitor chooses to send us a message using the form on the
        ‘contact us’ page. This is an optional submission whereby individuals
        may submit an inquiry. In order to respond, we ask that the individual
        submit their email and name but otherwise no information is retained by
        https://www.madaboutfabrics.com. In this instance, your first and last
        name are retained along with your email. Other than these two details no
        information is retained by us. WE DO NOT SHARE THIS INFORMATION
        KNOWINGLY TO ANY THIRD PARTY COMPANIES IN ANY FORM.
      </p>
      <p className='space'>
        IF YOU CHOOSE TO PURCHASE PRODUCTS FROM OUR E-COMMERCE STORE
      </p>
      <p className='space'>
        Our policy is in accordance with GDPR rules concerning the security of
        your personal details and the protection of your information. We do not
        harvest or disseminate your information to any third party company but
        rather retain your name, number, email, address details in order that
        your products are delivered to you via post. AT NO STAGE ARE WE PRIVY TO
        YOUR CREDIT CARD DETAILS as this is processed through Realex Payments
        Limited. The communication between your browser and our website uses a
        secure encrypted connection wherever your payment details are involved.
        It is our policy to purge all personal details of past customers every
        month in accordance with the law surrounding GDPR and personal
        protection.
      </p>
      <p>Thanks and enjoy our website.</p>
    </div>
  )
}
