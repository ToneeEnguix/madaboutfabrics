/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '70%'],
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.2rem',
    letterSpacing: 0,
    fontWeight: 600,
    marginBottom: '0.5em',
  },
  p: {
    fontWeight: 300,
    lineHeight: '1.3rem',
  },
  '.space': {
    marginBottom: '1rem',
  },
})

export default function SamplePolicy() {
  return (
    <div css={show}>
      <h1>Sample Policy</h1>
      <p className='space'>
        We provide a free sampling service, up to a maximum of 6 free samples.
        Customer satisfaction is our priority so, with your best interest in
        mind, we strongly advise you to avail of this free service before you
        purchase. This will give you an opportunity to examine the fabric for
        quality, colour and design.
      </p>
      <p className='space'>
        Whilst the greatest care and attention has been taken, some photos and
        differing computer images do not always fully represent the various
        textural characteristics and exact colours of our beautiful fabrics.
        Please understand that our photographs are for illustration only.
      </p>
      <p className='space'>How to request free samples:</p>
      <p>
        After requesting 1 sample, and before entering your details, go to the
        navigation bar situated immediately below the black madaboutfabrics
        header and click on your chosen category to continue browsing – repeat
        for up to 6 free samples. Lastly, enter your details and hit send.
      </p>
    </div>
  )
}
