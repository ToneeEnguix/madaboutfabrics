/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '70%'],
  flexDirection: 'column',
  color: 'black',
  margin: ['1rem 0 3rem', '0 0 0 3rem'],
  fontFamily: 'Roboto, sans-serif',
  h1: {
    fontSize: '1.1rem',
    letterSpacing: 0,
    fontWeight: '700',
    marginBottom: '0.5em',
  },
})

export default function CurtainMakers() {
  return (
    <div css={show}>
      <h1>List Of Curtain Makers</h1>
    </div>
  )
}
