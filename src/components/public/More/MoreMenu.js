/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { NavLink } from 'react-router-dom'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const style = mq({
  display: ['none', 'flex'],
  margin: ['0.5rem 0 2rem 2.5rem', '0.5rem 0 26rem'],
  paddingTop: '1rem',
  width: '15vw',
  color: 'black',
  borderTop: ['none', '1px solid lightGrey'],
  fontFamily: 'Roboto, sans-serif',
  fontWeight: '100',
  minWidth: '150px',
  nav: {
    width: '100%',
  },
  ul: {
    display: 'flex',
    flexDirection: 'column',
    listStyle: 'none',
    width: '100%',
  },
  li: {
    lineHeight: '1.3rem',
    width: '100%',
  },
  a: {
    padding: '0.5rem .9rem',
    display: 'block',
    color: 'inherit',
    textDecoration: 'none',
    width: '100%',
    fontSize: '0.95rem',
    whiteSpace: 'nowrap',
  },
  '.activeSection': {
    fontWeight: '400',
    backgroundColor: '#F9F9F9',
    width: '100%',
  },
})

export default function MoreMenu(props) {
  return (
    <div css={style}>
      <nav>
        <ul>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/`}
            >
              Terms
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/privacy`}
            >
              Privacy Policy
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/careers`}
            >
              Careers
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/about`}
            >
              About Us
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/shipping`}
            >
              Shipping
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/contact`}
            >
              Contact
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/help`}
            >
              Help
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/samplepolicy`}
            >
              Sample Policy
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/tracking`}
            >
              Track Order
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/returns`}
            >
              Returns Policy
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/upholsterers`}
            >
              List of Upholsterers
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/curtainmakers`}
            >
              List of Curtain Makers
            </NavLink>
          </li>
        </ul>
      </nav>
    </div>
  )
}
