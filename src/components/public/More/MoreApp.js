/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Route } from 'react-router-dom'
import facepaint from 'facepaint'

import MoreMenu from './MoreMenu.js'
import MobileMoreMenu from './MobileMoreMenu.js'
import Terms from './Terms.js'
import Privacy from './Privacy'
import Careers from './Careers'
import Upholsterers from './Upholsterers'
import CurtainMakers from './CurtainMakers'
import About from './About'
import Shipping from './Shipping'
import Contact from './Contact'
import Help from './Help'
import SamplePolicy from './SamplePolicy'
import TrackOrder from './TrackOrder'
import ReturnsPolicy from './ReturnsPolicy'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function MoreApp(props) {
  return (
    <div css={show}>
      <section css={dashboard}>
        <MoreMenu {...props} />
        <MobileMoreMenu {...props} />
        <div css={desktopOnly}>
          <Route exact path={`${props.match.path}/`} component={Terms} />
          <Route path={`${props.match.path}/privacy`} component={Privacy} />

          <Route path={`${props.match.path}/careers`} component={Careers} />
          <Route path={`${props.match.path}/about`} component={About} />
          <Route path={`${props.match.path}/shipping`} component={Shipping} />
          <Route path={`${props.match.path}/contact`} component={Contact} />
          <Route path={`${props.match.path}/help`} component={Help} />
          <Route
            path={`${props.match.path}/samplepolicy`}
            component={SamplePolicy}
          />
          <Route path={`${props.match.path}/tracking`} component={TrackOrder} />
          <Route
            path={`${props.match.path}/returns`}
            component={ReturnsPolicy}
          />
          <Route
            path={`${props.match.path}/upholsterers`}
            component={Upholsterers}
          />
          <Route
            path={`${props.match.path}/curtainmakers`}
            component={CurtainMakers}
          />
        </div>
      </section>
    </div>
  )
}

const show = {
  minWidth: '100vw',
}

const dashboard = mq({
  display: ['auto', 'flex'],
  width: '100%',
  color: 'black',
  padding: ['0 0 5rem 0', '1.5rem 0 5rem 2rem'],
})

const desktopOnly = mq({
  display: ['none', 'block'],
})
