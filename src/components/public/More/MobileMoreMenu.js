/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom'

import back from '../../admin/pictures/arrow.svg'
import About from './About'
import Careers from './Careers'
import Contact from './Contact'
import CurtainMakers from './CurtainMakers'
import Help from './Help'
import Privacy from './Privacy'
import ReturnsPolicy from './ReturnsPolicy'
import SamplePolicy from './SamplePolicy'
import Shipping from './Shipping'
import Terms from './Terms'
import TrackOrder from './TrackOrder'
import Upholsterers from './Upholsterers'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function MobileMoreMenu(props) {
  const [openTerms, setOpenTerms] = useState(false)
  const [openPrivacy, setOpenPrivacy] = useState(false)
  const [openCareers, setOpenCareers] = useState(false)
  const [openAbout, setOpenAbout] = useState(false)
  const [openShipping, setOpenShipping] = useState(false)
  const [openContact, setOpenContact] = useState(false)
  const [openHelp, setOpenHelp] = useState(false)
  const [openSamplePolicy, setOpenSamplePolicy] = useState(false)
  const [openTracking, setOpenTracking] = useState(false)
  const [openReturns, setOpenReturns] = useState(false)
  const [openUpholsterers, setOpenUpholsterers] = useState(false)
  const [openCurtainMakers, setOpenCurtainMakers] = useState(false)

  const [previousPage, setPreviousPage] = useState(null)

  useEffect(() => {
    const closeAll = (page) => {
      page !== '' && setOpenTerms(false)
      page !== 'privacy' && setOpenPrivacy(false)
      page !== 'careers' && setOpenCareers(false)
      page !== 'about' && setOpenAbout(false)
      page !== 'shipping' && setOpenShipping(false)
      page !== 'contact' && setOpenContact(false)
      page !== 'help' && setOpenHelp(false)
      page !== 'samplepolicy' && setOpenSamplePolicy(false)
      page !== 'tracking' && setOpenTracking(false)
      page !== 'returns' && setOpenReturns(false)
      page !== 'upholsterers' && setOpenUpholsterers(false)
      page !== 'curtainmakers' && setOpenCurtainMakers(false)
    }
    const slashIdx = props.location.pathname.slice(1).indexOf('/') + 2 // +2 bc 2 slashes are removed
    const page = props.location.pathname.slice(slashIdx)
    switch (page) {
      case '':
        closeAll('')
        !openTerms && previousPage !== '' && setOpenTerms(true)
        setPreviousPage('')
        break
      case 'privacy':
        closeAll('privacy')
        !openPrivacy && previousPage !== 'privacy' && setOpenPrivacy(true)
        setPreviousPage('privacy')
        break
      case 'careers':
        closeAll('careers')
        !openCareers && previousPage !== 'careers' && setOpenCareers(true)
        setPreviousPage('careers')
        break
      case 'about':
        closeAll('about')
        !openAbout && previousPage !== 'about' && setOpenAbout(true)
        setPreviousPage('about')
        break
      case 'shipping':
        closeAll('shipping')
        !openShipping && previousPage !== 'shipping' && setOpenShipping(true)
        setPreviousPage('shipping')
        break
      case 'contact':
        closeAll('contact')
        !openContact && previousPage !== 'contact' && setOpenContact(true)
        setPreviousPage('contact')
        break
      case 'help':
        closeAll('help')
        !openHelp && previousPage !== 'help' && setOpenHelp(true)
        setPreviousPage('help')
        break
      case 'samplepolicy':
        closeAll('samplepolicy')
        !openSamplePolicy &&
          previousPage !== 'samplepolicy' &&
          setOpenSamplePolicy(true)
        setPreviousPage('samplepolicy')
        break
      case 'tracking':
        closeAll('tracking')
        !openTracking && previousPage !== 'tracking' && setOpenTracking(true)
        setPreviousPage('tracking')
        break
      case 'returns':
        closeAll('returns')
        !openReturns && previousPage !== 'returns' && setOpenReturns(true)
        setPreviousPage('returns')
        break
      case 'upholsterers':
        closeAll('upholsterers')
        !openUpholsterers &&
          previousPage !== 'upholsterers' &&
          setOpenUpholsterers(true)
        setPreviousPage('upholsterers')
        break
      case 'curtainmakers':
        closeAll('curtainmakers')
        !openCurtainMakers &&
          previousPage !== 'curtainmakers' &&
          setOpenCurtainMakers(true)
        setPreviousPage('curtainmakers')
        break
      default:
        setOpenTerms(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  return (
    <div css={mainStyle}>
      <nav className='backStyle pointer' onClick={() => props.history.goBack()}>
        <img alt='back' src={back} />
        <span>Back</span>
      </nav>
      <nav className='desktop'>
        <ul>
          <li onClick={() => setOpenTerms(!openTerms)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openTerms && 500 }}>Terms</span>
                <img
                  style={{ transform: openTerms && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openTerms && <Terms />}
          </li>
          <li onClick={() => setOpenPrivacy(!openPrivacy)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/privacy`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openPrivacy && 500 }}>
                  Privacy Policy
                </span>
                <img
                  style={{ transform: openPrivacy && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openPrivacy && <Privacy />}
          </li>
          <li onClick={() => setOpenCareers(!openCareers)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/careers`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openCareers && 500 }}>Careers</span>
                <img
                  style={{ transform: openCareers && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openCareers && <Careers />}
          </li>
          <li onClick={() => setOpenAbout(!openAbout)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/about`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openAbout && 500 }}>About Us</span>
                <img
                  style={{ transform: openAbout && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openAbout && <About />}
          </li>
          <li onClick={() => setOpenShipping(!openShipping)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/shipping`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openShipping && 500 }}>
                  Shipping
                </span>
                <img
                  style={{ transform: openShipping && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openShipping && <Shipping />}
          </li>
          <li onClick={() => setOpenContact(!openContact)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/contact`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openContact && 500 }}>Contact</span>
                <img
                  style={{ transform: openContact && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openContact && <Contact />}
          </li>
          <li onClick={() => setOpenHelp(!openHelp)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/help`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openHelp && 500 }}>Help</span>
                <img
                  style={{ transform: openHelp && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openHelp && <Help />}
          </li>
          <li onClick={() => setOpenSamplePolicy(!openSamplePolicy)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/samplepolicy`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openSamplePolicy && 500 }}>
                  Sample Policy
                </span>
                <img
                  style={{ transform: openSamplePolicy && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openSamplePolicy && <SamplePolicy />}
          </li>
          <li onClick={() => setOpenTracking(!openTracking)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/tracking`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openTracking && 500 }}>
                  Track Order
                </span>
                <img
                  style={{ transform: openTracking && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openTracking && <TrackOrder />}
          </li>
          <li onClick={() => setOpenReturns(!openReturns)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/returns`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openReturns && 500 }}>
                  Returns Policy
                </span>
                <img
                  style={{ transform: openReturns && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openReturns && <ReturnsPolicy />}
          </li>
          <li onClick={() => setOpenUpholsterers(!openUpholsterers)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/upholsterers`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openUpholsterers && 500 }}>
                  List of Upholsterers
                </span>
                <img
                  style={{ transform: openUpholsterers && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openUpholsterers && <Upholsterers />}
          </li>
          <li onClick={() => setOpenCurtainMakers(!openCurtainMakers)}>
            <NavLink
              exact
              activeClassName='activeSection'
              to={`${props.match.path}/curtainmakers`}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openCurtainMakers && 500 }}>
                  List of Curtain Makers
                </span>
                <img
                  style={{ transform: openCurtainMakers && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openCurtainMakers && <CurtainMakers />}
          </li>
        </ul>
      </nav>
    </div>
  )
}

const mainStyle = mq({
  display: ['auto', 'none'],
  fontFamily: 'Roboto, sans-serif',
  fontWeight: 300,
  padding: '0 2rem',
  '.backStyle': {
    display: 'flex',
    alignItems: 'center',
    margin: '0 0 2rem -.5rem',
  },
  li: {
    listStyle: 'none',
  },
  a: {
    textDecoration: 'none',
    color: 'inherit',
    display: 'block',
    marginBottom: '1.5rem',
    h1: {
      display: 'none',
    },
  },
  '.dropdown': {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  '.arrow': {
    transition: 'all 200ms linear',
    transform: 'rotate(270deg)',
  },
})
