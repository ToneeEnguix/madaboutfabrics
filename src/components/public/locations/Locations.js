/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useEffect, useState } from 'react'

import UserContext from '../../../contexts/userContext'
import { regions } from './regions'

import { Link } from 'react-router-dom'
import close from '../../../resources/closeBlack.svg'
import location from '../../../resources/locationBlack.svg'
import { Redirect } from 'react-router-dom'

const submenuPosition = {
  position: 'absolute',
  top: '0',
  left: '0',
  zIndex: '100000000',
  width: '100vw',
  height: '100vh',
  padding: '4rem 2.7rem 1rem 6rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  color: 'black',
  backgroundColor: 'white',
  a: {
    color: 'black',
    textDecoration: 'none',
  },
}

const titles = {
  display: 'grid',
  gridTemplateColumns: '1fr 1fr 1fr',
  alignItems: 'end',
  color: 'black',
  fontWeight: 'bold',
  width: '100%',
  margin: '0 0 2rem',
  h1: {
    fontFamily: 'cooper-black-std, serif',
    fontSize: '1.7rem',
    cursor: 'pointer',
  },
  h2: {
    whiteSpace: 'nowrap',
    justifySelf: 'center',
    fontFamily: 'Roboto, sans-serif',
    fontSize: '1.4rem',
    fontWeight: '400',
    letterSpacing: '4px',
    paddingRight: '1rem',
  },
  img: {
    justifySelf: 'end',
    height: '25px',
    cursor: 'pointer',
  },
  'img:hover': {
    filter:
      'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(108%) contrast(97%)',
  },
}

const regionsStyle = {
  display: 'grid',
  gridTemplateRows: 'repeat(7, 1fr)',
  gridTemplateColumns: 'repeat(4,1fr)',
  gridAutoFlow: 'column',
  fontFamily: 'Roboto, sans-serif',
  fontWeight: '400',
  letterSpacing: '1.2px',
  width: '100%',
  div: {
    padding: '2rem 1rem 1rem 0',
    textTransform: 'uppercase',
    fontSize: '1rem',
    marginRight: '4rem',
    cursor: 'pointer',
  },
  'div:hover': {
    color: '#FBA586',
  },
}

const regionWrapper = {
  borderBottom: '1px solid lightgray',
  height: 'fit-content',
  'a:hover': {
    color: '#FBA586',
  },
}

const regionWrapperSelect = {
  borderBottom: '1px solid lightgray',
  color: '#FBA586 !important',
  a: {
    color: '#FBA586 !important',
  },
  display: 'flex',
  alignItems: 'center',
}

const regionWrapperEndColumn = {}

const regionWrapperEndColumnSelect = {
  color: '#FBA586',
}

const warningWrapper = {
  display: 'flex',
  marginTop: '5vh',
  alignSelf: 'flex-start',
  fontFamily: 'Roboto, sans-serif',
  letterSpacing: '1.4px',
}

const warning = {
  color: 'grey',
  fontSize: '0.9em',
  textSpacing: '0.4',
  marginRight: '1em',
}

const shopFreely = {
  textDecoration: 'underline',
  fontSize: '0.9em',
  textSpacing: '0.4',
  cursor: 'pointer',
}

const locationStyle = {
  margin: '0 1vw 0 -33px',
  width: '20px',
  alignSelf: 'center',
  filter:
    'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(108%) contrast(97%)',
}

export default function Locations(props) {
  const contextType = useContext(UserContext)

  const [w, setW] = useState(1000)

  useEffect(() => {
    function updateWidth() {
      setW(window.innerWidth)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
    return () => {
      window.removeEventListener('resize', updateWidth)
    }
  }, [])

  const handleRegionChange = async (click, toggleRegion) => {
    goBack()
    const newRegion = findNewRegion(click)

    toggleRegion(newRegion)
  }

  const findNewRegion = (click) => {
    const activeRegionId = click.target.id
    return regions.find((region) => region._id === activeRegionId)
  }

  const resetRegion = () => {
    contextType.toggleRegion({
      _id: 'NoRegion',
      name: 'Location',
      coin: 'GBP',
      shippingRate: 'N/A',
      situation: 'N/A',
    })
    goBack()
  }

  const goBack = () => {
    props.history.goBack()
  }

  if (w < 950) return <Redirect to='/home' />
  return (
    <div css={submenuPosition}>
      <div css={titles}>
        <Link to='/'>
          <h1>Mad About Fabrics</h1>
        </Link>
        <h2>SELECT YOUR REGION</h2>
        <img alt='cross' src={close} onClick={() => goBack()} />
      </div>
      <div css={regionsStyle}>
        {regions.map((region, index) => {
          if (
            (index + 1) % 7 === 0 &&
            region.name !== contextType.user.region?.name
          ) {
            return (
              <Region
                style={regionWrapperEndColumn}
                key={region._id}
                region={region}
                handleRegionChange={handleRegionChange}
                toggleRegion={contextType.toggleRegion}
              />
            )
          } else if (
            (index + 1) % 7 !== 0 &&
            region.name !== contextType.user.region?.name
          ) {
            return (
              <Region
                style={regionWrapper}
                key={region._id}
                region={region}
                handleRegionChange={handleRegionChange}
                toggleRegion={contextType.toggleRegion}
              />
            )
          } else if (
            (index + 1) % 7 !== 0 &&
            region.name === contextType.user.region?.name
          ) {
            return (
              <Region
                style={regionWrapperSelect}
                key={region._id}
                region={region}
                handleRegionChange={handleRegionChange}
                toggleRegion={contextType.toggleRegion}
              />
            )
          } else {
            return (
              <Region
                style={regionWrapperEndColumnSelect}
                key={region._id}
                region={region}
                handleRegionChange={handleRegionChange}
                toggleRegion={contextType.toggleRegion}
              />
            )
          }
        })}
        {contextType.user.region?.situation === 'Britain' ? (
          <div css={regionWrapperSelect}>
            <img alt='location' src={location} css={locationStyle} />
            <Link to='/uklocations'>UK</Link>
          </div>
        ) : (
          <div css={regionWrapper}>
            <Link to='/uklocations'>UK</Link>
          </div>
        )}
      </div>
      <div css={warningWrapper}>
        <p css={warning}>
          WE ARE CURRENTLY UNABLE TO SHIP TO REGIONS OUTSIDE THESE DESTINATIONS
        </p>
        <p css={shopFreely} onClick={resetRegion}>
          SHOP WITHOUT REGION
        </p>
      </div>
    </div>
  )
}

function Region(props) {
  return (
    <div
      onClick={(event) => {
        props.handleRegionChange(event, props.toggleRegion)
      }}
      id={props.region._id}
      css={props.style}
    >
      <img
        alt='location'
        src={location}
        css={locationStyle}
        style={{
          display:
            props.style !== regionWrapperSelect &&
            props.style !== regionWrapperEndColumnSelect
              ? 'none'
              : 'block',
        }}
      />
      {props.region.name}
    </div>
  )
}
