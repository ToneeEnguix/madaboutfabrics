/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useEffect, useState } from 'react'
import { Redirect, Link } from 'react-router-dom'

import UserContext from '../../../contexts/userContext'
import { ukregions } from './regions'

import close from '../../../resources/closeBlack.svg'
import back from '../../../components/admin/pictures/arrow.svg'
import location from '../../../resources/locationBlack.svg'

const submenuPosition = {
  position: 'absolute',
  top: '0',
  left: '0',
  zIndex: '100000000',
  width: '100vw',
  height: '100vh',
  padding: '3.95rem 2.7rem 1rem 6rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  color: 'black',
  backgroundColor: 'white',
  fontFamily: 'Roboto, sans-serif',
  a: {
    color: 'black',
    textDecoration: 'none',
  },
}

const titles = {
  display: 'grid',
  gridTemplateColumns: '1fr 2fr 1fr',
  alignItems: 'center',
  color: 'black',
  fontWeight: 'bold',
  width: '100%',
  margin: '0 0 2rem',
  fontSize: '0.6em',
  paddingBottom: '.3rem',
  h1: {
    justifySelf: 'center',
    paddingRight: '5rem',
    paddingBottom: '.3rem',
    fontSize: '1.4rem',
    letterSpacing: '4px',
  },
}

const backStyle = {
  textTransform: 'uppercase',
  letterSpacing: '2px',
  fontSize: '.6rem',
  cursor: 'pointer',
  ':hover': {
    img: {
      filter:
        'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(108%) contrast(97%)',
    },
    color: '#FBA586',
  },
  img: {
    height: '45px',
  },
}

const closeStyle = {
  justifySelf: 'end',
  cursor: 'pointer',
  ':hover': {
    filter:
      'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(108%) contrast(97%)',
  },
  img: {
    height: '25px',
  },
}

const regionsStyle = {
  display: 'grid',
  gridTemplateRows: 'repeat(5, 1fr)',
  gridTemplateColumns: 'repeat(2,1fr)',
  gridAutoFlow: 'column',
  marginTop: '2rem',
  div: {
    padding: '2rem 3rem 1rem 0',
    textTransform: 'uppercase',
    fontSize: '1rem',
    marginRight: '5rem',
    cursor: 'pointer',
    height: 'fit-content',
    letterSpacing: '1px',
  },
  'div:hover': {
    color: '#FBA586 !important',
  },
}

const regionWrapper = {
  borderBottom: '1px solid lightgray',
}

const locationStyle = {
  margin: '0 1vw 0 -33px',
  width: '20px',
  alignSelf: 'center',
  filter:
    'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(108%) contrast(97%)',
}

export default function UKlocations(props) {
  const contextType = useContext(UserContext)

  const [w, setW] = useState(1000)

  useEffect(() => {
    function updateWidth() {
      setW(window.innerWidth)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
    return () => {
      window.removeEventListener('resize', updateWidth)
    }
  }, [])

  const handleRegionChange = (click, toggleRegion) => {
    const newRegion = findNewRegion(click)
    toggleRegion(newRegion)
    props.history.push('/')
  }

  const findNewRegion = (click) => {
    const activeRegionId = click.target.id
    return ukregions.find((region) => region._id === activeRegionId)
  }

  const goBack = () => {
    props.history.goBack()
  }

  if (w < 950) return <Redirect to='/home' />
  return (
    <div css={submenuPosition}>
      <div css={titles}>
        <div
          css={backStyle}
          className='flexCenter'
          style={{ justifySelf: 'flex-start' }}
          onClick={() => goBack()}
        >
          <img alt='back' src={back} />
          <h2>Back</h2>
        </div>
        <h1>UNITED KINGDOM</h1>
        <Link css={closeStyle} to='/'>
          <img alt='close' src={close} />
        </Link>
      </div>
      <div css={regionsStyle}>
        {ukregions.map((region, index) => {
          if ((index + 1) % 9 === 0) {
            return (
              <Region
                key={region._id}
                region={region}
                handleRegionChange={handleRegionChange}
                toggleRegion={contextType.toggleRegion}
              />
            )
          } else {
            return (
              <Region
                style={regionWrapper}
                key={region._id}
                region={region}
                handleRegionChange={handleRegionChange}
                toggleRegion={contextType.toggleRegion}
              />
            )
          }
        })}
      </div>
    </div>
  )
}

function Region(props) {
  const contextType = useContext(UserContext)

  return (
    <div
      onClick={(event) => {
        props.handleRegionChange(event, props.toggleRegion)
      }}
      id={props.region._id}
      css={props.style}
      style={{
        color:
          contextType.user.region?.name === props.region.name
            ? '#FBA586'
            : 'black',
        display: 'flex',
        alignItems: 'center',
      }}
    >
      <img
        alt='location'
        src={location}
        css={locationStyle}
        style={{
          display:
            contextType.user.region?.name === props.region.name
              ? 'block'
              : 'none',
        }}
      />
      {props.region.name}
    </div>
  )
}
