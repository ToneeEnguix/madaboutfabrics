/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'
import { estimatedTime } from '../helpers/EstimatedTime'

import CartProduct from './CartProduct'

// RESPONSIVENESS SETTINGS
const breakpoints = [600, 1000, 1082, 1200]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const rightColumn = mq({
  display: 'flex',
  flexDirection: 'column',
  width: ['100%', '100%', '37%', '37%'],
  position: ['', '', '', 'sticky'],
  top: 0,
  height: '100%',
  paddingBottom: '60px',
})

const summaryWrapper = {
  display: 'flex',
  flexDirection: 'column',
  paddingBottom: '2rem',
  boxShadow: '0px 3px 6px #00000029',
  h3: {
    textTransform: 'uppercase',
    borderBottom: '1px solid lightgray',
    paddingBottom: '0.5rem',
    margin: '2rem 1rem 1rem',
    fontWeight: '500',
    fontSize: '1.1rem',
  },
}

const cartTitle = mq({
  display: 'flex',
  justifyContent: 'space-around',
  alignItems: 'center',
  backgroundColor: '#E5E5E5',
  flexDirection: 'row',
  h2: {
    padding: [
      '1rem 0 1rem 1.2rem',
      '0 0 0 1.2rem',
      '1rem 0 1rem 1.2rem',
      '1rem 0 1rem 1.2rem',
      '0 0 0 1.2rem',
    ],
    lineHeight: ['2rem', '4rem', '2rem', '2rem', '4rem'],
    letterSpacing: '2px',
    fontWeight: '500',
    width: '100%',
  },
  a: {
    color: 'black',
    fontSize: '0.7rem',
    marginRight: 'auto',
    padding: '0 0.5rem 0.5rem',
    textAlign: 'right',
    width: 'fit-content',
    alignSelf: ['', '', '', '', 'center'],
  },
})

export default function RightInformation(props) {
  const context = useContext(UserContext)

  const [deliveryTime, setDeliveryTime] = useState(1)

  useEffect(() => {
    setDeliveryTime(estimatedTime(context.user.region?.situation))
  }, [context])

  return (
    <div css={rightColumn}>
      <div css={summaryWrapper}>
        <div css={cartTitle}>
          <h2>SAMPLES IN YOUR BAG</h2>
          <Link to='/userdashboard/samples'> Edit</Link>
        </div>
        <h3>Receive it in {deliveryTime} days</h3>
        {context.user.sampleCart.length > 0 &&
          context.user.sampleCart.map((product, index) => {
            return <CartProduct key={index} product={product} sample={true} />
          })}
      </div>
    </div>
  )
}
