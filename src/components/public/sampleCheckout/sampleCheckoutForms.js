/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState, useContext } from 'react'
import { Redirect } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'

import LeftInformation from './LeftInformation'
import RightInformation from './RightInformation.js'

// RESPONSIVENESS SETTINGS
const breakpoints = [600, 1000, 1200]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const centerTitle = mq({
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
  padding: ['2rem 1rem 15rem 1rem', '2rem 8% 15rem 9%', '2rem 8% 15rem 9%'],
  h1: {
    textAlign: 'center',
    fontSize: '2.1rem',
    marginBottom: '1.5rem',
    fontFamily: 'Montserrat, sans-serif',
    letterSpacing: '3px',
    fontWeight: '700',
    paddingRight: [0, 0, '2rem'],
  },
})

const wholeForm = mq({
  display: 'flex',
  justifyContent: 'space-between',
  fontFamily: 'Roboto, sans-serif',
  width: '100%',
  flexDirection: ['column-reverse', 'column-reverse', 'row'],
})

export default function SampleCheckoutForms(props) {
  const context = useContext(UserContext)
  const [redirect, setRedirect] = useState(false)

  const [shipmentData, setShipmentData] = useState({
    name: '',
    lastName: '',
    direction: '',
    postalCode: '',
    region: '',
    town: '',
    email: '',
    phoneNumber: '',
  })

  useEffect(() => {
    setShipmentData({
      name: context.user.name || '',
      lastName: context.user.address?.lastName || '',
      direction: context.user.address?.direction || '',
      postalCode: context.user.address?.postalCode || '',
      town: context.user.address?.town || '',
      email: context.user.email,
      phoneNumber: context.user.address?.phoneNumber || '',
    })
  }, [context])

  useEffect(() => {
    if (context.user._id && context.user.sampleCart.length === 0) {
      setRedirect(true)
    }
  }, [context])

  if (redirect) {
    return <Redirect to={'/home'} />
  }
  return (
    <section>
      <div css={centerTitle}>
        <h1>SAMPLE CHECKOUT</h1>
        <div css={wholeForm}>
          <LeftInformation
            context={context}
            shipmentData={shipmentData}
            setShipmentData={setShipmentData}
            {...props}
          />
          <RightInformation />
        </div>
      </div>
    </section>
  )
}
