/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'
import ShortDisplay from '../generalPurpose/display/ShortDisplay'
import SampleCheckoutForms from './SampleCheckoutForms.js'
import SampleCheckoutConfirmation from './SampleCheckoutConfirmation.js'
import { Route } from 'react-router-dom'

export default function SampleCheckout(props) {
  return (
    <React.Fragment>
      <ShortDisplay />
      <Route
        exact
        path={`${props.match.path}`}
        render={(props) => <SampleCheckoutForms {...props} />}
      />
      <Route
        exact
        path={`${props.match.path}/sample_confirmation/:request_id`}
        component={SampleCheckoutConfirmation}
      />
    </React.Fragment>
  )
}
