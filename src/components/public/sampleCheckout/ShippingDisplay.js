/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'

import { estimatedTime } from '../helpers/EstimatedTime'
import { calculatePickupTime } from '../helpers/getTime'

const contentWrapper = {
  display: 'flex',
  flexDirection: 'column',
  padding: '1.5rem 2rem',
  span: { lineHeight: '1.65rem' },
  fontSize: '1rem',
  color: 'gray',
  fontWeight: '300',
  '.titleStyle': {
    color: 'black',
    fontWeight: '400',
  },
}

export default function ShippingDisplay({ shipmentData, shipped }) {
  return (
    <div css={contentWrapper}>
      <span className='titleStyle'>Shipping Address</span>
      <span>
        {shipmentData.name} {shipmentData.lastName}
      </span>
      <span>{shipmentData.direction}</span>
      <span>
        {shipmentData.town}, {shipmentData.postalCode}
      </span>
      <span>{shipmentData.region.name}</span>
      <span>{shipmentData.email}</span>
      <span>{shipmentData.phoneNumber}</span>
      {shipped ? (
        <React.Fragment>
          <span className='titleStyle' style={{ marginTop: '1rem' }}>
            Shipping Time
          </span>
          <span className='titleStyle'>
            Get it in {estimatedTime(shipmentData.region.situation)}{' '}
            {estimatedTime(shipmentData.region.situation) === 1 ? (
              <React.Fragment> day </React.Fragment>
            ) : (
              <React.Fragment> days </React.Fragment>
            )}
            after shipment confirmation.
          </span>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <span className='titleStyle' style={{ marginTop: '1rem' }}>
            Pick-Up Time
          </span>
          <span>Your samples will be ready for collection by:</span>
          <span>{calculatePickupTime()}</span>
        </React.Fragment>
      )}
    </div>
  )
}
