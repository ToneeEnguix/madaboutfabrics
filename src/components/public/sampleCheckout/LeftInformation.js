/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState } from 'react'
import facepaint from 'facepaint'

import Shipping from './Shipping'
import SampleOrderReview from './SampleOrderReview'

// RESPONSIVENESS SETTINGS
const breakpoints = [600, 1000, 1200]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const leftColumn = mq({
  display: 'flex',
  flexDirection: 'column',
  width: ['100%', '100%', '61%'],
  transition: 'all linear 120ms',
})

export default function LeftInformation(props) {
  const [shipped, setShipped] = useState(true)
  const [openConfirm, setOpenConfirm] = useState(false)

  return (
    <div css={leftColumn}>
      <Shipping
        context={props.context}
        shipped={shipped}
        setShipped={setShipped}
        shipmentData={props.shipmentData}
        setShipmentData={props.setShipmentData}
        setOpenConfirm={(bool) => setOpenConfirm(bool)}
      />
      <SampleOrderReview
        {...props}
        shipped={shipped}
        openConfirm={openConfirm}
        shipmentData={props.shipmentData}
      />
    </div>
  )
}
