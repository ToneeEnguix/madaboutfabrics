/** @jsx jsx */
import { jsx } from '@emotion/react'

import { URL } from '../../../config'

const productWrapper = {
  margin: '0.5em 1.5rem',
  display: 'grid',
  gridTemplateColumns: '1.2fr 3fr 2fr',
}

const circleWrapper = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '60px',
  width: '60px',
  boxShadow: '0px 3px 6px #00000029',
  marginRight: '5px',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const contentWrapper = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  p: {
    marginBottom: '0.2rem',
  },
  '.gray': {
    fontWeight: '300',
  },
}

export default function ChartProduct(props) {
  return (
    <div css={productWrapper}>
      <div css={circleWrapper}>
        <img
          alt={'product'}
          src={`${URL}/assets/${
            props.product.range.variants[props.product.variantIndex]
              .imageURLs[0].filename
          }`}
        />
      </div>
      <div css={contentWrapper}>
        <p>Range: {props.product.range.name}</p>
        <p className='gray'>
          Colour:{' '}
          {props.product.range.variants[props.product.variantIndex].color}
        </p>
      </div>
    </div>
  )
}
