/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, useContext } from 'react'
import axios from 'axios'
import ReactLoading from 'react-loading'
import ReactModal from 'react-modal'

import { URL } from '../../../config'
import UserContext from '../../../contexts/userContext'

import { estimatedTime } from '../helpers/EstimatedTime'
import { sendSampleConfirmationEmail } from '../../../services/sendEmail'

const title = {
  display: 'flex',
  justifyContent: 'space-between',
  backgroundColor: '#00263E',
  height: '4rem',
  alignItems: 'center',
  padding: '0 1rem',
  h2: {
    textTransform: 'uppercase',
    backgroundColor: '#00263E',
    color: 'white',
    letterSpacing: '2px',
    fontSize: '1.35rem',
    marginLeft: '2%',
  },
}

const main = {
  boxShadow: '0px 3px 6px #00000029',
  padding: '2rem',
  fontSize: '0.85rem',
  lineHeight: '1.3rem',
  color: '#898989',
  fontWeight: '300',
  textJustify: 'inter-word',
  textAlign: 'justify',
  display: 'flex',
  flexDirection: 'column',
  button: {
    backgroundColor: '#FA5402',
    outline: 'none',
    color: 'white',
    border: 'none',
    padding: '0.83rem 1.7rem',
    cursor: 'pointer',
    textTransform: 'uppercase',
    fontSize: '0.8rem',
    fontWeight: '500',
    fontFamily: 'Roboto, sans-serif',
    width: 'fit-content',
    margin: '1rem 0.75rem',
    alignSelf: 'flex-end',
  },
  u: {
    cursor: 'pointer',
  },
}

export default function OrderReview({
  shipmentData,
  openConfirm,
  shipped,
  match,
}) {
  const context = useContext(UserContext)

  const [loading, setLoading] = useState(false)

  console.log('shipmentData: ', shipmentData)

  const handleRequest = async () => {
    setLoading(true)
    localStorage.removeItem('shipmentData')
    // prepare object to send minimmum info
    // safely duplicate array to manipulate it
    // very doubtful of method
    // let onlyIdsSampleCart = JSON.parse(JSON.stringify(context.user.sampleCart));
    // onlyIdsSampleCart.forEach((item) => {
    //   item.range = item.range._id;
    //   delete item["_id"];
    // });
    try {
      const res = await axios.post(
        `${URL}/samples/request/${context.user._id}`,
        {
          sampleCart: context.user.sampleCart,
          shipmentAddress: shipmentData,
          shipped,
        }
      )
      const sample = res.data.sample
      let someDate = new Date()
      let numberOfDaysToAdd = estimatedTime(shipmentData.region.situation)
      let result = someDate.setDate(someDate.getDate() + numberOfDaysToAdd)
      const date = new Date(result).toUTCString().slice(0, 11)

      if (res.data.ok) {
        const data = {
          name: shipmentData.name + ' ' + (shipmentData.lastName || ''),
          email: shipmentData.email,
          message: 'YOUR SAMPLE REQUEST WAS SUCCESSFUL',
          subject: 'Sample Request Confirmation',
          sampleNumber: sample.index,
          orderDate: date,
          shippingAddress: shipmentData,
          shipped,
          eta: estimatedTime(shipmentData.region.situation),
          productsBought: sample.requestedSamples,
        }
        if (!(await sendSampleConfirmationEmail(data))) {
          return false
        }
        window.location.href = `${match.path}/sample_confirmation/${sample._id}`
      }
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <React.Fragment>
      <div css={title}>
        <h2>2. SAMPLE ORDER REVIEW</h2>
      </div>
      {openConfirm ? (
        <div css={main}>
          <span>
            By clicking the Place Order button, you conﬁrm that you have read
            and agreed to our{' '}
            <u onClick={() => window.open('/more/')}>Terms and Conditions</u>{' '}
            and our{' '}
            <u onClick={() => window.open('/more/returns')}>Returns Policy</u>{' '}
            and acknowledge that you have read Mad About Fabric's{' '}
            <u onClick={() => window.open('/more/privacy')}>Privacy Policy</u>.
          </span>
          <button onClick={() => handleRequest()}>Make an order</button>
          <Modal loading={loading} />
        </div>
      ) : null}
    </React.Fragment>
  )
}

const Modal = (props) => {
  return (
    <ReactModal
      ariaHideApp={false}
      isOpen={props.loading}
      style={{
        overlay: {
          backgroundColor: '#ffffffdd',
        },
        content: {
          position: 'relative',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          backgroundColor: 'transparent',
          border: 'none',
          fontFamily: 'Roboto, sans-serif',
          fontSize: '0.85rem',
          letterSpacing: '2px',
          textTransform: 'uppercase',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
          width: '80vw',
          textAlign: 'center',
        },
      }}
    >
      <ReactLoading type={'spin'} color='#030303' />
      <h3 style={{ marginTop: '2rem' }}>
        Your order is being processed. Do not go back or refresh the page until
        the order is complete
      </h3>
    </ReactModal>
  )
}
