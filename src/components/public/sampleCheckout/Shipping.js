/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useEffect, useState } from 'react'
import ShippingForm from './ShippingForm'
import ShippingDisplay from './ShippingDisplay'

const show = {
  transition: 'height 500ms',
  marginBottom: '2rem',
  boxShadow: '0px 3px 6px #00000029',
  h2: {
    textTransform: 'uppercase',
    backgroundColor: '#00263E',
    color: 'white',
    letterSpacing: '2px',
    fontSize: '1.35rem',
    marginLeft: '2%',
  },
}

const title = {
  display: 'flex',
  justifyContent: 'space-between',
  backgroundColor: '#00263E',
  height: '4rem',
  alignItems: 'center',
  padding: '0 1rem',
  span: {
    color: 'white',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
  i: {
    color: 'green',
  },
}

export default function Shipping({
  context,
  shipped,
  setShipped,
  shipmentData,
  setShipmentData,
  setOpenConfirm,
}) {
  const [showingForm, setShowingForm] = useState(true)

  const showForm = () => {
    !shipped &&
      setShipmentData({
        name: context.user.name,
        lastName: context.user.lastName || '',
        direction: context.user.address?.direction || '',
        postalCode: context.user.address?.postalCode || '',
        region: context.user.address?.region || '',
        town: context.user.address?.town || '',
        email: context.user.email,
        phoneNumber: context.user.address?.phoneNumber || '',
      })
    setShowingForm(true)
    setOpenConfirm(false)
  }

  useEffect(() => {
    if (!shipped) {
      setShowingForm(false)
      setOpenConfirm(true)
    }
  }, [shipped, setOpenConfirm])

  const submit = () => {
    setOpenConfirm(true)
    setShowingForm(false)
  }

  return (
    <div key={shipmentData.name} css={show}>
      {showingForm === true ? (
        <React.Fragment>
          <div css={title}>
            <h2>1. Shipping</h2>
          </div>
          <ShippingForm
            shipmentData={shipmentData}
            setShipmentData={setShipmentData}
            shipped={shipped}
            setShipped={setShipped}
            submit={submit}
          />
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div css={title}>
            <div css={{ display: 'flex', alignItems: 'flex-end' }}>
              <i className='material-icons #15D032'>done</i>
              <h2>1.Shipping</h2>
            </div>
            <span
              onClick={() => {
                showForm()
                setShipped(true)
              }}
            >
              Edit
            </span>
          </div>
          <ShippingDisplay shipped={shipped} shipmentData={shipmentData} />
        </React.Fragment>
      )}
    </div>
  )
}
