/** @jsx jsx */
import { jsx } from '@emotion/react'
import { keyframes } from '@emotion/react/'

export default function CustomAlert(props) {
  const bounce = keyframes`
    from  {
      bottom:-2rem
    }
    to {bottom:0}
    }`

  const alertStyle = {
    position: 'fixed',
    backgroundColor: props.color,
    color: 'white',
    bottom: '0',
    right: '0',
    padding: '1rem',
    fontSize: '0.8rem',
    animation: `${bounce} 0.8s`,
    fontFamily: 'Montserrat,sans-serif',
    borderRadius: '8px 0 0 0',
  }

  return <div css={alertStyle}>{props.content}</div>
}
