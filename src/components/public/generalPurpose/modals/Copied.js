/** @jsx jsx */
import { jsx } from '@emotion/react'
import { keyframes } from '@emotion/react/'

export default function Copied(props) {
  const bounce = keyframes`
    0%  {
      bottom: 60px,
    }
    
    50% {bottom: 90px}
    100% {bottom: 90px}
    }`

  const copied = {
    position: 'absolute',
    zIndex: 2,
    color: 'white',
    fontSize: '1rem',
    animation: `${bounce} 1.2s`,
    bottom: '60px',
    fontFamily: 'Montserrat,sans-serif',
    right: '60px',
    opacity: '0.8',
  }

  return <div css={copied}>Copied!</div>
}
