/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'

export default class StyledSelectUser extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: { _id: undefined, name: 'Location' },
      regions: [
        {
          _id: 'AUS',
          name: 'Austria',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'BELG',
          name: 'Belgium',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'BOHE',
          name: 'Bosnia & Herzegovina',
          coin: 'GBP',
          shippingRate: 40,
          situation: 'Mainland',
        },
        {
          _id: 'CRO',
          name: 'Croatia',
          coin: 'GBP',
          shippingRate: 40,
          situation: 'Mainland',
        },
        {
          _id: 'CZR',
          name: 'Czech Republic',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'DEN',
          name: 'Denmark',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'ESTO',
          name: 'Estonia',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'FIN',
          name: 'Finland',
          coin: 'GBP',
          shippingRate: 30,
          situation: 'Mainland',
        },
        {
          _id: 'FR',
          name: 'France',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'GER',
          name: 'Germany',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'HUNG',
          name: 'Hungary',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'ITA',
          name: 'Italy',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'LATV',
          name: 'Latvia',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'LUX',
          name: 'Luxemburg',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'NETH',
          name: 'Netherlands',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'NOR',
          name: 'Norway',
          coin: 'GBP',
          shippingRate: 40,
          situation: 'Mainland',
        },
        {
          _id: 'POL',
          name: 'Poland',
          coin: 'GBP',
          shippingRate: 25,
          situation: 'Mainland',
        },
        {
          _id: 'PORT',
          name: 'Portugal',
          coin: 'GBP',
          shippingRate: 30,
          situation: 'Mainland',
        },
        {
          _id: 'IRE',
          name: 'Republic of Ireland',
          coin: 'GBP',
          shippingRate: 7.5,
          situation: 'Ireland',
        },
        {
          _id: 'SERB',
          name: 'Serbia/Montenegre',
          coin: 'GBP',
          shippingRate: 40,
          situation: 'Mainland',
        },
        {
          _id: 'SLOVK',
          name: 'Slovakia',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'SLOVE',
          name: 'Slovenia',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'SP',
          name: 'Spain',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'SW',
          name: 'Sweden',
          coin: 'GBP',
          shippingRate: 27.5,
          situation: 'Mainland',
        },
        {
          _id: 'SWITZ',
          name: 'Switzwerland',
          coin: 'GBP',
          shippingRate: 40,
          situation: 'Mainland',
        },
        {
          _id: 'ENG',
          name: 'England',
          coin: 'GBP',
          shippingRate: 10,
          situation: 'Britain',
        },
        {
          _id: 'NIRE',
          name: 'Northern Ireland',
          coin: 'GBP',
          shippingRate: 10,
          situation: 'Ireland',
        },
        {
          _id: 'SCOT',
          name: 'Scotland',
          coin: 'GBP',
          shippingRate: 10,
          situation: 'Britain',
        },
        {
          _id: 'WAL',
          name: 'Wales',
          coin: 'GBP',
          shippingRate: 10,
          situation: 'Britain',
        },
        {
          _id: 'SCOTIS',
          name: 'Scottish Islands',
          coin: 'GBP',
          shippingRate: 30,
          situation: 'Britain',
        },
        {
          _id: 'MAN',
          name: 'Isle of Man',
          coin: 'GBP',
          shippingRate: 30,
          situation: 'Britain',
        },
        {
          _id: 'WIGHT',
          name: 'Isle of Wight',
          coin: 'GBP',
          shippingRate: 30,
          situation: 'Britain',
        },
        {
          _id: 'CHAN',
          name: 'Chanel Islands',
          coin: 'GBP',
          shippingRate: 30,
          situation: 'Britain',
        },
        {
          _id: 'SCILLY',
          name: 'Isles of Scilly',
          coin: 'GBP',
          shippingRate: 30,
          situation: 'Britain',
        },
      ],
    }

    this.handleChange = this.handleChange.bind(this)
  }

  componentDidMount() {
    const activeRegion = this.state.regions.find(
      (region) => region._id === this.props.region?._id
    )

    if (activeRegion) {
      this.setState({ value: activeRegion })
    } else {
      const activeRegion = this.state.regions.find(
        (region) => region._id === 'NIRE'
      )
      this.setState({ value: activeRegion })
      this.props.toggleRegion(activeRegion)
    }
  }

  handleChange(e) {
    const activeRegion = this.state.regions.find(
      (region) => region._id === e.target.value
    )
    this.setState({ value: activeRegion })
    this.props.toggleRegion(activeRegion)
  }

  render() {
    return (
      <div
        css={{
          display: 'flex',
          flexDirection: 'column',
          width: this.props.width || 'auto',
          marginBottom: '1rem',

          label: {
            display: 'block',
            position: 'relative',
            top: '-1.8rem',
            color: '#999',
            fontSize: '0.8rem',
            padding: '0',
            zIndex: '1',
            transition: 'all 0.3s ease-out',
            outline: 'none',
          },
          'label span': {
            backgroundColor: 'white',
            padding: '0 3px',
            width: '100%',
          },
          select: {
            width: '100%',
            outline: 'none',
            display: 'block',
            position: 'relative',
            padding: '0.7rem',
            border: '1px solid lightgray',
            zIndex: '2',
          },
          'select:focus, select:valid,': {
            outline: 'none',
          },
          'select:disabled': {
            backgroundColor: 'grey',
          },
          'select:focus + label,select:valid + label,select:disabled + label': {
            top: '-3.2rem',
            fontSize: '0.8rem',
            backgroundColor: 'transparent',
            zIndex: '3',
          },
        }}
      >
        <select
          disabled={this.props.disabled}
          onChange={this.handleChange}
          defaultValue={'ENG'}
          value={this.state.value._id}
          required
          name={this.props.innerName}
        >
          {this.state.regions.map((region) => {
            return (
              <option key={region._id} value={region._id}>
                {region.name}
              </option>
            )
          })}
        </select>
      </div>
    )
  }
}
