/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'

export default class StyledInputUser extends React.Component {
  constructor(props) {
    super(props)
    this.state = { value: this.props.defaultValue }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState({ value: event.target.value })
  }

  render() {
    return (
      <div
        css={{
          display: 'flex',
          flexDirection: 'column',
          width: this.props.width,

          label: {
            display: 'block',
            position: 'relative',
            top: '-1.7rem',
            left: '0rem',
            color: '#999',
            fontSize: '0.8rem',
            padding: '0',
            zIndex: '1',
            transition: 'all 0.3s ease-out',
            outline: 'none',
          },

          'label span': {
            backgroundColor: 'white',
            padding: '0 3px',
            width: '100%',
          },

          input: {
            width: '100%',
            outline: 'none',
            display: 'block',
            position: 'relative',
            background: 'none',
            padding: '0.7rem',
            border: '1px solid lightGrey',
            zIndex: '2',
          },

          'input:focus, input:valid,': {
            outline: 'none',
          },

          'input:disabled': {
            backgroundColor: 'grey',
          },

          'input:focus + label,input:valid + label': {
            top: '-3rem',
            left: '0.5rem',
            backgroundColor: 'transparent',
            zIndex: '3',
          },
        }}
      >
        <input
          onChange={this.handleChange}
          value={this.state.value}
          disabled={this.props.disabled}
          required={this.props.required}
          type={this.props.type}
          name={this.props.innerName}
        ></input>
        <label htmlFor={this.props.name}>
          <span>{this.props.name} </span>
        </label>
      </div>
    )
  }
}
