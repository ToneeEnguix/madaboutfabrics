/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect, useContext } from 'react'

import UserContext from '../../../../contexts/userContext'
import arrow from '../../../admin/pictures/arrow.svg'

const main = {
  display: 'flex',
  flexDirection: 'column',
  'label span': {
    padding: '0 3px',
  },
  select: {
    background: 'none',
    outline: 'none',
    display: 'block',
    backgroundColor: '#e8f0fe',
    padding: '1.116rem 1.1rem',
    border: 'none',
    zIndex: '2',
    WebkitAppearance: 'none',
    MozAppearance: 'none',
    msAppearance: 'none',
    OAppearance: 'none',
    appearance: 'none',
  },
  'select:focus, select:valid,': {
    outline: 'none',
  },
  'select:disabled': {
    backgroundColor: '#FAFAFA',
  },
  'select:focus + label,select:valid + label,select:disabled + label': {
    top: '-2.9rem',
    left: '0.7rem',
    fontSize: '0.7rem',
    backgroundColor: 'transparent',
    zIndex: '3',
  },
  img: {
    zIndex: 5,
    display: 'block',
    transform: 'rotate(-90deg)',
    position: 'absolute',
    top: '13px',
    right: '8px',
  },
}

export default function StyledSelect(props) {
  const [value, setValue] = useState({
    _id: undefined,
    name: 'Location',
  })
  const regions = [
    {
      _id: 'AUS',
      name: 'Austria',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'BELG',
      name: 'Belgium',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'BOHE',
      name: 'Bosnia & Herzegovina',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'CRO',
      name: 'Croatia',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'CZR',
      name: 'Czech Republic',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'DEN',
      name: 'Denmark',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'ESTO',
      name: 'Estonia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'FIN',
      name: 'Finland',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Mainland',
    },
    {
      _id: 'FR',
      name: 'France',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'GER',
      name: 'Germany',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'HUNG',
      name: 'Hungary',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'ITA',
      name: 'Italy',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'LATV',
      name: 'Latvia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'LUX',
      name: 'Luxemburg',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'NETH',
      name: 'Netherlands',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'NOR',
      name: 'Norway',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'POL',
      name: 'Poland',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'PORT',
      name: 'Portugal',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Mainland',
    },
    {
      _id: 'IRE',
      name: 'Republic of Ireland',
      coin: 'GBP',
      shippingRate: 7.5,
      situation: 'Ireland',
    },
    {
      _id: 'SERB',
      name: 'Serbia/Montenegre',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'SLOVK',
      name: 'Slovakia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SLOVE',
      name: 'Slovenia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SP',
      name: 'Spain',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SW',
      name: 'Sweden',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SWITZ',
      name: 'Switzwerland',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'ENG',
      name: 'England',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Britain',
    },
    {
      _id: 'NIRE',
      name: 'Northern Ireland',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Ireland',
    },
    {
      _id: 'SCOT',
      name: 'Scotland',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Britain',
    },
    {
      _id: 'WAL',
      name: 'Wales',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Britain',
    },
    {
      _id: 'SCOTIS',
      name: 'Scottish Islands',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'MAN',
      name: 'Isle of Man',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'WIGHT',
      name: 'Isle of Wight',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'CHAN',
      name: 'Chanel Islands',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'SCILLY',
      name: 'Isles of Scilly',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
  ]

  const context = useContext(UserContext)

  useEffect(() => {
    const activeRegion = regions.find((region) => {
      return region._id === props.region?._id
    })
    if (activeRegion) {
      setValue(activeRegion)
    } else {
      const activeRegion = regions.find((region) => region._id === 'NIRE')
      setValue(activeRegion)
      props.toggleRegion(activeRegion)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [context])

  const handleChange = (e) => {
    const activeRegion = regions.find(
      (region) => region.name === e.target.value
    )
    setValue(activeRegion)
    props.toggleRegion(activeRegion)
  }

  return (
    <div css={main} style={{ width: props.width || 'auto' }}>
      <select
        disabled={props.disabled}
        onChange={handleChange}
        value={value.name}
        required
        name={props.innerName}
        className='pointer'
      >
        {regions.map((region) => {
          return (
            <option key={region._id} name={region.name} value={region.name}>
              {region.name}
            </option>
          )
        })}
      </select>
      <img src={arrow} alt='dropdown triangle' />
    </div>
  )
}
