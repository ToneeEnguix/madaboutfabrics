/** @jsx jsx */
import { jsx } from '@emotion/react'

import { URL } from '../../../../config'
import arrowBack from '../../../../components/admin/pictures/arrow.svg'

const rightColumn = {
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  padding: '0 20%',
  p: {
    fontSize: '0.85rem',
    fontWeight: '300',
    textAlign: 'justify',
    lineHeight: '1.2rem',
  },
}

const titles = {
  h2: {
    fontFamily: 'cooper-black-std, serif',
    fontSize: '1.8rem',
  },
  p: {
    fontSize: '0.7rem',
    margin: '0.2rem 0',
    color: 'black',
    fontWeight: '500',
  },
  h1: {
    fontSize: '1.4em',
    letterSpacing: '1px',
  },
}

const mailInputCont = {
  margin: '0.5rem 0 0.7rem',
  fontSize: '.9rem',
  height: '3rem',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  input: {
    borderRadius: '10px 0 0 10px',
    height: '100%',
    padding: '0 1rem',
    width: '90%',
    border: 'none',
    boxShadow: '0px 3px 6px #00000029',
  },
  button: {
    borderRadius: '0 10px 10px 0',
    marginLeft: '-1px',
    height: '100%',
    border: 'none',
    backgroundColor: 'white',
    boxShadow: '0px 3px 6px #00000029',
  },
  img: {
    height: '100%',
    cursor: 'pointer',
    transform: 'rotate(180deg)',
    padding: '10%',
    ':hover': {
      backgroundColor: 'lightgray',
    },
  },
}

const flex = {
  display: 'flex',
  alignItems: 'center',
  p: {
    lineHeight: '1.1rem',
  },
  margin: '0.5rem 0',
  width: '80%',
  input: {
    width: '4rem',
    height: '4rem',
    marginRight: '1rem',
  },
  button: {
    padding: '0.5em',
    border: 'none',
    backgroundColor: 'white',
    borderColor: '-internal-light-dark(rgb(118, 118, 118), rgb(195, 195, 195))',
  },
}

export default function NewsLetterSignUp(props) {
  const handleSubmit = (e) => {
    e.preventDefault()
    const email = e.target.email.value
    const acceptTerms = e.target.acceptTerms.checked
    if (!acceptTerms) {
      alert('You must accept our terms and conditions')
    } else {
      sendEmail(email)
    }
  }

  const sendEmail = (email) => {
    var myHeaders = new Headers()
    myHeaders.append('Content-Type', 'application/x-www-form-urlencoded')

    var urlencoded = new URLSearchParams()
    urlencoded.append('email', email)

    var requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
      redirect: 'follow',
    }

    fetch(`${URL}/subscribers/subscribe`, requestOptions)
      .then((response) => {
        if (response.ok) {
          return response.text()
        } else {
          throw new Error(response.text())
        }
      })
      .then((result) => alert('Added Successfully'))
      .catch((error) => alert('Error: email already there'))
  }

  return (
    <div css={rightColumn}>
      <div css={titles}>
        <h2>Mad About Fabrics</h2>
        <p style={{ fontFamily: 'Montserrat, sans-serif' }}>
          FOR ALL NEW MAD ABOUT FABRICS EMAIL SUBSCRIBERS
        </p>
        <h1 style={{ fontFamily: 'Montserrat, sans-serif' }}>
          NO MAIL, JUST GREAT DEALS
        </h1>
      </div>
      <form onSubmit={handleSubmit}>
        <div css={mailInputCont}>
          <input type='email' name='email' placeholder='Email address' />
          <button type='submit'>
            <img alt='subscribe' src={arrowBack} />
          </button>
        </div>
        <p>
          By submitting my email address, I agree to receive Personalised emails
          from the <strong>Talbot Textiles Group</strong> regarding exclusive
          offers, new arrivals, event and contest Invites and more as explained
          in our <strong>Privacy Policy</strong>*. I may{' '}
          <strong>unsubscribe</strong> at any time.
        </p>
        <div css={flex}>
          <input type='checkbox' name='acceptTerms' defaultChecked></input>
          <p>
            I allow the Talbot Textiles group to match data From third parties
            with my email address to Personalise its offer.*
          </p>
        </div>
        <p>*Only available if you are above 16 years old</p>
      </form>
    </div>
  )
}
