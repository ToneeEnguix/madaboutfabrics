/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import facepaint from 'facepaint'

import UserContext from '../../../../contexts/userContext'
import creditCardIcons from '../../../../resources/creditCardIcons.png'
import NewsLetterSignUp from './NewsLetterSignUp'
import location from '../../../../resources/locationBlack.svg'
import { Link } from 'react-router-dom'
import applePay from '../../../../resources/apple-pay.svg'
import facebookIcon from '../../../../resources/facebook.svg'
import instagramIcon from '../../../../resources/instagram.svg'
import whatsAppIcon from '../../../../resources/whatsapp.svg'

// RESPONSIVENESS SETTINGS
const breakpoints = [561, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const footer = {
  display: 'flex',
  justifyContent: 'center',
  alignContent: 'center',
  fontFamily: 'Roboto, sans-serif',
}

const content = mq({
  display: ['flex', 'grid'],
  flexDirection: 'column',
  gridTemplateColumns: ['1fr 1fr'],
  padding: ['0', '2rem 2rem 6rem', '4rem 4rem 8rem'],
})

const leftColumn = {
  borderRight: '1px solid grey',
  padding: '0 10%',
  fontWeight: '500',
}

const acceptedPaymentCont = {
  margin: '0.5rem 0 1.5rem -.3rem',
  img: {
    width: 'auto',
    height: '35px',
    filter: 'grayscale(100%)',
  },
  '.applepay': {
    filter: 'invert(100%)',
  },
}

const smallerColumns = {
  display: 'grid',
  gridTemplateColumns: 'repeat(3, 1fr)',
  width: '100%',
  justifyContent: 'space-between',
  '& ul': {
    listStyle: 'none',
  },
  h5: {
    marginBottom: '0.8em',
    fontWeight: '400',
    fontSize: '0.8rem',
  },
  a: {
    fontSize: '0.8em',
    padding: '1em 0',
    fontWeight: '400',
    textDecoration: 'none',
    color: 'inherit',
    display: 'block',
  },
}

const locationPlusSocialBoxes = {
  a: {
    textDecoration: 'none',
    color: 'inherit',
    fontSize: '.8rem',
    display: 'flex',
    alignItems: 'center',
    width: 'fit-content',
    fontWeight: '300',
    img: {
      marginRight: '.3rem',
      width: '40px',
      height: '40px',
    },
    ':hover': {
      filter:
        'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(108%) contrast(97%)',
    },
    transition: 'all linear 200ms',
  },
  div: {
    marginTop: '1rem',
    a: {
      display: 'inline',
    },
    img: {
      // height: '1.7vw',
      margin: '1rem 1.7rem 0 0',
      transition: 'all linear 200ms',
      filter: 'invert(100%)',
      ':hover': {
        filter:
          'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(100%) contrast(97%)',
      },
    },
  },
}

export default function Footer(props) {
  const context = useContext(UserContext)

  return (
    <footer css={footer}>
      <div css={content}>
        <div css={leftColumn}>
          <p>We Accept</p>
          <div css={acceptedPaymentCont}>
            <img alt='creditIcons' src={creditCardIcons} />
            <img
              className='applepay'
              alt='applepay'
              style={{ padding: '0.4rem 0.1rem 0.1rem' }}
              src={applePay}
            />
          </div>
          <div css={smallerColumns}>
            <div>
              <h5>QUICK LINKS</h5>
              <ul>
                <Link to='/more/returns'>Return Policy</Link>
                <Link to='/more/tracking'>Track Order</Link>
                <Link to='/more/shipping'>Shipping</Link>
                <Link to='/more/contact'>Find Store</Link>
              </ul>
            </div>
            <div>
              <h5>SUPPORT</h5>
              <ul>
                <Link to='/more/help'>Help</Link>
                <Link to='/more/contact'>Contact Us</Link>
                <Link to='/more/contact'>Feedback</Link>
              </ul>
            </div>
            <div>
              <h5>COMPANY</h5>
              <ul>
                <Link to='/more/about'>About Us</Link>
                <Link to='/more/careers'>Careers</Link>
                <Link to='/more/privacy'>Privacy Policy</Link>
                <Link to='/more/'>Terms and Conditions of Purchase</Link>
              </ul>
            </div>
          </div>
          <div css={locationPlusSocialBoxes}>
            <Link to='/locations'>
              <img alt='location' src={location} />
              <span>{context.user.region?.name}</span>
            </Link>
            <div>
              <a
                href='https://www.facebook.com/MadAboutFabrics/'
                target='_blank'
                rel='noopener noreferrer'
              >
                <img alt='facebook' src={facebookIcon} />
              </a>
              <a
                href='https://www.instagram.com/madaboutfabrics/'
                target='_blank'
                rel='noopener noreferrer'
              >
                <img alt='instagram' src={instagramIcon} />
              </a>
              <a href='https://api.whatsapp.com/send?phone=442890370390'>
                <img alt='phone' src={whatsAppIcon} />
              </a>
            </div>
          </div>
        </div>
        <NewsLetterSignUp />
      </div>
    </footer>
  )
}
