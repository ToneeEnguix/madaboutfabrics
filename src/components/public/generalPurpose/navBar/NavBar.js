/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { NavLink } from 'react-router-dom'
import HorizontalScroll from 'react-scroll-horizontal'
import { isMobile } from 'react-device-detect'

// RESPONSIVENESS SETTINGS
const breakpoints = [950, 1250]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const flexContainer = mq({
  display: 'flex',
  alignItems: 'center',
  width: '100vw',
  height: '75px',
  overflowX: 'scroll',
  overflowY: 'hidden',
  // SCROOLLBAR STYLE
  msOverflowStyle: 'none' /* IE and Edge */,
  scrollbarWidth: 'none',
  '::-webkit-scrollbar': {
    display: 'none' /* for Chrome, Safari, and Opera */,
  },
  '.link_wrapper': {
    maxHeight: '72px',
    position: 'relative',
    maxWidth: ['auto', 'auto', '12.5vw'],
    width: ['auto', 'auto', '12.5vw'],
  },
  a: {
    color: 'black',
    textDecoration: 'none',
    whiteSpace: 'nowrap',
    display: 'block',
    fontFamily: 'Montserrat,serif',
    letterSpacing: '0.5px',
    fontSize: '1rem',
    fontWeight: '500',
    padding: isMobile ? '1.8rem 2rem 1.07rem' : '1.8rem 2rem 1.3rem',
    borderBottom: 'solid 6px transparent',
    textAlign: 'center',
    borderRadius: '8px 8px 0 0',
    ':hover + .navbar_link_line': {
      borderColor: '#fba586',
      transform: 'scaleX(1)',
    },
  },
})

export default function NavBar() {
  if (isMobile) {
    return <MobileNavbar />
  } else return <DesktopNavbar />
}

const MobileNavbar = () => {
  return (
    <nav css={flexContainer}>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/home'>
          HOME
        </NavLink>
        <div className='navbar_link_line' />
      </div>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/catalogue'>
          CATALOGUE
        </NavLink>
        <div className='navbar_link_line' />
      </div>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/ranges'>
          RANGES
        </NavLink>
        <div className='navbar_link_line' />
      </div>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/inspired'>
          BE INSPIRED
        </NavLink>
        <div className='navbar_link_line' />
      </div>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/new'>
          NEW ARRIVALS
        </NavLink>
        <div className='navbar_link_line' />
      </div>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/searchterms'>
          SEARCH TERMS
        </NavLink>
        <div className='navbar_link_line' />
      </div>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/accessories'>
          ACCESSORIES
        </NavLink>
        <div className='navbar_link_line' />
      </div>
      <div className='link_wrapper'>
        <NavLink activeClassName='active' to='/sale'>
          SALE
        </NavLink>
        <div className='navbar_link_line' />
      </div>
    </nav>
  )
}

const DesktopNavbar = () => {
  return (
    <nav css={flexContainer}>
      <HorizontalScroll reverseScroll={true}>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/home'>
            HOME
          </NavLink>
          <div className='navbar_link_line' />
        </div>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/catalogue'>
            CATALOGUE
          </NavLink>
          <div className='navbar_link_line' />
        </div>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/ranges'>
            RANGES
          </NavLink>
          <div className='navbar_link_line' />
        </div>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/inspired'>
            BE INSPIRED
          </NavLink>
          <div className='navbar_link_line' />
        </div>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/new'>
            NEW ARRIVALS
          </NavLink>
          <div className='navbar_link_line' />
        </div>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/searchterms'>
            SEARCH TERMS
          </NavLink>
          <div className='navbar_link_line' />
        </div>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/accessories'>
            ACCESSORIES
          </NavLink>
          <div className='navbar_link_line' />
        </div>
        <div className='link_wrapper'>
          <NavLink activeClassName='active' to='/sale'>
            SALE
          </NavLink>
          <div className='navbar_link_line' />
        </div>
      </HorizontalScroll>
    </nav>
  )
}
