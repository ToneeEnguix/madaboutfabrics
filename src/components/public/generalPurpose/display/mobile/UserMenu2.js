/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'

import arrow from '../../../../admin/pictures/arrow.svg'

export default function Bottom({ context }) {
  return (
    <div css={bottomStyle}>
      <div className='linkWrapper'>
        <Link to='/userdashboard/profile'>View Profile</Link>
        <img alt='arrow' src={arrow} />
      </div>
      <div className='linkWrapper'>
        <Link to='/userdashboard/password'>Password</Link>
        <img alt='arrow' src={arrow} />
      </div>
      <div className='linkWrapper'>
        <Link to='/userdashboard/address'>Address</Link>
        <img alt='arrow' src={arrow} />
      </div>
      <div onClick={context.logOut} className='linkWrapper pointer'>
        <p>Log Out</p>
        <img alt='arrow' src={arrow} />
      </div>
    </div>
  )
}

const bottomStyle = {
  '.linkWrapper': {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '1rem 2rem',
    width: '100%',
    'a, p': {
      fontSize: '1.2rem',
      fontWeight: 300,
      letterSpacing: '0px',
      color: 'inherit',
      textDecoration: 'none',
    },
    img: {
      transform: 'rotate(180deg)',
    },
  },
}
