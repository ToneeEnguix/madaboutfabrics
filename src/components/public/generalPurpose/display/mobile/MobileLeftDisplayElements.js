/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useCallback, useContext, useEffect } from 'react'
import facepaint from 'facepaint'
import { Animate } from 'react-simple-animate'

import UserContext from '../../../../../contexts/userContext'

import Top from './Top/Top'
import Bottom from './Bottom'
import burgerMenu from '../../../../../resources/mobile/burger.png'
import UserMenu from './UserMenu/UserMenu'
import UserMenu2 from './UserMenu2'
import MenuOptions from './MenuOptions/MenuOptions'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function MobileLeftDisplayElements(props) {
  const context = useContext(UserContext)

  useEffect(() => {
    if (props.openMenu) {
      props.setOpenMenu(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.location.pathname])

  const escFunction = useCallback(
    (event) => {
      if (event.keyCode === 27) {
        props.setOpenMenu(false)
      }
    },
    [props]
  )

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)
    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  return (
    <div css={mobile}>
      <div className='burgerWrapper' onClick={() => props.setOpenMenu(true)}>
        <img alt='burger menu' src={burgerMenu} />
      </div>
      {props.openMenu && [
        <div
          key='1'
          css={toClickOutside}
          onClick={() => props.setOpenMenu(false)}
        />,
        <div key='2' css={menuWrapper}>
          <Animate
            duration={0.4}
            play={true}
            start={{
              transform: 'translateX(-400px)',
              opacity: 0.8,
            }}
            end={{
              transform: 'translateX(0px)',
              opacity: 1,
            }}
          >
            <div
              style={{
                overflowY: 'auto',
                paddingBottom: '2rem',
                height: '100vh',
                backgroundColor: 'white',
              }}
            >
              <Top setOpenMenu={props.setOpenMenu} context={context} />
              <div className='line' />
              {context.user._id && <UserMenu />}
              {context.user._id && <div className='line' />}
              <Bottom />
              {context.user._id && <div className='line' />}
              {context.user._id && <UserMenu2 context={context} />}

              <div className='line' />
              <MenuOptions setOpenMenu={(bool) => props.setOpenMenu(bool)} />
            </div>
          </Animate>
        </div>,
      ]}
    </div>
  )
}

const mobile = mq({
  display: ['block', 'block', 'none'],
  height: '100%',
  position: 'relative',
  '.burgerWrapper': {
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    paddingLeft: ['20px', '30px'],
    cursor: 'pointer',
    img: {
      height: ['25px', '30px'],
    },
  },
})

const menuWrapper = mq({
  font: 'normal normal normal 14px Roboto',
  width: '85vw',
  position: 'fixed',
  top: 0,
  left: 0,
  zIndex: 12,
  boxShadow: '0px 3px 6px #00000029',
  // SCROOLLBAR STYLE
  msOverflowStyle: 'none' /* IE and Edge */,
  scrollbarWidth: 'none',
  '::-webkit-scrollbar': {
    display: 'none' /* for Chrome, Safari, and Opera */,
  },
  '.line': {
    margin: '.5rem 0',
    width: '100%',
    height: '2px',
    backgroundColor: 'rgba(225, 225, 225)',
  },
})

const toClickOutside = {
  position: 'fixed',
  top: 0,
  right: 0,
  zIndex: 11,
  width: '100vw',
  height: '100vh',
  cursor: 'pointer',
  backgroundColor: 'rgba(0, 0, 0, 0.4)',
}
