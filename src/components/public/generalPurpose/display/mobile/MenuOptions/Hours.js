/** @jsx jsx */
import { jsx } from '@emotion/react'

export default function Hours() {
  return (
    <div css={main}>
      <div className='day'>
        <p>Monday</p>
        <p>9 AM - 4:45 PM</p>
      </div>
      <div className='day'>
        <p>Tuesday</p>
        <p>9 AM - 4:45 PM</p>
      </div>
      <div className='day'>
        <p>Wednesday</p>
        <p>9 AM - 4:45 PM</p>
      </div>
      <div className='day'>
        <p>Thursday</p>
        <p>9 AM - 8:45 PM</p>
      </div>
      <div className='day'>
        <p>Friday</p>
        <p>9 AM - 4:45 PM</p>
      </div>
      <div className='day'>
        <p>Saturday</p>
        <p>9 AM - 4:45 PM</p>
      </div>
      <div className='day'>
        <p>Sunday</p>
        <p>Closed</p>
      </div>
    </div>
  )
}

const main = {
  padding: '0 2rem',
  fontSize: '1.1rem',
  fontWeight: 300,
  letterSpacing: '0px',
  '.day': {
    display: 'grid',
    gridTemplateColumns: '1fr 1fr',
    margin: '1rem 0 2rem',
  },
}
