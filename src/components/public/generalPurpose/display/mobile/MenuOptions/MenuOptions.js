/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState } from 'react'

import arrow from '../../../../../admin/pictures/arrow.svg'
import Phone from './Phone'
import Hours from './Hours'
import Locations from './MobileLocations'

export default function MenuOptions({ setOpenMenu }) {
  const [showPhone, setShowPhone] = useState(false)
  const [showHours, setShowHours] = useState(false)
  const [showLocations, setShowLocations] = useState(false)

  return (
    <div css={bottomStyle}>
      <div onClick={() => setShowPhone(!showPhone)} className='linkWrapper'>
        <p>Call Store</p>
        <img
          className={showPhone ? 'arrowOpen' : 'arrow'}
          alt='arrow'
          src={arrow}
        />
      </div>
      {showPhone && <Phone />}
      <div onClick={() => setShowHours(!showHours)} className='linkWrapper'>
        <p>Opening Hours</p>
        <img
          className={showHours ? 'arrowOpen' : 'arrow'}
          alt='arrow'
          src={arrow}
        />
      </div>
      {showHours && <Hours />}
      <div
        onClick={() => setShowLocations(!showLocations)}
        className='linkWrapper'
      >
        <p>My Locations</p>
        <img
          className={showLocations ? 'arrowOpen' : 'arrow'}
          alt='arrow'
          src={arrow}
        />
      </div>
      {showLocations && <Locations setOpenMenu={setOpenMenu} />}
    </div>
  )
}

const bottomStyle = {
  '.linkWrapper': {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '1rem 2rem',
    width: '100%',
    cursor: 'pointer',
    color: 'inherit',
    textDecoration: 'none',
    fontSize: '1.2rem',
    fontWeight: 300,
    letterSpacing: '0px',
    img: {
      transition: 'all 200ms ease-in-out',
    },
    '.arrow': {
      transform: 'rotate(180deg)',
    },
    '.arrowOpen': {
      transform: 'rotate(270deg)',
    },
  },
}
