/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useState } from 'react'

import UserContext from '../../../../../../contexts/userContext'

import location from '../../../../../../resources/locationBlack.svg'
import arrow from '../../../../../admin/pictures/arrow.svg'

export default function MobileLocations({ setOpenMenu }) {
  const context = useContext(UserContext)

  const regions = [
    {
      _id: 'AUS',
      name: 'Austria',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'BELG',
      name: 'Belgium',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'BOHE',
      name: 'Bosnia & Herzegovina',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'CRO',
      name: 'Croatia',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'CZR',
      name: 'Czech Republic',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'DEN',
      name: 'Denmark',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'ESTO',
      name: 'Estonia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'FIN',
      name: 'Finland',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Mainland',
    },
    {
      _id: 'FR',
      name: 'France',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'GER',
      name: 'Germany',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'HUNG',
      name: 'Hungary',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'ITA',
      name: 'Italy',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'LATV',
      name: 'Latvia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'LUX',
      name: 'Luxemburg',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'NETH',
      name: 'Netherlands',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'NOR',
      name: 'Norway',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'POL',
      name: 'Poland',
      coin: 'GBP',
      shippingRate: 25,
      situation: 'Mainland',
    },
    {
      _id: 'PORT',
      name: 'Portugal',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Mainland',
    },
    {
      _id: 'IRE',
      name: 'Republic of Ireland',
      coin: 'GBP',
      shippingRate: 7.5,
      situation: 'Ireland',
    },
    {
      _id: 'SERB',
      name: 'Serbia/Montenegre',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
    {
      _id: 'SLOVK',
      name: 'Slovakia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SLOVE',
      name: 'Slovenia',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SP',
      name: 'Spain',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SW',
      name: 'Sweden',
      coin: 'GBP',
      shippingRate: 27.5,
      situation: 'Mainland',
    },
    {
      _id: 'SWITZ',
      name: 'Switzwerland',
      coin: 'GBP',
      shippingRate: 40,
      situation: 'Mainland',
    },
  ]

  const UKregions = [
    {
      _id: 'ENG',
      name: 'England',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Britain',
    },
    {
      _id: 'NIRE',
      name: 'Northern Ireland',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Ireland',
    },
    {
      _id: 'SCOT',
      name: 'Scotland',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Britain',
    },
    {
      _id: 'WAL',
      name: 'Wales',
      coin: 'GBP',
      shippingRate: 10,
      situation: 'Britain',
    },
    {
      _id: 'SCOTIS',
      name: 'Scottish Islands',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'MAN',
      name: 'Isle of Man',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'WIGHT',
      name: 'Isle of Wight',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'CHAN',
      name: 'Chanel Islands',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
    {
      _id: 'SCILLY',
      name: 'Isles of Scilly',
      coin: 'GBP',
      shippingRate: 30,
      situation: 'Britain',
    },
  ]

  const [showUKLocations, setShowUKLocations] = useState(false)

  const resetRegion = () => {
    context.toggleRegion({
      _id: 'NoRegion',
      name: 'Location',
      coin: 'GBP',
      shippingRate: 'N/A',
      situation: 'N/A',
    })
  }

  return (
    <div css={main}>
      {regions.map((region) => {
        if (region._id === 'UK') {
          return (
            <div
              key={region._id}
              onClick={() => setShowUKLocations(!showUKLocations)}
              className='linkWrapper'
            >
              <p
                style={{
                  color:
                    context.user.region?.situation === 'Britain' && '#FBA586',
                }}
              >
                {region.name}
              </p>
              <img
                className={showUKLocations ? 'arrowOpen' : 'arrow'}
                alt='arrow'
                src={arrow}
              />
            </div>
          )
        } else
          return (
            <div
              key={region._id}
              className='locationWrapper'
              onClick={() => {
                context.toggleRegion(region)
                setOpenMenu(false)
              }}
            >
              <p
                style={{
                  color: context.user.region?.name === region.name && '#FBA586',
                }}
              >
                {region.name}
              </p>
              {context.user.region?.name === region.name && (
                <img src={location} css={locationStyle} alt='location' />
              )}
            </div>
          )
      })}
      {showUKLocations &&
        UKregions.map((region) => {
          return (
            <div
              key={region._id}
              className='locationWrapper'
              onClick={() => {
                context.toggleRegion(region)
                setOpenMenu(false)
              }}
            >
              <p
                style={{
                  color: context.user.region?.name === region.name && '#FBA586',
                }}
              >
                {region.name}
              </p>
              {context.user.region?.name === region.name && (
                <img src={location} css={locationStyle} alt='location' />
              )}
            </div>
          )
        })}
      <div
        onClick={() => {
          resetRegion()
          setOpenMenu(false)
        }}
        className='locationWrapper'
      >
        <p>Shop without region</p>
      </div>
    </div>
  )
}

const main = {
  padding: '0 2rem',
  fontSize: '1.1rem',
  fontWeight: 300,
  letterSpacing: '0px',
  '.locationWrapper': {
    margin: '1rem 0 2rem',
    display: 'flex',
    justifyContent: 'space-between',
    cursor: 'pointer',
    ':hover': {
      color: '#FBA586',
    },
  },
  '.linkWrapper': {
    margin: '1rem 0 2rem',
    display: 'flex',
    fontSize: '1.1rem',
    justifyContent: 'space-between',
    padding: 0,
    cursor: 'pointer',
    letterSpacing: '0px',
    img: {
      transition: 'all 200ms ease-in-out',
    },
    '.arrow': {
      transform: 'rotate(180deg)',
    },
    '.arrowOpen': {
      transform: 'rotate(270deg)',
    },
  },
}

const locationStyle = {
  width: '20px',
  filter:
    'invert(61%) sepia(71%) saturate(338%) hue-rotate(324deg) brightness(108%) contrast(97%)',
}
