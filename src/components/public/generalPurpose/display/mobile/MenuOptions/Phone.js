/** @jsx jsx */
import { jsx } from '@emotion/react'

export default function Phone() {
  return (
    <div css={main}>
      <a href='tel:+44-28-90-370-390'>Call +44 28 90 370 390</a>
    </div>
  )
}

const main = {
  padding: '1rem 2rem 2rem',
  fontSize: '1.1rem',
  fontWeight: 300,
  letterSpacing: '0px',
  a: {
    cursor: 'pointer',
    color: 'inherit',
    ':hover': {
      color: '#FBA586',
    },
  },
}
