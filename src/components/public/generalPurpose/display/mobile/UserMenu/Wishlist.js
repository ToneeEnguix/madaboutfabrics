/** @jsx jsx */
import { jsx } from '@emotion/react'

import { URL } from '../../../../../../config'
import { Link } from 'react-router-dom'

const show = {
  padding: '0 2rem',
  cursor: 'default',
  h2: {
    fontFamily: 'cooper-black-std, serif',
    fontSize: '1.7rem',
    letterSpacing: '-1px',
  },
  '.buttonWrapper': {
    textDecoration: 'none',
    backgroundColor: '#FBA586',
    color: 'white',
    width: '100%',
    fontSize: '.9rem',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: 500,
    borderRadius: '6px',
    padding: '.8rem',
    border: '3px solid #FBA586',
    margin: '1rem 0',
  },
}

const itemWrapper = {
  display: 'flex',
  alignItems: 'center',
  width: '100%',
  justifyContent: 'space-between',
  fontWeight: '300',
  marginTop: '1rem',
  i: {
    cursor: 'pointer',
  },
}

const square = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '60px',
  width: '60px',
  borderRadius: '100%',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const center = {
  fontSize: '1.2em',
  width: '50%',
  paddingLeft: '1em',
  div: {
    padding: '0.2rem 0 0.2rem 0',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '.9rem',
  },
  '.name': {
    fontWeight: 'bold',
    letterSpacing: '-1px',
  },
}

const centerRow = {
  display: 'flex',
  alignItems: 'center',
}

const roundButton = {
  borderRadius: '100%',
  width: '4em',
  height: '4em',
  boxShadow: '0px 3px 6px 0px rgba(235,235,235,1)',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  margin: '0 0 0 1rem',
  cursor: 'pointer',
  transition: 'all linear 200ms',
  ':hover': {
    backgroundColor: '#e7e7e7',
  },
}

const noItems = {
  color: '#9D9D9D',
  fontSize: '0.8rem',
  margin: '1rem 0',
}

const resize = {
  fontSize: '24px !important',
}

export default function Wishlist(props) {
  const handleClick = (e) => {
    e.stopPropagation()
  }

  const moveToCart = async (item) => {
    if (item.range.inStock > 0) {
      await props.removeFromWishlist(item.range, item.variantIndex)
      props.addToCart(item.range, item.variantIndex, 1)
    } else {
      alert('Item currently out of stock')
    }
  }

  return (
    <div
      css={show}
      onMouseLeave={props.toggleWishList}
      onClick={(e) => handleClick(e)}
    >
      <h2>My Wishlist</h2>
      {props.wishlist.length === 0 ? (
        <div css={noItems}>No items added to the wishlist.</div>
      ) : (
        <div>
          {props.wishlist.slice(0, 5).map((item, i) => {
            return (
              <div key={i} css={itemWrapper}>
                <Link
                  onClick={props.toggleWishList}
                  to={{
                    pathname: item.range?.url
                      ? `/productdetails/${item.range.url}/${i}`
                      : `/productdetails/${item.range._id}/${i}`,
                    state: {
                      item: item.range,
                      index: i,
                    },
                  }}
                >
                  <div css={square}>
                    <img
                      src={`${URL}/assets/${
                        item.range.variants[item.variantIndex].imageURLs[0]
                          .filename
                      }`}
                      alt='product'
                    />
                  </div>
                </Link>

                <div css={center}>
                  <div>{item.range.name}</div>
                  <div className='name upper'>
                    {item.range.variants[item.variantIndex].color}
                  </div>
                </div>
                <div css={centerRow}>
                  <i
                    css={resize}
                    onClick={() => {
                      props.removeFromWishlist(item)
                    }}
                    className='material-icons'
                  >
                    delete_forever
                  </i>
                  <div css={roundButton} onClick={() => moveToCart(item)}>
                    <i className='material-icons'>add_shopping_cart</i>
                  </div>
                </div>
              </div>
            )
          })}
          <Link
            className='buttonWrapper flexCenter upper'
            to='/userdashboard/wishlist'
          >
            SEE ALL
          </Link>
        </div>
      )}
    </div>
  )
}
