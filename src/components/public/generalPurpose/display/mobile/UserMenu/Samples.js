/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, useEffect } from 'react'
import { keyframes } from '@emotion/react/'
import axios from 'axios'
import { Link } from 'react-router-dom'

import { URL } from '../../../../../../config'

const show = {
  padding: '0rem 2rem',
  cursor: 'default',
  h2: {
    fontFamily: 'cooper-black-std, serif',
    fontSize: '1.7rem',
    letterSpacing: '-1px',
  },
}

const square = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '60px',
  width: '60px',
  borderRadius: '100%',
  marginRight: '1rem',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const center = {
  fontSize: '1.2em',
  div: {
    padding: '0.2rem 0 0.2rem 0',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '.9rem',
  },
  '.name': {
    fontWeight: 'bold',
    letterSpacing: '-1px',
  },
}

const centerRow = {
  display: 'flex',
  justifyContent: 'flex-end',
  i: {
    fontSize: '28px',
  },
}

const noItems = {
  color: '#9D9D9D',
  fontSize: '0.8rem',
  margin: '1rem 0',
}

const requestBtnStyle = {
  width: '100%',
  fontSize: '.9rem',
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: 500,
  borderRadius: '6px',
  cursor: 'pointer',
  padding: '.8rem',
  border: '2px solid black',
  textTransform: 'uppercase',
  backgroundColor: '#fff',
  margin: '1rem 0',
}

const disabledButton = {
  outline: 'none',
  backgroundColor: 'gray',
  boxShadow: '0px 3px 6px #00000029',
  borderRadius: '100px',
  border: '1px solid transparent',
  fontWeight: 'bold',
  fontFamily: 'Montserrat,sans-serif',
  textTransform: 'uppercase',
  padding: '0.9rem 20%',
  margin: '2rem 0 -0.5rem',
  transition: 'all linear 150ms',
  cursor: 'not-allowed',
  ':hover': {
    color: '#000',
    border: '1px solid #000',
  },
}

const bounce = keyframes`
    from  {
      opacity:1
    }
  
    to {opacity:0}
    }`

export default function Samples(props) {
  const [animation, setAnimation] = useState('none')
  const [samplesLength, setSamplesLength] = useState(0)

  useEffect(() => {
    const getSamplesLenght = async () => {
      try {
        const res = await axios.get(
          `${URL}/samples/getlength/${props.user._id}`
        )
        setSamplesLength(res.data.samplesLength)
      } catch (err) {
        console.error(err)
      }
    }
    props.user._id && getSamplesLenght()
  }, [props])

  const removeFromSampleCart = (item) => {
    setAnimation(`${bounce} 0.2s ease 1`)
    setTimeout(() => {
      props.removeFromSampleCart(item)
    }, 200)
  }

  const sampleCart = props.user.sampleCart

  const itemStyle = {
    display: 'grid',
    gridTemplateColumns: '1fr 3fr 1fr',
    alignItems: 'center',
    width: '100%',
    margin: '1rem 0 0',
    fontWeight: '300',
    animation: animation,
    i: {
      cursor: 'pointer',
    },
  }

  return (
    <div css={show}>
      <div>
        <h2>My Samples</h2>
        {sampleCart.length === 0 ? (
          <div css={noItems}>No items added to the sample cart.</div>
        ) : (
          <React.Fragment>
            {sampleCart.map((item, i) => {
              return (
                <div key={i} css={itemStyle}>
                  <Link
                    to={`${
                      item.range.url
                        ? `/productdetails/${item.range.url}/${item.variantIndex}`
                        : `/productdetails/${item.range._id}/${item.variantIndex}`
                    }`}
                    css={square}
                  >
                    <img
                      alt='product'
                      src={`${URL}/assets/${
                        item.range.variants[item.variantIndex].imageURLs[0]
                          .filename
                      }`}
                    />
                  </Link>
                  <div css={center}>
                    <div>{item.range.name}</div>
                    <div className='upper name'>
                      {item.range.variants[item.variantIndex].color}
                    </div>
                  </div>
                  <div css={centerRow}>
                    <i
                      onClick={() => removeFromSampleCart(item)}
                      className='material-icons'
                    >
                      delete_forever
                    </i>
                  </div>
                </div>
              )
            })}
            {props.user._id ? (
              samplesLength + sampleCart.length <= 6 ? (
                <button
                  css={requestBtnStyle}
                  className='add'
                  onClick={() => (window.location.href = '/samplecheckout')}
                >
                  Order Samples
                </button>
              ) : (
                <button css={disabledButton}>Max 6 samples/user</button>
              )
            ) : (
              <button css={disabledButton}>Log in to order samples</button>
            )}
          </React.Fragment>
        )}
      </div>
    </div>
  )
}
