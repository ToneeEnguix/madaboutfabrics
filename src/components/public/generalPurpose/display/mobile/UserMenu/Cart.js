/** @jsx jsx */
import { jsx } from '@emotion/react'
import { keyframes } from '@emotion/react/'
import { useState } from 'react'
import { Link } from 'react-router-dom'

import { URL } from '../../../../../../config'

const show = {
  padding: '0 2rem',
  fontSize: '1.1rem',
  fontWeight: '300',
  cursor: 'default',
  h2: {
    fontFamily: 'cooper-black-std, serif',
    letterSpacing: '-1px',
  },
}

const noitems = {
  margin: '1rem 0 1rem',
  fontSize: '0.8rem',
  color: 'grey',
}

const buttonAlignment = {
  marginTop: '1.5rem',
  a: {
    display: 'block',
    margin: '1rem auto',
    padding: '.8rem',
    color: 'inherit',
    fontSize: '.9rem',
    textAlign: 'center',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: 500,
    borderRadius: '6px',
    cursor: 'pointer',
    border: '2px solid black',
    textDecoration: 'none',
  },
  '.orangeButton': {
    border: '2px solid #EFA88C',
    color: 'white',
    backgroundColor: '#EFA88C',
  },
}

export default function Cart({ cart, remove }) {
  return (
    <div css={show}>
      <h2>Shopping Bag</h2>
      {cart.length === 0 ? (
        <div css={noitems}>No items added to the cart list.</div>
      ) : (
        <div>
          {cart.map((item, index) => {
            return <CartItem remove={remove} item={item} key={index} />
          })}
          <div css={buttonAlignment}>
            <Link to='/checkout'>CHECKOUT NOW</Link>
            <Link className='orangeButton' to='/userdashboard/cart'>
              EDIT ORDER
            </Link>
          </div>
        </div>
      )}
    </div>
  )
}

const CartItem = ({ remove, item, index }) => {
  const fadeOut = keyframes`from  {
        opacity:1
      }
    
      to {opacity:0}
      }`

  const [animation, setAnimation] = useState('none')

  const removeItem = (range, variantIndex) => {
    setAnimation(`${fadeOut} 0.5s ease 1`)
    setTimeout(() => {
      remove(range, variantIndex)
      setAnimation('none')
    }, 400)
  }

  const itemWrapper = {
    display: 'grid',
    gridTemplateColumns: '1fr 4fr 1fr',
    alignItems: 'center',
    animation: animation,
    marginTop: '1rem',
  }

  return (
    <div css={itemWrapper}>
      <Link
        to={
          item?.range?.url
            ? `/productdetails/${item?.range?.url}/${item.variantIndex}`
            : `/productdetails/${item?.range?._id}/${item.variantIndex}`
        }
        css={circleWrapper}
      >
        <img
          alt={'product thumbnail'}
          src={`${URL}/assets/${
            item.range?.variants[item.variantIndex].imageURLs[0].filename
          }`}
        />
      </Link>
      <div css={center}>
        <div>{item.amount} metres</div>
        <div className='upper name'>
          {item.range?.name} {item.range?.variants[item.variantIndex].color}
        </div>
        <div>£{(item.amount * item.range?.price).toFixed(2)}</div>
      </div>
      <div css={centerRow}>
        <i
          onClick={(e) => {
            removeItem(item.range, item.variantIndex)
            e.stopPropagation()
          }}
          className='material-icons pointer'
        >
          delete_forever
        </i>
      </div>
    </div>
  )
}

const circleWrapper = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '60px',
  width: '60px',
  borderRadius: '100%',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const center = {
  marginLeft: '1rem',
  div: {
    padding: '0.2rem 0 0.2rem 0',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '.9rem',
  },
  '.name': {
    fontWeight: 'bold',
    letterSpacing: '-1px',
  },
}

const centerRow = {
  display: 'flex',
  alignItems: 'center',
  justifySelf: 'flex-end',
  i: {
    fontSize: '30px',
  },
}
