/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useState } from 'react'
import { Link } from 'react-router-dom'

import UserContext from '../../../../../../contexts/userContext'

import arrow from '../../../../../admin/pictures/arrow.svg'
import Cart from './Cart'
import Samples from './Samples'
import Wishlist from './Wishlist'

export default function Bottom() {
  const context = useContext(UserContext)

  const [showCart, setShowCart] = useState(false)
  const [showSamples, setShowSamples] = useState(false)
  const [showWishlist, setShowWishlist] = useState(false)

  return (
    <div css={bottomStyle}>
      <div onClick={() => setShowCart(!showCart)} className='linkWrapper'>
        <p>My Bag</p>
        <img
          className={showCart ? 'arrowOpen' : 'arrow'}
          alt='arrow'
          src={arrow}
        />
      </div>
      {showCart && (
        <Cart remove={context.removeFromCart} cart={context.user.cart} />
      )}
      <div onClick={() => setShowSamples(!showSamples)} className='linkWrapper'>
        <p>My Samples</p>
        <img
          className={showSamples ? 'arrowOpen' : 'arrow'}
          alt='arrow'
          src={arrow}
        />
      </div>
      {showSamples && (
        <Samples
          user={context.user}
          removeFromSampleCart={context.removeFromSampleCart}
        />
      )}
      <div
        onClick={() => setShowWishlist(!showWishlist)}
        className='linkWrapper'
      >
        <p>My Wishlist</p>
        <img
          className={showWishlist ? 'arrowOpen' : 'arrow'}
          alt='arrow'
          src={arrow}
        />
      </div>
      {showWishlist && (
        <Wishlist
          wishlist={context.user.wishlist}
          removeFromWishlist={context.removeFromWishlist}
          addToCart={context.addToCart}
        />
      )}
      <Link to='/userdashboard/' className='linkWrapper'>
        <p>My Orders</p>
        <img className='arrow' alt='arrow' src={arrow} />
      </Link>
    </div>
  )
}

const bottomStyle = {
  '.linkWrapper': {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '1rem 2rem',
    width: '100%',
    cursor: 'pointer',
    color: 'inherit',
    textDecoration: 'none',
    fontSize: '1.2rem',
    fontWeight: 300,
    letterSpacing: '0px',
    img: {
      transition: 'all 200ms ease-in-out',
    },
    '.arrow': {
      transform: 'rotate(180deg)',
    },
    '.arrowOpen': {
      transform: 'rotate(270deg)',
    },
  },
}
