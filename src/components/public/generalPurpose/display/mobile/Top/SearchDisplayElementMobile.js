/** @jsx jsx */
import { jsx } from '@emotion/react/'
import { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const wrapper = mq({
  display: 'block',
  'textarea:focus, input:focus': {
    outline: 'none',
  },
})

const inputWrapper = {
  display: 'flex',
  alignItems: 'center',
  boxShadow: 'inset 2px 2px 5px #BABECC, inset -5px -5px 10px #FFFF',
  borderRadius: '100px',
  padding: '0 1.5rem 0 2rem',
  height: '60px',
  i: {
    cursor: 'pointer',
  },
}

const input = {
  width: '100%',
  border: 'none',
  fontSize: '1.4rem',
  '::placeholder': {
    fontSize: '1.2rem',
  },
  '&:focus': {
    boxShadow:
      'inset 1px 1px 2px $color-shadow, inset -1px -1px 2px $color-white',
  },
}

export default function SearchDisplayElement({ setOpenMenu }) {
  const [search, setSearch] = useState('')
  const [redirect, setRedirect] = useState(false)

  useEffect(() => {
    setRedirect(false)
  }, [redirect])

  const handleChange = (e) => {
    setSearch(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    e.stopPropagation()
    setRedirect(true)
  }

  if (redirect) {
    return (
      <Redirect
        to={{
          pathname: `/catalogue`,
          state: {
            search: search,
          },
        }}
      />
    )
  } else {
    return (
      <form css={wrapper} onSubmit={handleSubmit}>
        <div css={inputWrapper}>
          <input
            onChange={handleChange}
            value={search}
            name='search'
            type='text'
            placeholder='Search fabrics...'
            css={input}
            autoComplete='off'
          />
          <i className='material-icons' onClick={handleSubmit}>
            search
          </i>
        </div>
      </form>
    )
  }
}
