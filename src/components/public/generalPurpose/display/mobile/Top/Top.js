/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import facepaint from 'facepaint'
import { Link } from 'react-router-dom'

import cross from '../../../../../../resources/closeBlack.svg'
import Login from './Login'
import Signup from './Signup'
import SearchDisplayElementMobile from './SearchDisplayElementMobile'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function Top({ setOpenMenu, context }) {
  const [openLogin, setOpenLogin] = useState(false)
  const [openSignup, setOpenSignup] = useState(false)

  useEffect(() => {
    if (context.user._id) {
      setOpenLogin(false)
      setOpenSignup(false)
    }
  }, [context.user._id])

  return (
    <div style={{ padding: '0 1rem 1rem' }}>
      <div css={topWrapper}>
        <div className='topLeft'>
          <img
            onClick={() => setOpenMenu(false)}
            alt='close menu'
            src={cross}
            className='cross'
          />
        </div>
        <div className='topRight'>
          {context.user._id ? (
            <Link to='/userdashboard'>{context.user.name}</Link>
          ) : (
            <p onClick={() => setOpenLogin(!openLogin)}>Sign in</p>
          )}
          <i className='material-icons #15D032'>account_circle</i>
        </div>
      </div>
      {openLogin && (
        <Login
          openSignup={openSignup}
          setOpenSignup={setOpenSignup}
          toggleUser={context.toggleUser}
        />
      )}
      {openSignup && <Signup toggleUser={context.toggleUser} />}
      <SearchDisplayElementMobile setOpenMenu={setOpenMenu} />
    </div>
  )
}

const topWrapper = mq({
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  height: '84px',
  '.topLeft, .topRight': {
    display: 'flex',
    alignItems: 'center',
  },
  '.topLeft': {
    fontSize: '1.5rem',
    '.cross': { cursor: 'pointer', marginRight: '5%' },
  },
  '.topRight': {
    i: { fontSize: '1.5rem', marginLeft: ['.5rem', '1rem'] },
    'p, a': {
      fontSize: '1.1rem',
      fontFamily: 'Roboto, sans-serif',
      fontWeight: 300,
      cursor: 'pointer',
      whiteSpace: 'nowrap',
    },
    a: { color: 'inherit', textDecoration: 'none' },
  },
})
