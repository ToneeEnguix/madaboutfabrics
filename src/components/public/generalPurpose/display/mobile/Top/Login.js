/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

import { URL } from '../../../../../../config'

export default function Login({ setOpenSignup, openSignup, toggleUser }) {
  const [message, setMessage] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    if (message) {
      setTimeout(() => {
        setMessage('')
      }, 3000)
    }
  }, [message])

  const handleSubmit = async (e) => {
    e.preventDefault()
    try {
      const res = await axios.post(`${URL}/users/signin`, { email, password })
      if (!res.data.ok) {
        setMessage('Incorrect Credentials')
      } else {
        localStorage.setItem('token', res.data.token)
        toggleUser(res.data.userData)
      }
    } catch (err) {
      console.error(err)
    }
  }

  return (
    <div css={loginStyle}>
      <p className='upper welcome'>welcome back</p>
      <form onSubmit={handleSubmit}>
        <input
          onChange={(e) => setEmail(e.target.value)}
          type='email'
          name='email'
          placeholder='Email'
        />
        <input
          onChange={(e) => setPassword(e.target.value)}
          type='password'
          name='password'
          placeholder='Password'
        />
        {message && <p className='message'>{message}</p>}
        <button onClick={handleSubmit} className='upper signin'>
          Sign In
        </button>
      </form>
      <Link to='/userdashboard/password' className='forgot'>
        Forgot Password?
      </Link>
      {!openSignup && (
        <button
          onClick={() => setOpenSignup(!openSignup)}
          className='upper create'
        >
          create new account
        </button>
      )}
    </div>
  )
}

const loginStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '1.5rem',
  padding: '0 1rem',
  '.welcome': {
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '1rem',
    fontWeight: 500,
    marginBottom: '1rem',
  },
  input: {
    width: '100%',
    padding: '1rem',
    margin: '.5rem 0',
    fontSize: '1.1rem',
    '::placeholder': {
      fontSize: '1rem',
    },
  },
  button: {
    width: '100%',
    fontSize: '1rem',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: 500,
    borderRadius: '6px',
    cursor: 'pointer',
    padding: '.9rem',
    border: '2px solid black',
  },
  '.message': {
    color: 'red',
    marginTop: '1rem',
  },
  '.signin': {
    backgroundColor: 'black',
    color: 'white',
    marginTop: '1.5rem',
  },
  '.forgot': {
    alignSelf: 'flex-start',
    fontSize: '1rem',
    fontWeight: 100,
    margin: '1.8rem 1rem',
    letterSpacing: '0px',
    whiteSpace: 'nowrap',
    color: 'inherit',
  },
  '.create': {
    backgroundColor: 'white',
    color: 'inherit',
    marginBottom: '2rem',
  },
}
