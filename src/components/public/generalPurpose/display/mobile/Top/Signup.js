/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'

import { URL } from '../../../../../../config'

export default function Signup({ toggleUser }) {
  const [message, setMessage] = useState('')
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  useEffect(() => {
    if (message) {
      setTimeout(() => {
        setMessage('')
      }, 3000)
    }
  }, [message])

  const handleSubmit = async (e) => {
    e.preventDefault()
    if (password.length < 8) {
      setMessage('Password must be at least 8 characters')
    } else {
      try {
        const res = await axios.post(`${URL}/users/signup`, {
          name,
          email,
          password,
        })
        if (!res.data.ok) {
          setMessage(res.data.message)
        } else {
          localStorage.setItem('token', res.data.token)
          toggleUser(res.data.userData)
        }
      } catch (err) {
        console.error(err)
      }
    }
  }

  return (
    <div css={signupStyle}>
      <p className='upper welcome'>create new account</p>
      <form onSubmit={handleSubmit}>
        <input
          onChange={(e) => setName(e.target.value)}
          type='text'
          name='username'
          placeholder='Pick a username'
        />
        <input
          onChange={(e) => setEmail(e.target.value)}
          type='email'
          name='email'
          placeholder='Email'
        />
        <input
          onChange={(e) => setPassword(e.target.value)}
          type='password'
          name='password'
          placeholder='Password'
        />
        {message && <p className='message'>{message}</p>}
        <button className='upper create'>create new account</button>
      </form>
      <div className='terms'>
        <span>By registering, you agree to our </span>
        <Link to='/more'> terms and conditions</Link>
      </div>
    </div>
  )
}

const signupStyle = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '1.5rem',
  padding: '0 1rem',
  '.welcome': {
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '1rem',
    fontWeight: 500,
    marginBottom: '1rem',
  },
  input: {
    width: '100%',
    padding: '1rem',
    margin: '.5rem 0',
    fontSize: '1.1rem',
    '::placeholder': {
      fontSize: '1rem',
    },
  },
  button: {
    width: '100%',
    fontSize: '1rem',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: 500,
    borderRadius: '6px',
    cursor: 'pointer',
    padding: '.8rem',
    border: '5px solid black',
  },
  '.message': {
    color: 'red',
    margin: '1rem 0 .5rem',
  },
  '.create': {
    backgroundColor: 'white',
    margin: '1rem 0',
    color: 'black',
  },
  '.terms': {
    marginBottom: '2rem',
  },
  '.terms span, .terms a': {
    fontSize: '1rem',
    color: 'inherit',
  },
}
