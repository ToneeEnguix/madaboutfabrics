/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'

import arrow from '../../../../admin/pictures/arrow.svg'

export default function Bottom() {
  return (
    <div css={bottomStyle}>
      <Link to='/catalogue' className='linkWrapper'>
        <p>Look at all fabrics</p>
        <img alt='arrow' src={arrow} />
      </Link>
      <Link to='/new' className='linkWrapper'>
        <p>Look at new fabrics</p>
        <img alt='arrow' src={arrow} />
      </Link>
      <Link to='/ranges' className='linkWrapper'>
        <p>Look at fabric ranges</p>
        <img alt='arrow' src={arrow} />
      </Link>
      <Link to='/accessories' className='linkWrapper'>
        <p>View accessories</p>
        <img alt='arrow' src={arrow} />
      </Link>
    </div>
  )
}

const bottomStyle = {
  '.linkWrapper': {
    display: 'flex',
    justifyContent: 'space-between',
    padding: '1rem 2rem',
    width: '100%',
    color: 'inherit',
    textDecoration: 'none',
    p: {
      fontSize: '1.2rem',
      fontWeight: 300,
      letterSpacing: '0px',
    },
    img: {
      transform: 'rotate(180deg)',
    },
  },
}
