/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import { withRouter } from 'react-router-dom'

const style = {
  display: 'grid',
  height: '84px',
  alignItems: 'center',
  textAlign: 'center',
  boxShadow: '0px 3px 6px #00000029',
  '.titleStyle': {
    textAlign: 'center',
    fontFamily: 'cooper-black-std, serif',
    fontSize: '1.8rem',
    letterSpacing: '-1px',
  },
  a: {
    color: 'inherit',
    textDecoration: 'inherit',
  },
}

function ShortDisplay(props) {
  return (
    <header>
      <div css={style}>
        <Link className='titleStyle' to='/home'>
          Mad About Fabrics
        </Link>
      </div>
    </header>
  )
}

export default withRouter(ShortDisplay)
