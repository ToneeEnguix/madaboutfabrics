/** @jsx jsx */
import { jsx } from '@emotion/react'
import axios from 'axios'
import facepaint from 'facepaint'
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'

import { URL } from '../../../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [550]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  width: ['100vw', '28vw'],
  minWidth: [0, '410px'],
  position: 'absolute',
  top: '84px',
  right: [0, '10%'],
  zIndex: '30000000',
  backgroundColor: 'white',
  color: 'black',
  cursor: 'context-menu',
  padding: '2.65rem 0 3rem',
  boxShadow: '0px 3px 6px #00000029',
  borderRadius: ['0 0 30px 30px', 0],
})

const column = {
  display: 'flex',
  flexDirection: 'column',
  h2: {
    fontFamily: 'cooper-black-std, serif',
    color: 'black',
    fontSize: '1.8rem',
    letterSpacing: '-1px',
    margin: '0 auto 2px',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: '1em',
  },
  h3: {
    alignSelf: 'center',
    fontFamily: 'Montserrat,serif',
    marginBottom: '2.6rem',
    fontSize: '1.1em',
  },
  input: {
    width: '70%',
    display: 'block',
    position: 'relative',
    background: 'none',
    padding: '.9rem 0.7rem',
    border: '1px solid lightGrey',
    zIndex: '2',
    marginTop: '.5rem',
  },
  'input:focus, input:valid': {
    outline: 'none',
  },
  '.customInput::-webkit-input-placeholder': {
    fontWeight: '300',
  },
  a: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto,serif',
    color: 'blue',
    fontSize: '1em',
  },
  button: {
    marginTop: '1rem',
    padding: '1.2em',
    width: '70%',
    boxShadow: '0px 3px 6px 0px rgba(235,235,235,1)',
    borderRadius: '8px',
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'Montserrat,sans-serif',
    backgroundColor: '#00263E',
    cursor: 'pointer',
    border: '2px solid transparent',
    transition: 'all linear 150ms',
  },
  'button:hover': {
    backgroundColor: 'white',
    border: '2px solid #00263E',
    color: '#00263E',
  },
}

const centerText = {
  width: '70%',
  marginTop: '0.5rem',
  a: {
    letterSpacing: '.5px',
    color: '#4990E2',
    fontWeight: '300',
    cursor: 'not-allowed',
  },
  'a:hover': {
    fontWeight: '400',
  },
  div: {
    fontFamily: 'Roboto, sans-serif !important',
    marginTop: '0.5rem',
    color: 'grey',
    textAlign: 'justify',
    lineHeight: '1rem',
  },
}

const joinUs = {
  marginTop: '1rem',
  fontFamily: 'Roboto, sans-serif !important',
  span: {
    color: 'gray',
  },
  p: {
    display: 'inline',
    letterSpacing: '.5px',
    color: 'black',
    fontWeight: '300',
    textDecoration: 'underline',
    fontSize: '0.8rem',
    cursor: 'pointer',
  },
  'p:hover': {
    color: '#FBA586',
  },
}

export default function SignInMenuRight(props) {
  const [message, setMessage] = useState({ text: '', color: '' })

  useEffect(() => {
    message.text &&
      setTimeout(() => {
        setMessage({ text: '', color: '' })
      }, 2000)
  }, [message])

  const handleSubmit = async (e) => {
    e.preventDefault()
    const email = e.target.email.value
    const password = e.target.password.value
    try {
      const res = await axios.post(`${URL}/users/signin`, { email, password })
      if (!res.data.ok) {
        setMessage({
          text: 'Incorrect Credentials',
          color: 'red',
        })
      } else {
        localStorage.setItem('token', res.data.token)
        props.toggleUser(res.data.userData)
        props.toggleWishList()
      }
    } catch (err) {
      console.error(err)
    }
  }

  const messageStyle = {
    color: message.color,
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: '500',
    textAlign: 'center',
    height: '20px',
    fontSize: '0.9rem',
    marginTop: '0.5rem',
  }

  return (
    <div
      css={show}
      onMouseLeave={props.toggleWishList}
      onClick={(e) => e.stopPropagation()}
    >
      <div css={column}>
        <h2>Mad About Fabrics</h2>
        <form onSubmit={handleSubmit}>
          <h3>CHECK IN FOR WISHLIST</h3>
          <input placeholder='Email' required type='email' name='email' />
          <input
            placeholder='Password'
            required
            type='password'
            name='password'
          />
          <div css={centerText}>
            <Link to='/forgotpassword'>Forgot password?</Link>
            <h4 css={messageStyle}>{message.text}</h4>
            <div>
              By registering you agree to our <strong>Terms of Use</strong> and
              confirm that you have read and understood our{' '}
              <strong>Privacy Policy</strong>.
            </div>
          </div>
          <button type='submit'>CHECK IN</button>
          <div css={joinUs}>
            <span>Not a member?</span>
            <p
              onClick={() => {
                props.toggleWishList()
                props.toggleSignOptionsMenu()
              }}
            >
              Join us
            </p>
          </div>
        </form>
      </div>
    </div>
  )
}
