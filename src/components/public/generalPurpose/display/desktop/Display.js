/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import LeftDisplayElements from './LeftDisplayElements'
import RightDisplayElements from './RightDisplayElements'
import MobileLeftDisplayElements from '../mobile/MobileLeftDisplayElements'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 561, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const style = mq({
    display: 'grid',
    // display: ['flex', 'grid'],
    justifyContent: 'space-between',
    gridTemplateColumns: 'repeat(3, 1fr)',
    alignItems: 'center',
    width: '100vw',
    minWidth: '100vw',
    backgroundColor: 'white',
    borderBottom: '0.25px solid rgb(230,230,230)',
    height: '84px',
  }),
  logo = mq({
    color: 'black',
    textDecoration: 'none',
    fontSize: ['1.2rem', '1.5rem', '1.8rem'],
    letterSpacing: '-1px',
    marginLeft: ['1rem', 0],
    fontFamily: 'cooper-black-std, serif',
    padding: '0.5em 0',
    textAlign: 'center',
    whiteSpace: 'nowrap',
  })

export default function Display(props) {
  const [toggleSIM, setToggleSIM] = useState(false)
  // DESKTOP
  const [showSignInMenu, setShowSignInMenu] = useState(false)
  const [showWishList, setShowWishList] = useState(false)
  const [showCart, setshowCart] = useState(false)
  const [showSampleCart, setShowSampleCart] = useState(false)
  // MOBILE
  const [openMenu, setOpenMenu] = useState(false)

  useEffect(() => {
    if (openMenu) {
      setShowSignInMenu(false)
      setShowWishList(false)
      setshowCart(false)
      setShowSampleCart(false)
    }
  }, [openMenu])

  const toggleSignOptionsMenu = () => {
    setToggleSIM(true)
  }
  return (
    <header css={style}>
      <LeftDisplayElements
        {...props}
        toggleSIM={toggleSIM}
        setToggleSIM={() => {
          setToggleSIM(false)
        }}
      />
      <MobileLeftDisplayElements
        openMenu={openMenu}
        setOpenMenu={setOpenMenu}
        {...props}
      />
      <Link css={logo} to='/home'>
        Mad About Fabrics
      </Link>
      <RightDisplayElements
        {...props}
        cart={props.cart}
        toggleSignOptionsMenu={toggleSignOptionsMenu}
        setShowSignInMenu={setShowSignInMenu}
        setShowWishList={setShowWishList}
        setshowCart={setshowCart}
        setShowSampleCart={setShowSampleCart}
        showSignInMenu={showSignInMenu}
        showWishList={showWishList}
        showCart={showCart}
        showSampleCart={showSampleCart}
      />
    </header>
  )
}
