/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState, useEffect, useCallback } from 'react'
import { keyframes } from '@emotion/react/'
import axios from 'axios'
import { Link } from 'react-router-dom'

import { URL } from '../../../../../config'

const show = {
  position: 'absolute',
  top: '84px',
  right: '10%',
  zIndex: '30000000',
  width: '28vw',
  minWidth: '410px',
  backgroundColor: 'white',
  color: 'black',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '1rem 1rem 3rem',
  fontWeight: 'strong',
  boxShadow: '0px 3px 6px #00000029',
  cursor: 'default',
  h2: {
    fontFamily: 'cooper-black-std, serif',
    color: 'black',
    textDecoration: 'none',
    fontSize: '1.7rem',
    letterSpacing: '-1px',
    margin: '1rem 0 0.3rem',
  },
  h3: {
    alignSelf: 'center',
    fontFamily: 'Montserrat,serif',
    fontSize: '0.9rem',
  },
}

const reminder = {
  position: 'relative',
  top: '-1rem',
  left: '-1rem',
  backgroundColor: '#EE9D7F',
  letterSpacing: '1px',
  color: 'white',
  alignSelf: 'flex-start',
  borderRadius: '0 0 15px 0',
  padding: '0.6rem 1rem',
  boxShadow: '0px 3px 6px #00000029',
  h3: {
    fontSize: '0.7rem !important',
    fontFamily: 'Montserrat,serif',
    fontWeight: '500',
  },
}

const contentWrapper = {
  width: '100%',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '-2em',
}

const square = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '60px',
  width: '60px',
  borderRadius: '100%',
  marginRight: '1rem',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const center = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'flex-start',
  fontSize: '1.2em',
  justifySelf: 'flex-start',
  div: {
    padding: '0.2em',
    fontFamily: 'Nanum Myeongjo,serif !important',
    spaceLinig: '1px',
  },
}

const centerRow = {
  display: 'flex',
  alignItems: 'center',
  i: {
    fontSize: '28px',
  },
}

const noItems = {
  color: '#9D9D9D',
  fontWeight: '400',
  fontSize: '0.8rem',
  marginTop: '1.3rem',
}

const requestBtnStyle = {
  outline: 'none',
  backgroundColor: '#000',
  boxShadow: '0px 3px 6px #00000029',
  borderRadius: '100px',
  border: '1px solid transparent',
  fontWeight: 'bold',
  fontFamily: 'Montserrat,sans-serif',
  cursor: 'pointer',
  textTransform: 'uppercase',
  color: 'white',
  padding: '0.9rem 20%',
  margin: '2rem 0 -0.5rem',
  transition: 'all linear 150ms',
  ':hover': {
    backgroundColor: '#fff',
    color: '#000',
    border: '1px solid #000',
  },
}

const disabledButton = {
  outline: 'none',
  backgroundColor: 'black',
  color: 'white',
  boxShadow: '0px 3px 6px #00000029',
  borderRadius: '100px',
  border: '1px solid transparent',
  fontWeight: 'bold',
  fontFamily: 'Montserrat,sans-serif',
  textTransform: 'uppercase',
  padding: '0.9rem 20%',
  margin: '2rem 0 -0.5rem',
  transition: 'all linear 150ms',
  cursor: 'not-allowed',
}

const bounce = keyframes`
    from  {
      opacity:1
    }
  
    to {opacity:0}
    }`

export default function SampleCart(props) {
  const [animation, setAnimation] = useState('none')
  const [samplesLength, setSamplesLength] = useState(0)

  useEffect(() => {
    const getSamplesLenght = async () => {
      try {
        const res = await axios.get(
          `${URL}/samples/getlength/${props.user._id}`
        )
        setSamplesLength(res.data.samplesLength)
      } catch (err) {
        console.error(err)
      }
    }
    props.user._id && getSamplesLenght()
  }, [props])

  const removeFromSampleCart = (item) => {
    setAnimation(`${bounce} 0.2s ease 1`)
    setTimeout(() => {
      props.removeFromSampleCart(item)
    }, 200)
  }

  const escFunction = useCallback(
    (event) => {
      if (event.keyCode === 27) {
        props.toggleSampleCart()
      }
    },
    [props]
  )

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)
    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  const sampleCart = props.user.sampleCart

  const itemStyle = {
    display: 'grid',
    gridTemplateColumns: '1fr 3fr 1fr',
    alignItems: 'center',
    width: '100%',
    margin: '.5rem 0 0',
    fontWeight: '300',
    animation: animation,
    padding: '0 2rem',
    i: {
      cursor: 'pointer',
      margin: '1em',
    },
  }

  return (
    <div
      css={show}
      onMouseLeave={props.toggleSampleCart}
      onClick={(e) => e.stopPropagation()}
    >
      <div css={reminder}>
        <h3>YOU HAVE {6 - (sampleCart.length + samplesLength)} SAMPLES LEFT</h3>
      </div>
      <div css={contentWrapper}>
        <h2>Mad About Fabrics</h2>
        <h3>YOUR SAMPLE REQUESTS</h3>
        {sampleCart.length === 0 ? (
          <div css={noItems}>No items added to the sample cart.</div>
        ) : (
          <React.Fragment>
            {sampleCart.map((item, i) => {
              return (
                <div key={i} css={itemStyle}>
                  <Link
                    to={`${
                      item.range.url
                        ? `/productdetails/${item.range.url}/${item.variantIndex}`
                        : `/productdetails/${item.range._id}/${item.variantIndex}`
                    }`}
                    css={square}
                  >
                    <img
                      alt='product'
                      src={`${URL}/assets/${
                        item.range.variants[item.variantIndex].imageURLs[0]
                          .filename
                      }`}
                    />
                  </Link>
                  <div css={center}>
                    <div>{item.range.name}</div>
                    <div>{item.range.variants[item.variantIndex].color}</div>
                  </div>
                  <div css={centerRow}>
                    <i
                      onClick={() => removeFromSampleCart(item)}
                      className='material-icons'
                    >
                      delete_forever
                    </i>
                  </div>
                </div>
              )
            })}
            {props.user._id ? (
              samplesLength + sampleCart.length <= 6 ? (
                <button
                  css={requestBtnStyle}
                  className='add'
                  onClick={() => (window.location.href = '/samplecheckout')}
                >
                  Order Samples
                </button>
              ) : (
                <button css={disabledButton}>Max 6 samples/user</button>
              )
            ) : (
              <button css={disabledButton}>Log in to order samples</button>
            )}
          </React.Fragment>
        )}
      </div>
    </div>
  )
}
