/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'

const submenuPosition = {
  position: 'absolute',
  top: '84px',
  left: '125px',
  zIndex: '1000000000000',
  backgroundColor: 'white',
  color: 'black',
  display: 'flex',
  fontWeight: '300',
  height: 'max-content',
  width: '180px',
  boxShadow: '0px 3px 6px #00000029',
  fontFamily: 'Roboto,serif',
}

const userMenuContent = {
  listStyle: 'none',
  padding: '2rem 1rem',
  margin: '0 1.5rem',
  width: '100%',
  'a, .special_li': {
    display: 'block',
    padding: '1rem 0',
    margin: '0 px',
    width: '100%',
  },
  'li:hover': {
    fontWeight: '500',
    color: '#EE9D7F',
  },
}

export default function UserMenu(props) {
  const onMouseLeave = () => {
    props.closeUserMenu()
  }

  return (
    <div
      css={submenuPosition}
      onClick={onMouseLeave}
      onMouseLeave={onMouseLeave}
    >
      <ul css={userMenuContent}>
        <li>
          <Link to='/userdashboard/cart'>Cart</Link>
        </li>
        <li>
          <Link to='/userdashboard/'>Orders</Link>
        </li>
        <li>
          <Link to='/userdashboard/samples'>Samples</Link>
        </li>
        <li>
          <Link to='/userdashboard/wishlist'>Wishlist</Link>
        </li>
        <li>
          <Link to='/userdashboard/profile'>Profile</Link>
        </li>
        <li>
          <Link to='/userdashboard/password'>Password</Link>
        </li>
        <li>
          <Link to='/userdashboard/address'>Address</Link>
        </li>
        <li className='special_li' onClick={props.logOut}>
          Log Out
        </li>
      </ul>
    </div>
  )
}
