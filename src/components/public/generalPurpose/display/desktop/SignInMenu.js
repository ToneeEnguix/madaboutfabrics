/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import axios from 'axios'

import { URL } from '../../../../../config'

const column = {
    display: 'flex',
    flexDirection: 'column',
    width: '50%',
    button: {
      fontSize: '.9rem',
      margin: '2rem 0rem 1.5rem 0rem',
      padding: '1rem',
      width: '70%',
      boxShadow: '0px 3px 6px #00000029',
      borderRadius: '8px',
      color: 'white',
      border: '2px solid transparent',
      fontWeight: 'bold',
      fontFamily: 'Montserrat,sans-serif',
      backgroundColor: '#00263E',
      cursor: 'pointer',
      transition: 'all linear 150ms',
    },
    'button:hover': {
      backgroundColor: 'white',
      color: '#00263E',
      border: '2px solid #171717',
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      marginBottom: '1rem',
    },
    input: {
      width: '70%',
      display: 'block',
      position: 'relative',
      background: 'none',
      padding: '.9rem 0.7rem',
      border: '1px solid lightGrey',
      zIndex: '2',
      margin: '0.25em 0',
    },
    h2: {
      fontFamily: 'cooper-black-std, serif',
      color: 'black',
      fontSize: '1.4rem',
      letterSpacing: '-1px',
      padding: '0.5rem 0',
      margin: '0 auto',
    },
    h3: {
      alignSelf: 'center',
      fontFamily: 'Montserrat,serif',
      marginBottom: '2rem',
    },
    '.customInput::-webkit-input-placeholder': {
      fontWeight: '300',
    },
    a: {
      alignSelf: 'flex-start',
      fontFamily: 'Roboto,serif',
      color: 'blue',
    },
  },
  centerText = {
    width: '70%',
    marginTop: '0.5rem',
    color: 'grey',
    lineHeight: '1.3rem',
    textJustify: 'inter-word',
    textAlign: 'justify',
    a: {
      cursor: 'not-allowed !important',
    },
  }

export default function SignInMenu(props) {
  const [message, setMessage] = useState({ text: '', color: '' })

  useEffect(() => {
    message.text &&
      setTimeout(() => {
        setMessage({ text: '', color: '' })
      }, 2000)
  }, [message])

  const handleSubmit = async (e) => {
    e.preventDefault()
    const password = e.target.password.value
    const email = e.target.email.value
    try {
      const res = await axios.post(`${URL}/users/signin`, { email, password })
      if (!res.data.ok) {
        setMessage({
          text: 'Incorrect Credentials',
          color: 'red',
        })
      } else {
        localStorage.setItem('token', res.data.token)
        props.closeSignInMenu()
        props.toggleUser(res.data.userData)
      }
    } catch (err) {
      console.error(err)
    }
  }

  const messageStyle = {
    color: message.color,
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: '500',
    textAlign: 'center',
  }

  return (
    <div css={column} onClick={(e) => e.stopPropagation()}>
      <h2>Mad About Fabrics</h2>
      <form onSubmit={handleSubmit}>
        <h3>WELCOME AGAIN</h3>
        <input placeholder='Email' required type='email' name='email' />
        <input
          placeholder='Password'
          required
          type='password'
          name='password'
        />
        <div css={centerText}>
          {/* <Link
          to="/forgotpassword"
          >
            Forgot Password?
          </Link> */}
        </div>
        <button type='submit'>CHECK IN</button>
      </form>
      {message.text && <h4 css={messageStyle}>{message.text}</h4>}
    </div>
  )
}
