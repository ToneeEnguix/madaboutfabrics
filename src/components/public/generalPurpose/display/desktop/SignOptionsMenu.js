/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'

import SignInMenu from './SignInMenu'
import SignUpMenu from './SignUpMenu'

const show = {
  position: 'absolute',
  top: '84px',
  zIndex: '30000000000000',
  backgroundColor: 'white',
  color: 'black',
  width: '70vw',
  pointerEvents: 'auto',
  cursor: 'context-menu',
  boxShadow: '0px 3px 6px #00000029',
  padding: '1rem 0 0',
}
const contentWrapper = {
  display: 'flex',
}

export default class SignOptionsMenu extends React.Component {
  onMouseLeave = (e) => {
    e.stopPropagation()
    if (e.target.tagName !== 'INPUT') {
      this.props.closeSignInMenu()
    }
  }

  render() {
    return (
      <div css={show} onMouseLeave={this.onMouseLeave}>
        <div css={contentWrapper}>
          <SignInMenu
            closeSignInMenu={this.props.closeSignInMenu}
            toggleUser={this.props.toggleUser}
          />
          <SignUpMenu
            closeSignInMenu={this.props.closeSignInMenu}
            toggleUser={this.props.toggleUser}
          />
        </div>
      </div>
    )
  }
}
