/** @jsx jsx */
import { jsx } from '@emotion/react/'
import { useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const wrapper = mq({
  display: ['none', 'block'],
  'textarea:focus, input:focus': {
    outline: 'none',
  },
  "input[type='text']": {
    paddingLeft: '1rem',
  },
})

const input = {
  marginRight: '1em',
  boxShadow: 'inset 2px 2px 5px #BABECC, inset -5px -5px 10px #FFFF',
  minWidth: '220px',
  width: '12vw',
  transition: 'all 0.2s ease-in-out',
  appearance: 'none',
  border: 'none',
  borderRadius: '100px',
  padding: '0.5em 0',
  height: '40px',
  '&:focus': {
    boxShadow:
      'inset 1px 1px 2px $color-shadow, inset -1px -1px 2px $color-white',
  },
}

export default function SearchDisplayElement() {
  const [search, setSearch] = useState('')
  const [redirect, setRedirect] = useState(false)

  useEffect(() => {
    setRedirect(false)
  }, [redirect])

  const handleChange = (e) => {
    setSearch(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    e.stopPropagation()
    setRedirect(true)
  }

  if (redirect) {
    return (
      <Redirect
        to={{
          pathname: `/catalogue`,
          state: {
            search: search,
          },
        }}
      />
    )
  } else {
    return (
      <form css={wrapper} onSubmit={handleSubmit}>
        <input
          onChange={handleChange}
          value={search}
          name='search'
          type='text'
          placeholder='Search fabrics...'
          css={input}
          autoComplete='off'
        ></input>
      </form>
    )
  }
}
