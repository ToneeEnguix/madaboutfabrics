/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState, useContext } from 'react'
import facepaint from 'facepaint'
import { Link } from 'react-router-dom'

import UserContext from '../../../../../contexts/userContext'

import SignOptionsMenu from './SignOptionsMenu'
import CallSubMenu from './CallSubMenu'
import UserMenu from './UserMenu'
import HoursSubMenu from './HoursSubMenu'
import clock from '../../../../../resources/mobile/clock.png'

// RESPONSIVENESS SETTINGS
const breakpoints = [950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const desktop = mq({
  display: ['none', 'block'],
  width: '100%',
  '.locationTitle, .hoursText, .userName': {
    display: ['none', 'none', 'block'],
  },
  '.hoursImg': {
    display: ['none', 'block', 'none'],
    height: '26px',
    cursor: 'pointer',
  },
})

const flexContainer = {
  font: 'normal normal normal 14px Roboto',
  fontWeight: '300',
  display: 'flex',
  justifyContent: 'space-between',
  width: '100%',
  maxWidth: '400px',
  listStyle: 'none',
  alignItems: 'center',
  height: '100%',
  padding: '1.7em 0 1.7em 1.7em',
  a: {
    color: 'inherit',
    textDecoration: 'inherit',
    fontSize: '1em',
    letterSpacing: '0',
    height: '100%',
  },
  '.hoverEffect': {
    margin: 'auto 0.5em',
    cursor: 'pointer',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  '.hoverEffect:hover': {
    color: '#FBA586',
  },
  '.hoverEffect:after': {
    display: 'block',
    content: '""',
    position: 'relative',
    top: '2em',
    borderBottom: 'solid 6px #FBA586',
    borderRadius: '8px 8px 0px 0px',
    transform: 'scaleX(0)',
    transition: 'transform 100ms ease-in-out',
    zIndex: '100000000000000000000',
  },
  ' .hoverEffect:hover:after': {
    transform: 'scaleX(1)',
  },
  i: {
    padding: '0 0.3rem',
    fontSize: '1.6em',
  },
}

const flex = mq({
  display: 'flex',
  height: '100%',
  alignItems: 'center',
})

const centered = {
  alignSelf: 'center',
  whiteSpace: 'nowrap',
}

export default function LeftDisplayElements(props) {
  const [showSignInMenu, setShowSignInMenu] = useState(false)
  const [showCallMenu, setShowCallMenu] = useState(false)
  const [showHoursMenu, setShowHoursMenu] = useState(false)
  const [showUserMenu, setShowUserMenu] = useState(false)

  useEffect(() => {
    if (props.toggleSIM) {
      setShowSignInMenu(true)
      props.setToggleSIM()
    }
  }, [props])

  const context = useContext(UserContext)

  const closeAll = () => {
    setShowSignInMenu(false)
    setShowCallMenu(false)
    setShowHoursMenu(false)
    setShowUserMenu(false)
  }

  const toggleSignIn = () => {
    const currentToken = localStorage.getItem('token')
    if (!currentToken) {
      setShowSignInMenu(!showSignInMenu)
      setShowCallMenu(false)
      setShowHoursMenu(false)
      setShowUserMenu(false)
    } else {
      setShowSignInMenu(false)
      setShowCallMenu(false)
      setShowHoursMenu(false)
      setShowUserMenu(!showUserMenu)
    }
  }

  const toggleCall = () => {
    setShowSignInMenu(false)
    setShowCallMenu(!showCallMenu)
    setShowHoursMenu(false)
    setShowUserMenu(false)
  }

  const toggleHours = () => {
    setShowSignInMenu(false)
    setShowCallMenu(false)
    setShowHoursMenu(!showHoursMenu)
    setShowUserMenu(false)
  }

  const closeSignInMenu = () => {
    setShowSignInMenu(false)
  }

  const userLogo = {
    color: props.url?.includes('userdashboard') ? '#FBA586' : 'inherit',
    display: 'flex',
    height: '100%',
    alignItems: 'center',
    i: { fontSize: '1.6rem' },
  }

  return (
    <div css={desktop} id='displayLeft'>
      <ul css={flexContainer}>
        <li className='hoverEffect' onClick={closeAll} onMouseEnter={closeAll}>
          <Link css={flex} to='/locations'>
            <i className='material-icons'>location_on</i>
            <div className='locationTitle' css={centered}>
              {/* {context.region.name} */}
            </div>
          </Link>
        </li>

        <li className='hoverEffect' onClick={toggleSignIn}>
          <div css={userLogo}>
            <i className='material-icons'>person_outline</i>
            <div className='userName' css={centered}>
              {context.user?.name}
            </div>
          </div>
          {showSignInMenu && (
            <SignOptionsMenu
              toggleUser={context.toggleUser}
              closeSignInMenu={closeSignInMenu}
            />
          )}
          {showUserMenu && (
            <UserMenu logOut={context.logOut} closeUserMenu={toggleSignIn} />
          )}
        </li>

        <li className='hoverEffect' onClick={toggleHours}>
          <div css={flex}>
            <div className='hoursText' css={centered}>
              Opening Hours
            </div>
            <img className='hoursImg' css={centered} alt='clock' src={clock} />
          </div>
          {showHoursMenu && <HoursSubMenu onMouseLeaveHours={toggleHours} />}
        </li>

        <li className='hoverEffect' onClick={toggleCall}>
          <div css={flex}>
            <i className='material-icons'>phone</i>
          </div>
          {showCallMenu && <CallSubMenu toggleCall={toggleCall} />}
        </li>
      </ul>
    </div>
  )
}
