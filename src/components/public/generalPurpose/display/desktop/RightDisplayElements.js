/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import facepaint from 'facepaint'

import UserContext from '../../../../../contexts/userContext'

import SignInMenuRight from './SignInMenuRight'
import WishListMenu from './WishListMenu'
import Cart from './Cart'
import SampleCart from './SampleCart'
import SearchDisplayElement from './SearchDisplayElement'
import shoppingBag from '../../../../../resources/shopping_bag.svg'

// RESPONSIVENESS SETTINGS
const breakpoints = [561, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const margin = {
  marginRight: '15px',
}
const flexContainer = mq({
  fontSize: '0.8em',
  fontWeight: '300',
  display: 'flex',
  listStyle: 'none',
  justifyContent: ['space-evenly', 'center', 'flex-end'],
  fontFamily: 'Montserrat,serif',
  '& li': {
    display: 'flex',
    padding: ['0 0.2rem', '0 0.5em'],
    margin: ['0', '0 0.5em'],
    cursor: 'pointer',
    fontSize: '1em',
    alignItems: 'center',
  },
  '.hoverEffect:hover': {
    color: '#FBA586',
  },
  i: {
    padding: '0 0.3rem',
    fontSize: '1.5em',
  },
})
const sampleWrapper = mq({
  display: ['none', 'flex'],
  padding: '0 0.5em',
  margin: '0 0.5em',
  cursor: 'pointer',
  fontSize: '1em',
  alignItems: 'center',
})
const shoppingBagStyle = {
  height: '22px',
  marginTop: '-2px',
  ':hover': {
    filter:
      'invert(66%) sepia(8%) saturate(2056%) hue-rotate(328deg) brightness(109%) contrast(97%)',
  },
}
const centered = mq({
  display: ['none', 'block'],
  alignSelf: 'center',
  paddingLeft: '0.5em',
})

export default function RightDisplayElements(props) {
  const contextType = useContext(UserContext)

  const toggleWishList = () => {
    closeAll()
    const user = contextType.user
    user?.name === 'Sign In'
      ? props.setShowSignInMenu(!props.showSignInMenu)
      : props.setShowWishList(!props.showWishList)
  }

  const closeAll = () => {
    props.setShowSignInMenu(false)
    props.setShowWishList(false)
    props.setshowCart(false)
    props.setShowSampleCart(false)
  }

  const toggleSampleCart = () => {
    closeAll()
    props.setShowSampleCart(!props.showSampleCart)
  }

  const toggleCart = () => {
    closeAll()
    props.setshowCart(!props.showCart)
  }

  return (
    <div css={margin}>
      <ul css={flexContainer}>
        <SearchDisplayElement {...props} />
        <li
          onClick={() => toggleWishList()}
          className='hoverEffect'
          style={{
            color: (props.showWishList || props.showSignInMenu) && '#FBA586',
          }}
        >
          <i className='material-icons'>favorite_border</i>
          {props.showSignInMenu && (
            <SignInMenuRight
              toggleUser={contextType.toggleUser}
              toggleWishList={toggleWishList}
              toggleSignOptionsMenu={props.toggleSignOptionsMenu}
            />
          )}
          {props.showWishList && (
            <WishListMenu
              addToCart={contextType.addToCart}
              removeFromWishlist={contextType.removeFromWishlist}
              wishlist={contextType.user.wishlist}
              toggleWishList={toggleWishList}
              user={contextType.user}
            />
          )}
        </li>

        <div
          css={sampleWrapper}
          className='hoverEffect'
          onClick={() => toggleSampleCart()}
          style={{ color: props.showSampleCart && '#FBA586' }}
        >
          <i className='material-icons'>texture</i>
          <div css={centered}>{contextType.user?.sampleCart?.length}</div>
          {props.showSampleCart && (
            <SampleCart
              removeFromSampleCart={contextType.removeFromSampleCart}
              toggleSampleCart={toggleSampleCart}
              user={contextType.user}
            />
          )}
        </div>
        {props.cart !== false ? (
          <li
            onClick={() => toggleCart()}
            style={{ color: props.showCart && '#FBA586' }}
          >
            {/* <i className='material-icons'>shopping_bag</i> */}
            <img css={shoppingBagStyle} alt='shopping bag' src={shoppingBag} />
            <div css={centered}>{contextType.user?.cart?.length}</div>
            {props.showCart && (
              <Cart
                remove={contextType.removeFromCart}
                cart={contextType.user.cart}
                toggleCart={toggleCart}
              />
            )}
          </li>
        ) : null}
      </ul>
    </div>
  )
}
