/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'

const show = {
  position: 'absolute',
  top: '84px',
  zIndex: '30000000',
  backgroundColor: 'white',
  color: 'black',
  pointerEvents: 'auto',
  cursor: 'context-menu',
  display: 'flex',
  flexDirection: 'column',
  fontSize: '.75rem',
  fontWeight: '300',
  boxShadow: '0px 3px 6px #00000029',
  padding: '1rem',
}
const wrapper = {
  display: 'flex',
  padding: '0.6em',
  justifyContent: 'space-between',
  width: '240px',
}
const currentDate = {
  color: '#FBA586',
  display: 'flex',
  width: '50%',
  marginRight: '1em',
}
const currentHour = {
  color: '#FBA586',
  width: '50%',
}
const date = {
  display: 'flex',
  width: '50%',
  marginRight: '1em',
}
const hour = {
  width: '50%',
}

export default class HoursSubMenu extends React.Component {
  state = { day: new Date().getDay() }

  onMouseLeave = (e) => {
    e.preventDefault()
    this.props.onMouseLeaveHours()
  }

  render() {
    const days = [
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
      'Sunday',
    ]
    const hours = [
      '9.00 am - 4.45 pm',
      '9.00 am - 4.45 pm',
      '9.00 am - 4.45 pm',
      '9.00 am - 8.45 pm',
      '9.00 am - 4.45 pm',
      '9.00 am - 4.45 pm',
      'Closed',
    ]
    return (
      <div css={show} onMouseLeave={this.onMouseLeave}>
        {days.map((day, index) => {
          return (
            <div key={day} css={wrapper}>
              <div css={this.state.day === index + 1 ? currentDate : date}>
                {day}
              </div>
              <div css={this.state.day === index + 1 ? currentHour : hour}>
                {hours[index]}
              </div>
            </div>
          )
          // return this.state.day === index + 1 ||
          //   (this.state.day === 0 && index === 6) ? (
          //   <div key={day} css={wrapper}>
          //     <div css={currentDate}>{day}</div>
          //     {index + 1 !== 7 ? (
          //       <div css={currentHour}>9:00 AM - 4:45 PM</div>
          //     ) : (
          //       <div css={currentHour}>Closed</div>
          //     )}
          //   </div>
          // ) : (
          //   <div key={day} css={wrapper}>
          //     <div css={date}>{day}</div>
          //     {index + 1 !== 7 ? (
          //       <div css={hour}>9:00 AM - 4:45 PM</div>
          //     ) : (
          //       <div css={hour}>Closed</div>
          //     )}
          //   </div>
          // )
        })}
      </div>
    )
  }
}
