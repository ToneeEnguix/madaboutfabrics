/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'

import { URL } from '../../../../../config'
import axios from 'axios'

const column = {
  display: 'flex',
  flexDirection: 'column',
  width: '50%',
  padding: '0 0 1.5rem 0',
  button: {
    margin: '1em auto 1.25rem',
    padding: '0.9rem',
    width: '70%',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '8px',
    backgroundColor: 'white',
    border: '2px solid #171717',
    fontWeight: 'bold',
    fontFamily: 'Montserrat,sans-serif',
    color: 'black',
    cursor: 'pointer',
    transition: 'all linear 150ms',
  },
  'button:hover': {
    backgroundColor: '#00263E',
    color: 'white',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    borderLeft: '1px solid lightGray',
    marginBottom: '1em',
  },
  input: {
    width: '70%',
    display: 'block',
    position: 'relative',
    background: 'none',
    padding: '.9rem 0.7rem',
    border: '1px solid lightGrey',
    zIndex: '2',
    margin: '0.5rem 0 0',
  },
  'input:focus, input:valid,': {
    outline: 'none',
  },
  h2: {
    fontFamily: 'cooper-black-std, serif',
    color: 'black',
    fontSize: '1.4rem',
    letterSpacing: '-1px',
    padding: '0.5rem 0',
    margin: '0 auto',
  },
  h3: {
    alignSelf: 'center',
    fontFamily: 'Montserrat,serif',
    marginBottom: '2rem',
  },
  '.customInput::-webkit-input-placeholder': {
    fontWeight: '300',
  },
  a: {
    alignSelf: 'flex-start',
    fontFamily: 'Roboto,serif',
    color: 'blue',
  },
  span: {
    width: '70%',
    color: 'grey',
    margin: '1rem 0',
  },
}
const checkBoxAlign = {
  width: '70%',
  display: 'flex',
  color: 'grey',
  textJustify: 'inter-word',
  textAlign: 'justify',
  input: {
    width: '5rem',
    height: '5rem',
    margin: '0 .75rem',
  },
}

export default function SignUpMenu(props) {
  const [message, setMessage] = useState({
    text: 'Password must be at least 8 characters',
    color: '',
  })

  useEffect(() => {
    message.color === 'red' &&
      setTimeout(() => {
        setMessage({
          text: 'Password must be at least 8 characters',
          color: '',
        })
      }, 2000)
  }, [message])

  const handleSubmit = async (e) => {
    e.preventDefault()
    const name = e.target.name.value.trim()
    const email = e.target.email.value.trim()
    const password = e.target.password.value.trim()

    if (password.length < 8) {
      setMessage({
        text: 'Password must be at least 8 characters',
        color: 'red',
      })
    } else {
      try {
        const res = await axios.post(`${URL}/users/signup`, {
          name,
          email,
          password,
        })
        if (!res.data.ok) {
          setMessage({
            text: res.data.message,
            color: 'red',
          })
        } else {
          localStorage.setItem('token', res.data.token)
          props.closeSignInMenu()
          props.toggleUser(res.data.userData)
        }
      } catch (err) {
        console.error(err)
      }
    }
  }

  const messageStyle = {
    color: message.color || 'gray',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: '500',
    textAlign: 'center',
    margin: '1rem auto',
  }

  return (
    <div css={column} onClick={(e) => e.stopPropagation()}>
      <h2>Mad About Fabrics</h2>

      <form onSubmit={handleSubmit}>
        <h3>NEW TO MAD ABOUT FABRICS?</h3>
        <input placeholder='Username' required type='text' name='name'></input>
        <input placeholder='Email' required type='text' name='email'></input>
        <input
          placeholder='Password'
          required
          type='password'
          name='password'
        ></input>
        <h4 css={messageStyle}>{message.text}</h4>
        <div css={checkBoxAlign}>
          <input type='checkbox'></input>
          <div>
            Yes, I wish to receive exclusive offers, news and invitations to
            events and Competitions from the Talbot Textiles Group by email**. I
            can unsubscribe at any time
          </div>
        </div>
        <button type='submit'>JOIN NOW</button>
        <div css={checkBoxAlign}>
          By registering you agree to our Terms of Use and confirm that you have
          read and understood our Privacy Policy.
        </div>
      </form>
    </div>
  )
}
