/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { useCallback, useEffect } from 'react'
import { Link } from 'react-router-dom'

import { URL } from '../../../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [550]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  width: ['100vw', '28vw'],
  minWidth: [0, '410px'],
  position: 'absolute',
  zIndex: '1000000',
  top: '84px',
  right: [0, '10%'],
  backgroundColor: 'white',
  boxShadow: '0px 3px 6px #00000029',
  borderRadius: ['0 0 30px 30px', 0],
  color: 'black',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: ['2.5rem 4.2rem 4rem 5rem', '2.5rem 4.2rem 2rem 5rem'],
  pointerEvents: 'auto',
  fontSize: '1.1em',
  fontWeight: 'bold',
  height: 'max-content',
  cursor: 'default',
  h1: {
    fontFamily: 'cooper-black-std, serif',
    color: 'black',
    textDecoration: 'none',
    fontSize: '1.7rem',
    letterSpacing: '-1px',
    margin: '0 0 0.25rem',
    whiteSpace: 'nowrap',
  },
  h2: {
    marginBottom: '1em',
    fontFamily: 'Montserrat,serif',
    fontSize: '0.9rem',
  },
})

const contentWrapper = {
  marginTop: '-1.8rem',
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
  '.buttonWrapper': {
    margin: '1.5rem auto 0',
    padding: '1em',
    width: '100%',
    boxShadow: '0px 3px 6px 0px rgba(235,235,235,1)',
    borderRadius: '100px',
    backgroundColor: '#FBA586',
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'Montserrat,sans-serif',
    cursor: 'pointer',
    border: '1px solid transparent',
    transition: 'all linear 200ms',
    textDecoration: 'none',
    ':hover': {
      backgroundColor: 'white',
      color: '#FBA586',
      border: '1px solid #FBA586',
      boxShadow: '5px 10px 15px 5px rgba(235,235,235,1)',
    },
  },
}

const itemWrapper = {
  display: 'flex',
  alignItems: 'center',
  width: '120%',
  justifyContent: 'space-between',
  fontWeight: '300',
  marginTop: '1rem',
  i: {
    cursor: 'pointer',
  },
}

const square = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '60px',
  width: '60px',
  borderRadius: '100%',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const center = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  width: '50%',
  paddingLeft: '1em',
  div: {
    padding: '0.2em',
    fontSize: '1.2em',
    fontFamily: 'Nanum Myeongjo,serif !important',
  },
}

const centerRow = {
  display: 'flex',
  alignItems: 'center',
}

const roundButton = {
  borderRadius: '100%',
  width: '4em',
  height: '4em',
  boxShadow: '0px 3px 6px 0px rgba(235,235,235,1)',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  margin: '0 0 0 1rem',
  cursor: 'pointer',
  transition: 'all linear 200ms',
  ':hover': {
    backgroundColor: '#e7e7e7',
  },
}

const noItems = mq({
  color: '#9D9D9D',
  fontWeight: '400',
  fontSize: '0.8rem',
  marginTop: '0.5rem',
  paddingbottom: ['2rem', 0],
})

const resize = {
  fontSize: '24px !important',
}

export default function WishListMenu(props) {
  const handleClick = (e) => {
    e.stopPropagation()
  }

  const moveToCart = async (item) => {
    if (item.range.inStock > 0) {
      await props.removeFromWishlist(item.range, item.variantIndex)
      props.addToCart(item.range, item.variantIndex, 1)
    } else {
      alert('Item currently out of stock')
    }
  }

  const escFunction = useCallback(
    (event) => {
      if (event.keyCode === 27) {
        props.toggleWishList()
      }
    },
    [props]
  )

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)
    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  return (
    <div
      css={show}
      onMouseLeave={props.toggleWishList}
      onClick={(e) => handleClick(e)}
    >
      <h1>Mad About Fabrics </h1>
      <h2>THIS IS YOUR WISHLIST</h2>
      {props.wishlist.length === 0 ? (
        <div css={noItems}>No items added to the wishlist.</div>
      ) : (
        <div css={contentWrapper}>
          {props.wishlist.slice(0, 5).map((item, i) => {
            console.log(i, item)
            return (
              <div key={i} css={itemWrapper}>
                <Link
                  onClick={props.toggleWishList}
                  to={{
                    pathname: item.range.url
                      ? `/productdetails/${item.range.url}/${i}`
                      : `/productdetails/${item.range._id}/${i}`,
                    state: {
                      item: item.range,
                      index: i,
                    },
                  }}
                >
                  <div css={square}>
                    <img
                      src={`${URL}/assets/${
                        item.range.variants[item.variantIndex].imageURLs[0]
                          .filename
                      }`}
                      alt='product'
                    />
                  </div>
                </Link>

                <div css={center}>
                  <div>{item.range.name}</div>
                  <div>{item.range.variants[item.variantIndex].color}</div>
                </div>
                <div css={centerRow}>
                  <i
                    css={resize}
                    onClick={() => {
                      props.removeFromWishlist(item)
                    }}
                    className='material-icons'
                  >
                    delete_forever
                  </i>
                  <div css={roundButton} onClick={() => moveToCart(item)}>
                    <i className='material-icons'>add_shopping_cart</i>
                  </div>
                </div>
              </div>
            )
          })}
          <Link
            className='buttonWrapper flexCenter'
            to='/userdashboard/wishlist'
          >
            SEE ALL
          </Link>
        </div>
      )}
    </div>
  )
}
