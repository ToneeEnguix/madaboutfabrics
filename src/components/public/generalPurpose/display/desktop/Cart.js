/** @jsx jsx */
import { jsx } from '@emotion/react'
import { keyframes } from '@emotion/react/'
import React, { useCallback, useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import { URL } from '../../../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [550]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  position: 'absolute',
  top: '84px',
  right: [0, '10%'],
  zIndex: '30000000',
  backgroundColor: 'white',
  color: 'black',
  width: ['100vw', '28vw'],
  borderRadius: ['0 0 30px 30px', 0],
  minWidth: [0, '410px'],
  flexDirection: 'column',
  alignItems: 'center',
  padding: '2.3rem 4rem 3rem',
  height: 'max-content',
  display: 'flex',
  fontSize: '1.1rem',
  fontWeight: '300',
  boxShadow: '0px 3px 6px #00000029',
  cursor: 'default',
  h2: {
    fontFamily: 'cooper-black-std, serif',
    whiteSpace: 'nowrap',
  },
  h3: {
    marginTop: '0.2rem',
    fontFamily: 'Montserrat,serif',
    fontSize: '0.8em',
    marginBottom: '.2rem',
  },
})

const circleWrapper = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '60px',
  width: '60px',
  borderRadius: '100%',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const center = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  width: '100%',
  fontSize: '1.2rem',
  marginLeft: '1rem',
  div: {
    padding: '0.2rem 0 0.2rem 0',
  },
}

const price = {
  fontWeight: '300',
  letterSpacing: '1px',
  fontSize: '0.8rem',
}

const nanoum = {
  fontFamily: 'Nanum Myeongjo,serif !important',
  fontSize: '0.9rem',
  overflow: 'hidden',
}

const centerRow = {
  display: 'flex',
  width: '25%',
  alignItems: 'center',
  'i:hover': {
    cursor: 'hover',
  },
  i: {
    fontSize: '30px',
  },
}

const buttonAlignment = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-around',
  alignItems: 'center',
  width: '100%',
  marginTop: '1.7rem',
  a: {
    width: '100%',
    margin: '0',
    padding: 0,
  },
  button: {
    height: '45px',
    width: '100%',
    boxShadow: '0px 3px 6px 0px rgba(235,235,235,1)',
    borderRadius: '100px',
    backgroundColor: '#FBA586',
    color: 'white',
    border: 'none',
    fontWeight: 'bold',
    fontFamily: 'Montserrat,sans-serif',
    cursor: 'pointer',
    transition: 'all linear 200ms',
  },
  '.whiteButton': {
    border: '2px solid #171717',
    color: '#171717',
    backgroundColor: 'white',
    marginBottom: '1rem',
    ':hover': {
      backgroundColor: '#171717',
      color: 'white',
      border: '2px solid transparent',
    },
  },
  '.orangeButton:hover': {
    backgroundColor: 'white',
    color: '#FBA586',
    border: '2px solid #FBA586',
  },
}

const noitems = mq({
  margin: '1rem 0 -1rem',
  fontSize: '0.8rem',
  color: 'grey',
  paddingBottom: ['2rem', 0],
})

export default function Cart(props) {
  const handleClick = (e) => {
    e.stopPropagation()
  }

  const escFunction = useCallback(
    (event) => {
      if (event.keyCode === 27) {
        props.toggleCart()
      }
    },
    [props]
  )

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false)
    return () => {
      document.removeEventListener('keydown', escFunction, false)
    }
  }, [escFunction])

  const cart = props.cart

  return (
    <div
      css={show}
      onMouseLeave={props.toggleCart}
      onClick={(e) => handleClick(e)}
    >
      <h2>Shopping Bag</h2>
      <h3>QUICK VIEW OF YOUR BAG</h3>
      {cart.length === 0 ? (
        <div css={noitems}>No items added to the cart list.</div>
      ) : (
        <React.Fragment>
          {cart.map((item, index) => {
            return <CartItem remove={props.remove} item={item} key={index} />
          })}
          <div css={buttonAlignment}>
            <Link to='/checkout'>
              <button className='whiteButton'>CHECKOUT NOW</button>
            </Link>
            <Link to='/userdashboard/cart' onClick={props.toggleCart}>
              <button className='orangeButton'>EDIT ORDER</button>
            </Link>
          </div>
        </React.Fragment>
      )}
    </div>
  )
}

const CartItem = (props) => {
  const fadeOut = keyframes`from  {
        opacity:1
      }
    
      to {opacity:0}
      }`

  const [animation, setAnimation] = useState('none')

  const remove = (range, variantIndex) => {
    setAnimation(`${fadeOut} 0.5s ease 1`)
    setTimeout(() => {
      props.remove(range, variantIndex)
      setAnimation('none')
    }, 400)
  }

  const itemWrapper = {
    display: 'grid',
    gridTemplateColumns: '1fr 4fr 1fr',
    alignItems: 'center',
    width: '100%',
    animation: animation,
    marginTop: '1rem',
    i: {
      cursor: 'pointer',
    },
  }

  return (
    <div key={props.index} css={itemWrapper}>
      <Link
        to={
          props.item?.range?.url
            ? `/productdetails/${props.item?.range?.url}/${props.item.variantIndex}`
            : `/productdetails/${props.item?.range?._id}/${props.item.variantIndex}`
        }
        css={circleWrapper}
      >
        <img
          alt={'product thumbnail'}
          src={`${URL}/assets/${
            props.item.range?.variants[props.item.variantIndex].imageURLs[0]
              .filename
          }`}
        />
      </Link>
      <div css={center}>
        <div css={nanoum}>{props.item.amount} metres</div>
        <div css={nanoum}>
          {props.item.range?.name}{' '}
          {props.item.range?.variants[props.item.variantIndex].color}
        </div>
        <div css={price}>
          {(props.item.amount * props.item.range?.price).toFixed(2)}£
        </div>
      </div>
      <div css={centerRow}>
        <i
          onClick={(e) => {
            remove(props.item.range, props.item.variantIndex)
            e.stopPropagation()
          }}
          className='material-icons'
        >
          delete_forever
        </i>
      </div>
    </div>
  )
}
