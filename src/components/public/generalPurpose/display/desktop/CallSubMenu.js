/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'

const show = {
  position: 'absolute',
  top: '84px',
  zIndex: '30000000',
  backgroundColor: 'white',
  color: 'black',
  display: 'flex',
  fontWeight: '300',
  padding: '1.5em',
  boxShadow: '0px 3px 6px #00000029',
  a: {
    fontFamily: 'Roboto, sans-serif',
  },
  'a:hover span': {
    color: '#FBA586',
  },
}

export default class callSubMenu extends React.Component {
  onMouseLeave = () => {
    this.props.toggleCall()
  }

  render() {
    return (
      <div css={show} onMouseLeave={this.onMouseLeave}>
        <a href='tel:+44 28 90 370 390'>
          <span>Call </span> +44 28 90 370 390
        </a>
      </div>
    )
  }
}
