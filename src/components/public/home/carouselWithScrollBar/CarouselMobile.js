/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import HorizontalScroll from 'react-scroll-horizontal'
import { isMobile } from 'react-device-detect'

import { URL } from '../../../../config'

export default function CarouselMobile({ items, item }) {
  if (isMobile) {
    return <MobileCarousel items={items} item={item} />
  } else return <DesktopCarousel items={items} item={item} />
}

const DesktopCarousel = ({ items }) => {
  return (
    <div css={main} className='categories_slider'>
      <HorizontalScroll reverseScroll={true}>
        {items.map((ele, i) => {
          return (
            <Link
              key={ele._id}
              css={itemStyle}
              style={{ marginLeft: i === 0 ? '32px' : '16px' }}
              to={{
                pathname: ele.url
                  ? `/productdetails/${ele.url}/${ele.index || 0}`
                  : `/productdetails/${ele._id}/${ele.index || 0}`,
                state: {
                  index: ele.index || 0,
                },
              }}
            >
              <img
                alt='product'
                src={`${URL}/assets/${ele.variants[0].imageURLs[0].filename}`}
              />
              <p>{ele.name}</p>
              <p className='bold upper'>{ele.variants[0].color}</p>
              <p>£{ele.price} per metre</p>
            </Link>
          )
        })}
      </HorizontalScroll>
    </div>
  )
}

const MobileCarousel = ({ items }) => {
  return (
    <div css={main} className='categories_slider'>
      {items.map((ele, i) => {
        return (
          <Link
            key={ele._id}
            css={itemStyle}
            style={{ marginLeft: i === 0 ? '32px' : '16px' }}
            to={{
              pathname: ele.url
                ? `/productdetails/${ele.url}/${ele.index || 0}`
                : `/productdetails/${ele._id}/${ele.index || 0}`,
              state: {
                index: ele.index || 0,
              },
            }}
          >
            <img
              alt='product'
              src={`${URL}/assets/${ele.variants[0].imageURLs[0].filename}`}
            />
            <p>{ele.name}</p>
            <p className='bold upper'>{ele.variants[0].color}</p>
            <p>£{ele.price} per metre</p>
          </Link>
        )
      })}
    </div>
  )
}

const main = {
  marginTop: '1rem',
  height: 'calc(100vw + 64px)',
}

const itemStyle = {
  display: 'block',
  width: 'calc(100vw - 64px)',
  minWidth: 'calc(100vw - 64px)',
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: 300,
  textDecoration: 'none',
  color: 'inherit',
  img: {
    width: 'calc(100vw - 64px)',
    height: 'calc(100vw - 64px)',
    marginBottom: '1rem',
    borderRadius: '10px',
  },
  '.upper': {
    fontSize: '1.5rem',
    marginBottom: '.8rem',
  },
  '.bold': {
    fontWeight: 700,
  },
}
