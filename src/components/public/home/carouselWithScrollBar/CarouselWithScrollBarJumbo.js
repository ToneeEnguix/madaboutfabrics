/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useEffect, useState } from 'react'
import Carousel from 'react-multi-carousel'

import UserContext from '../../../../contexts/userContext'

import 'react-multi-carousel/lib/styles.css'
import './withScrollBar.css'

import ProductRowRegular from './productRow/ProductRowRegular'

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
  },
  mobile: {
    breakpoint: { max: 700, min: 0 },
    items: 1,
  },
}

const topMiniSliderWrapper = {
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centeredCarousel = {
    width: '95%',
    margin: '1rem 0',
  }

export default function CarouselWithScrollBarJumbo(props) {
  const context = useContext(UserContext)

  const [slidesToSlide, setSlidesToSlide] = useState(1)

  useEffect(() => {
    function updateWidth() {
      let tempValue =
        window.innerWidth > 1024 ? 3 : window.innerWidth > 464 ? 2 : 1
      setSlidesToSlide(tempValue)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
    return () => {
      window.removeEventListener('resize', updateWidth)
    }
  }, [])

  return (
    <section css={topMiniSliderWrapper}>
      <div css={centeredCarousel}>
        {props.items?.length > 0 && (
          <Carousel
            itemClass='slider-image-item'
            responsive={responsive}
            containerClass='carousel-container-with-scrollbar'
            slidesToSlide={slidesToSlide}
          >
            {props.items.map((item, index) => {
              return (
                <ProductRowRegular
                  size={'jumbo'}
                  wishlist={context.user.wishlist}
                  key={index}
                  item={item}
                />
              )
            })}
          </Carousel>
        )}
      </div>
    </section>
  )
}
