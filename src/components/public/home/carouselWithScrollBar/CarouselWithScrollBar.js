/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useEffect, useState } from 'react'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'

import ProductRowRegular from './productRow/ProductRowRegular'
import UserContext from '../../../../contexts/userContext'
import './withScrollBar.css'

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 5.8,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 3,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
}

const topMiniSliderWrapper = {
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  centeredCarousel = {
    width: '95%',
    margin: '2.5rem 0',
  }

export default function CarouselWithScrollBar(props) {
  const context = useContext(UserContext)

  const [slidesToSlide, setSlidesToSlide] = useState(1)

  useEffect(() => {
    function updateWidth() {
      let tempValue =
        window.innerWidth > 1024 ? 5 : window.innerWidth > 464 ? 3 : 1
      setSlidesToSlide(tempValue)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
    return () => {
      window.removeEventListener('resize', updateWidth)
    }
  }, [])

  return (
    <section css={topMiniSliderWrapper}>
      <div css={centeredCarousel}>
        <Carousel
          itemClass='slider-image-item'
          responsive={responsive}
          containerClass='carousel-container-with-scrollbar'
          slidesToSlide={slidesToSlide}
        >
          {props.items.map((item, index) => {
            return (
              <ProductRowRegular
                key={`${index}${item._id}`}
                size={'regular'}
                wishlist={context.user.wishlist}
                item={item}
              />
            )
          })}
        </Carousel>
      </div>
    </section>
  )
}
// const CustomSlider = ({ carouselState }) => {
//   let value = 0
//   let carouselItemWidth = 0
//   if (this.Carousel) {
//     carouselItemWidth = this.Carousel.state.itemWidth
//     const maxTranslateX = Math.round(
//       // so that we don't over-slide
//       carouselItemWidth *
//         (this.Carousel.state.totalItems -
//           this.Carousel.state.slidesToShow) +
//         0
//     )
//     value = maxTranslateX / 100 // calculate the unit of transform for the slider
//   }
//   const { transform } = carouselState
//   return (
//     <div className='custom-slider'>
//       <input
//         type='range'
//         value={Math.round(Math.abs(transform) / value)}
//         max={
//           (carouselItemWidth *
//             (carouselState.totalItems - carouselState.slidesToShow) +
//             (this.state.additionalTransfrom === 0 ? 0 : 0)) /
//           value
//         }
//         onChange={(e) => {
//           if (this.Carousel.isAnimationAllowed) {
//             this.Carousel.isAnimationAllowed = false
//           }
//           const nextTransform = e.target.value * value
//           const nextSlide = Math.round(nextTransform / carouselItemWidth)
//           if (
//             e.target.value === 0 &&
//             this.state.additionalTransfrom === 0
//           ) {
//             this.Carousel.isAnimationAllowed = true
//             this.setState({ additionalTransfrom: 0 })
//           }
//           this.Carousel.setState({
//             transform: -nextTransform,
//             currentSlide: nextSlide,
//           })
//         }}
//         className='custom-slider__input'
//       />
//     </div>
//   )
// }

// }
