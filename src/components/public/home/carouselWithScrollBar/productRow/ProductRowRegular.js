/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'

import { Link } from 'react-router-dom'
import Countdown from 'react-countdown'
import { URL } from '../../../../../config'

const show = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  fontFamily: 'Montserrat, sans-serif',
  '& a': {
    color: 'inherit',
    textDecoration: 'inherit',
  },
}

const imageFitter = {
  flexShrink: 0,
  objectFit: 'cover',
  maxWidth: '100%',
  maxHeight: '100%',
  minWidth: '100%',
  minHeight: '100%',
}

const variantMiniatures = {
  justifyContent: 'flex-start',
  display: 'flex',
  flexWrap: 'wrap',
  alignItems: 'center',
}

export default function ProductRowRegular(props) {
  const [size, setSize] = useState('1rem')

  const content = {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    marginBottom: '0.5rem',
    flexDirection: 'column',
    'p:first-of-type, span': {
      fontSize: size.slice(0, 1) * 0.9 + 'rem !important',
      fontWeight: 300,
      textTransform: 'inherit',
    },
    p: {
      fontSize: size.slice(0, 1) * 1.3 + 'rem !important',
      fontWeight: 700,
      textTransform: 'uppercase',
    },
  }

  const [w, setW] = useState(null)

  useEffect(() => {
    function updateWidth() {
      setW(window.innerWidth)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
  }, [])

  const imageWrapper = {
    width: props.width
      ? w <= 600
        ? '50vw'
        : w <= 959
        ? '40vw'
        : w <= 1200
        ? '20vw'
        : props.width
      : props.size === 'big'
      ? w <= 700
        ? '40vw'
        : w <= 1024
        ? '30vw'
        : '20vw'
      : props.size === 'jumbo'
      ? w <= 700
        ? '50vw'
        : w <= 1024
        ? '40vw'
        : '30vw'
      : w <= 464
      ? '40vw'
      : w <= 600
      ? '30vw'
      : w <= 800
      ? '25vw'
      : w <= 1024
      ? '20vw'
      : '15vw',
    position: 'relative',
    marginBottom: '1.2rem',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: props.size === 'jumbo' ? '14px' : '10px',
    // for img centered
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    maxHeight: '70vh',
  }

  const imageWrapper2 = {
    width: imageWrapper.width.slice(0, 2) / 8 + 'rem',
    minWidth: '35px',
    minHeight: '35px',
    margin: `${'0.5rem ' + imageWrapper.width.slice(0, 2) / 30 + 'rem 0 0'}`,
    // for img centered
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
    borderRadius: '8px',
    p: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  }

  useEffect(() => {
    if (props.size === 'big') {
      setSize('1.3rem')
    } else if (props.size === 'jumbo') {
      setSize('1.4rem')
    }
  }, [props])

  return (
    <div css={show} style={{ margin: props.margin }}>
      <Link
        to={{
          pathname: props.item.url
            ? `/productdetails/${props.item.url}/${props.item.index || 0}`
            : `/productdetails/${props.item._id}/${props.item.index || 0}`,
          state: {
            item: props.item,
            index: props.item.index || 0,
          },
        }}
      >
        <div
          css={imageWrapper}
          style={{ height: imageWrapper.width.slice(0, 2) * 1.3 + 'vw' }}
        >
          <CoverNotices item={props.item} />
          <img
            alt='product'
            css={imageFitter}
            src={`${URL}/assets/${props.item.variants[0].imageURLs[0]?.filename}`}
          />
        </div>
      </Link>
      <div css={content} style={{ maxWidth: imageWrapper.width }}>
        <p>{props.item.name}</p>
        <p>
          {props.item.variants[0].color.slice(0, 13) +
            (props.item.variants[0].color.length > 13 ? '...' : '')}
        </p>
        <Prices item={props.item} size={size} />
      </div>
      <div css={variantMiniatures} style={{ width: imageWrapper.width }}>
        {props.showVariantPictures &&
          props.item.variants.map((variant, i) => {
            if (i < 3) {
              return (
                <Link
                  to={
                    props.item.url
                      ? `/productdetails/${props.item.url}/${i}`
                      : `/productdetails/${props.item._id}/${i}`
                  }
                  key={i}
                  css={imageWrapper2}
                  style={{ height: imageWrapper2.width }}
                >
                  <img
                    alt='product'
                    css={imageFitter}
                    src={`${URL}/assets/${variant.imageURLs[0]?.filename}`}
                  />
                </Link>
              )
            } else if (i === 3) {
              return (
                <Link
                  to={{
                    pathname: props.item.url
                      ? `/productdetails/${props.item.url}/3`
                      : `/productdetails/${props.item._id}/3`,
                    state: {
                      item: props.item,
                      index: 3,
                    },
                  }}
                  key={i}
                  css={imageWrapper2}
                  style={{
                    height: imageWrapper2.width,
                    borderRadius: '100px',
                    boxShadow: '0px 3px 6px #00000029',
                  }}
                >
                  <p css={imageFitter}>
                    {'+' + (props.item.variants.length - 3)}
                  </p>
                </Link>
              )
            } else return null
          })}
      </div>
    </div>
  )
}

const notice = {
  backgroundColor: 'black',
  color: 'white',
  padding: '0.5rem 1em',
  fontSize: '0.7em',
  letterSpacing: '0.1em',
  zIndex: '2',
  position: 'absolute',
  top: 0,
  left: 0,
  fontFamily: 'Roboto, sans-serif',
}

const CoverNotices = (props) => {
  if (
    props.item.variants[0].priceDrop === true &&
    new Date(props.item.variants[0].priceDropDate).getTime() > Date.now() &&
    props.item.variants[0].saleDiscount > 0
  ) {
    return (
      <div css={notice}>
        <Countdown date={props.item.variants[0].priceDropDate} />
      </div>
    )
  } else return null
}

const prices = {
  margin: '0.4rem 0 0',
}

const discounted = {
  textDecoration: 'line-through',
  marginRight: '0.5rem',
}

const Prices = (props) => {
  if (
    props.item.variants[0].priceDrop === true &&
    new Date(props.item.variants[0].priceDropDate).getTime() > Date.now() &&
    props.item.variants[0].saleDiscount > 0
  ) {
    return (
      <div css={prices}>
        <span css={discounted}>£{props.item.price.toFixed(2)}</span>
        <span>
          £
          {(
            props.item.price -
            (props.item.price * props.item.variants[0].saleDiscount) / 100
          ).toFixed(2)}
        </span>
      </div>
    )
  } else
    return (
      <div css={prices}>
        <span>£{props.item.price.toFixed(2)}</span>
      </div>
    )
}
