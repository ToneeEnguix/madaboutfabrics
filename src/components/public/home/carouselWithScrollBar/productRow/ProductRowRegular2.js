/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'

import { Link } from 'react-router-dom'
import Countdown from 'react-countdown'
import { URL } from '../../../../../config'

const show = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  margin: '1rem',
  '& a': {
    color: 'inherit',
    textDecoration: 'inherit',
  },
}

const imageFitter = {
  flexShrink: 0,
  objectFit: 'cover',
  maxWidth: '100%',
  maxHeight: '100%',
  minWidth: '100%',
  minHeight: '100%',
}

const content = {
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  marginBottom: '0.5rem',
  flexDirection: 'column',
  'p:first-of-type': {
    fontSize: '0.7rem !important',
    fontWeight: 300,
    textTransform: 'inherit',
  },
  p: {
    fontSize: '1.3rem !important',
    fontWeight: 700,
    textTransform: 'uppercase',
  },
}

export default function ProductRowRegular2(props) {
  const [w, setW] = useState(null)

  useEffect(() => {
    function updateWidth() {
      setW(window.innerWidth)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
  })

  const imageWrapper = {
    width:
      props.size === 'big'
        ? w <= 700
          ? '40vw'
          : w <= 1024
          ? '30vw'
          : '20vw'
        : props.size === 'jumbo'
        ? w <= 700
          ? '50vw'
          : w <= 1024
          ? '40vw'
          : '30vw'
        : w <= 464
        ? '40vw'
        : w <= 700
        ? '25vw'
        : w <= 1024
        ? '20vw'
        : '15vw',
    position: 'relative',
    marginBottom: '1.2rem',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '10px',
    // for img centered
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  }

  return (
    <div css={show}>
      <Link
        to={
          props.item.url
            ? `/productdetails/${props.item.url}/${props.item.index || 0}`
            : `/productdetails/${props.item._id}/${props.item.index || 0}`
        }
      >
        <div
          css={imageWrapper}
          style={{ height: imageWrapper.width.slice(0, 2) * 1.3 + 'vw' }}
        >
          {props.size !== 'big' ? <CoverNotices item={props.item} /> : null}
          <img
            alt='product'
            css={imageFitter}
            src={`${URL}/assets/${props.item.variant.imageURLs[0]?.filename}`}
          />
        </div>
      </Link>
      <div css={content} style={{ maxWidth: imageWrapper.width }}>
        <p>{props.item.name}</p>
        <p>{props.item.variant.color}</p>
        <Prices item={props.item} />
      </div>
    </div>
  )
}

const notice = {
  backgroundColor: 'black',
  color: 'white',
  padding: '0.5rem 1em',
  fontSize: '0.7em',
  letterSpacing: '0.1em',
  zIndex: '2',
  position: 'absolute',
  top: 0,
  left: 0,
  fontFamily: 'Roboto, sans-serif',
}

const CoverNotices = (props) => {
  if (
    props.item.variant.priceDrop === true &&
    new Date(props.item.variant.priceDropDate).getTime() > Date.now() &&
    props.item.variant.saleDiscount > 0
  ) {
    return (
      <div css={notice}>
        <Countdown date={props.item.variant.priceDropDate} />
      </div>
    )
  } else return null
}

const prices = {
  margin: '0.4rem 0 0',
  fontSize: '0.7rem !important',
}

const discounted = {
  textDecoration: 'line-through',
  marginRight: '0.5rem',
}

const Prices = (props) => {
  if (
    props.item.variants[0].priceDrop === true &&
    new Date(props.item.variants[0].priceDropDate).getTime() > Date.now() &&
    props.item.variants[0].saleDiscount > 0
  ) {
    return (
      <div css={prices}>
        <span css={discounted}>£{props.item.price.toFixed(2)}</span>
        <span>
          £
          {(
            props.item.price -
            (props.item.price * props.item.variants[0].saleDiscount) / 100
          ).toFixed(2)}
        </span>
      </div>
    )
  } else
    return (
      <div css={prices}>
        <span>£{props.item.price.toFixed(2)}</span>
      </div>
    )
}
