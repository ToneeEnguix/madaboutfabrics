/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'
import { useEffect, useState } from 'react'

import CarouselWithScrollBar from './carouselWithScrollBar/CarouselWithScrollBar'
import Carousel from './carouselWithScrollBar/CarouselMobile'
import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const newArrivalsWrapper = mq({
  marginTop: '2rem',
  padding: ['2rem 0', '2rem 0', '1rem 0', 0],
})

export default function NewArrivals() {
  const [newItems, setNewItems] = useState([])

  useEffect(() => {
    const abort = new AbortController()
    const getNewsData = () => {
      const signal = abort.signal
      const requestOptions = {
        method: 'GET',
        redirect: 'follow',
        signal,
      }

      fetch(`${URL}/ranges/new`, requestOptions)
        .then((response) => response.json())
        .then((result) => setNewItems(result))
        .catch((error) => console.error('error', error))
    }
    getNewsData()
    return () => {
      abort.abort()
    }
  }, [])

  if (newItems.length > 0) {
    return (
      <section css={newArrivalsWrapper} className='AQUI'>
        <h1 className='title'>NEW ARRIVALS</h1>
        <Link
          to={{
            pathname: `/new`,
          }}
          className='see_all'
        >
          SEE ALL
        </Link>
        <div css={desktop}>
          <CarouselWithScrollBar items={newItems} size={'regular'} />
        </div>
        <div css={mobile}>
          <Carousel items={newItems} />
        </div>
      </section>
    )
  } else {
    return null
  }
}

const desktop = mq({
  display: ['none', 'none', 'block'],
})

const mobile = mq({
  display: ['block', 'block', 'none'],
})
