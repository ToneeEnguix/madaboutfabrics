/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import Carousel from 'react-multi-carousel'
import 'react-multi-carousel/lib/styles.css'

import { URL } from '../../../config'
import axios from 'axios'

export default function TopMiniSlider() {
  const [setting, setSetting] = useState({
    fields: [{ text: '' }],
    image: {
      filename: '',
      pathname: '',
    },
  })

  useEffect(() => {
    const getSetting = async () => {
      try {
        const res = await axios.get(`${URL}/settings/mini_slider`)
        setSetting(res.data.data)
      } catch (err) {
        console.error(err)
      }
    }
    getSetting()
  }, [])

  const topMiniSliderWrapper = {
    display: 'flex',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    height: '50px',
    backgroundColor: setting?.useBgColour ? `#${setting?.bgColour}` : '#aaa',
    color: setting?.colour ? `#${setting?.colour}` : 'black',
    fontFamily: 'Roboto, sans-serif !important',
    textTransform: 'uppercase',
  }

  return (
    <section
      css={topMiniSliderWrapper}
      style={
        !setting?.useBgColour && setting?.image.filename
          ? { backgroundImage: `url(${URL}/assets/${setting?.image.filename})` }
          : null
      }
    >
      <div css={centeredCarousel}>
        <Carousel
          responsive={responsive}
          autoPlaySpeed={3000}
          autoPlay={true}
          infinite
          removeArrowOnDeviceType={[
            'tablet',
            'mobile',
            'desktop',
            'superLargeDesktop',
          ]}
          showDots={false}
        >
          {setting?.fields.map((field, i) => {
            if (field.text) {
              return (
                <div css={center} key={i}>
                  <div css={column}>
                    <h3>{field.text}</h3>
                  </div>
                </div>
              )
            } else {
              return null
            }
          })}
        </Carousel>
      </div>
    </section>
  )
}

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 1,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 1,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 1,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
  },
}

const centeredCarousel = {
  width: '100%',
  '*': { textAlign: 'center' },
}

const center = {
  display: 'flex',
  justifyContent: 'center',
}

const column = {
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  h3: {
    fontWeight: '300',
    fontSize: '0.8em',
    letterSpacing: '1px',
  },
  span: {
    fontsize: '1em',
  },
}
