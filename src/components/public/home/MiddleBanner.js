/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { URL } from '../../../config'
import axios from 'axios'

const bannerWrapper = {
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '95vh',
}

export default function MiddleBanner() {
  const [setting, setSetting] = useState({
    fields: [{ text: '' }],
    image: {
      filename: '',
      pathname: '',
    },
  })

  useEffect(() => {
    const getSetting = async () => {
      try {
        const res = await axios.get(`${URL}/settings/featured`)
        setSetting(res.data.data)
      } catch (err) {
        console.error(err)
      }
    }
    getSetting()
  }, [])

  const actualBanner = {
    width: '95%',
    height: '90%',
    backgroundColor: setting?.useBgColour
      ? `#${setting?.bgColour}`
      : 'lightgray',
    color: setting?.colour ? `#${setting?.colour}` : 'black',
    textDecoration: 'none',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    fontSize: '3rem',
    fontFamily: 'Montserrat, sans-serif',
    textAlign: 'center',
  }

  return (
    <section css={bannerWrapper}>
      <a
        className='flexCenter'
        css={actualBanner}
        href={setting.fields[0].url || ''}
        style={
          !setting?.useBgColour && setting?.image.filename
            ? {
                backgroundImage: `url(${URL}/assets/${setting?.image.filename})`,
              }
            : null
        }
      >
        <div>{setting.fields[0].text}</div>
      </a>
    </section>
  )
}
