/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'
import { useEffect, useState } from 'react'

import CarouselWithScrollBar from './carouselWithScrollBar/CarouselWithScrollBar'
import Carousel from './carouselWithScrollBar/CarouselMobile'
import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const trendingWrapper = mq({
  marginTop: '2rem',
  padding: ['2rem 0', '2rem 0', '1rem 0', 0],
})

export default function Trending() {
  const [trendingItems, setTrendingItems] = useState([])

  useEffect(() => {
    const abort = new AbortController()
    const getTrending = () => {
      const signal = abort.signal
      const requestOptions = {
        method: 'GET',
        redirect: 'follow',
        signal,
      }

      fetch(`${URL}/ranges/trending`, requestOptions)
        .then((response) => response.json())
        .then((result) => setTrendingItems(result))
        .catch((error) => console.error('error', error))
    }
    getTrending()
    return () => {
      abort.abort()
    }
  }, [])

  if (trendingItems.length > 0) {
    return (
      <section css={trendingWrapper} className='trending'>
        <h1 className='title'>#TRENDING</h1>
        <Link
          to={{
            pathname: `/catalogue`,
            state: {
              sortBy: 'Most Popular',
            },
          }}
          className='see_all'
        >
          SEE ALL
        </Link>
        <div css={desktop}>
          <CarouselWithScrollBar items={trendingItems} size={'regular'} />
        </div>
        <div css={mobile}>
          <Carousel items={trendingItems} />
        </div>
      </section>
    )
  } else {
    return null
  }
}

const desktop = mq({
  display: ['none', 'none', 'block'],
})

const mobile = mq({
  display: ['block', 'block', 'none'],
})
