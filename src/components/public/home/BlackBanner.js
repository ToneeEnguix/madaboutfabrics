/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { URL } from '../../../config'
import axios from 'axios'

const flexColumn = {
  display: 'flex',
  flexDirection: 'column',
  textAlign: 'center',
  fontSize: '1.3rem',
  fontWeight: '100',
  letterSpacing: '0.3rem',
}

const flexList = {
  display: 'flex',
  listStyle: 'none',
  justifyContent: 'space-evenly',
  fontSize: '0.5em',
  fontWeight: '400',
  letterSpacing: '0.2em',
  '& li': {
    padding: '1em',
  },
  '& li:hover': {
    cursor: 'pointer',
  },
  '& li:after': {
    display: 'block',
    content: '""',
    borderBottom: 'solid 2px white',
    transform: 'scaleX(0)',
    transition: 'transform 100ms ease-in-out',
  },
  'li:hover:after': {
    transform: 'scaleX(1)',
  },
}

export default function BlackBanner() {
  const [setting, setSetting] = useState({
    fields: [{ text: '' }],
    image: {
      filename: '',
      pathname: '',
    },
  })

  useEffect(() => {
    const getSetting = async () => {
      try {
        const res = await axios.get(`${URL}/settings/hero_banner`)
        setSetting(res.data.data)
      } catch (err) {
        console.error(err)
      }
    }
    getSetting()
  }, [])

  const blackBannerWrapper = {
    height: '70vh',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: setting?.useBgColour
      ? `#${setting?.bgColour}`
      : 'lightgray',
    color: setting?.colour ? `#${setting?.colour}` : 'black',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    fontFamily: 'Roboto, sans-serif !important',
    textTransform: 'uppercase',
  }

  return (
    <section
      css={blackBannerWrapper}
      style={
        !setting?.useBgColour && setting?.image.filename
          ? { backgroundImage: `url(${URL}/assets/${setting?.image.filename})` }
          : null
      }
    >
      <div css={flexColumn}>
        <h1>{setting.title}</h1>
        <ul css={flexList}>
          {setting.fields.map((field, i) => {
            if (field.text) {
              return (
                <li key={i} onClick={() => (window.location.href = field.url)}>
                  {field.text}
                </li>
              )
            } else return null
          })}
        </ul>
      </div>
    </section>
  )
}
