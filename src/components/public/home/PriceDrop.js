/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import facepaint from 'facepaint'
import axios from 'axios'
import { Link } from 'react-router-dom'

import CarouselWithScrollBarJumbo from './carouselWithScrollBar/CarouselWithScrollBarJumbo'
import Carousel from './carouselWithScrollBar/CarouselMobile'
import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const priceDropWrapper = mq({
  marginTop: '2rem',
  padding: ['2rem 0', '2rem 0', '1rem 0', 0],
})

export default function PriceDrop(props) {
  const [priceDropItems, setPriceDropItems] = useState(null)

  useEffect(() => {
    let source = axios.CancelToken.source()

    const getPriceDropData = async () => {
      try {
        const res = await axios.get(`${URL}/ranges/priceDrop`, {
          cancelToken: source.token,
        })
        setPriceDropItems(res.data)
      } catch (err) {
        console.error(err)
      }
    }
    getPriceDropData()
    return () => source.cancel()
  }, [])

  if (priceDropItems?.length > 0) {
    return (
      <section css={priceDropWrapper}>
        <h1 className='title'>PRICE DROP</h1>
        <Link
          to={{
            pathname: `/catalogue`,
            state: {
              sale: 'All Sale',
            },
          }}
          className='see_all'
        >
          SEE ALL
        </Link>
        <div css={desktop}>
          <CarouselWithScrollBarJumbo items={priceDropItems} />
        </div>
        <div css={mobile}>
          <Carousel items={priceDropItems} />
        </div>
      </section>
    )
  } else {
    return null
  }
}

const desktop = mq({
  display: ['none', 'none', 'block'],
})

const mobile = mq({
  display: ['block', 'block', 'none'],
})
