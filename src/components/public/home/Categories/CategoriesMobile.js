import React from 'react'
import { Link } from 'react-router-dom'
import HorizontalScroll from 'react-scroll-horizontal'
import { isMobile } from 'react-device-detect'

import './categories.css'

function CategoriesMobile({ upholstery, curtain, craft, URL }) {
  if (isMobile) {
    return (
      <MobileCategories
        curtain={curtain}
        upholstery={upholstery}
        craft={craft}
        URL={URL}
      />
    )
  } else
    return (
      <DesktopCategories
        curtain={curtain}
        upholstery={upholstery}
        craft={craft}
        URL={URL}
      />
    )
}

export default CategoriesMobile

const DesktopCategories = ({ curtain, upholstery, craft, URL }) => {
  return (
    <div className='categories_slider'>
      <HorizontalScroll reverseScroll={true}>
        <Link
          className='link'
          to={{
            pathname: `/catalogue`,
            state: {
              usage: 'upholstery',
            },
          }}
          style={{
            backgroundImage:
              !upholstery.useBgColour &&
              upholstery.image.filename &&
              `url(${URL}/assets/${upholstery.image.filename})`,
            // backgroundColor: upholstery?.useBgColour
            //   ? `#${upholstery.bgColour}`
            //   : 'lightgray',
            color: upholstery?.colour
              ? `#${upholstery?.colour} !important`
              : 'black',
            marginLeft: '32px',
          }}
        >
          <h1>Upholstery</h1>
          <p>{upholstery?.title}</p>
        </Link>
        <Link
          className='link'
          to={{
            pathname: `/catalogue`,
            state: {
              usage: 'curtains',
            },
          }}
          style={
            !curtain?.useBgColour && curtain?.image?.filename
              ? {
                  backgroundImage: `url(${URL}/assets/${curtain?.image?.filename})`,
                }
              : {
                  backgroundColor: curtain?.useBgColour
                    ? `#${curtain.bgColour}`
                    : 'lightgray',
                  color: curtain?.colour
                    ? `#${curtain?.colour} !important`
                    : 'black',
                }
          }
        >
          <h1>Curtain</h1>
          <p>{curtain?.title}</p>
        </Link>
        <Link
          className='link last_link'
          to={{
            pathname: `/catalogue`,
            state: {
              usage: 'craft',
            },
          }}
          style={
            !craft?.useBgColour && craft?.image?.filename
              ? {
                  backgroundImage: `url(${URL}/assets/${craft?.image?.filename})`,
                }
              : {
                  backgroundColor: craft?.useBgColour
                    ? `#${craft.bgColour}`
                    : 'lightgray',
                  color: craft?.colour
                    ? `#${craft?.colour} !important`
                    : 'black',
                }
          }
        >
          <h1>Craft</h1>
          <p>{craft?.title}</p>
        </Link>
      </HorizontalScroll>
    </div>
  )
}

const MobileCategories = ({ curtain, upholstery, craft, URL }) => {
  return (
    <div className='categories_slider'>
      <Link
        className='link'
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'upholstery',
          },
        }}
        style={{
          backgroundImage:
            !upholstery?.useBgColour && upholstery?.image?.filename
              ? `url(${URL}/assets/${upholstery?.image?.filename})`
              : 'none',
          backgroundColor: upholstery?.useBgColour
            ? `#${upholstery.bgColour}`
            : 'lightgray',
          color: upholstery?.colour
            ? `#${upholstery?.colour} !important`
            : 'black',
          marginLeft: '32px',
        }}
      >
        <h1>Upholstery</h1>
        <p>{upholstery?.title}</p>
      </Link>
      <Link
        className='link'
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'curtains',
          },
        }}
        style={
          !curtain?.useBgColour && curtain?.image?.filename
            ? {
                backgroundImage: `url(${URL}/assets/${curtain?.image?.filename})`,
              }
            : {
                backgroundColor: curtain?.useBgColour
                  ? `#${curtain.bgColour}`
                  : 'lightgray',
                color: curtain?.colour
                  ? `#${curtain?.colour} !important`
                  : 'black',
              }
        }
      >
        <h1>Curtain</h1>
        <p>{curtain?.title}</p>
      </Link>
      <Link
        className='link last_link'
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'craft',
          },
        }}
        style={
          !craft?.useBgColour && craft?.image?.filename
            ? {
                backgroundImage: `url(${URL}/assets/${craft?.image?.filename})`,
              }
            : {
                backgroundColor: craft?.useBgColour
                  ? `#${craft.bgColour}`
                  : 'lightgray',
                color: craft?.colour ? `#${craft?.colour} !important` : 'black',
              }
        }
      >
        <h1>Craft</h1>
        <p>{craft?.title}</p>
      </Link>
    </div>
  )
}
