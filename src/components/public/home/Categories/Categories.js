/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import facepaint from 'facepaint'

import './categories.css'

import { URL } from '../../../../config'
import CategoriesMobile from './CategoriesMobile.js'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const main = mq({
  marginTop: '2rem',
  padding: '2rem 0',
})

const categoriesWrapper = mq({
  width: ['auto', 'auto', '100%'],
  overflowX: ['auto', 'auto', 'none'],
  display: ['none', 'none', 'grid'],
  gridTemplateColumns: '1fr 1fr',
  height: ['50vh', '65vh', '80vh', '100vh'],
  h1: {
    fontFamily: 'cooper-black-std, serif',
    fontSize: ['1rem', '1.5rem', '2rem', '2.5rem'],
  },
  p: {
    fontFamily: 'Roboto',
    fontSize: '1.5rem',
    fontWeight: '300',
  },
  a: {
    textDecoration: 'inherit',
    margin: '1rem 16px',
    padding: '1rem',
    borderRadius: '10px',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    width: ['calc(100vw - 64px)', 'calc(100vw - 64px)', 'auto'],
    height: ['calc(100vw - 64px)', 'calc(100vw - 64px)', 'auto'],
  },
  // SCROOLLBAR STYLE
  msOverflowStyle: 'none' /* IE and Edge */,
  scrollbarWidth: 'none',
  '::-webkit-scrollbar': {
    display: 'none' /* for Chrome, Safari, and Opera */,
  },
  '.line': {
    margin: '.5rem 0',
    width: '100%',
    height: '2px',
    backgroundColor: 'rgba(225, 225, 225)',
  },
})

const doubleColumn = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr 1fr', '1fr 1fr', '1fr'],
  padding: '1rem 0',
  a: {
    margin: ['0 16px 0 0', '0 16px 0 0', '0 32px 0 0rem'],
    borderRadius: '10px',
  },
})

export default function Categories() {
  const [upholstery, setUpholstery] = useState(null)
  const [curtain, setCurtain] = useState(null)
  const [craft, setCraft] = useState(null)

  useEffect(() => {
    const getSettings = async () => {
      try {
        Promise.all([
          axios.get(`${URL}/settings/upholstery`),
          axios.get(`${URL}/settings/curtain`),
          axios.get(`${URL}/settings/craft`),
        ]).then((values) => {
          setUpholstery(values[0].data.data)
          setCurtain(values[1].data.data)
          setCraft(values[2].data.data)
        })
      } catch (err) {
        console.error(err)
      }
    }
    getSettings()
  }, [])

  const singleColumn = {
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: upholstery?.useBgColour
      ? `#${upholstery?.bgColour}`
      : 'lightgray',
    color: upholstery?.colour ? `#${upholstery?.colour} !important` : 'black',
  }

  const curtainElement = mq({
    height: ['100%', '100%', '95% !important'],
    backgroundColor: curtain?.useBgColour
      ? `#${curtain?.bgColour}`
      : 'lightgray',
    color: curtain?.colour ? `#${curtain?.colour} !important` : 'black',
  })

  const craftElement = {
    backgroundColor: craft?.useBgColour ? `#${craft?.bgColour}` : 'lightgray',
    color: craft?.colour ? `#${craft?.colour} !important` : 'black',
  }

  if (curtain && upholstery && craft) {
    return (
      <section css={main}>
        <div css={categoriesWrapper}>
          <Upholstery />
          <div css={doubleColumn}>
            <Curtain />
            <Craft />
          </div>
        </div>
        <div css={mobile}>
          <CategoriesMobile
            upholstery={upholstery}
            curtain={curtain}
            craft={craft}
            URL={URL}
          />
        </div>
      </section>
    )
  } else return null

  function Upholstery() {
    return (
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'upholstery',
          },
        }}
        css={singleColumn}
        style={{
          backgroundImage:
            !upholstery.useBgColour && upholstery.image.filename
              ? `url(${URL}/assets/${upholstery.image.filename})`
              : null,
          marginLeft: '32px',
        }}
      >
        <h1>Upholstery</h1>
        <p>{upholstery.title}</p>
      </Link>
    )
  }

  function Curtain() {
    return (
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'curtains',
          },
        }}
        css={curtainElement}
        style={
          !curtain.useBgColour && curtain.image.filename
            ? {
                backgroundImage: `url(${URL}/assets/${curtain.image.filename})`,
              }
            : null
        }
      >
        <h1>Curtain</h1>
        <p>{curtain.title}</p>
      </Link>
    )
  }
  function Craft() {
    return (
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'craft',
          },
        }}
        css={craftElement}
        style={
          !craft.useBgColour && craft.image.filename
            ? {
                backgroundImage: `url(${URL}/assets/${craft.image.filename})`,
              }
            : null
        }
      >
        <h1>Craft</h1>
        <p>{craft.title}</p>
      </Link>
    )
  }
}

const mobile = mq({
  display: ['block', 'block', 'none'],
})
