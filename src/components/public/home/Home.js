/** @jsx jsx */
import { jsx } from '@emotion/react'

import './home.css'

import TopMiniSlider from './TopMiniSlider'
import BlackBanner from './BlackBanner'
import Categories from './Categories/Categories'
import NewArrivals from './NewArrivals'
import Trending from './Trending'
import Ranges from './Ranges'
import TagCarousel from './TagCarousel'
// import MiddleBanner from './MiddleBanner'
import PriceDrop from './PriceDrop'

const show = {
  minWidth: '100vw',
  width: '100vw',
  maxWidth: '100vw',
}

export default function Home(props) {
  return (
    <div css={show}>
      <TagCarousel />
      <TopMiniSlider />
      <BlackBanner />
      <Categories />
      <PriceDrop />
      <NewArrivals />
      <Ranges />
      {/* <MiddleBanner /> */}
      <Trending />
    </div>
  )
}
