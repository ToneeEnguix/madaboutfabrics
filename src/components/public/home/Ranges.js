/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import CarouselWithScrollBarBig from './carouselWithScrollBar/CarouselWithScrollBarBig'
import Carousel from './carouselWithScrollBar/CarouselMobile'
import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700, 950, 1100]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const saleWrapper = mq({
  marginTop: '2rem',
  padding: ['2rem 0', '2rem 0', '1rem 0', 0],
})

export default function Ranges() {
  const [rangeItems, setRangeItems] = useState([])

  useEffect(() => {
    const abort = new AbortController()
    const getRangesData = () => {
      const signal = abort.signal
      const requestOptions = {
        method: 'GET',
        redirect: 'follow',
        signal: signal,
      }

      fetch(`${URL}/ranges/ranges`, requestOptions)
        .then((response) => response.json())
        .then((result) => setRangeItems(result))
        .catch((error) => console.error('error', error))
    }
    getRangesData()
    return () => {
      abort.abort()
    }
  }, [])

  if (rangeItems.length > 0) {
    return (
      <section css={saleWrapper}>
        <h1 className='title'>RANGES</h1>
        <Link className='see_all' to='/ranges'>
          SEE ALL
        </Link>
        <div css={desktop}>
          <CarouselWithScrollBarBig items={rangeItems} size={'big'} />
        </div>
        <div css={mobile}>
          <Carousel items={rangeItems} />
        </div>
      </section>
    )
  } else {
    return null
  }
}

const desktop = mq({
  display: ['none', 'none', 'block'],
})

const mobile = mq({
  display: ['block', 'block', 'none'],
})
