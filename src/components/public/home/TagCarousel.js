/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import axios from 'axios'
import 'react-multi-carousel/lib/styles.css'

import { URL } from '../../../config'
import Carousel from 'react-multi-carousel'

export default function TagCarousel() {
  const [tags, setTags] = useState([{}])

  useEffect(() => {
    const getMiniSlider = async () => {
      try {
        const res = await axios.get(`${URL}/tags/minislider`)
        setTags(res.data)
      } catch (err) {
        console.error(err)
      }
    }
    getMiniSlider()
  }, [])

  return (
    <section css={topMiniSliderWrapper}>
      <div css={centeredCarousel}>
        <Carousel
          additionalTransfrom={0}
          arrows={false}
          autoPlay
          autoPlaySpeed={1}
          centerMode={false}
          className='carousel'
          customTransition='all 4s linear'
          draggable
          focusOnSelect={false}
          infinite
          keyBoardControl
          minimumTouchDrag={80}
          pauseOnHover
          renderArrowsWhenDisabled={false}
          renderButtonGroupOutside={false}
          renderDotsOutside={false}
          responsive={responsive}
          rewind={false}
          rewindWithAnimation={false}
          rtl={false}
          shouldResetAutoplay
          showDots={false}
          sliderClass=''
          slidesToSlide={1}
          swipeable
          transitionDuration={4000}
        >
          {tags.map((tag, i) => {
            if (tag.url) {
              return (
                <div key={i} css={tagStyle}>
                  <a href={tag.url} rel='noopener noreferrer'>
                    {tag.name}
                  </a>
                </div>
              )
            } else {
              return (
                <div key={i} css={tagStyle}>
                  {tag.name}
                </div>
              )
            }
          })}
        </Carousel>
      </div>
    </section>
  )
}

const responsive = {
  superLargeDesktop: {
    breakpoint: { max: 4000, min: 3000 },
    items: 10,
  },
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 8,
  },
  tablet1: {
    breakpoint: { max: 1024, min: 800 },
    items: 6,
  },
  tablet2: {
    breakpoint: { max: 800, min: 464 },
    items: 4,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 3,
  },
}

const topMiniSliderWrapper = {
  borderTop: '0.25px solid rgb(230,230,230)',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  height: '55px',
}

const centeredCarousel = {
  height: '100%',
  width: '100%',
  '*': { textAlign: 'center' },
  '.carousel': {
    height: '100%',
    ul: {
      height: '100%',
      alignItems: 'center',
    },
  },
}

const tagStyle = {
  width: '90%',
  padding: '0.3rem 1rem',
  borderRadius: '20px',
  margin: '0 1rem 0 0',
  fontSize: '0.7rem',
  fontFamily: 'Roboto,sans-serif',
  fontWeight: '300',
  boxShadow: '0px 3px 6px #00000029',
  cursor: 'pointer',
  transition: 'all linear 150ms',
  whiteSpace: 'nowrap',
  a: {
    color: 'inherit',
    textDecoration: 'none',
    display: 'block',
  },
  ':hover': {
    backgroundColor: '#212121',
    color: 'white',
  },
}
