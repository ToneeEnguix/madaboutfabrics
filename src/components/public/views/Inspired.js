/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'
import ReactLoading from 'react-loading'

import { URL } from '../../../config'

import Slider from 'infinite-react-carousel'
import arrow from '../../admin/pictures/arrow.svg'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const loadingStyle = {
  height: 'calc(100vh - 159px)',
  backgroundColor: 'white',
  'img, h2': {
    marginTop: '-2rem',
    height: '5rem',
    fontFamily: 'Roboto, sans-serif',
    fontWeight: '500',
  },
}

const slider = mq({
  display: ['none', 'block'],
  height: '90vh',
  minHeight: '640px',
  width: '100vw',
  border: 'none',
  padding: 0,
  div: {
    padding: 0,
    border: 'none',
  },
})

const show = {
  height: '90vh',
  minHeight: '640px',
  display: 'grid !important',
  gridTemplateColumns: '1fr 1fr',
  div: {
    width: '50vw',
  },
}

const sliderMobile = mq({
  display: ['auto', 'none'],
  height: '90vh',
  minHeight: '640px',
  width: '100vw',
  border: 'none',
  padding: 0,
})

const mobileWrapper = mq({
  position: 'relative',
  minHeight: '640px',
  '.productImage': {
    position: 'absolute',
    width: '100vw',
    height: '100%',
    objectFit: 'cover',
  },
  '.fade': {
    position: 'absolute',
    width: '100vw',
    height: '70%',
    background:
      'linear-gradient(rgba(0, 0, 0, 0.9), rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0))',
  },
  '.productInfo': {
    position: 'absolute',
    color: 'white',
    padding: '2rem',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: '1.2rem',
    fontWeight: 100,
    height: '100%',

    display: 'flex',
    flexDirection: 'column',
  },
  '.description': {
    marginTop: '2rem',
    fontWeight: 400,
  },
  '.seeall': {
    color: 'inherit',
    textDecoration: 'none',
    backgroundColor: 'black',
    display: 'block',
    width: '80%',
    textAlign: 'center',
    padding: '.8rem 0',
    fontSize: '1.1rem',
    fontWeight: 400,
    border: '1px solid white',
    alignSelf: 'center',
    borderRadius: '6px',
    marginTop: '2rem',
    position: 'absolute',
    bottom: '100px',
  },
  '.bold': {
    fontSize: '2rem',
  },
})

const imageWrapper = {
  width: '50vw',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  img: {
    flexShrink: '0',
    minWidth: '100%',
    minHeight: '100%',
  },
}

const infoWrapper = {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  textAlign: 'center',
  h3: {
    fontFamily: 'Montserrat, sans-serif',
    textTransform: 'uppercase',
    fontSize: '1rem',
    letterSpacing: '1px',
    marginTop: '8rem',
  },
  'h1, p': {
    fontFamily: 'Nanum Myeongjo, serif',
    fontWeight: '100',
  },
  h1: {
    marginTop: '2rem',
    maxWidth: '30vw',
    fontSize: '2.2rem',
    fontWeight: '100',
  },
  p: {
    marginTop: '3rem',
    maxWidth: '30vw',
    fontSize: '1.2rem',
    letterSpacing: '.2px',
    lineHeight: '1.7rem',
  },
  button: {
    marginTop: '5rem',
    fontSize: '1rem',
    textTransform: 'uppercase',
    fontWeight: '600',
    letterSpacing: '1px',
    fontFamily: 'Montserrat, sans-serif',
    outline: 'none',
    border: 'none',
    backgroundColor: '#00263E',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '8px',
    color: 'white',
    height: '60px',
    padding: '0 6vw',
    ':hover': {
      backgroundColor: 'whitesmoke',
      color: '#00263E',
      boxShadow: '2px 8px 15px #00001029',
    },
  },
}

export default function Inspired() {
  const [ranges, setRanges] = useState([{ variants: [{ imageURLs: [{}] }] }])
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const getInspiredRanges = async () => {
      try {
        const res = await axios.get(`${URL}/ranges/inspired`)
        setRanges(res.data.data)
        setLoaded(true)
      } catch (err) {
        console.error(err)
      }
    }
    window.scrollTo(0, 0)
    getInspiredRanges()
  }, [])

  return (
    <div>
      <div style={{ height: '90vh', minHeight: '640px' }}>
        {!loaded ? (
          <div className='flexCenter' css={loadingStyle}>
            <ReactLoading type={'spin'} color='#030303' />
          </div>
        ) : ranges[0] && ranges[0].name ? (
          [
            <Slider
              autoplay
              css={slider}
              key='1'
              nextArrow={
                <div>
                  <img src={arrow} alt='arrow' />
                </div>
              }
              prevArrow={
                <div>
                  <img src={arrow} alt='arrow' />
                </div>
              }
            >
              {ranges.map((range) => {
                return (
                  <div css={show} key={range._id}>
                    <div css={imageWrapper}>
                      <img
                        alt='product'
                        src={`${URL}/assets/${range.variants[0].imageURLs[0].filename}`}
                      />
                    </div>
                    <div css={infoWrapper}>
                      <h3>New Arrivals</h3>
                      <h1>
                        {range.name} | {range.variants[0].color}
                      </h1>
                      <p>'{range.description}'</p>
                      <Link
                        to={
                          range.url
                            ? `/productdetails/${range.url}/0`
                            : `/productdetails/${range._id}/0`
                        }
                      >
                        <button className='pointer'>View Product</button>
                      </Link>
                      {/* dots */}
                    </div>
                  </div>
                )
              })}
            </Slider>,
            <Slider arrows={false} autoplay css={sliderMobile} key='2'>
              {ranges.map((range) => {
                return (
                  <div key={range._id} css={mobileWrapper}>
                    <img
                      alt='product'
                      src={`${URL}/assets/${range.variants[0].imageURLs[0].filename}`}
                      className='productImage'
                    />
                    <div className='fade' />
                    <div className='productInfo'>
                      <p>{range.name}</p>
                      <p className='bold upper'>{range.variants[0].color}</p>
                      <p>£{range.price} per metre</p>
                      <p className='description'>{range.description}</p>
                      {/* dots */}
                      <Link
                        to={
                          range.url
                            ? `/productdetails/${range.url}/0`
                            : `/productdetails/${range._id}/0`
                        }
                        className='seeall upper'
                      >
                        See all
                      </Link>
                    </div>
                  </div>
                )
              })}
            </Slider>,
          ]
        ) : (
          <div className='flexCenter' css={loadingStyle}>
            <h2>Much more coming soon!</h2>
          </div>
        )}
      </div>
    </div>
  )
}
