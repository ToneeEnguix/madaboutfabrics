/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import ReactLoading from 'react-loading'
import facepaint from 'facepaint'
import backgroundImg from './imgs/absolutelyFabulous.jpg'

import { URL } from '../../../config'

import CountDown from '../helpers/countdown.js'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 800]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const loadingStyle = {
  height: 'calc(100vh - 159px)',
  backgroundColor: 'white',
  'img, h2': {
    marginTop: '-2rem',
    height: '5rem',
    fontFamily: 'Roboto, sans-serif',
    fontWeight: '500',
  },
}

const infoWrapper = mq({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  textAlign: 'center',
  a: {
    width: '100%',
  },
  button: {
    fontSize: '1.1rem',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: '600',
    letterSpacing: '1px',
    marginTop: '1rem',
    textTransform: 'uppercase',
    outline: 'none',
    border: '2px solid black',
    backgroundColor: 'white',
    borderRadius: '8px',
    height: '55px',
    width: '50%',
    minWidth: '200px',
    ':hover': {
      backgroundColor: 'whitesmoke',
      boxShadow: '2px 8px 15px #00001029',
    },
  },
  // this is the countdown
  span: {
    fontSize: ['2rem', '3rem', '4rem'],
  },
})

const titles = mq({
  margin: '1rem auto 0',
  '.normal': {
    textTransform: 'uppercase',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: 800,
    fontSize: ['1.2rem', '1.5rem', '1.7rem'],
  },
  '.crazy': {
    margin: ['-1rem auto -.5rem', '-1rem auto -.5rem'],
    fontSize: ['3rem', '3.5rem', '5rem'],
    fontWeight: 100,
    maxWidth: '90vw',
    fontFamily: 'Avallon_Alt, sans-serif',
    letterSpacing: '2px',
  },
})

export default function Sale() {
  const [loaded, setLoaded] = useState(false)
  const [range, setRange] = useState([
    { variants: [{ imageURLs: [{ filename: '' }] }] },
  ])
  const [idx, setIdx] = useState(null)

  useEffect(() => {
    const getNewRanges = async () => {
      try {
        const res = await axios.get(`${URL}/ranges/sale`)
        setIdx(res.data.idx)
        setRange(res.data.data)
        setLoaded(true)
      } catch (err) {
        console.error(err)
      }
    }
    window.scrollTo(0, 0)
    getNewRanges()
  }, [])

  const show = mq({
    minHeight: '640px',
    height: 'calc(100vh - 159px)',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundImage: `url(${backgroundImg})`,
    backgroundRepeat: 'no-repeat',
    WebkitBackgroundSize: 'cover',
    MozBackgroundSize: 'cover',
    OBackgroundSize: 'cover',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    img: {
      width: '50vw',
      minHeight: '100%',
      minWidth: '100%',
    },
  })

  return (
    <div>
      <div style={{ height: '640px' }}>
        {!loaded ? (
          <div className='flexCenter' css={loadingStyle}>
            <ReactLoading type={'spin'} color='#030303' />
          </div>
        ) : range && range.name ? (
          <div css={show}>
            <div css={infoWrapper}>
              <div className='flexColumn' css={titles}>
                <p className='normal'>the</p>
                <p className='crazy'>Absolutely Fabulous</p>
                <p className='normal'>pricedrops</p>
              </div>
              <CountDown futureDate={range.variants[idx].priceDropDate} />
              <Link
                to={{
                  pathname: `/catalogue`,
                  state: {
                    sale: 'All Sale',
                  },
                }}
              >
                <button className='pointer'>SEE ALL</button>
              </Link>
            </div>
          </div>
        ) : (
          <div className='flexCenter' css={loadingStyle}>
            <h2>Much more coming soon!</h2>
          </div>
        )}
      </div>
    </div>
  )
}
