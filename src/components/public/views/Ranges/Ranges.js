/** @jsx jsx */
import { jsx } from '@emotion/react'
import axios from 'axios'
import { useEffect, useState } from 'react'
import ReactLoading from 'react-loading'
import facepaint from 'facepaint'

import { URL } from '../../../../config'

import RangeView from './RangeView'
import Pagination from './Pagination'

// RESPONSIVENESS SETTINGS
const breakpoints = [360, 484, 600, 835]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function Ranges() {
  const [loaded, setLoaded] = useState(false)
  const [allRanges, setAllRanges] = useState(null)
  const [count, setCount] = useState(0)
  const [page, setPage] = useState(0)
  const [size, setSize] = useState(50)
  const pageSizeOptions = [25, 50, 75]

  useEffect(() => {
    const getAllRanges = async () => {
      setLoaded(false)
      try {
        const res = await axios.get(`${URL}/ranges/all/${size}/${page}`)
        setCount(res.data.count)
        setAllRanges(res.data.data)
      } catch (err) {
        console.error(err)
      }
      setLoaded(true)
    }
    getAllRanges()
  }, [page, size])

  return (
    <div>
      <div css={show}>
        <div css={countStyle}>
          <p>{count} products</p>
          <div className='flexCenter' style={{ marginTop: '.5rem' }}>
            <select
              onChange={(e) => {
                setPage(0)
                setSize(Number(e.target.value))
              }}
              value={size}
              className='pointer'
            >
              {pageSizeOptions.map((item) => {
                return (
                  <option key={item} value={item}>
                    {item}
                  </option>
                )
              })}
            </select>
            <p style={{ marginLeft: '.25rem' }}>per page</p>
          </div>
        </div>
        <Pagination
          totalCount={count}
          page={page}
          setPage={setPage}
          size={size}
        />
        {!loaded ? (
          <div className='loadingStyle flexCenter' css={loadingStyle}>
            <ReactLoading type={'spin'} color='#030303' />
          </div>
        ) : (
          <div css={rangesWrapper}>
            {allRanges.map((range, idx) => {
              return <RangeView key={idx} item={range} />
            })}
          </div>
        )}
        <Pagination
          totalCount={count}
          page={page}
          setPage={setPage}
          size={size}
        />
      </div>
    </div>
  )
}

const loadingStyle = {
  backgroundColor: 'white',
  minHeight: '80vh',
  img: {
    width: '8vw',
  },
}

const show = mq({
  borderTop: '0.25px solid rgb(230,230,230)',
  minHeight: '1000px',
  width: '100%',
  padding: '1.5rem 2vw 3rem',
  select: {
    borderRadius: '6px',
    padding: '3px 0 3px 2px',
  },
})

const countStyle = mq({
  display: 'flex',
  flexDirection: 'column',
  padding: '0 2rem 0 0',
  alignItems: 'flex-end',
  fontFamily: 'Montserrat, sans-serif',
  marginBottom: ['1.5rem', '1.5rem', '1.5rem', 0],
})

const rangesWrapper = mq({
  // display: 'flex',
  // justifyContent: 'center',
  // flexWrap: 'wrap',
  // width: 'fit-content',
  margin: '2rem auto 0',
  // test
  width: '90vw',
  display: 'grid',
  gridTemplateColumns: [
    'repeat(auto-fill, 90vw)',
    'repeat(auto-fill, 40vw)',
    'repeat(auto-fill, 40vw)',
    'repeat(auto-fill, 210px)',
    'repeat(auto-fill, 250px)',
  ],
  justifyContent: 'space-between',
  // gridGap: '20px',
})
