/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { useState } from 'react'

import PhotoLink from './PhotoLink'
import VariantMiniatures from './VariantMiniatures'

// RESPONSIVENESS SETTINGS
const breakpoints = [360, 484, 600]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function ProductRowRegular({ item }) {
  const [idx, setIdx] = useState(0)

  return (
    <div css={show}>
      <PhotoLink item={item} idx={idx} />
      <div css={content}>
        <p>{item.name}</p>
        <p>
          {item.variants[idx].color.slice(0, 16) +
            (item.variants[idx].color.length > 16 ? '...' : '')}
        </p>
        <Prices item={item} />
      </div>
      <VariantMiniatures item={item} setIdx={(i) => setIdx(i)} idx={idx} />
    </div>
  )
}

const show = mq({
  width: 'fit-content',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  fontFamily: 'Montserrat, sans-serif',
  margin: '0 auto 3rem',
  '& a': {
    color: 'inherit',
    textDecoration: 'inherit',
  },
})

const content = mq({
  width: ['80vw', '39vw', '200px'],
  display: 'flex',
  justifyContent: 'center',
  marginBottom: '0.75rem',
  flexDirection: 'column',
  'p:first-of-type, span': {
    fontWeight: 300,
    textTransform: 'inherit',
  },
  p: {
    fontWeight: 700,
    textTransform: 'uppercase',
  },
})

const Prices = ({ item }) => {
  if (
    item.variants[0].priceDrop === true &&
    new Date(item.variants[0].priceDropDate).getTime() > Date.now() &&
    item.variants[0].saleDiscount > 0
  ) {
    return (
      <div css={prices}>
        <span css={discounted}>£{item.price.toFixed(2)}</span>
        <span>
          £
          {(
            item.price -
            (item.price * item.variants[0].saleDiscount) / 100
          ).toFixed(2)}
        </span>
      </div>
    )
  } else
    return (
      <div css={prices}>
        <span>£{item.price.toFixed(2)}</span>
      </div>
    )
}

const prices = {
  margin: '0.4rem 0 0',
}

const discounted = {
  textDecoration: 'line-through',
  marginRight: '0.5rem',
}
