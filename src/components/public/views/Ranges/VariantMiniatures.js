/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import { URL } from '../../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [360, 484, 600]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function VariantMiniatures({ item, idx, setIdx }) {
  return (
    <div css={variantMiniatures}>
      {item.variants.map((variant, i) => {
        if (i < 4 && i !== idx) {
          return (
            <div
              css={miniatureWrapper}
              className='pointer'
              onClick={() => setIdx(i)}
              key={i}
            >
              <img
                alt='product'
                css={imageFitter}
                src={`${URL}/assets/${variant.imageURLs[0]?.filename}`}
              />
            </div>
          )
        } else if (i === 4) {
          return (
            <Link
              to={{
                pathname: item.url
                  ? `/productdetails/${item.url}/4`
                  : `/productdetails/${item._id}/4`,
                state: {
                  item: item,
                  index: 4,
                },
              }}
              key={i}
              css={miniatureWrapper}
              className='miniature_plus'
            >
              <p css={imageFitter}>{'+' + (item.variants.length - 3)}</p>
            </Link>
          )
        } else return null
      })}
    </div>
  )
}

const variantMiniatures = mq({
  display: 'flex',
  justifyContent: 'flex-start',
  alignItems: 'center',
  flexWrap: 'wrap',
  width: '100%',
  '.miniature_plus': {
    width: ['40px', '32px', '40px'],
    height: ['40px', '32px', '40px'],
    borderRadius: '100px',
    boxShadow: '0px 3px 6px #00000029',
  },
})

const miniatureWrapper = mq({
  width: ['40px', '32px', '40px'],
  height: ['40px', '32px', '40px'],
  margin: ['0 .5rem 0 0', '0 .2rem 0 0', '0 .5rem 0 0'],
  // for img centered
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  borderRadius: '8px',
  p: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

const imageFitter = {
  flexShrink: 0,
  objectFit: 'cover',
  maxWidth: '100%',
  maxHeight: '100%',
  minWidth: '100%',
  minHeight: '100%',
}
