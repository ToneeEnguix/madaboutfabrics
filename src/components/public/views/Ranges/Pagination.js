/** @jsx jsx */
import { jsx } from '@emotion/react'
import Pagination from 'react-js-pagination'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [360]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function PaginationComponent({
  totalCount,
  page,
  setPage,
  size,
}) {
  return (
    <div css={pageSelectorWrapper}>
      <Pagination
        activePage={page + 1}
        itemsCountPerPage={size}
        totalItemsCount={totalCount}
        pageRangeDisplayed={3}
        onChange={(e) => setPage(e - 1)}
        hideDisabled={true}
        activeLinkClass={'selectedIndex'}
      />
    </div>
  )
}

const pageSelectorWrapper = mq({
  display: 'flex',
  ul: {
    display: 'flex',
    margin: '0 auto',
  },
  li: {
    listStyle: 'none',
    margin: ['0 .2rem', '0 .5rem'],
  },
  a: {
    textDecoration: 'none',
    color: 'black',
    borderRadius: '100px',
    backgroundColor: 'whitesmoke',
    width: ['30px', '35px'],
    height: ['30px', '35px'],
    fontSize: ['.9rem', '1rem'],
    fontFamily: 'Roboto, sans-serif',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  '.selectedIndex': {
    border: '1px solid black',
  },
})
