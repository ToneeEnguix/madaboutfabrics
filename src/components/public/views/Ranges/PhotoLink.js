/** @jsx jsx */
import { jsx } from '@emotion/react'
import Countdown from 'react-countdown'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import { URL } from '../../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [360, 484, 600]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function PhotoLink({ item, idx }) {
  return (
    <Link
      to={{
        pathname: item.url
          ? `/productdetails/${item.url}/${idx}`
          : `/productdetails/${item._id}/${idx}`,
        state: {
          item: item,
          index: idx,
        },
      }}
    >
      <div css={imageWrapper}>
        <CoverNotices item={item} />
        <img
          alt='product'
          css={imageFitter}
          src={`${URL}/assets/${item.variants[idx].imageURLs[0]?.filename}`}
        />
      </div>
    </Link>
  )
}

const imageWrapper = mq({
  position: 'relative',
  marginBottom: '.5rem',
  boxShadow: '0px 3px 6px #00000029',
  borderRadius: '10px',
  height: ['80vw', '39vw', '200px'],
  width: ['80vw', '39vw', '200px'],
  // for img centered
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
})

const imageFitter = {
  flexShrink: 0,
  objectFit: 'cover',
  maxWidth: '100%',
  maxHeight: '100%',
  minWidth: '100%',
  minHeight: '100%',
}

const CoverNotices = ({ item }) => {
  if (
    item.variants[0].priceDrop === true &&
    new Date(item.variants[0].priceDropDate).getTime() > Date.now() &&
    item.variants[0].saleDiscount > 0
  ) {
    return (
      <div css={notice}>
        <Countdown date={item.variants[0].priceDropDate} />
      </div>
    )
  } else return null
}

const notice = {
  backgroundColor: 'black',
  color: 'white',
  padding: '0.5rem 1em',
  fontSize: '0.7em',
  letterSpacing: '0.1em',
  zIndex: '2',
  position: 'absolute',
  top: 0,
  left: 0,
  fontFamily: 'Roboto, sans-serif',
}
