/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { useEffect, useState } from 'react'

import close from '../../../../resources/closeBlack.svg'
import tick from '../../../admin/pictures/tick.svg'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 600]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function FiltersMobile({
  updateState,
  searched,
  sortBy,
  sort,
  setSortBy,
  usage,
  selectedUsage,
  prices,
  selectedPrice,
  colors,
  selectedColor,
  type,
  selectedType,
  design,
  selectedDesign,
  fabricCare,
  selectedCare,
  fabricWidth,
  selectedWidth,
  onSale,
  selectedSale,
  texture,
  selectedTexture,
  industry,
  selectedIndustry,
  selectedPvc,
  showFilters,
  setShowFilters,
  removeFilters,
}) {
  const [filtersAplied, setFiltersApplied] = useState(0)

  useEffect(() => {
    let count = 0
    searched && count++
    selectedColor && count++
    selectedPrice && count++
    selectedUsage && count++
    selectedSale && count++
    selectedDesign && count++
    selectedTexture && count++
    selectedType && count++
    selectedWidth && count++
    selectedCare && count++
    selectedIndustry && count++
    selectedPvc && count++
    setFiltersApplied(count)
  }, [
    searched,
    selectedCare,
    selectedColor,
    selectedDesign,
    selectedIndustry,
    selectedPrice,
    selectedPvc,
    selectedSale,
    selectedTexture,
    selectedType,
    selectedUsage,
    selectedWidth,
  ])

  if (showFilters) {
    return (
      <div css={filtersStyle}>
        <div>
          <div onClick={() => setShowFilters(false)} className='cross pointer'>
            <img src={close} alt='cross' />
          </div>
          <div className='top'>
            <p>Filters</p>
          </div>
          <div className='filtersWrapper'>
            <div css={sortByStyle}>
              <h6>Sort by</h6>
              <div css={filterWrapper}>
                {sort.map((sortItem, i) => {
                  let equal = sortBy === sortItem
                  return (
                    <div
                      className='optionWrapper pointer flex'
                      value={sortItem}
                      key={i}
                      onClick={() => setSortBy(sortItem)}
                    >
                      <div
                        className='circle flexCenter'
                        style={{ borderColor: equal ? 'black' : 'gray' }}
                      >
                        <div
                          className='circle2'
                          style={{
                            backgroundColor: equal ? 'black' : 'whitesmoke',
                            height: equal ? '12px' : '19px',
                            width: equal ? '12px' : '19px',
                          }}
                        />
                      </div>
                      <span>{sortItem}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>I need this fabric for</h6>
              <div css={filterWrapper}>
                {usage.map((use) => {
                  let equal = use.toLowerCase() === selectedUsage
                  return (
                    <div
                      onClick={() => updateState('usage', use.toLowerCase())}
                      key={use}
                      className='flex optionWrapper pointer'
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{use}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <p>Price per metre</p>
              <div css={filterWrapper}>
                {prices.map((price) => {
                  let equal = price === selectedPrice
                  return (
                    <div
                      onClick={() => updateState('price', price)}
                      key={price}
                      className='flex optionWrapper pointer'
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{price}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>Colour: </h6>
              <div css={filterWrapper}>
                {colors.map((color) => {
                  let equal = color.name === selectedColor?.name
                  return (
                    <div
                      key={color._id || Date.now()}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('color', color)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{color.name}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>Fabric Type: </h6>
              <div css={filterWrapper}>
                {type.map((item) => {
                  let equal = item.name === selectedType?.name
                  return (
                    <div
                      key={item._id || Date.now()}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('type', item)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{item.name}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>Fabric Design: </h6>
              <div css={filterWrapper}>
                {design.map((item) => {
                  let equal = item.name === selectedDesign?.name
                  return (
                    <div
                      key={item._id || Date.now()}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('design', item)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{item.name}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>Fabric Care: </h6>
              <div css={filterWrapper}>
                {fabricCare.map((item) => {
                  let equal = item === selectedCare
                  return (
                    <div
                      key={item}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('care', item)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{item}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>Fabric Width: </h6>
              <div css={filterWrapper}>
                {fabricWidth.map((item) => {
                  let equal = item === selectedWidth
                  return (
                    <div
                      key={item}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('width', item)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{item}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>On Sale: </h6>
              <div css={filterWrapper}>
                {onSale.map((item) => {
                  let equal = item === selectedSale
                  return (
                    <div
                      key={item}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('sale', item)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{item}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>Texture: </h6>
              <div css={filterWrapper}>
                {texture.map((item) => {
                  let equal = item.name === selectedTexture?.name
                  return (
                    <div
                      key={item._id || Date.now()}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('texture', item)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{item.name}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>Industry: </h6>
              <div css={filterWrapper}>
                {industry.map((item) => {
                  let equal = item === selectedIndustry
                  return (
                    <div
                      key={item}
                      className='flex optionWrapper pointer'
                      onClick={() => updateState('industry', item)}
                    >
                      <div
                        className='square'
                        style={{
                          backgroundColor: equal && 'black',
                          borderColor: equal && 'black',
                        }}
                      >
                        {equal && <img src={tick} alt='tick' />}
                      </div>
                      <span>{item}</span>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className='line' />
            <div>
              <h6>PVC TableCloth: </h6>
              <div css={filterWrapper}>
                <div
                  className='flex optionWrapper pointer'
                  onClick={() => updateState('pvc', 'pvc')}
                >
                  <div
                    className='square'
                    style={{
                      backgroundColor: selectedPvc && 'black',
                      borderColor: selectedPvc && 'black',
                    }}
                  >
                    {selectedPvc && <img src={tick} alt='tick' />}
                  </div>
                  <span>PVC</span>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div css={buttonWrapper}>
          <button
            onClick={() => removeFilters()}
            className='uppercase pointer cancel'
          >
            Delete ({filtersAplied})
          </button>
          <button
            onClick={() => setShowFilters(false)}
            className='uppercase pointer apply'
          >
            Apply
          </button>
        </div>
      </div>
    )
  } else return null
}

const filtersStyle = {
  position: 'fixed',
  top: 0,
  left: 0,
  width: '100vw',
  height: '100vh',
  backgroundColor: 'white',
  zIndex: 100,
  padding: '1rem 1rem 6rem 2rem',
  fontFamily: 'Roboto, sans-serif',
  overflowY: 'scroll',
  // SCROOLLBAR STYLE
  msOverflowStyle: 'none' /* IE and Edge */,
  scrollbarWidth: 'none',
  '::-webkit-scrollbar': {
    display: 'none' /* for Chrome, Safari, and Opera */,
  },
  '.cross img': {
    backgroundColor: 'white',
    borderRadius: '100px',
    border: '1px solid gray',
    width: '30px',
    height: '30px',
    padding: '3px',
    position: 'fixed',
    top: '15px',
    right: '15px',
  },
  '.top': {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  '.line': {
    width: '100%',
    height: '2px',
    backgroundColor: '#F0F0F0',
    margin: '2rem 0',
  },
  h6: {
    fontSize: '1rem',
    margin: '1.5rem 0 1rem',
    fontWeight: 500,
  },
  '.square': {
    height: '20px',
    width: '20px',
    border: 'solid 1px #C0C0C0',
    backgroundColor: 'whitesmoke',
    borderRadius: '6px',
  },
  span: {
    margin: '0 0 0 .5rem',
    fontWeight: 300,
  },
  '.optionWrapper': {
    alignItems: 'center',
    margin: '1.25rem 0',
  },
}

const sortByStyle = {
  '.circle': {
    height: '20px',
    width: '20px',
    border: 'solid 1px whitesmoke',
    borderRadius: '100px',
  },
  '.circle2': { borderRadius: '100px' },
}

const filterWrapper = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr 1fr', '1fr 1fr 1fr'],
  '.optionWrapper': {
    margin: '.625rem 0',
  },
})

const buttonWrapper = {
  position: 'fixed',
  padding: '40px 0 20px',
  bottom: 0,
  left: 0,
  display: 'flex',
  justifyContent: 'center',
  width: '100%',
  // backgroundColor: 'rgba(255,255,255, 0.7)',
  backgroundImage: 'linear-gradient(transparent, rgba(255,255,255, 0.7) 25%)',
  button: {
    margin: '0 1rem',
    padding: '1rem 0',
    border: '2px solid black',
    borderRadius: '4px',
    width: '40vw',
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: 600,
  },
  '.cancel': {
    backgroundColor: 'white',
    color: 'black',
  },
  '.apply': {
    backgroundColor: 'black',
    color: 'white',
  },
}
