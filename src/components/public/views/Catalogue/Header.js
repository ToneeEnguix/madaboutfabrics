/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

import arrow from '../../../../resources/closeBlack.svg'
import filtersIcon from '../../../../resources/filters.svg'
import PaginationComponent from './Pagination'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function Header({
  loaded,
  displayRanges,
  displayRangesSupplier,
  sortBy,
  sort,
  setSortBy,
  updateState,
  selectedColor,
  selectedPrice,
  selectedUsage,
  selectedSale,
  selectedDesign,
  selectedTexture,
  selectedWidth,
  selectedCare,
  selectedType,
  selectedIndustry,
  selectedPvc,
  searched,
  setSearched,
  count,
  page,
  setPage,
  size,
  setSize,
  pageSizeOptions,
  setShowFilters,
}) {
  return (
    <div css={topWrapper}>
      <div className='top'>
        <div css={sortStyle} className='flex'>
          <p>Sort By</p>
          <select
            value={sortBy}
            onChange={(e) => setSortBy(e.target.value)}
            className='pointer'
          >
            {sort.map((sortItem, i) => {
              return (
                <option value={sortItem} key={i}>
                  {sortItem}
                </option>
              )
            })}
          </select>
        </div>
        <div>
          <div>
            <span>Search Results</span>
            <span> | </span>
            <span>
              {(!loaded
                ? '-'
                : displayRanges?.length +
                  (displayRangesSupplier?.length > 0 &&
                    displayRangesSupplier?.length)) + ' Products'}
            </span>
          </div>
          <div className='flexCenter' css={selectWrapperStyle}>
            <select
              onChange={(e) => setSize(Number(e.target.value))}
              value={size}
              className='pointer'
            >
              {pageSizeOptions.map((item) => {
                return (
                  <option key={item} value={item}>
                    {item}
                  </option>
                )
              })}
            </select>
            <p style={{ marginLeft: '.25rem' }}>per page</p>
          </div>
        </div>
      </div>
      <button
        onClick={() => setShowFilters(true)}
        className='pointer uppercase'
        css={filterFabricsStyle}
      >
        <img src={filtersIcon} alt='filters' className='reverse' />
        <span>Filter Fabrics</span>
      </button>
      <div className='topFilters'>
        <span
          onClick={() => updateState('color', selectedColor)}
          style={{ display: !selectedColor && 'none' }}
        >
          {selectedColor?.name}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('price', selectedPrice)}
          style={{ display: !selectedPrice && 'none' }}
        >
          {selectedPrice}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('usage', selectedUsage)}
          style={{ display: !selectedUsage && 'none' }}
        >
          {selectedUsage?.charAt(0).toUpperCase() + selectedUsage?.slice(1)}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('sale', selectedSale)}
          style={{ display: !selectedSale && 'none' }}
        >
          {selectedSale}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('design', selectedDesign)}
          style={{ display: !selectedDesign && 'none' }}
        >
          {selectedDesign?.name}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('texture', selectedTexture)}
          style={{ display: !selectedTexture && 'none' }}
        >
          {selectedTexture?.name}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('width', selectedWidth)}
          style={{ display: !selectedWidth && 'none' }}
        >
          {selectedWidth}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('care', selectedCare)}
          style={{ display: !selectedCare && 'none' }}
        >
          {selectedCare}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('type', selectedType)}
          style={{ display: !selectedType && 'none' }}
        >
          {selectedType?.name}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('industry', selectedIndustry)}
          style={{ display: !selectedIndustry && 'none' }}
        >
          {selectedIndustry}
          <img src={arrow} alt='remove' />
        </span>
        <span
          onClick={() => updateState('pvc', selectedPvc)}
          style={{
            display: !selectedPvc && 'none',
            textTransform: 'uppercase',
          }}
        >
          {selectedPvc}
          <img src={arrow} alt='remove' />
        </span>

        <span
          onClick={() => setSearched('')}
          style={{ display: !searched && 'none' }}
        >
          {searched}
          <img src={arrow} alt='remove' />
        </span>
      </div>
      <div css={desktop}>
        <PaginationComponent
          count={count}
          page={page}
          setPage={setPage}
          size={size}
        />
      </div>
    </div>
  )
}

const topWrapper = {
  minHeight: '100px',
  display: 'flex',
  flexDirection: 'column',
  padding: '1.5rem 0 1rem',
  '.top': {
    padding: '0 10% 0 2rem',
    marginBottom: '1rem',
    display: 'flex',
    justifyContent: 'space-between',
  },
  '.topFilters': {
    padding: '0 2rem',
    display: 'flex',
    flexWrap: 'wrap',
    span: {
      display: 'flex',
      flexWrap: 'no-wrap',
      alignItems: 'center',
      width: 'fit-content',
      padding: '0 .5rem 0 1rem',
      borderRadius: '100px',
      margin: '0 0.4rem 1rem 0',
      cursor: 'pointer',
      transition: 'all linear 150ms',
      boxShadow: '0px 3px 6px #00000029',
      boxSizing: 'border-box',
      height: '36px',
      fontSize: '1rem',
      ':hover': {
        img: {
          borderRadius: '100px',
          boxShadow: '0px 3px 6px #00000029',
        },
      },
      img: {
        boxSizing: 'border-box',
        padding: '0.4rem',
        margin: '0 0 -.1rem .4rem',
        height: '100%',
        maxHeight: '100%',
      },
    },
  },
}

const sortStyle = mq({
  display: ['none', 'flex'],
  boxShadow: '0px 3px 6px #00000029',
  padding: '1rem',
  borderRadius: '14px',
  alignItems: 'center',
  fontSize: '0.9rem',
  p: {
    marginRight: '0.25rem',
  },
  select: {
    outline: 'none',
    border: 'none',
    fontFamily: 'inherit',
    fontSize: 'inherit',
    padding: '0 2rem 0 0',
    mozAppearance: 'none',
    webkitAppearance: 'none',
    appearance: 'none',
  },
})

const selectWrapperStyle = mq({
  display: ['none', 'flex'],
  marginTop: '.5rem',
  justifyContent: 'flex-end',
  select: {
    borderRadius: '6px',
    padding: '3px 0 3px 5px',
    mozAppearance: 'none',
    webkitAppearance: 'none',
    appearance: 'none',
  },
})

const filterFabricsStyle = mq({
  width: 'fit-content',
  display: ['flex', 'none'],
  alignItems: 'center',
  margin: '0 2rem 1rem',
  backgroundColor: 'black',
  color: 'white',
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: 500,
  fontSize: '.9rem',
  border: 'none',
  borderRadius: '6px',
  padding: '.75rem 1.2rem',
  img: {
    height: '20px',
    marginRight: '.5rem',
  },
})

const desktop = mq({
  display: ['none', 'block'],
})
