/** @jsx jsx */
import { jsx } from '@emotion/react'
import axios from 'axios'
import { useState, useEffect, useContext } from 'react'
import ReactLoading from 'react-loading'
import facepaint from 'facepaint'

import { URL } from '../../../../config'
import UserContext from '../../../../contexts/userContext'

import FiltersWrapper from './FiltersWrapper'
import Products from './Products'
import Header from './Header'
import PaginationComponent from './Pagination'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function All(props) {
  // DISPLAY
  const [loaded, setLoaded] = useState(false)
  const [allRanges, setAllRanges] = useState([{}])
  const [displayRanges, setDisplayRanges] = useState([{}])
  const [displayRangesSupplier, setDisplayRangesSupplier] = useState(null)
  const [update, setUpdate] = useState(false)
  // SELECTABLE FILTERS
  const [colors, setColors] = useState([{}])
  const [design, setDesign] = useState([{}])
  const [texture, setTexture] = useState([{}])
  const [type, setType] = useState([{}])
  const [prices] = useState(['£0 - £15', '£15 - £30', '£30 - £99'])
  const [usage] = useState(['Curtains', 'Upholstery', 'Craft'])
  const [onSale] = useState(['Ending Soon', 'All Sale'])
  const [fabricWidth] = useState(['Double Width', 'Standard (138cm)'])
  const fabricCare = [
    'Machine Washable',
    'Dry Clean',
    'Wipe Clean',
    'Hand Wash',
    'Thirty Clean',
  ]
  const industry = ['Hospitality', 'Healthcare', 'Contract']
  const sort = [
    'Newest',
    'Alphabetical',
    'Most Popular',
    'Price High To Low',
    'Price Low To High',
  ]
  // FILTERING
  const [showFilters, setShowFilters] = useState(false)
  const [selectedColor, setSelectedColor] = useState(null)
  const [selectedPrice, setSelectedPrice] = useState(null)
  const [selectedUsage, setSelectedUsage] = useState(null)
  const [selectedSale, setSelectedSale] = useState(null)
  const [selectedDesign, setSelectedDesign] = useState(null)
  const [selectedTexture, setSelectedTexture] = useState(null)
  const [selectedType, setSelectedType] = useState(null)
  const [selectedWidth, setSelectedWidth] = useState(null)
  const [selectedCare, setSelectedCare] = useState(null)
  const [selectedIndustry, setSelectedIndustry] = useState(null)
  const [selectedPvc, setSelectedPvc] = useState(false)
  // SORTING
  const [sortBy, setSortBy] = useState('')
  const [searched, setSearched] = useState('')
  // PAGINATION
  const [count, setCount] = useState(0)
  const [page, setPage] = useState(0)
  const [size, setSize] = useState(50)
  const pageSizeOptions = [50, 75, 100]

  const context = useContext(UserContext)

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [page])

  // Listening to location state
  useEffect(() => {
    if (allRanges[0].name) {
      setLoaded(false)
      setSearched(props.location.state?.search)
      setSelectedColor(props.location.state?.color)
      setSelectedPrice(props.location.state?.price)
      setSelectedUsage(props.location.state?.usage)
      setSelectedSale(props.location.state?.sale)
      setSelectedDesign(props.location.state?.design)
      setSelectedTexture(props.location.state?.texture)
      setSelectedType(props.location.state?.type)
      setSelectedWidth(props.location.state?.width)
      setSelectedCare(props.location.state?.care)
      setSelectedIndustry(props.location.state?.industry)
      setSelectedPvc(props.location.state?.pvc)
      setSortBy(props.location.state?.sortBy)
      setUpdate(!update)
    }
    return () => {
      window.history.replaceState({}, document.title)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props, allRanges])

  // Getting info from backend
  useEffect(() => {
    const rangesToOneVariant = (rangesArr) => {
      let tempRanges = []
      let tempOneVariantRange
      rangesArr.forEach((range) => {
        range.variants.forEach((variant, idx) => {
          tempOneVariantRange = { ...range }
          tempOneVariantRange.variant = { ...variant }
          tempOneVariantRange.index = idx
          tempRanges.push(tempOneVariantRange)
        })
      })
      const sortedRanges = tempRanges.sort((a, b) => a.dateAdded - b.dateAdded)
      setCount(sortedRanges.length)
      setAllRanges(sortedRanges)
      setDisplayRanges(sortedRanges)
      setLoaded(true)
    }
    const getThings = async () => {
      await Promise.all([
        axios.get(`${URL}/ranges/allranges`),
        axios.get(`${URL}/colours/all`),
        axios.get(`${URL}/designs/all`),
        axios.get(`${URL}/textures/all`),
        axios.get(`${URL}/types/all`),
      ]).then((values) => {
        setColors(values[1].data.data)
        setDesign(values[2].data.data)
        setTexture(values[3].data.data)
        setType(values[4].data.data)
        rangesToOneVariant(values[0].data.data)
      })
    }
    window.scrollTo(0, 0)
    !allRanges[0].name && getThings()
  }, [allRanges])

  const updateState = (filter, value) => {
    setLoaded(false)
    if (filter === 'color') {
      value === selectedColor ? setSelectedColor(null) : setSelectedColor(value)
    } else if (filter === 'price') {
      value === selectedPrice ? setSelectedPrice(null) : setSelectedPrice(value)
    } else if (filter === 'usage') {
      value === selectedUsage
        ? setSelectedUsage(null)
        : setSelectedUsage(value.toLowerCase())
    } else if (filter === 'sale') {
      value === selectedSale ? setSelectedSale(null) : setSelectedSale(value)
    } else if (filter === 'design') {
      value === selectedDesign
        ? setSelectedDesign(null)
        : setSelectedDesign(value)
    } else if (filter === 'texture') {
      value === selectedTexture
        ? setSelectedTexture(null)
        : setSelectedTexture(value)
    } else if (filter === 'type') {
      value === selectedType ? setSelectedType(null) : setSelectedType(value)
    } else if (filter === 'width') {
      value === selectedWidth ? setSelectedWidth(null) : setSelectedWidth(value)
    } else if (filter === 'care') {
      value === selectedCare ? setSelectedCare(null) : setSelectedCare(value)
    } else if (filter === 'industry') {
      value === selectedIndustry
        ? setSelectedIndustry(null)
        : setSelectedIndustry(value)
    } else if (filter === 'pvc') {
      selectedPvc ? setSelectedPvc(false) : setSelectedPvc(value)
    }
    setUpdate(!update)
  }

  // Listening for changes on state => filter ranges
  useEffect(() => {
    const filterRanges = () => {
      let tempDisplayRanges = allRanges
      if (searched) {
        tempDisplayRanges = filterBySearch(tempDisplayRanges)
      } else {
        setDisplayRangesSupplier(null)
      }
      if (selectedColor) {
        tempDisplayRanges = filterByColor(tempDisplayRanges)
      }
      if (selectedPrice) {
        tempDisplayRanges = filterByPrice(tempDisplayRanges)
      }
      if (selectedUsage) {
        tempDisplayRanges = filterByUsage(tempDisplayRanges)
      }
      if (selectedSale) {
        tempDisplayRanges = filterBySale(tempDisplayRanges)
      }
      if (selectedDesign) {
        tempDisplayRanges = filterByDesign(tempDisplayRanges)
      }
      if (selectedTexture) {
        tempDisplayRanges = filterByTexture(tempDisplayRanges)
      }
      if (selectedType) {
        tempDisplayRanges = filterByType(tempDisplayRanges)
      }
      if (selectedWidth) {
        tempDisplayRanges = filterByWidth(tempDisplayRanges)
      }
      if (selectedCare) {
        tempDisplayRanges = filterByCare(tempDisplayRanges)
      }
      if (selectedIndustry) {
        tempDisplayRanges = filterByIndustry(tempDisplayRanges)
      }
      if (selectedPvc) {
        tempDisplayRanges = filterByPvc(tempDisplayRanges)
      }
      setCount(tempDisplayRanges.length)
      setDisplayRanges(tempDisplayRanges)
      sortRanges(tempDisplayRanges)
      setLoaded(true)
      window.scrollTo(0, 0)
    }
    if (allRanges[0].name) {
      filterRanges()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [update, searched])

  // Listening for changes on state => sort ranges
  useEffect(() => {
    if (sortBy?.length > 0) {
      setLoaded(false)
      sortRanges()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sortBy])

  const filterBySearch = (ranges) => {
    let tempDisplayRanges = []
    // add message render section! ❗️
    let tempDisplayRangesSupplier = []
    // split searched by spaces
    let searchedArr = searched.split(/[ ,]+/)
    searchedArr.forEach((searchTerm) => {
      // loop searched array
      ranges.forEach((range) => {
        if (
          range.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
          range.variant.color
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.description.toLowerCase().includes(searchTerm.toLowerCase()) ||
          range.design?.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
          range.type?.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
          range.texture?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.composition.toLowerCase().includes(searchTerm.toLowerCase()) ||
          range.keyword01?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.keyword02?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.keyword03?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.keyword04?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.adminTag01?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.adminTag02?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.adminTag03?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.adminTag04?.name
            .toLowerCase()
            .includes(searchTerm.toLowerCase()) ||
          range.variant.sku.toLowerCase().includes(searchTerm.toLowerCase())
        ) {
          // check it is NOT already there!
          let foundIdx = tempDisplayRanges.findIndex(
            (element) => element === range
          )
          foundIdx === -1 && tempDisplayRanges.push(range)
        } else if (
          range.supplier.toLowerCase().includes(searchTerm.toLowerCase())
        ) {
          let foundIdx = tempDisplayRangesSupplier.findIndex(
            (element) => element === range
          )
          let foundIdx2 = tempDisplayRanges.findIndex(
            (element) => element === range
          )
          foundIdx === -1 &&
            foundIdx2 === -1 &&
            tempDisplayRangesSupplier.push(range)
        }
      })
    })
    setDisplayRangesSupplier(tempDisplayRangesSupplier)
    return tempDisplayRanges
  }

  const filterByColor = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      let tempRange = null
      range.variant.realColor.forEach((realColor) => {
        if (realColor.name === selectedColor?.name) {
          tempRange = range
        }
      })
      if (tempRange?.usage) {
        tempDisplayRanges.push(tempRange)
      }
    })
    return tempDisplayRanges
  }

  const filterByPrice = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      if (
        range.price >= selectedPrice.slice(1, 3).trim() &&
        range.price <= selectedPrice.slice(-2).trim()
      ) {
        tempDisplayRanges.push(range)
      }
    })
    return tempDisplayRanges
  }

  const filterByUsage = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      Object.keys(range.usage).forEach((usage, i) => {
        let values = Object.values(range.usage)
        if (usage.toLowerCase() === selectedUsage && values[i]) {
          tempDisplayRanges.push(range)
        }
      })
    })
    return tempDisplayRanges
  }

  const filterBySale = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      if (range.variant.saleDiscount > 0 && range.variant.priceDrop === true) {
        if (
          selectedSale === 'Ending Soon' &&
          new Date(range.variant.priceDropDate).getTime() - Date.now() <
            86400000
        ) {
          tempDisplayRanges.push(range)
        } else if (
          selectedSale === 'All Sale' &&
          new Date(range.variant.priceDropDate).getTime() > Date.now()
        ) {
          tempDisplayRanges.push(range)
        }
      }
    })
    return tempDisplayRanges
  }

  const filterByDesign = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      if (range.design?.name === selectedDesign?.name) {
        tempDisplayRanges.push(range)
      }
    })
    return tempDisplayRanges
  }

  const filterByTexture = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      if (range.texture?.name === selectedTexture?.name) {
        tempDisplayRanges.push(range)
      }
    })
    return tempDisplayRanges
  }

  const filterByType = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      if (range.type?.name === selectedType?.name) {
        tempDisplayRanges.push(range)
      }
    })
    return tempDisplayRanges
  }

  const filterByWidth = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      if (selectedWidth === 'Double Width' && range.fabricWidth > 138) {
        tempDisplayRanges.push(range)
      } else if (
        selectedWidth === 'Standard (138cm)' &&
        range.fabricWidth === 138
      ) {
        tempDisplayRanges.push(range)
      }
    })
    return tempDisplayRanges
  }

  const filterByCare = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      Object.keys(range.fabricCare).forEach((care, i) => {
        let values = Object.values(range.fabricCare)
        if (
          care.trim().toLowerCase() ===
            selectedCare.replace(/\s/g, '').toLowerCase() &&
          values[i]
        ) {
          tempDisplayRanges.push(range)
        }
      })
    })
    return tempDisplayRanges
  }

  const filterByIndustry = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      Object.keys(range.industry).forEach((industry, i) => {
        let values = Object.values(range.industry)
        if (
          industry.toLowerCase() === selectedIndustry.toLowerCase() &&
          values[i]
        ) {
          tempDisplayRanges.push(range)
        }
      })
    })
    return tempDisplayRanges
  }

  const filterByPvc = (ranges) => {
    let tempDisplayRanges = []
    ranges.forEach((range) => {
      if (range.pvc) {
        tempDisplayRanges.push(range)
      }
    })
    return tempDisplayRanges
  }

  const sortRanges = (filteredRanges) => {
    let tempDisplayRanges = filteredRanges
      ? [...filteredRanges]
      : [...displayRanges]
    sortBy === 'Alphabetical'
      ? (tempDisplayRanges = sortByAlphabetical(tempDisplayRanges))
      : sortBy === 'Newest'
      ? (tempDisplayRanges = sortByNewest(tempDisplayRanges))
      : sortBy === 'Price Low To High'
      ? (tempDisplayRanges = sortByPriceLowToHigh(tempDisplayRanges))
      : sortBy === 'Price High To Low'
      ? (tempDisplayRanges = sortByPriceHighToLow(tempDisplayRanges))
      : sortBy === 'Most Popular' &&
        (tempDisplayRanges = sortByPopularity(tempDisplayRanges))
    setDisplayRanges(tempDisplayRanges)
    setLoaded(true)
  }

  const sortByAlphabetical = (ranges) => {
    // return ranges.sort((a, b) => {
    //   return b.name - a.name
    // })
    return ranges.sort((a, b) => a.name.localeCompare(b.name))
  }

  const sortByNewest = (ranges) => {
    return ranges.sort((a, b) => new Date(b.dateAdded) - new Date(a.dateAdded))
  }

  const sortByPriceHighToLow = (ranges) => {
    return ranges.sort((a, b) => b.price - a.price)
  }

  const sortByPriceLowToHigh = (ranges) => {
    return ranges.sort((a, b) => a.price - b.price)
  }

  const sortByPopularity = (ranges) => {
    return ranges.sort((a, b) => b.variant.favorited - a.variant.favorited)
  }

  const removeFilters = () => {
    setLoaded(false)
    searched && setSearched('')
    selectedColor && setSelectedColor('')
    selectedPrice && setSelectedPrice('')
    selectedUsage && setSelectedUsage('')
    selectedSale && setSelectedSale('')
    selectedDesign && setSelectedDesign('')
    selectedTexture && setSelectedTexture('')
    selectedType && setSelectedType('')
    selectedWidth && setSelectedWidth('')
    selectedCare && setSelectedCare('')
    selectedIndustry && setSelectedIndustry('')
    selectedPvc && setSelectedPvc('')
    sortBy && setSortBy('')
    setUpdate(!update)
  }

  return (
    <div css={show}>
      <Header
        loaded={loaded}
        displayRanges={displayRanges}
        displayRangesSupplier={displayRangesSupplier}
        sortBy={sortBy}
        sort={sort}
        setSortBy={setSortBy}
        updateState={updateState}
        setShowFilters={setShowFilters}
        selectedColor={selectedColor}
        selectedPrice={selectedPrice}
        selectedUsage={selectedUsage}
        selectedSale={selectedSale}
        selectedDesign={selectedDesign}
        selectedTexture={selectedTexture}
        selectedWidth={selectedWidth}
        selectedCare={selectedCare}
        selectedType={selectedType}
        selectedIndustry={selectedIndustry}
        selectedPvc={selectedPvc}
        searched={searched}
        setSearched={setSearched}
        // PAGINATION
        count={count}
        page={page}
        setPage={setPage}
        size={size}
        setSize={(num) => {
          setSize(num)
          setPage(0)
        }}
        pageSizeOptions={pageSizeOptions}
      />
      <div css={mainContainer}>
        {colors && prices && (
          <FiltersWrapper
            searched={searched}
            colors={colors}
            selectedColor={selectedColor}
            prices={prices}
            selectedPrice={selectedPrice}
            usage={usage}
            selectedUsage={selectedUsage}
            onSale={onSale}
            selectedSale={selectedSale}
            design={design}
            selectedDesign={selectedDesign}
            texture={texture}
            selectedTexture={selectedTexture}
            fabricWidth={fabricWidth}
            selectedWidth={selectedWidth}
            fabricCare={fabricCare}
            selectedCare={selectedCare}
            type={type}
            selectedType={selectedType}
            industry={industry}
            selectedIndustry={selectedIndustry}
            selectedPvc={selectedPvc}
            updateState={updateState}
            sortBy={sortBy}
            sort={sort}
            setSortBy={setSortBy}
            showFilters={showFilters}
            setShowFilters={setShowFilters}
            removeFilters={removeFilters}
          />
        )}
        <div css={mobile}>
          <PaginationComponent
            count={count}
            page={page}
            setPage={setPage}
            size={size}
          />
        </div>
        {!loaded ? (
          <div className='flexCenter' css={loadingStyle}>
            <ReactLoading type={'spin'} color='#030303' />
          </div>
        ) : (selectedColor ||
            selectedPrice ||
            selectedUsage ||
            selectedSale ||
            selectedDesign ||
            selectedTexture ||
            selectedType ||
            selectedWidth ||
            selectedCare ||
            selectedIndustry ||
            selectedPvc ||
            searched !== '') &&
          displayRanges.length === 0 ? (
          <div css={noResultsStyle}>No results found</div>
        ) : displayRanges.length > 0 && displayRanges[0].name ? (
          <Products
            displayRanges={displayRanges}
            context={context}
            displayRangesSupplier={displayRangesSupplier}
            page={page}
            size={size}
          />
        ) : (
          <div className='flexCenter' css={loadingStyle}>
            <h2>Much more coming soon!</h2>
          </div>
        )}
      </div>
      <PaginationComponent
        count={count}
        page={page}
        setPage={setPage}
        size={size}
      />
    </div>
  )
}

const show = {
  borderTop: '0.25px solid rgb(230,230,230)',
  fontFamily: 'Montserrat, sans-serif',
}

const loadingStyle = {
  height: 'calc(100vh - 159px)',
  width: '100%',
  backgroundColor: 'white',
  'img, h2': {
    marginTop: '-2rem',
    height: '5rem',
    fontFamily: 'Roboto, sans-serif',
    fontWeight: '500',
  },
}

const mainContainer = mq({
  paddingLeft: [0, '2rem'],
  display: 'flex',
  flexDirection: ['column', 'row'],
})

const mobile = mq({
  display: ['block', 'none'],
})

const noResultsStyle = mq({
  paddingLeft: ['2rem', 0],
  height: '30vw',
})
