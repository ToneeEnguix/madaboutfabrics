/** @jsx jsx */
import { jsx } from '@emotion/react'

import tick from '../../../admin/pictures/tick.svg'

export default function FiltersDesktop({
  colors,
  selectedColor,
  prices,
  selectedPrice,
  usage,
  selectedUsage,
  onSale,
  selectedSale,
  design,
  selectedDesign,
  texture,
  selectedTexture,
  fabricWidth,
  selectedWidth,
  fabricCare,
  selectedCare,
  type,
  selectedType,
  industry,
  selectedIndustry,
  selectedPvc,
  updateState,
}) {
  return (
    <div css={scrollStyle} className='scrollStyle'>
      <div css={filtersWrapper}>
        <details open css={oneFilterWrapper}>
          <summary>Color</summary>
          {colors.map((color, idx) => {
            return (
              <InputCont
                updateState={updateState}
                filter='color'
                value={color}
                background={selectedColor}
                text={color.name}
                key={idx}
                idx={idx}
              />
            )
          })}
        </details>
        <details open css={oneFilterWrapper}>
          <summary>Price Per Metre</summary>
          {prices.map((price, idx) => (
            <InputCont
              updateState={updateState}
              filter='price'
              value={price}
              background={selectedPrice}
              text={price}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details open css={oneFilterWrapper}>
          <summary>Usage</summary>
          {usage.map((usage, idx) => (
            <InputCont
              updateState={updateState}
              filter='usage'
              value={usage.toLowerCase()}
              background={selectedUsage}
              text={usage}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>On Sale</summary>
          {onSale.map((sale, idx) => (
            <InputCont
              updateState={updateState}
              filter='sale'
              value={sale}
              background={selectedSale}
              text={sale}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>Design</summary>
          {design.map((design, idx) => (
            <InputCont
              updateState={updateState}
              filter='design'
              value={design}
              background={selectedDesign}
              text={design.name}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>Texture</summary>
          {texture.map((texture, idx) => (
            <InputCont
              updateState={updateState}
              filter='texture'
              value={texture}
              background={selectedTexture}
              text={texture.name}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>Fabric Width</summary>
          {fabricWidth.map((width, idx) => (
            <InputCont
              updateState={updateState}
              filter='width'
              value={width}
              background={selectedWidth}
              text={width}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>Fabric Care</summary>
          {fabricCare.map((care, idx) => (
            <InputCont
              updateState={updateState}
              filter='care'
              value={care}
              background={selectedCare}
              text={care}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>Type</summary>
          {type.map((type, idx) => (
            <InputCont
              updateState={updateState}
              filter='type'
              value={type}
              background={selectedType}
              text={type.name}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>Industry</summary>
          {industry.map((industry, idx) => (
            <InputCont
              updateState={updateState}
              filter='industry'
              value={industry}
              background={selectedIndustry}
              text={industry}
              key={idx}
              idx={idx}
            />
          ))}
        </details>
        <details css={oneFilterWrapper}>
          <summary>PVC TableCloth</summary>
          <InputCont
            updateState={updateState}
            filter='pvc'
            value='pvc'
            background={selectedPvc}
            text='PVC'
          />
        </details>
      </div>
    </div>
  )
}

const scrollStyle = {
  height: '80vh',
  width: '20vw',
  float: 'left',
  position: 'sticky',
  top: '100px',
  overflow: 'scroll',
}

const filtersWrapper = {
  display: 'flex',
  flexDirection: 'column',
  height: '100%',
  paddingRight: '2rem',
  marginBottom: '5rem',
}

const oneFilterWrapper = {
  borderTop: '1px solid lightgray',
  padding: '1rem 0',
  fontWeight: '300',
  div: {
    display: 'flex',
    alignItems: 'center',
    marginTop: '.5rem',
    cursor: 'pointer',
    input: {
      marginRight: '.5rem',
    },
  },
}

const InputCont = (props) => {
  return (
    <div
      css={inputCont}
      onClick={() => props.updateState(props.filter, props.value)}
      key={props.idx}
      style={{ marginTop: `${props.idx === 0 && '1rem'}` }}
    >
      <div
        css={checkbox}
        className='flexCenter'
        style={{
          backgroundColor: `${
            props.background !== props.value ? '#F4F4F4' : '#414141'
          }`,
        }}
      >
        <img
          src={tick}
          className={`ps_tick2 ${props.background !== props.value && 'nope'}`}
          alt='tick'
        />
      </div>
      <p>{props.text}</p>
    </div>
  )
}

const inputCont = {
  display: 'flex',
  alignItems: 'center',
  marginBottom: '0.75rem',
}

const checkbox = {
  cursor: 'pointer',
  border: 'none',
  borderRadius: '8px',
  width: '20px',
  minWidth: '20px',
  height: '20px',
  marginTop: '0 !important',
  margin: '0 .5rem 0 0',
}
