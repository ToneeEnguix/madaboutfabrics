/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

import ProductRowRegular2 from '../../home/carouselWithScrollBar/productRow/ProductRowRegular2'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function Products({ displayRanges, context, page, size }) {
  return (
    <div css={rangesWrapper}>
      {displayRanges.slice(page * size, page * size + size).map((range, i) => (
        <ProductRowRegular2
          key={`${i}${range._id}`}
          size={'regular'}
          wishlist={context.user.wishlist}
          item={range}
        />
      ))}
    </div>
  )
}

const rangesWrapper = mq({
  display: 'flex',
  width: '100vw',
  justifyContent: 'center',
  flexWrap: 'wrap',
  padding: ['0 0 2rem', '0 0 2rem'],
})
