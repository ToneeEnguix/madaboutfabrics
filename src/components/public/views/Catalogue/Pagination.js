/** @jsx jsx */
import { jsx } from '@emotion/react'
import Pagination from 'react-js-pagination'
import facepaint from 'facepaint'
import { useEffect, useState } from 'react'

// RESPONSIVENESS SETTINGS
const breakpoints = [360]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function PaginationComponent({ count, page, setPage, size }) {
  const [w, setW] = useState(null)

  useEffect(() => {
    function updateWidth() {
      setW(window.innerWidth)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
  })

  return (
    <div css={pageSelectorWrapper}>
      <Pagination
        activePage={page + 1}
        itemsCountPerPage={size}
        totalItemsCount={count}
        pageRangeDisplayed={w <= 700 ? 3 : 5}
        onChange={(e) => setPage(e - 1)}
        hideDisabled={true}
        activeLinkClass={'selectedIndex'}
      />
    </div>
  )
}

const pageSelectorWrapper = mq({
  display: 'flex',
  ul: {
    display: 'flex',
    margin: '0 auto',
  },
  li: {
    listStyle: 'none',
    margin: ['0 .2rem', '0 .5rem'],
  },
  a: {
    textDecoration: 'none',
    color: 'black',
    borderRadius: '100px',
    backgroundColor: 'whitesmoke',
    width: ['30px', '35px'],
    height: ['30px', '35px'],
    fontSize: ['.9rem', '1rem'],
    fontFamily: 'Roboto, sans-serif',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  '.selectedIndex': {
    backgroundColor: 'black',
    color: 'white',
  },
})
