import React, { useEffect, useState } from 'react'

import FiltersDesktop from './FiltersDesktop'
import FiltersMobile from './FiltersMobile'

export default function FiltersWrapper(props) {
  const [w, setW] = useState(null)

  useEffect(() => {
    function updateWidth() {
      setW(window.innerWidth)
    }
    window.addEventListener('resize', updateWidth)
    updateWidth()
  }, [])

  if (w <= 700) {
    return <FiltersMobile {...props} />
  } else {
    return <FiltersDesktop {...props} />
  }
}
