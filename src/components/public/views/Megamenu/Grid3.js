/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import { URL } from '../../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 1000]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  fontFamily: 'Montserrat, sans-serif',
  padding: ['0 2rem', '0 2rem 0 0', '0 2rem'],
  h3: {
    textTransform: 'uppercase',
    fontWeight: '500',
    fontSize: '1rem',
    lineHeight: '2.3rem',
    cursor: 'default',
  },
  'p, a': {
    cursor: 'pointer',
    fontSize: '.9rem',
    lineHeight: '1.5rem',
    ':hover': {
      color: '#FBA586',
    },
  },
  div: {
    marginBottom: '1rem',
  },
  a: {
    display: 'block',
    textDecoration: 'none',
    color: 'inherit',
  },
})

export default function Grid1(props) {
  const [textures, setTextures] = useState([])
  const [trending, setTrending] = useState([])
  const usage = ['Craft', 'Curtains', 'Upholstery']
  const industry = [
    'Automotive',
    'Hospitality',
    'Film',
    'Healthcare',
    'Home',
    'Contract',
  ]

  useEffect(() => {
    const getPropierties = async () => {
      try {
        Promise.all([
          axios.get(`${URL}/textures/all`),
          axios.get(`${URL}/ranges/trending`),
        ]).then((values) => {
          setTextures(values[0].data.data)
          setTrending(values[1].data)
        })
      } catch (err) {
        console.error(err)
      }
    }
    getPropierties()
  }, [])

  return (
    <div css={show}>
      <div>
        <div>
          <h3>Usage</h3>
          {usage?.map((item) => (
            <Link
              to={{
                pathname: `/catalogue`,
                state: {
                  usage: item.toLowerCase(),
                },
              }}
              key={item}
            >
              {item}
            </Link>
          ))}
        </div>
        <div>
          <h3>Textures</h3>
          {textures?.map((texture) => (
            <Link
              to={{
                pathname: `/catalogue`,
                state: {
                  texture: texture,
                },
              }}
              key={texture._id}
            >
              {texture.name}
            </Link>
          ))}
        </div>
      </div>
      <div>
        <div>
          <h3>Popular Terms</h3>
          {trending?.map((item) => (
            <Link
              to={
                item.url
                  ? `/productdetails/${item.url}/0`
                  : `/productdetails/${item._id}/0`
              }
              key={item._id}
            >
              {item.name}
            </Link>
          ))}
        </div>
        <div>
          <h3>Industry</h3>
          {industry?.map((industry) => (
            <Link
              to={{
                pathname: `/catalogue`,
                state: {
                  industry: industry,
                },
              }}
              key={industry}
            >
              {industry}
            </Link>
          ))}
        </div>
      </div>
    </div>
  )
}
