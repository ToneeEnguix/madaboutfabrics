/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import facepaint from 'facepaint'

import { URL } from '../../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 1000]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'grid',
  gridTemplateColumns: '1fr 1fr',
  fontFamily: 'Montserrat, sans-serif',
  borderRight: [0, 0, '1px solid lightgray'],
  padding: ['0 2rem', '0 0 0 2rem', '0 2rem'],
  h3: {
    textTransform: 'uppercase',
    fontWeight: '500',
    fontSize: '1rem',
    lineHeight: '2.3rem',
    cursor: 'default',
  },
  'p, a': {
    cursor: 'pointer',
    fontSize: '.9rem',
    lineHeight: '1.5rem',
    display: 'block',
    color: 'inherit',
    textDecoration: 'inherit',
    ':hover': {
      color: '#FBA586',
    },
  },
  div: {
    marginBottom: '1rem',
  },
})

export default function Grid1(props) {
  const [colors, setColors] = useState([])
  const [designs, setDesigns] = useState([])
  const [types, setTypes] = useState([])

  useEffect(() => {
    const getPropierties = () => {
      try {
        Promise.all([
          axios.get(`${URL}/colours/all`),
          axios.get(`${URL}/designs/all`),
          axios.get(`${URL}/types/all`),
        ]).then((values) => {
          setColors(values[0].data.data)
          setDesigns(values[1].data.data)
          setTypes(values[2].data.data)
        })
      } catch (err) {
        console.error(err)
      }
    }
    getPropierties()
  }, [])

  return (
    <div css={show}>
      <div>
        <h3>Color</h3>
        {colors?.map((color) => (
          <Link
            to={{
              pathname: `/catalogue`,
              state: {
                color: color,
              },
            }}
            key={color._id}
          >
            {color.name}
          </Link>
        ))}
      </div>
      <div>
        <div>
          <h3>Design</h3>
          {designs?.map((design) => (
            <Link
              to={{
                pathname: `/catalogue`,
                state: {
                  design: design,
                },
              }}
              key={design._id}
            >
              {design.name}
            </Link>
          ))}
        </div>
        <div>
          <h3>Type</h3>
          {types?.map((type) => (
            <Link
              to={{
                pathname: `/catalogue`,
                state: {
                  type: type,
                },
              }}
              key={type._id}
            >
              {type.name}
            </Link>
          ))}
        </div>
      </div>
    </div>
  )
}
