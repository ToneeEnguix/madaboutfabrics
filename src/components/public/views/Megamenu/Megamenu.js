/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect } from 'react'
import facepaint from 'facepaint'

import Grid1 from './Grid1.js'
import Grid2 from './Grid2.js'
import Grid3 from './Grid3.js'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 1000]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = {
  borderTop: '0.25px solid rgb(230,230,230)',
  display: 'flex',
  flexDirection: 'column',
}

const megamenu = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr 1fr', '1fr 1fr 1.5fr'],
  padding: ['1rem', '2rem'],
  minHeight: 'calc(100vh - 158px)',
})

export default function Megamenu(props) {
  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  return (
    <div css={show}>
      <section css={megamenu}>
        <Grid1 />
        <Grid2 />
        <Grid3 />
      </section>
    </div>
  )
}
