/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 1000]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  order: [0, 2, 0],
  gridColumn: ['auto', 'span 2', 'auto'],
  width: ['calc(100% - 4rem)', '100%', 'fit-content'],
  justifySelf: ['flex-start', 'flex-end'],
  textTransform: 'uppercase',
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: '500',
  lineHeight: '2.3rem',
  borderRight: ['none', 'none', '1px solid lightgray'],
  borderBottom: ['1px solid lightgray', 'none', 'none'],
  padding: ['0 0 1rem', '0 2rem 0 0'],
  // padding: ['0 2rem 1rem', '0 2rem'],
  fontSize: '1rem',
  cursor: 'pointer',
  textAlign: 'start',
  display: ['block', 'none', 'block'],
  margin: ['0 0 1rem 2rem', 0],
  'p:hover, a:hover': {
    color: '#FBA586',
  },
  a: {
    color: 'inherit',
    textDecoration: 'inherit',
    display: 'block',
  },
})

export default function Grid1(props) {
  return (
    <div css={show}>
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            sortBy: 'Most Popular',
          },
        }}
      >
        Best Sellers
      </Link>
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            sale: 'Ending Soon',
          },
        }}
      >
        Offers Ending Soon
      </Link>
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'curtains',
          },
        }}
      >
        Curtain Fabrics
      </Link>
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'upholstery',
          },
        }}
      >
        Upholstery Fabrics
      </Link>
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            usage: 'craft',
          },
        }}
      >
        Craft Fabrics
      </Link>
      <Link
        to={{
          pathname: `/catalogue`,
        }}
      >
        All the fabrics
      </Link>
      <Link
        to={{
          pathname: `/catalogue`,
          state: {
            pvc: 'pvc',
          },
        }}
      >
        PVC Tablecloth
      </Link>
    </div>
  )
}
