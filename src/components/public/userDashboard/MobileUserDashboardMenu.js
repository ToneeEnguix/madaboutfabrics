/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'
import { useEffect, useState, useContext } from 'react'
import { NavLink } from 'react-router-dom'

import back from '../../admin/pictures/arrow.svg'
import UserContext from '../../../contexts/userContext'

import Address from './Address'
import Cart from './Cart'
import Orders from './Orders'
import Password from './Password'
import Profile from './Profile'
import Samples from './Samples'
import Wishlist from './Wishlist'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function MobileUserDashboardMenu(props) {
  const context = useContext(UserContext)

  const [openOrders, setOpenOrders] = useState(false)
  const [openCart, setOpenCart] = useState(false)
  const [openSamples, setOpenSamples] = useState(false)
  const [openWishlist, setOpenWishlist] = useState(false)
  const [openProfile, setOpeProfile] = useState(false)
  const [openPassword, setOpenPassword] = useState(false)
  const [openAddress, setOpenAddress] = useState(false)

  const [previousPage, setPreviousPage] = useState(null)

  useEffect(() => {
    const closeAll = (page) => {
      page !== '' && setOpenOrders(false)
      page !== 'cart' && setOpenCart(false)
      page !== 'samples' && setOpenSamples(false)
      page !== 'wishlist' && setOpenWishlist(false)
      page !== 'profile' && setOpeProfile(false)
      page !== 'password' && setOpenPassword(false)
      page !== 'address' && setOpenAddress(false)
    }
    if (!props.location.pathname.includes('userdashboard')) return false
    const slashIdx = props.location.pathname.slice(1).indexOf('/') + 2 // +2 bc 2 slashes are removed
    const page = props.location.pathname.slice(slashIdx)
    switch (page) {
      case '':
        closeAll('')
        !openOrders && previousPage !== '' && setOpenOrders(true)
        setPreviousPage('')
        break
      case 'cart':
        closeAll('cart')
        !openCart && previousPage !== 'cart' && setOpenCart(true)
        setPreviousPage('cart')
        break
      case 'samples':
        closeAll('samples')
        !openSamples && previousPage !== 'samples' && setOpenSamples(true)
        setPreviousPage('samples')
        break
      case 'wishlist':
        closeAll('wishlist')
        !openWishlist && previousPage !== 'wishlist' && setOpenWishlist(true)
        setPreviousPage('wishlist')
        break
      case 'profile':
        closeAll('profile')
        !openProfile && previousPage !== 'profile' && setOpeProfile(true)
        setPreviousPage('profile')
        break
      case 'password':
        closeAll('password')
        !openPassword && previousPage !== 'password' && setOpenPassword(true)
        setPreviousPage('password')
        break
      case 'address':
        closeAll('address')
        !openAddress && previousPage !== 'address' && setOpenAddress(true)
        setPreviousPage('address')
        break
      default:
        setOpenOrders(true)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props])

  return (
    <div css={mainStyle}>
      <nav className='backStyle pointer' onClick={() => props.history.goBack()}>
        <img alt='back' src={back} />
        <span>Back</span>
      </nav>
      <nav className='desktop'>
        <ul>
          <li>
            <NavLink
              exact
              to={`${props.match.path}/`}
              style={{ backgroundColor: openOrders && '#F9F9F9' }}
              onClick={() => setOpenOrders(!openOrders)}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openOrders && 500 }}>Orders</span>
                <img
                  style={{ transform: openOrders && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openOrders && <Orders />}
          </li>
          <li>
            <NavLink
              exact
              to={`${props.match.path}/cart`}
              style={{ backgroundColor: openCart && '#F9F9F9' }}
              onClick={() => setOpenCart(!openCart)}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openCart && 500 }}>Your Basket</span>
                <img
                  style={{ transform: openCart && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openCart && <Cart />}
          </li>
          <li>
            <NavLink
              exact
              to={`${props.match.path}/samples`}
              style={{ backgroundColor: openSamples && '#F9F9F9' }}
              onClick={() => setOpenSamples(!openSamples)}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openSamples && 500 }}>Samples</span>
                <img
                  style={{ transform: openSamples && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openSamples && <Samples />}
          </li>
          <li>
            <NavLink
              exact
              to={`${props.match.path}/wishlist`}
              style={{ backgroundColor: openWishlist && '#F9F9F9' }}
              onClick={() => setOpenWishlist(!openWishlist)}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openWishlist && 500 }}>
                  Wishlist
                </span>
                <img
                  style={{ transform: openWishlist && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openWishlist && (
              <Wishlist
                wishlist={context.user.wishlist}
                addToCart={context.addToCart}
                removeFromWishlist={context.removeFromWishlist}
              />
            )}
          </li>
          <li>
            <NavLink
              exact
              to={`${props.match.path}/profile`}
              style={{ backgroundColor: openProfile && '#F9F9F9' }}
              onClick={() => setOpeProfile(!openProfile)}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openProfile && 500 }}>Profile</span>
                <img
                  style={{ transform: openProfile && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openProfile && (
              <Profile
                modifyUsername={context.modifyUsername}
                user={context.user}
              />
            )}
          </li>
          <li>
            <NavLink
              exact
              to={`${props.match.path}/password`}
              style={{ backgroundColor: openPassword && '#F9F9F9' }}
              onClick={() => setOpenPassword(!openPassword)}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openPassword && 500 }}>
                  Password
                </span>
                <img
                  style={{ transform: openPassword && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openPassword && <Password />}
          </li>
          <li>
            <NavLink
              exact
              to={`${props.match.path}/address`}
              style={{ backgroundColor: openAddress && '#F9F9F9' }}
              onClick={() => setOpenAddress(!openAddress)}
            >
              <div className='dropdown'>
                <span style={{ fontWeight: openAddress && 500 }}>Address</span>
                <img
                  style={{ transform: openAddress && 'rotate(90deg)' }}
                  className='arrow'
                  src={back}
                  alt='arrow'
                />
              </div>
            </NavLink>
            {openAddress && (
              <Address
                saveAddress={context.saveAddress}
                user={context.user}
                toggleRegion={context.toggleRegion}
              />
            )}
          </li>
          <li style={{ padding: '.5rem 1rem' }}>Log Out</li>
        </ul>
      </nav>
    </div>
  )
}

const mainStyle = mq({
  display: ['auto', 'none'],
  fontFamily: 'Roboto, sans-serif',
  fontWeight: 300,
  padding: '0 2rem 0 1rem',
  '.backStyle': {
    display: 'flex',
    alignItems: 'center',
    margin: '0 0 2rem .5rem',
  },
  li: {
    listStyle: 'none',
  },
  a: {
    padding: '.5rem 1rem',
    textDecoration: 'none',
    color: 'inherit',
    display: 'block',
    marginBottom: '.5rem',
    h1: {
      display: 'none',
    },
  },
  '.dropdown': {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  '.arrow': {
    transition: 'all 200ms linear',
    transform: 'rotate(270deg)',
  },
})
