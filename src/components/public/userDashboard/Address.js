/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState } from 'react'
import facepaint from 'facepaint'

import StyledSelectUser from '../generalPurpose/StyledInput/StyledSelectUser'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 900, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  alignItems: 'flex-start',
  width: ['100%', '90%', '90%', '80%'],
  flexDirection: 'column',
  color: 'black',
  marginLeft: ['1rem', '3rem'],
  marginBottom: ['2rem', 0],
  h1: {
    fontSize: '1.1rem',
    letterSpacing: '1px',
    fontWeight: 700,
    margin: ['1.5rem 0 0', '0 0 .5rem 0'],
    fontFamily: 'Montserrat, sans-serif',
  },
})

const mainContainer = mq({
  marginTop: '1.5rem',
  fontFamily: 'Roboto, sans-serif',
  width: '100%',
  form: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '1rem',
    input: {
      width: '80vw',
      maxWidth: '400px',
    },
  },
  button: {
    fontWeight: 600,
    backgroundColor: '#00263E',
    width: '70vw',
    maxWidth: '250px',
    color: 'white',
    border: 'none',
    padding: '1rem',
    cursor: 'pointer',
    fontSize: '0.9rem',
    transition: 'all linear 120ms',
    whiteSpace: 'nowrap',
    ':hover': {
      backgroundColor: 'white',
      color: '#00263E',
      boxShadow: '0px 3px 6px #00000029',
    },
  },
})

const formStyle = {
  marginBottom: '2rem',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'flex-start',
  select: {
    width: '80vw',
    maxWidth: '400px',
  },
}

const messageStyle = {
  marginBottom: '1rem',
  color: '#17C132',
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: '500',
}

export default function Address(props) {
  return (
    <div css={show}>
      <h1>ADDRESS</h1>
      <div css={mainContainer}>
        <AddressFormInputs
          address={props.user.address || ''}
          toggleRegion={props.toggleRegion}
          region={props?.user?.region}
          saveAddress={props.saveAddress}
        />
      </div>
    </div>
  )
}

const AddressFormInputs = (props) => {
  const [message, setMessage] = useState(false)

  const handleSubmit = async (e) => {
    e.preventDefault()
    const name = e.target.name.value.trim()
    const lastName = e.target.lastName.value.trim()
    const direction = e.target.direction.value.trim()
    const town = e.target.town.value.trim()
    const postalCode = e.target.postalCode.value.trim()
    const phoneNumber = e.target.phoneNumber.value.trim()
    const address = {
      name,
      lastName,
      direction,
      town,
      postalCode,
      phoneNumber,
    }
    await props.saveAddress(address)
    setMessage(true)
    setTimeout(() => {
      setMessage(false)
    }, 2000)
  }

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit} css={formStyle}>
        <StyledSelectUser
          key={props.user?.region._id}
          defaultValue={props.user?.region}
          width={'100%'}
          toggleRegion={props.toggleRegion}
          region={props.user?.region}
        />
        <input
          className='styledInput'
          placeholder='Name'
          type='text'
          name='name'
          defaultValue={props.address.name}
          style={{ width: '100%' }}
        />
        <input
          className='styledInput'
          placeholder='Last Name'
          type='text'
          name='lastName'
          defaultValue={props.address.lastName}
          style={{ width: '100%' }}
        />
        <input
          className='styledInput'
          placeholder='Direction'
          type='text'
          name='direction'
          defaultValue={props.address.direction}
          style={{ width: '100%' }}
        />
        <input
          className='styledInput'
          placeholder='Town/City'
          type='text'
          name='town'
          defaultValue={props.address.town}
          style={{ width: '100%' }}
        />
        <input
          className='styledInput'
          placeholder='Postal Code'
          type='text'
          name='postalCode'
          defaultValue={props.address.postalCode}
          style={{ width: '100%' }}
        />
        <input
          className='styledInput'
          placeholder='Phone Number'
          type='number'
          name='phoneNumber'
          defaultValue={props.address.phoneNumber}
          style={{ width: '100%' }}
        />
        <button style={{ width: '100%' }}>SAVE</button>
      </form>
      {message === true ? <div css={messageStyle}>Changes Saved!</div> : null}
    </React.Fragment>
  )
}
