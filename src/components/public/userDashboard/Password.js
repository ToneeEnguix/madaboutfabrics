/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import facepaint from 'facepaint'

import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 900, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '90%', '90%', '80%'],
  flexDirection: 'column',
  color: 'black',
  marginLeft: ['1rem', '3rem'],
  marginBottom: ['2rem', 0],
  h1: {
    fontSize: '1.1rem',
    letterSpacing: '1px',
    fontWeight: 700,
    margin: ['1.5rem 0 0', '0 0 .5rem 0'],
    fontFamily: 'Montserrat, sans-serif',
  },
})

const mainContainer = mq({
  marginTop: '1.5rem',
  fontFamily: 'Roboto, sans-serif',
  form: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '1rem',
    input: {
      width: '80vw',
      maxWidth: '400px',
    },
  },
  button: {
    fontWeight: 600,
    backgroundColor: '#00263E',
    width: '70vw',
    maxWidth: '250px',
    color: 'white',
    border: 'none',
    padding: '1rem',
    cursor: 'pointer',
    fontSize: '0.9rem',
    transition: 'all linear 120ms',
    whiteSpace: 'nowrap',
    ':hover': {
      backgroundColor: 'white',
      color: '#00263E',
      boxShadow: '0px 3px 6px #00000029',
    },
  },
})

const span = {
  color: '#8B8B8B',
  fontSize: '0.8rem',
  margin: '-0.75rem 0 2rem 0',
}

export default function Password(props) {
  const [message, setMessage] = useState({
    text: '',
    color: '',
  })

  const submitNewPassword = (password, newPass) => {
    const token = localStorage.getItem('token')

    const myHeaders = new Headers()
    myHeaders.append('Content-Type', 'application/x-www-form-urlencoded')
    myHeaders.append('access-token', token)

    const urlencoded = new URLSearchParams()
    urlencoded.append('oldPassword', password)
    urlencoded.append('newPassword', newPass)
    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: urlencoded,
      redirect: 'follow',
    }

    fetch(`${URL}/users/newpassword`, requestOptions)
      .then((response) => response.json())
      .then((result) => setMessage({ text: result.message, color: '#17C132' }))
      .catch((error) => console.error(error))
  }

  const confirmPasswordsMatch = (newPass, confirmNewPass) => {
    if (newPass === confirmNewPass && newPass.length >= 8) {
      return true
    } else if (newPass !== confirmNewPass) {
      setMessage({ text: 'Passwords do not match', color: 'red' })
      return false
    } else {
      setMessage({
        text: 'Password must be at least 8 characters long',
        color: 'red',
      })
    }
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    const password = e.target.currentPass.value.trim()
    const newPass = e.target.newPass.value.trim()
    const confirmNewPass = e.target.confirmNewPass.value.trim()
    const passwordsMatch = confirmPasswordsMatch(newPass, confirmNewPass)
    if (passwordsMatch) {
      submitNewPassword(password, newPass)
    }
  }

  const messageStyle = {
    color: message.color,
    fontFamily: 'Montserrat, sans-serif',
    fontWeight: '500',
  }

  useEffect(() => {
    message.text &&
      setTimeout(() => {
        setMessage({ text: '', color: '' })
      }, 2000)
  }, [message])

  return (
    <div css={show}>
      <h1>PASSWORD</h1>
      <div css={mainContainer}>
        <form onSubmit={handleSubmit}>
          <input
            className='styledInput'
            placeholder='Current Password'
            required={true}
            type='password'
            name='currentPass'
          />
          <input
            className='styledInput'
            placeholder='New Password'
            required={true}
            type='password'
            name='newPass'
          />
          <input
            className='styledInput'
            placeholder='Confirm New Password'
            required={true}
            type='password'
            name='confirmNewPass'
          />

          <div css={span}>The password must be at least 8 characters long.</div>
          <button className='uppercase' type='submit'>
            Save new password
          </button>
        </form>
        {message.text && <h4 css={messageStyle}>{message.text}</h4>}
      </div>
    </div>
  )
}
