/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'
import CartProduct from './CartProduct'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 900, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr', '2fr 1.2fr'],
  width: ['100%', '90%', '90%', '80%'],
  color: 'black',
  marginLeft: ['1rem', '3rem'],
  marginBottom: ['3rem', 0],
  fontFamily: 'Montserrat, sans-serif',
  padding: ['0 1rem 0 0', '0 3rem 0 0'],
  h1: {
    fontSize: '1.1rem',
    letterSpacing: '1px',
    fontWeight: '700',
    margin: ['2rem 0 1.5rem', '0 0 1.5rem 0'],
    textTransform: 'uppercase',
  },
})

const leftColumn = mq({
  paddingRight: [0, '2rem'],
})

const rightColumn = mq({
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'column',
  h1: {
    alignSelf: 'flex-start',
  },
  '.elements': {
    width: '100%',
    padding: '0 0 1rem 0',
    borderBottom: '1px solid lightgray',
  },
  '.gridRight': {
    display: 'grid',
    gridTemplateColumns: ['1fr', '3fr 1fr'],
    width: '100%',
    gap: '3%',
    span: {
      fontWeight: '200',
    },
  },
  '.total': {
    marginTop: '1rem',
    textTransform: 'uppercase',
    fontWeight: '600',
  },
})

const updateButton = {
  padding: '1.2rem 5rem',
  borderRadius: '100px',
  backgroundColor: 'black',
  color: 'white',
  border: 'none',
  fontWeight: '600',
  cursor: 'pointer',
  marginTop: '4rem',
  fontSize: '0.9rem',
  fontFamily: 'Montserrat, sans-serif',
  whiteSpace: 'nowrap',
}

const orange = {
  color: '#FA5402',
}

export default function Cart(props) {
  const context = useContext(UserContext)
  const [shipping, setShipping] = useState(0)
  const [subtotal, setSubtotal] = useState(0)
  const [total, setTotal] = useState('£0')

  useEffect(() => {
    const calculateNumbers = () => {
      let tempSubtotal = 0
      context.user.cart.forEach((item) => {
        tempSubtotal += item.amount * item.range.price
      })
      setSubtotal(tempSubtotal.toFixed(2))
      if (typeof context.user.region?.shippingRate === 'number') {
        setShipping('£' + context.user.region?.shippingRate.toFixed(2))
        setTotal(
          '£' + (context.user.region?.shippingRate + tempSubtotal).toFixed(2)
        )
      } else {
        setShipping('N/A')
        setTotal('£' + tempSubtotal.toFixed(2))
      }
    }
    calculateNumbers()
  }, [context])

  return (
    <div css={show}>
      <div css={leftColumn}>
        <h1>Your Cart</h1>
        {context.user.cart.length !== 0 ? (
          context.user.cart.map((product) => {
            return (
              <CartProduct
                context={context}
                key={`${product.range._id}${product.variantIndex}`}
                product={product}
              />
            )
          })
        ) : (
          <p>Your Cart is Currently Empty</p>
        )}
      </div>
      <div css={rightColumn}>
        <h1>Summary</h1>
        <div className='elements'>
          <div className='gridRight' style={{ marginBottom: '1rem' }}>
            <p>Subtotal</p>
            <span>£ {subtotal}</span>
          </div>
          <div className='gridRight'>
            <p>Estimated shipping and handling costs</p>
            <span>{shipping}</span>
          </div>
        </div>
        <div className='gridRight total'>
          <p>Total</p>
          <p css={orange}>{total}</p>
        </div>
        <Link to='/checkout'>
          <button css={updateButton}>GO TO CHECKOUT</button>
        </Link>
      </div>
    </div>
  )
}
