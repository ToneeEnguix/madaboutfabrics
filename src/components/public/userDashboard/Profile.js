/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import facepaint from 'facepaint'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 900, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '90%', '90%', '80%'],
  flexDirection: 'column',
  color: 'black',
  marginLeft: ['1rem', '3rem'],
  marginBottom: ['2rem', 0],
  h1: {
    fontSize: '1.1rem',
    letterSpacing: '1px',
    fontWeight: 700,
    margin: ['1.5rem 0 0', '0 0 .5rem 0'],
    fontFamily: 'Montserrat, sans-serif',
  },
})

const mainContainer = mq({
  marginTop: '1.5rem',
  fontFamily: 'Roboto, sans-serif',
  form: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: '1rem',
    input: {
      width: '80vw',
      maxWidth: '400px',
    },
  },
  button: {
    fontWeight: 600,
    backgroundColor: '#00263E',
    width: '70vw',
    maxWidth: '250px',
    color: 'white',
    border: 'none',
    padding: '1rem',
    cursor: 'pointer',
    fontSize: '0.9rem',
    transition: 'all linear 120ms',
    whiteSpace: 'nowrap',
    ':hover': {
      backgroundColor: 'white',
      color: '#00263E',
      boxShadow: '0px 3px 6px #00000029',
    },
  },
})

const messageStyle = {
  color: '#17C132',
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: '500',
}

export default function Profile(props) {
  const [message, setMessage] = useState(null)

  const handleSubmit = (e) => {
    e.preventDefault()
    const name = e.target.name.value.trim()
    const lastName = e.target.lastName.value.trim()
    const email = e.target.email.value.trim()
    props.modifyUsername({ name, lastName, email })
    setMessage(true)
  }

  useEffect(() => {
    message &&
      setTimeout(() => {
        setMessage(null)
      }, 2000)
  }, [message])

  return (
    <div css={show} key={props?.user?.name}>
      <h1 className='uppercase'>PROFILE</h1>
      <div css={mainContainer}>
        <form onSubmit={handleSubmit}>
          <input
            className='styledInput'
            placeholder='Name'
            required={true}
            type='text'
            defaultValue={props.user?.email && props.user?.name}
            name='name'
          />
          <input
            className='styledInput'
            placeholder='Last Name'
            type='text'
            defaultValue={props.user?.lastName}
            name='lastName'
          />
          <input
            className='styledInput'
            placeholder='Email'
            required={true}
            type='email'
            defaultValue={props.user?.email}
            name='email'
          />
          <button className='uppercase' type='submit'>
            Save changes
          </button>
        </form>
        {message && <h4 css={messageStyle}>Changes Saved!</h4>}
      </div>
    </div>
  )
}
