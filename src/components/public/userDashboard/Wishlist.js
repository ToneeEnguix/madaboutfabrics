/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import ReactLoading from 'react-loading'
import { Link } from 'react-router-dom'
import uuid from 'uuid'
import facepaint from 'facepaint'

import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 900, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = mq({
  display: 'flex',
  width: ['100%', '90%', '90%', '80%'],
  flexDirection: 'column',
  color: 'black',
  marginLeft: ['1rem', '3rem'],
  h1: {
    fontSize: '1.1rem',
    letterSpacing: '1px',
    fontWeight: '700',
    margin: ['2rem 0 2rem 0', '0 0 .5rem 0'],
    fontFamily: 'Montserrat, sans-serif',
  },
})

const mainContainer = mq({
  marginTop: [0, '1.5rem'],
  fontFamily: 'Helvetica Neue, sans-serif',
  marginBottom: '5rem',
})

const itemWrapper = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr 2fr .5fr', '1fr 2fr .5fr', '1fr 2fr .5fr 1fr'],
  alignItems: 'center',
  marginBottom: '2.5rem',
  color: 'inherit',
  textDecoration: 'inherit',
  div: {
    marginLeft: '5%',
  },
  '.nameAndPriceWrapper': {
    span: {
      fontWeight: '300',
    },
    p: {
      fontWeight: '500',
    },
  },
  '.cartWrapper': {
    display: 'flex',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '119px',
    padding: ['1rem 2.5rem', '1rem 2.5rem', '1.5rem 2.5rem'],
    alignItems: 'center',
    border: '1px solid transparent',
    transition: 'all ease-in-out 150ms',
    whiteSpace: 'nowrap',
    margin: ['1.5rem 0', '1.5rem 0', 0],
    width: ['fit-content', '100%'],
    gridColumn: ['1 / 3', 'auto'],
    i: {
      marginRight: '1rem',
    },
    ':hover': {
      color: '#FBA586',
      border: '1px solid #FBA586',
      boxShadow: '5px 10px 15px 5px rgba(235,235,235,1)',
    },
  },
})

const circleFitter = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '100px !important',
  minHeight: '100px !important',
  width: '100px !important',
  minWidth: '100px !important',
  borderRadius: '100%',
  boxShadow: '0px 3px 6px #00000029',
  padding: '0 !important',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const pointerPink = {
  marginTop: '.5rem',
  color: '#FBA586',
  fontSize: '2rem',
  transition: 'all linear 100ms',
  ':hover': {
    color: '#FF6566',
  },
}

const loadingStyle = {
  backgroundColor: 'white',
}

export default function Wishlist(props) {
  const [loaded, setLoaded] = useState(false)

  const moveToCart = async (item) => {
    if (item.range.inStock) {
      await props.addToCart(item.range, item.variantIndex, 1)
      props.removeFromWishlist(item.range, item.variantIndex)
    } else {
      alert('Item currently out of stock')
    }
  }

  useEffect(() => {
    if (props.wishlist) {
      setLoaded(true)
    }
  }, [props])

  if (props.wishlist) {
    return (
      <div css={show}>
        <h1>WISHLIST</h1>
        <div css={mainContainer}>
          {!loaded ? (
            <div className='flexCenter' css={loadingStyle}>
              <ReactLoading type={'spin'} color='#030303' />
            </div>
          ) : props.wishlist[0] ? (
            <div>
              {props.wishlist.map((item) => {
                return (
                  <div css={itemWrapper} key={uuid()}>
                    <Link
                      to={{
                        pathname: item.range.url
                          ? `/productdetails/${item.range.url}/${item.variantIndex}`
                          : `/productdetails/${item.range._id}/${item.variantIndex}`,
                        state: {
                          item: item.range,
                          index: item.variantIndex,
                        },
                      }}
                      css={circleFitter}
                    >
                      <img
                        alt='product'
                        src={`${URL}/assets/${
                          item.range.variants[item.variantIndex].imageURLs[0]
                            .filename
                        }`}
                      />
                    </Link>
                    <div className='nameAndPriceWrapper'>
                      <span>
                        {item.range.name} |{' '}
                        {item.range.variants[item.variantIndex].color}
                      </span>
                      <p>£{item.range.price} per metre</p>
                    </div>
                    <div className='pointer'>
                      <i
                        css={pointerPink}
                        onClick={() => {
                          props.removeFromWishlist(
                            item.range,
                            item.variantIndex
                          )
                        }}
                        className='material-icons'
                      >
                        favorite
                      </i>
                    </div>
                    <div
                      className='cartWrapper pointer'
                      onClick={(e) => {
                        e.preventDefault()
                        moveToCart(item)
                      }}
                    >
                      <i className='material-icons'>add_shopping_cart</i>
                      <p>Add To Cart</p>
                    </div>
                  </div>
                )
              })}
            </div>
          ) : (
            <div>No items on your wishlist!</div>
          )}
        </div>
      </div>
    )
  } else return null
}
