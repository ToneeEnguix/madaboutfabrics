/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import { Route } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'

import UserDashboardMenu from './UserDashboardMenu'
import MobileUserDashboardMenu from './MobileUserDashboardMenu'
import Address from './Address'
import Orders from './Orders'
import Samples from './Samples'
import Password from './Password'
import Profile from './Profile'
import Wishlist from './Wishlist'
import Cart from './Cart'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const UserDashboard = (props) => {
  const context = useContext(UserContext)

  return (
    <div>
      <section css={dashboard}>
        <UserDashboardMenu />
        <MobileUserDashboardMenu {...props} />
        <div css={desktopOnly}>
          <Route path={`${props.match.path}/password`} component={Password} />
          <Route
            exact
            path={`${props.match.path}/profile`}
            render={(props) => (
              <Profile
                {...props}
                modifyUsername={context.modifyUsername}
                user={context.user}
              />
            )}
          />
          <Route
            path={`${props.match.path}/address`}
            render={(props) => (
              <Address
                {...props}
                saveAddress={context.saveAddress}
                user={context.user}
                toggleRegion={context.toggleRegion}
              />
            )}
          />
          <Route exact path={`${props.match.path}/`} component={Orders} />
          <Route
            exact
            path={`${props.match.path}/samples`}
            render={(props) => (
              <Samples
                {...props}
                wishlist={context.user.wishlist}
                addToCart={context.addToCart}
                removeFromWishlist={context.removeFromWishlist}
              />
            )}
          />
          <Route exact path={`${props.match.path}/cart`} component={Cart} />
          <Route
            exact
            path={`${props.match.path}/wishlist`}
            render={(props) => (
              <Wishlist
                {...props}
                wishlist={context.user.wishlist}
                addToCart={context.addToCart}
                removeFromWishlist={context.removeFromWishlist}
              />
            )}
          />
        </div>
      </section>
    </div>
  )
}

const dashboard = mq({
  display: ['auto', 'flex'],
  width: '100%',
  color: 'black',
  padding: ['0 0 5rem 0', '1.5rem 0 5rem 2rem'],
})

const desktopOnly = mq({
  display: ['none', 'block'],
  width: '100%',
})

export default UserDashboard
