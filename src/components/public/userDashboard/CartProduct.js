/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [400, 700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const show = {
  display: 'flex',
  borderBottom: '1px solid lightgray',
  marginBottom: '2rem',
  paddingBottom: '1rem',
}

const circleFitter = mq({
  padding: '0 !important',
  overflow: 'hidden',
  height: '10vw',
  minHeight: ['50px', '100px'],
  width: '10vw',
  minWidth: ['50px', '100px'],
  boxShadow: '0px 3px 6px #00000029',
  marginBottom: '2rem',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
})

const content = mq({
  padding: '0 0 0 4%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'space-between',
  p: {
    fontWeight: '300',
    color: '#5A5A5A',
    lineHeight: '1.5rem',
  },
  '.amount': {
    paddingLeft: '0.5rem',
    outline: 'none',
    border: 'none',
    color: '#5A5A5A',
    cursor: 'pointer',
    fontSize: '0.95rem',
  },
  '.move': {
    color: '#FA5402',
    cursor: 'pointer',
    ':hover': {
      textDecoration: 'underline',
    },
  },
  '.remove': {
    alignSelf: ['flex-start', 'flex-start', 'flex-end'],
    fontSize: '0.85rem',
    color: 'black',
    cursor: 'pointer',
    ':hover': {
      textDecoration: 'underline',
    },
  },
})

const firstLine = {
  alignItems: 'flex-start',
  justifyContent: 'space-between',
  marginBottom: '1rem',
  '.bold': {
    fontWeight: 700,
    color: 'black',
    fontSize: '1.2rem',
  },
}

export default function CartProduct({ product, context }) {
  const [loopMe, setLoopMe] = useState([])

  useEffect(() => {
    const createLoopNumbers = () => {
      let tempArr = []
      for (let i = 1; i < 100; i++) {
        tempArr.push(i)
      }
      setLoopMe(tempArr)
    }
    createLoopNumbers()
  }, [])

  const moveToWishlist = async (range, variantIndex) => {
    await context.removeFromCart(range, variantIndex)
    context.addToWishlist(range, variantIndex)
  }

  const handleChange = (e, range, variantIndex) => {
    const newAmount = e.target.value
    context.updateCart(range, variantIndex, newAmount)
  }

  return (
    <div css={show}>
      <Link
        to={{
          pathname: product.range.url
            ? `/productdetails/${product.range.url}/${product.variantIndex}`
            : `/productdetails/${product.range._id}/${product.variantIndex}`,
          state: {
            item: product.range,
            index: product.variantIndex,
          },
        }}
        css={circleFitter}
        className='flexCenter'
      >
        <img
          alt='product'
          src={`${URL}/assets/${
            product.range.variants[product.variantIndex].imageURLs[0].filename
          }`}
        />
      </Link>
      <div css={content}>
        <div className='flexColumn' css={firstLine}>
          <p>{product.amount} Metres</p>
          <p className='uppercase bold'>
            {product.range.name}{' '}
            {product.range.variants[product.variantIndex].color}
          </p>
          <p>£{(product.amount * product.range.price).toFixed(2)}</p>
        </div>
        {/* <p>
          {product.range.usage &&
            Object.values(product.range.usage).map((usage, i) => {
              let keys = Object.keys(product.range.usage)
              i === 0 && (truth = [])
              usage && truth.push(keys[i])
              return (
                usage &&
                `${keys[i] !== truth[0] ? ', ' : ''} ${
                  keys[i].charAt(0).toUpperCase() + keys[i].slice(1) + ' fabric'
                }`
              )
            })}
        </p> */}
        <div className='flex'>
          <p style={{ paddingTop: '.15rem' }}>Quantity in metres</p>
          <select
            className='amount'
            value={product.amount}
            type='number'
            onChange={(e) =>
              handleChange(e, product.range, product.variantIndex)
            }
          >
            {loopMe.map((item) => (
              <option value={item} key={item}>
                {item}
              </option>
            ))}
          </select>
        </div>
        <p
          className='move'
          onClick={() => moveToWishlist(product.range, product.variantIndex)}
        >
          Move item to wishlist
        </p>
        <p
          className='remove'
          onClick={() =>
            context.removeFromCart(product.range, product.variantIndex)
          }
        >
          Remove item completely
        </p>
      </div>
    </div>
  )
}
