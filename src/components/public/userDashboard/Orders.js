/** @jsx jsx */
import { jsx } from '@emotion/react'
import axios from 'axios'
import { useContext, useEffect, useState } from 'react'
import ReactLoading from 'react-loading'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'
import { URL } from '../../../config'
import { estimatedTime } from '../helpers/EstimatedTime'
import { getTime } from '../helpers/getTime'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 900, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const loadingStyle = {
  backgroundColor: 'white',
  width: '100%',
  margin: '2rem 0',
}

const show = mq({
  display: 'flex',
  width: ['100%', '90%', '90%', '80%'],
  flexDirection: 'column',
  color: 'black',
  marginLeft: [0, '3rem'],
  h1: {
    fontSize: '1.1rem',
    letterSpacing: '1px',
    fontWeight: '700',
    margin: ['2rem 0 0 1rem', '0 0 .5rem 0'],
    fontFamily: 'Montserrat, sans-serif',
  },
})

const mainContainer = mq({
  marginTop: [0, '1.5rem'],
  fontFamily: 'Helvetica Neue, sans-serif',
  marginBottom: '3rem',
  '.orders': {
    fontSize: '.95rem',
    fontWeight: '500',
  },
})

const itemWrapper = mq({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  marginBottom: '2.5rem',
  boxShadow: ['none', '0px 3px 6px #00000029'],
  borderBottom: '1px solid lightgray',
  padding: ['0 0 3rem 1rem', '2rem 5rem 5rem'],
  borderRadius: '10px',
  '.first_flex': {
    display: 'flex',
    flexDirection: ['column', 'column', 'row'],
    justifyContent: 'space-between',
    p: {
      color: '#110B17',
      fontSize: '1.2rem',
    },
    '.this_black': {
      marginTop: ['1.5rem', '2rem'],
      fontWeight: 500,
    },
    '.this_gray': {
      fontWeight: '300',
      marginTop: '0.2rem',
    },
  },
  '.this_green': {
    color: '#64C455 !important',
    fontWeight: '500',
    marginTop: '0.2rem',
  },
  '.cartWrapper': {
    display: 'flex',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '119px',
    padding: '1.5rem 2.5rem',
    alignItems: 'center',
    border: '1px solid transparent',
    transition: 'all ease-in-out 150ms',
    i: {
      marginRight: '1rem',
    },
    ':hover': {
      color: '#FBA586',
      border: '1px solid #FBA586',
      boxShadow: '5px 10px 15px 5px rgba(235,235,235,1)',
    },
  },
  '.orders_info': {
    'p, span': {
      lineHeight: '2rem',
      fontSize: '1.3rem',
    },
  },
  '.orders_title': {
    fontFamily: 'cooper-black-std, serif',
    fontSize: ['1.5rem', '2.2rem !important'],
    color: '#00263E',
    marginBottom: [0, '1.5rem'],
    letterSpacing: '-2px !important',
  },
})

const requestsWrapper = {
  display: 'flex',
  alignItems: 'center',
  margin: '1rem 0',
  '.nameAndPriceWrapper': {
    p: {
      fontWeight: 100,
      fontFamily: 'Roboto, sans-serif',
      fontSize: '1.1rem',
      lineHeight: '1.5rem',
    },
    '.full_name': {
      fontWeight: 700,
      fontSize: '1.2rem',
      fontFamily: 'Montserrat, sans-serif',
    },
  },
}

const circleFitter = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '100px',
  width: '100px',
  minHeight: '100px',
  minWidth: '100px',
  borderRadius: '100%',
  boxShadow: '0px 3px 6px #00000029',
  marginRight: '2rem',
  padding: '0 !important',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

const trackingButton = {
  width: 'fit-content',
  background: '#00263E',
  border: 'none',
  outline: 'none',
  alignSelf: 'center',
  color: 'white',
  borderRadius: '8px',
  marginTop: '1.5rem',
  padding: '1rem 4rem',
  textTransform: 'uppercase',
  fontSize: '1rem',
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: '500',
  letterSpacing: '1px',
  cursor: 'pointer',
  transition: 'all linear 150ms',
  ':hover': {
    boxShadow: '0px 3px 10px #11111188',
    background: '#00364E',
  },
}

const trackingNumberWrapper = {
  alignSelf: 'center',
  marginTop: '1rem',
  fontSize: '1.5rem',
  span: {
    fontWeight: '300 !important',
  },
}

export default function Orders() {
  const context = useContext(UserContext)

  const [orders, setOrders] = useState(null)
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const getUserOrders = async () => {
      if (!context.user._id) return false
      try {
        const res = await axios.get(
          `${URL}/orders/get_user_orders/${context.user._id}`
        )

        const sortedOrders = res.data.orders.sort((a, b) => {
          if (b.orderDate < a.orderDate) {
            return -1
          }
          if (b.orderDate > a.orderDate) {
            return 1
          }
          return 0
        })
        sortedOrders.forEach((order) => {
          let tempTotal = 0
          order.productsBought.forEach((product) => {
            tempTotal +=
              product.price - (product.price / 100) * product.discount
            order.total = tempTotal.toFixed(2)
          })
        })
        if (res.data.orders.length > 0) setOrders(res.data.orders)
      } catch (err) {
        console.error(err)
      }
      setLoaded(true)
    }
    if (!orders) getUserOrders()
  }, [context, orders])

  if (!loaded) {
    return (
      <div className='flexCenter' css={loadingStyle}>
        <ReactLoading type={'spin'} color='#030303' />
      </div>
    )
  }
  return (
    <div css={show}>
      <h1>ORDERS</h1>
      <div css={mainContainer}>
        {orders?.[0] ? (
          <div>
            {orders.map((order) => {
              return (
                <div css={itemWrapper} key={order._id}>
                  <div className='first_flex'>
                    <div>
                      <p className='this_black'>Ordered on:</p>
                      <p className='this_green'>{getTime(order.orderDate)}</p>
                      <p className='this_black'>
                        Shipping Address:{' '}
                        {order.shipmentAddress.name +
                          ' ' +
                          order.shipmentAddress.lastName}
                      </p>
                      <p className='this_gray'>
                        {order.shipmentAddress.direction}
                      </p>
                      <p className='this_gray'>{order.shipmentAddress.town}</p>
                      <p className='this_gray'>
                        {order.shipmentAddress.postalCode}
                      </p>
                      <p className='this_gray'>
                        {order.shipmentAddress.region.name}
                      </p>
                      <p className='this_black'>Order Follow-up Code</p>
                      <p className='this_gray'>
                        #{order.orderNumber.toString().padStart(4, '0')}
                      </p>
                    </div>
                    <div>
                      <p className='this_black'>Shipping Courier</p>
                      <p className='this_gray'>Royal Mail</p>
                      <p className='this_black'>Payment</p>
                      <p className='this_gray'>Credit Card</p>
                    </div>
                  </div>
                  {order.productsBought.map((requestedOrder, i) => {
                    return (
                      <div
                        css={requestsWrapper}
                        key={requestedOrder._id}
                        style={{ margin: i === 0 && '3rem 0' }}
                      >
                        <Link
                          to={
                            requestedOrder.url
                              ? `/productdetails/${requestedOrder.url}/${requestedOrder.variantIndex}`
                              : `/productdetails/${requestedOrder._id}/${requestedOrder.variantIndex}`
                          }
                          css={circleFitter}
                        >
                          <img
                            alt='product'
                            src={`${URL}/assets/${requestedOrder.filename}`}
                          />
                        </Link>
                        <div className='nameAndPriceWrapper'>
                          <p>{requestedOrder.amount} Metres</p>
                          <p className='full_name uppercase'>
                            {requestedOrder.name} {requestedOrder.variant_name}
                          </p>
                          <p>
                            £
                            {(
                              requestedOrder.amount * requestedOrder.price -
                              (requestedOrder.price / 100) *
                                requestedOrder.discount
                            ).toFixed(2)}
                          </p>
                        </div>
                      </div>
                    )
                  })}
                  <div className='orders_info'>
                    <div style={{ marginBottom: '1rem' }}>
                      <span style={{ fontSize: '1.5rem' }}>Total: </span>
                      <span style={{ fontWeight: 300, fontSize: '1.3rem' }}>
                        £{order.total}
                      </span>
                    </div>
                    <p className='orders_title'>Mad About Fabrics</p>
                    <span className='this_green'>Order of Notice: </span>
                    <span>
                      The estimated date of delivery is an approximation and is
                      NOT indicative of the precise date of delivery. To track
                      your precise status visit DPD website.
                    </span>
                    <p className='this_green' style={{ fontWeight: 300 }}>
                      Estimated delivery date{' '}
                      {getTime(
                        order.orderDate,
                        estimatedTime(order.shipmentAddress.region)
                      )}
                    </p>
                  </div>
                  {order.trackingNumber && (
                    <button css={trackingButton}>Track Status</button>
                  )}
                  <div css={trackingNumberWrapper}>
                    <span className='this_green'>Tracking Number: </span>
                    <span className='this_green'>
                      {order.trackingNumber
                        ? order.trackingNumber
                        : 'To be assigned'}
                    </span>
                  </div>
                </div>
              )
            })}
          </div>
        ) : (
          <div style={{ padding: '1rem 0 0 1rem' }}>
            No items on your order list!
          </div>
        )}
      </div>
    </div>
  )
}
