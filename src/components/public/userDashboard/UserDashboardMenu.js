/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext } from 'react'
import facepaint from 'facepaint'
import { NavLink } from 'react-router-dom'

import UserContext from '../../../contexts/userContext'

// RESPONSIVENESS SETTINGS
const breakpoints = [700]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

export default function UserDashboardMenu() {
  const context = useContext(UserContext)

  return (
    <div css={style}>
      <nav>
        <ul>
          <li>
            <NavLink
              exact
              activeClassName={'activeSection'}
              to='/userdashboard/cart'
            >
              Your Basket
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName={'activeSection'}
              to='/userdashboard/'
            >
              Orders
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName={'activeSection'}
              to='/userdashboard/samples'
            >
              Samples
            </NavLink>
          </li>
          <li>
            <NavLink
              exact
              activeClassName={'activeSection'}
              to='/userdashboard/wishlist'
            >
              Wishlist
            </NavLink>
          </li>
          <li>
            <NavLink
              activeClassName={'activeSection'}
              to='/userdashboard/profile'
            >
              Profile
            </NavLink>
          </li>
          <li>
            <NavLink
              activeClassName={'activeSection'}
              to='/userdashboard/password'
            >
              Password
            </NavLink>
          </li>
          <li>
            <NavLink
              activeClassName={'activeSection'}
              to='/userdashboard/address'
            >
              Address
            </NavLink>
          </li>
          <li className='logout' onClick={context.logOut}>
            Log Out
          </li>
        </ul>
      </nav>
    </div>
  )
}

const style = mq({
  display: ['none', 'flex'],
  margin: '0.5rem 0 10rem',
  paddingTop: '1rem',
  width: '15vw',
  color: 'black',
  borderTop: '1px solid lightGrey',
  fontFamily: 'Roboto, sans-serif',
  fontWeight: '100',
  nav: {
    width: '100%',
  },
  ul: {
    display: 'flex',
    flexDirection: 'column',
    listStyle: 'none',
    width: '100%',
  },
  li: {
    lineHeight: '1.3rem',
    width: '100%',
    whiteSpace: 'nowrap',
  },
  a: {
    padding: '0.5rem .9rem',
    display: 'block',
    color: 'inherit',
    textDecoration: 'none',
    width: '100%',
    fontSize: '0.95rem',
  },
  '.activeSection': {
    fontWeight: '400',
    backgroundColor: '#F9F9F9',
    width: '100%',
  },
  '.logout': {
    padding: '0.5rem .9rem',
    cursor: 'pointer',
    fontSize: '0.95rem',
  },
})
