/** @jsx jsx */
import { jsx } from '@emotion/react'
import axios from 'axios'
import { useContext, useEffect, useState } from 'react'
import ReactLoading from 'react-loading'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'
import { URL } from '../../../config'
import { estimatedTime } from '../helpers/EstimatedTime'
import { getTime } from '../helpers/getTime'

// RESPONSIVENESS SETTINGS
const breakpoints = [700, 900, 1150]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const loadingStyle = {
  backgroundColor: 'white',
  width: '100%',
}

const show = mq({
  display: 'flex',
  width: ['100%', '90%', '90%', '80%'],
  flexDirection: 'column',
  color: 'black',
  marginLeft: [0, '3rem'],
  h1: {
    fontSize: '1.1rem',
    letterSpacing: '1px',
    fontWeight: '700',
    margin: ['2rem 0 0 1rem', '0 0 .5rem 0'],
    fontFamily: 'Montserrat, sans-serif',
  },
})

const mainContainer = mq({
  marginTop: [0, '1.5rem'],
  fontFamily: 'Helvetica Neue, sans-serif',
  margin: '2rem 0 5rem',
})

const itemWrapper = mq({
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  marginBottom: '2.5rem',
  boxShadow: ['none', '0px 3px 6px #00000029'],
  borderBottom: '1px solid lightgray',
  padding: ['0 0 3rem 1rem', '2rem 5rem 5rem'],
  borderRadius: '10px',
  '.first_flex': {
    display: 'flex',
    flexDirection: ['column', 'column', 'row'],
    justifyContent: 'space-between',
    p: {
      color: '#110B17',
      fontSize: '1.2rem',
    },
    '.this_black': {
      marginTop: ['1.5rem', '2rem'],
      fontWeight: 500,
    },
    '.this_gray': {
      fontWeight: '300',
      marginTop: '0.2rem',
    },
  },
  '.this_green': {
    color: '#64C455 !important',
    fontWeight: '500',
    marginTop: '0.2rem',
  },
  '.cartWrapper': {
    display: 'flex',
    boxShadow: '0px 3px 6px #00000029',
    borderRadius: '119px',
    padding: '1.5rem 2.5rem',
    alignItems: 'center',
    border: '1px solid transparent',
    transition: 'all ease-in-out 150ms',
    i: {
      marginRight: '1rem',
    },
    ':hover': {
      color: '#FBA586',
      border: '1px solid #FBA586',
      boxShadow: '5px 10px 15px 5px rgba(235,235,235,1)',
    },
  },
  '.samples_info': {
    'p, span': {
      lineHeight: '2rem',
      fontSize: '1.3rem',
    },
  },
  '.samples_title': {
    fontFamily: 'cooper-black-std, serif',
    fontSize: ['1.5rem', '2.5rem !important'],
    color: '#00263E',
    marginBottom: [0, '1.5rem'],
  },
})

const requestsWrapper = {
  display: 'flex',
  alignItems: 'center',
  margin: '1rem 0',
}

const circleFitter = {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  overflow: 'hidden',
  height: '100px',
  width: '100px',
  borderRadius: '100%',
  boxShadow: '0px 3px 6px #00000029',
  marginRight: '2rem',
  padding: '0 !important',
  img: {
    flexShrink: 0,
    minWidth: '100%',
    minHeight: '100%',
  },
}

export default function Samples() {
  const context = useContext(UserContext)

  const [samples, setSamples] = useState(null)
  const [loaded, setLoaded] = useState(false)

  useEffect(() => {
    const getUserSamples = async () => {
      if (!context.user._id) return false
      try {
        const res = await axios.get(
          `${URL}/samples/get_user_samples/${context.user._id}`
        )
        const sortedSamples = res.data.samples.sort((a, b) => {
          if (b.orderDate < a.orderDate) {
            return -1
          }
          if (b.orderDate > a.orderDate) {
            return 1
          }
          return 0
        })
        setSamples(sortedSamples)
      } catch (err) {
        console.error(err)
      }
      setLoaded(true)
    }
    getUserSamples()
  }, [context])

  if (!loaded) {
    return (
      <div className='flexCenter' css={loadingStyle}>
        <ReactLoading type={'spin'} color='#030303' />
      </div>
    )
  }
  return (
    <div css={show}>
      <h1>SAMPLE REQUESTS</h1>
      <div css={mainContainer}>
        {samples && samples[0] ? (
          <div>
            {samples.map((sample) => {
              return (
                <div css={itemWrapper} key={sample._id}>
                  <div className='first_flex'>
                    <div>
                      <p className='this_black'>Ordered on:</p>
                      <p className='this_green'>{getTime(sample.orderDate)}</p>
                      <p className='this_black'>
                        Shipping Address:{' '}
                        {sample.shipmentAddress.name +
                          ' ' +
                          sample.shipmentAddress.lastName}
                      </p>
                      <p className='this_gray'>
                        {sample.shipmentAddress.direction}
                      </p>
                      <p className='this_gray'>{sample.shipmentAddress.town}</p>
                      <p className='this_gray'>
                        {sample.shipmentAddress.postalCode}
                      </p>
                      <p className='this_gray'>
                        {sample.shipmentAddress.region}
                      </p>
                      <p className='this_black'>Samples Order Follow-up Code</p>
                      <p className='this_gray'>{sample._id}</p>
                    </div>
                    <div>
                      <p className='this_black'>Shipping Courier</p>
                      <p className='this_gray'>Royal Mail</p>
                      <p className='this_black'>Payment</p>
                      <p className='this_gray'>N/A</p>
                    </div>
                  </div>
                  {sample.requestedSamples.map((requestedSample, i) => {
                    return (
                      <div
                        css={requestsWrapper}
                        key={requestedSample._id}
                        style={{ margin: i === 0 && '3rem 0' }}
                      >
                        <Link
                          to={
                            requestedSample.url
                              ? `/productdetails/${requestedSample.url}/${requestedSample.variantIndex}`
                              : `/productdetails/${requestedSample._id}/${requestedSample.variantIndex}`
                          }
                          css={circleFitter}
                        >
                          <img
                            alt='product'
                            src={`${URL}/assets/${requestedSample.filename}`}
                          />
                        </Link>
                        <div className='nameAndPriceWrapper'>
                          <p style={{ fontWeight: 300 }}>
                            {requestedSample.name}
                          </p>
                          <p className='this_black'>
                            {requestedSample.variant_name}
                          </p>
                        </div>
                      </div>
                    )
                  })}
                  <div className='samples_info'>
                    <p className='samples_title'>Samples</p>
                    <span className='this_green'>Order of Notice: </span>
                    <span>
                      The estimated date of delivery is an approximation and is
                      NOT indicative of the precise date of delivery. To track
                      your precise status visit DPD website.
                    </span>
                    <p className='this_green' style={{ fontWeight: 300 }}>
                      Estimated delivery date{' '}
                      {getTime(
                        sample.orderDate,
                        estimatedTime(sample.shipmentAddress.region)
                      )}
                    </p>
                  </div>
                </div>
              )
            })}
          </div>
        ) : (
          <div>No items on your sample list!</div>
        )}
      </div>
    </div>
  )
}
