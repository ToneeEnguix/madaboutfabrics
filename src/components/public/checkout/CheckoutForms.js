/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useContext, useState, useEffect, useRef } from 'react'
import { Redirect } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'

import RightInformation from './RightInformation.js'
import LeftInformation from './LeftInformation'

// RESPONSIVENESS SETTINGS
const breakpoints = [600, 1000, 1200]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const centerTitle = mq({
  display: 'flex',
  justifyContent: 'center',
  flexDirection: 'column',
  alignItems: 'center',
  padding: ['2rem 1rem 15rem 1rem', '2rem 8% 15rem 9%', '2rem 8% 15rem 9%'],
  h1: {
    fontSize: '2.1rem',
    marginBottom: '1.5rem',
    fontFamily: 'Montserrat, sans-serif',
    letterSpacing: '3px',
    fontWeight: '700',
    paddingRight: [0, 0, '2rem'],
  },
})

const wholeForm = mq({
  display: 'flex',
  justifyContent: 'space-between',
  fontFamily: 'Roboto, sans-serif',
  width: '100%',
  flexDirection: ['column-reverse', 'column-reverse', 'row'],
})

export default function CheckoutForms(props) {
  const [availableForPickup, setAvailableForPickup] = useState(true)
  const [redirect, setRedirect] = useState(false)
  const [shipped, setShipped] = useState(true)
  const [region, setRegion] = useState({
    _id: 'NIRE',
    name: 'Northern Ireland',
    coin: 'GBP',
    shippingRate: 10,
    situation: 'Ireland',
  })

  const toggleShipment = (boolean) => {
    setShipped(boolean)
  }

  const context = useContext(UserContext)

  const updated = useRef(false)

  useEffect(() => {
    context.user.cart.forEach((item, i) => {
      if (!item.range.variants[item.variantIndex].availableForPickup)
        setAvailableForPickup(false)
    })
    if (context.user?.cart.length === 0) setRedirect(true)
    if (region?.shippingRate === 0 && context.user.region) {
      setRegion(context.user.region)
    }
    !updated.current && (updated.current = true)
  }, [context, region.shippingRate])

  if (redirect) return <Redirect to={'/home'} />
  return (
    <section>
      <div css={centerTitle}>
        <h1>CHECKOUT</h1>
        <div css={wholeForm}>
          <LeftInformation
            {...props}
            toggleShipment={toggleShipment}
            toggleRegion={(region) => setRegion(region)}
            region={region}
            context={context}
            shipped={shipped}
            setShipped={setShipped}
            availableForPickup={availableForPickup}
          />
          <RightInformation
            shipped={shipped}
            region={region}
            context={context}
          />
        </div>
      </div>
    </section>
  )
}
