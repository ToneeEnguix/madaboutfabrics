/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import { estimatedTime } from '../helpers/EstimatedTime'

import CartProduct from './CartProduct'

// RESPONSIVENESS SETTINGS
const breakpoints = [600, 1000, 1082, 1200]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const rightColumn = mq({
  display: 'flex',
  flexDirection: 'column',
  width: ['100%', '100%', '37%', '37%'],
  position: ['', '', '', 'sticky'],
  top: 0,
  height: '100%',
  paddingBottom: '60px',
})

const summaryWrapper = {
  display: 'flex',
  flexDirection: 'column',
  boxShadow: '0px 3px 6px #00000029',
  marginBottom: '2rem',
  width: '100%',
  h2: {
    paddingLeft: '1.2rem',
    lineHeight: '4rem',
    letterSpacing: '2px',
    fontWeight: '500',
    backgroundColor: '#E5E5E5',
  },
  pre: {
    margin: '0.5rem 1rem 0',
    borderTop: '1px solid LightGray',
  },
  '.costs': {
    fontWeight: '300',
    color: '#111111',
  },
  '.total': {
    color: '#FA5402',
  },
}

const flex = {
  display: 'flex',
  justifyContent: 'space-between',
  padding: '0.5rem 1rem',
}

const flexTotal = {
  display: 'flex',
  justifyContent: 'space-between',
  padding: '1rem',
  fontWeight: 500,
  '.total': { color: 'black ' },
}

const bagWrapper = {
  display: 'flex',
  flexDirection: 'column',
  paddingBottom: '2rem',
  boxShadow: '0px 3px 6px #00000029',
  h3: {
    textTransform: 'uppercase',
    borderBottom: '1px solid lightgray',
    paddingBottom: '0.5rem',
    margin: '2rem 1rem 1rem',
    fontWeight: '500',
    fontSize: '1.1rem',
  },
}

const cartTitle = mq({
  display: 'flex',
  justifyContent: 'space-around',
  alignItems: 'center',
  backgroundColor: '#E5E5E5',
  flexDirection: 'row',
  h2: {
    padding: [
      '1rem 0 1rem 1.2rem',
      '0 0 0 1.2rem',
      '1rem 0 1rem 1.2rem',
      '1rem 0 1rem 1.2rem',
      '0 0 0 1.2rem',
    ],
    lineHeight: ['2rem', '4rem', '2rem', '2rem', '4rem'],
    letterSpacing: '2px',
    fontWeight: '500',
    width: '100%',
  },
  a: {
    color: 'black',
    fontSize: '0.7rem',
    marginRight: 'auto',
    padding: '0 0.5rem 0.5rem',
    textAlign: 'right',
    width: 'fit-content',
    alignSelf: ['', '', '', '', 'center'],
  },
})

export default function RightInformation(props) {
  const [deliveryTime, setDeliveryTime] = useState(1)

  useEffect(() => {
    setDeliveryTime(estimatedTime(props.context.region?.situation))
  }, [props.context])

  return (
    <div css={rightColumn}>
      <div css={summaryWrapper}>
        <h2>SUMMARY</h2>
        <div css={flex} style={{ marginTop: '1rem' }}>
          <p>Subtotal</p>
          <Amount cart={props.context.user.cart} shipping={0} />
        </div>
        <div key={props.shipped} css={flex}>
          <p>Estimates Handling and Shipping</p>
          {props.shipped && props.region?.shippingRate ? (
            <p className='costs'>
              £
              {typeof props.region?.shippingRate === 'number'
                ? Number(props.region?.shippingRate).toFixed(2)
                : 'N/A'}
            </p>
          ) : (
            <p>N/A</p>
          )}
        </div>
        <pre />
        <div css={flexTotal}>
          <p className='total'>TOTAL</p>
          {props.shipped === true ? (
            <Amount
              cart={props.context.user.cart}
              shipping={props.region?.shippingRate}
            />
          ) : (
            <Amount cart={props.context.user.cart} shipping={0} />
          )}
        </div>
      </div>
      <div css={bagWrapper}>
        <div css={cartTitle}>
          <h2>PRODUCTS IN YOUR BAG</h2>
          <Link to='/userdashboard/cart'> Edit</Link>
        </div>
        <h3>Receive it in {deliveryTime} days</h3>
        {props.context.user.cart.length > 0 &&
          props.context.user.cart.map((product, i) => {
            return <CartProduct key={i} product={product} />
          })}
      </div>
    </div>
  )
}

const Amount = (props) => {
  let amount = 0

  props.cart.forEach((ele) => {
    if (ele.range.variants[ele.variantIndex].saleDiscount !== 0) {
      amount =
        amount +
        ele.amount *
          (ele.range.price -
            ele.range.price *
              (ele.range.variants[ele.variantIndex].saleDiscount / 100))
    } else {
      amount = amount + ele.amount * ele.range.price
    }
  })

  return (
    <span className='total'>
      £{' '}
      {typeof props.shipping === 'number'
        ? Number(amount + props.shipping).toFixed(2)
        : 'N/A'}
    </span>
  )
}
