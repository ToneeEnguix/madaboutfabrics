/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'
import ShortDisplay from '../generalPurpose/display/ShortDisplay'
import CheckoutForms from './CheckoutForms'
import CheckoutConfirmation from './CheckoutConfirmation'
import { Route } from 'react-router-dom'

import './checkout.css'

export default function Checkout(props) {
  return (
    <React.Fragment>
      <ShortDisplay />
      <Route
        exact
        path={`${props.match.path}`}
        render={(props) => <CheckoutForms {...props} />}
      />
      <Route
        exact
        path={`${props.match.path}/success`}
        component={CheckoutConfirmation}
      />
    </React.Fragment>
  )
}
