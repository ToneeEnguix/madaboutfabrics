/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState, useContext } from 'react'
import axios from 'axios'
import { Redirect } from 'react-router'
import { Link } from 'react-router-dom'
import facepaint from 'facepaint'

import UserContext from '../../../contexts/userContext'
import { URL } from '../../../config'

// RESPONSIVENESS SETTINGS
const breakpoints = [600, 800, 1000, 1200]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const page = {
  minWidth: '100vw',
  minHeight: '80vh',
  fontFamily: 'Roboto, sans-serif',
}

const main = mq({
  boxShadow: '0px 3px 6px #00000029',
  width: ['90vw', '80vw', '50vw'],
  margin: '3rem auto',
  div: {
    padding: '3rem 10%',
  },
})

const top = {
  borderBottom: '1px solid lightgray',
  h1: {
    fontFamily: 'cooper-black-std, serif',
    marginBottom: '1rem',
    fontSize: '1.5rem',
  },
  p: {
    fontWeight: 300,
    fontSize: '0.8rem',
  },
}

const bottom = {
  '.request_success': {
    letterSpacing: '2px',
    fontWeight: '600',
    fontSize: '1.1rem',
    margin: '0.75rem auto',
    textAlign: 'center',
  },
  '.open_email': {
    fontWeight: '200',
    fontSize: '0.9rem',
  },
  '.buttonContainer': {
    padding: 0,
    margin: '2rem auto 1rem',
    button: {
      cursor: 'pointer',
      outline: 'none',
      border: 'none',
      backgroundColor: '#028840',
      color: 'white',
      padding: '1rem 2rem',
      fontWeight: '700',
    },
  },
}

const closeWindow = {
  color: 'inherit',
  fontWeight: '700',
  fontSiz: '0.95rem',
}

export default function CheckoutConfirmation(props) {
  const context = useContext(UserContext)
  const [redirect, setRedirect] = useState(false)
  const request_id = new URLSearchParams(props.location.search).get(
    'payment_intent'
  )

  useEffect(() => {
    const doesRequestIdExist = async () => {
      try {
        const res = await axios.get(
          `${URL}/orders/get_order_info/${request_id}`
        )
        if (!res.data.ok) {
          setRedirect(true)
        } else {
          context.user.cart.forEach((item, i) => {
            context.removeFromCart(item)
          })
        }
      } catch (err) {
        console.error(err)
      }
    }
    request_id?.length === 24 && doesRequestIdExist()
  }, [request_id, context])

  if (redirect) {
    return <Redirect to='/' />
  }
  return (
    <div css={page} className='flexColumn'>
      <div css={main}>
        <div css={top} className='flexColumn'>
          <h1>THANK YOU</h1>
          <p>
            Order Number: <strong>{request_id}</strong>
          </p>
        </div>
        <div css={bottom} className='flexColumn'>
          <p className='uppercase request_success'>
            Your order request was a success!
          </p>
          <p className='open_email'>
            Open the email to see the order confirmation
          </p>
          {context.user._id ? (
            <div className='buttonContainer'>
              <Link to='/userdashboard/'>
                <button className='uppercase'>View your order</button>
              </Link>
            </div>
          ) : (
            <div className='buttonContainer'>
              <button className='uppercase' style={{ cursor: 'default' }}>
                Sign up to track your orders
              </button>
            </div>
          )}
        </div>
      </div>
      <Link to='/' css={closeWindow} className='uppercase'>
        Close Window
      </Link>
    </div>
  )
}
