/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState } from 'react'
import facepaint from 'facepaint'

import Shipping from './Shipping/Shipping'
import PaymentWrapper from './Payment/PaymentWrapper'
import Billing from './Billing/Billing'

// RESPONSIVENESS SETTINGS
const breakpoints = [600, 1000, 1200]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const leftColumn = mq({
  display: 'flex',
  flexDirection: 'column',
  width: ['100%', '100%', '61%'],
  transition: 'all linear 120ms',
})

export default function LeftInformation(props) {
  const [openPayment, setOpenPayment] = useState(false)
  const [openConfirm, setOpenConfirm] = useState(false)

  const [shipmentData, setShipmentData] = useState({
    shipmentData: {
      name: props.context.user.address?.name,
      lastName: props.context.user.address?.lastName,
      address: props.context.user.address?.direction,
      postalCode: props.context.user.address?.postalCode,
      region: props.context.user.address?.region,
      city: props.context.user.address?.town,
      email: props.context.user.email,
      phoneNumber: props.context.user.address?.phoneNumber,
    },
    shippingMethod: 'Ship',
  })
  const [useBilling, setUseBilling] = useState(true)
  const [billingData, setBillingData] = useState({
    name: props.context.user.address?.name,
    lastName: props.context.user.address?.lastName,
    address: props.context.user.address?.direction,
    postalCode: props.context.user.address?.postalCode,
    region: props.context.user.address?.region,
    city: props.context.user.address?.town,
    email: props.context.user.email,
    phoneNumber: props.context.user.address?.phoneNumber,
  })

  const getShipmentData = (shippingAddress) => {
    setShipmentData(shippingAddress)
    setOpenPayment(true)
  }

  const validateBilling = () => {
    setOpenConfirm(true)
  }

  return (
    <div css={leftColumn}>
      <Shipping
        sendShipmentData={getShipmentData}
        shipmentData={shipmentData}
        toggleShipment={(bool) => props.toggleShipment(bool)}
        togglePayment={(bool) => setOpenPayment(bool)}
        toggleConfirm={(bool) => setOpenConfirm(bool)}
        toggleRegion={(region) => props.toggleRegion(region)}
        setShipped={props.setShipped}
        context={props.context}
        availableForPickup={props.availableForPickup}
      />
      <Billing
        context={props.context}
        validateBilling={validateBilling}
        openPayment={openPayment}
        setOpenConfirm={() => setOpenConfirm(false)}
        shippingAddress={shipmentData.shipmentData}
        setBillingData={setBillingData}
        billingData={billingData}
        useBilling={useBilling}
        setUseBilling={() => setUseBilling(!useBilling)}
        shipped={props.shipped}
      />
      <PaymentWrapper
        {...props}
        openConfirm={openConfirm}
        shipmentData={shipmentData}
        billingData={billingData}
        useBilling={useBilling}
        region={props.region}
        context={props.context}
      />
    </div>
  )
}
