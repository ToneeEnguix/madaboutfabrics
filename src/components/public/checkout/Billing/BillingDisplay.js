/** @jsx jsx */
import { jsx } from '@emotion/react'

const main = {
  padding: '1.5rem 2.5rem 2.5rem',
}

const billingAddressStyle = {
  fontWeight: 300,
  lineHeight: '1.4rem',
  fontSize: '0.9rem',
}

export default function BillingDisplay({ billingData }) {
  return (
    <div css={main}>
      <div css={billingAddressStyle}>
        <p>Billing Address</p>
        <div>
          <p className='gray'>
            {billingData.name} {billingData.lastName}
          </p>
          <p className='gray'>{billingData.direction}</p>
          <p className='gray'>
            {billingData.town} {billingData.postalCode}
          </p>
          <p className='gray'>{billingData.region?.name}</p>
          <p className='gray'>{billingData.email}</p>
          <p className='gray'>{billingData.phoneNumber}</p>
        </div>
      </div>
    </div>
  )
}
