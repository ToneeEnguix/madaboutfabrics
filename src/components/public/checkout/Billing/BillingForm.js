/** @jsx jsx */
import { jsx } from '@emotion/react'
import facepaint from 'facepaint'

import tick from '../../../admin/pictures/tick.svg'
import StyledSelect from '../../generalPurpose/StyledInput/StyledSelect'

// RESPONSIVENESS SETTINGS
const breakpoints = [500, 800]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const formData = {
  display: 'flex',
  justifyContent: 'space-around',
  flexWrap: 'wrap',
  marginTop: '1.5rem',
  flexDirection: 'column',
  marginBottom: '1rem',
}

const billingRadio = {
  padding: '0 1rem 1rem 2.5rem',
  fontWeight: 400,
  display: 'flex',
  div: {
    border: '2px solid #B7B7B7',
    borderRadius: '5px',
    width: '20px',
    height: '20px',
    cursor: 'pointer',
    marginRight: '0.5rem',
    pre: {
      fontSize: '0.5rem',
      margin: 0,
      padding: 0,
    },
  },
  label: {
    marginTop: '2px',
  },
}

const buttonFlex = {
  display: 'flex',
  justifyContent: 'flex-end',
  width: '95%',
  marginBottom: '1rem',
}

const checkoutButton = {
  backgroundColor: '#FA5402',
  outline: 'none',
  color: 'white',
  border: 'none',
  padding: '0.83rem 1.7rem',
  cursor: 'pointer',
  textTransform: 'uppercase',
  fontSize: '0.8rem',
  fontWeight: '500',
  marginRight: '0.75rem',
  fontFamily: 'Roboto, sans-serif',
}

export default function BillingForm({
  useBilling,
  setUseBilling,
  setBillingData,
  billingData,
  validateBilling,
  shipped,
  shippingAddress,
  context,
}) {
  const handleCheckbox = () => {
    setUseBilling()
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    validateBilling()
  }

  return (
    <form css={formData} onSubmit={handleSubmit}>
      {shipped && (
        <div css={billingRadio} onClick={() => handleCheckbox()}>
          <div className='flexCenter'>
            <span
              role='img'
              aria-label='check'
              style={{ display: useBilling && 'none' }}
            >
              ✔️
            </span>
          </div>
          <label className='pointer'>
            The billing address is the same as the shipping address.
          </label>
        </div>
      )}
      {useBilling ? (
        <BillingAddressForm
          billingData={billingData}
          setBillingData={setBillingData}
          toggleRegion={(region) => {
            setBillingData({ ...billingData, region })
          }}
          context={context}
        />
      ) : (
        <BillingAddress shippingAddress={shippingAddress} />
      )}
      <div css={buttonFlex}>
        <button css={checkoutButton}>Continue</button>
      </div>
    </form>
  )
}

const billingForm = {
  padding: '0 1.5rem',
}

const inputWrapper = mq({
  position: 'relative',
  width: '100%',
  margin: ['0 0 1.3rem', '0 0 1.3rem', '0 0.75rem 1.3rem'],
  img: {
    position: 'absolute',
    top: '16px',
    right: '15px',
    filter:
      'invert(51%) sepia(99%) saturate(310%) hue-rotate(58deg) brightness(90%) contrast(90%)',
    display: 'none',
  },
})

const inputWrapper2 = {
  position: 'relative',
  width: '100%',
  img: {
    position: 'absolute',
    top: '16px',
    right: '15px',
    filter:
      'invert(51%) sepia(99%) saturate(310%) hue-rotate(58deg) brightness(90%) contrast(90%)',
  },
  margin: '0 0.75rem 1rem 0.75rem',
  boxSizing: 'border-box',
}

const doubleWrapper = mq({
  flexDirection: ['column', 'column', 'row'],
})

const tripleWrapper = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr', '1fr 1fr 1fr'],
  gap: [0, 0, '1.3rem'],
  boxSizing: 'border-box',
  padding: [0, 0, '0 0.75rem'],
  '.postalCodeInput': {
    marginRight: 0,
    marginLeft: 0,
  },
  '.cityInput': {
    margin: 0,
    marginBottom: ['1.3rem', '1.3rem', 0],
  },
  '.regionInput': {
    marginLeft: 0,
    marginBottom: ['1.3rem', '1.3rem', 0],
  },
})

const BillingAddressForm = (props) => {
  const handleChange = (e) => {
    props.setBillingData({
      ...props.billingData,
      [e.target.name]: e.target.value,
    })
  }

  return (
    <div css={billingForm}>
      <div css={doubleWrapper} className='inputsWrapper' id='form_validation'>
        <div css={inputWrapper}>
          <input
            defaultValue={props.billingData.name}
            className='styledInput2'
            name='name'
            placeholder='Name'
            autoFocus
            type='text'
            required
            onChange={handleChange}
          />
          <img src={tick} alt='tick' />
        </div>
        <div css={inputWrapper}>
          <input
            defaultValue={props.billingData.lastName}
            className='styledInput2'
            name='lastName'
            placeholder='Surname/s'
            onChange={handleChange}
            required
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
      </div>
      <div className='inputsWrapper' id='form_validation'>
        <div css={inputWrapper}>
          <input
            required
            defaultValue={props.billingData.direction}
            className='styledInput2'
            name='direction'
            placeholder='Address'
            onChange={handleChange}
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
      </div>
      <div className='inputsWrapper' css={tripleWrapper} id='form_validation'>
        <div className='postalCodeInput' css={inputWrapper}>
          <input
            required
            defaultValue={props.billingData.postalCode}
            className='styledInput2'
            name='postalCode'
            placeholder='Postal Code'
            onChange={handleChange}
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
        <div className='cityInput' css={inputWrapper}>
          <input
            required
            defaultValue={props.billingData.town}
            className='styledInput2'
            name='town'
            placeholder='City'
            onChange={handleChange}
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
        <div className='regionInput' css={inputWrapper2}>
          <StyledSelect
            innerName={'region'}
            name={'Region'}
            toggleRegion={(region) => props.toggleRegion(region)}
            region={props.billingData.region}
          />
        </div>
      </div>
      <div css={doubleWrapper} className='inputsWrapper' id='form_validation'>
        <div css={inputWrapper}>
          <input
            required
            defaultValue={props.billingData.email}
            className='styledInput2'
            name='email'
            placeholder='Email'
            onChange={handleChange}
            type='email'
          />
          <img src={tick} alt='tick' />
        </div>
        <div css={inputWrapper}>
          <input
            required
            defaultValue={props.billingData.phoneNumber}
            type='tel'
            className='styledInput2'
            name='phoneNumber'
            placeholder='Phone Number'
            onChange={handleChange}
          />
          <img src={tick} alt='tick' />
        </div>
      </div>
      <button className='none' />
    </div>
  )
}

const shippingDisplay = {
  padding: '0 1rem 1rem 2.5rem',
  display: 'flex',
  flexDirection: 'column',
  span: {
    fontWeight: 300,
    lineHeight: '1.4rem',
    fontSize: '0.9rem',
  },
}

const BillingAddress = (props) => {
  if (props.shippingAddress.name !== '') {
    return (
      <div css={shippingDisplay}>
        <span style={{ fontWeight: 400 }}>Shipping address</span>
        <span className='gray'>
          {props.shippingAddress.name} {props.shippingAddress.lastName}
        </span>
        <span className='gray'>{props.shippingAddress.direction}</span>
        <span className='gray'>
          {props.shippingAddress.town}, {props.shippingAddress.postalCode}
        </span>
        <span className='gray'>
          {props.shippingAddress.region && props.shippingAddress.region.name}
        </span>
        <span className='gray'>{props.shippingAddress.email}</span>
        <span className='gray'>{props.shippingAddress.phoneNumber}</span>
      </div>
    )
  }
}
