/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import BillingForm from './BillingForm'
import BillingDisplay from './BillingDisplay'

const main = {
  transition: 'height 1s',
  marginBottom: '2rem',
  overflow: 'hidden',
  boxShadow: '0px 3px 6px 0px rgba(235,235,235,1)',
}

const title = {
  display: 'flex',
  justifyContent: 'flex-start',
  backgroundColor: '#00263E',
  height: '4rem',
  alignItems: 'center',
  padding: '0 1rem',
  h2: {
    textTransform: 'uppercase',
    backgroundColor: '#00263E',
    color: 'white',
    letterSpacing: '2px',
    fontSize: '1.35rem',
    marginLeft: '2%',
  },
  span: {
    color: 'white',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
  i: {
    color: 'green',
  },
}

export default function Billing(props) {
  const [showForm, setShowForm] = useState(true)

  const validateBilling = () => {
    setShowForm(false)
    props.validateBilling()
  }

  useEffect(() => {
    props.openPayment && setShowForm(true)
  }, [props.openPayment])

  return (
    <div css={main}>
      <div css={title}>
        {props.openPayment && !showForm && (
          <i className='material-icons #15D032'>done</i>
        )}
        <h2>2. Billing</h2>
      </div>
      {props.openPayment &&
        (showForm === true ? (
          <BillingForm
            validateBilling={validateBilling}
            shippingAddress={props.shippingAddress}
            setBillingData={props.setBillingData}
            setUseBilling={() => props.setUseBilling()}
            billingData={props.billingData}
            useBilling={props.useBilling}
            shipped={props.shipped}
            contenxt={props.context}
          />
        ) : (
          <BillingDisplay
            billingData={
              props.useBilling ? props.billingData : props.shippingAddress
            }
          />
        ))}
    </div>
  )
}
