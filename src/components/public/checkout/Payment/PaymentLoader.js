import React, { useEffect, useState } from 'react'
import axios from 'axios'
import ReactLoading from 'react-loading'
import { Redirect } from 'react-router-dom'

import { URL } from '../../../../config'
import Payment from './Payment'

export default function PaymentLoader(props) {
  const [stripeSecret, setStripeSecret] = useState('')
  const [redirect, setRedirect] = useState(false)

  useEffect(() => {
    const calculateAmount = () => {
      let cart = [...props.context.user.cart]
      let shipping = props.region.shippingRate
      let amount = 0

      cart.forEach((ele) => {
        if (ele.range.variants[ele.variantIndex].saleDiscount > 0) {
          amount +=
            ele.amount * ele.range.price -
            (ele.range.price / 100) *
              ele.range.variants[ele.variantIndex].saleDiscount
        } else {
          amount += ele.amount * ele.range.price
        }
      })

      return amount + shipping
    }
    let paymentFetched = false
    const createPaymentIntent = async () => {
      try {
        const res = await axios.post(`${URL}/payments/create-payment-intent`, {
          amount: calculateAmount(),
        })
        paymentFetched = true
        setStripeSecret(res.data.paymentIntent.client_secret)
      } catch (err) {
        console.error(err)
      }
    }
    if (props.context.user._id && props.context.user.cart.length === 0) {
      setRedirect(true)
    } else !paymentFetched && createPaymentIntent()
  }, [
    props.context.user._id,
    props.context.user.cart,
    props.region.shippingRate,
  ])

  if (redirect) {
    return <Redirect to={'/home'} />
  } else if (stripeSecret) {
    return <Payment stripeSecret={stripeSecret} {...props} />
  } else return <ReactLoading />
}
