/** @jsx jsx */
import { jsx } from '@emotion/react'
import PaymentLoader from './PaymentLoader'

export default function PaymentWrapper(props) {
  return (
    <div>
      <div css={title}>
        <h2>3. PAYMENT</h2>
      </div>
      {props.openConfirm === true ? <PaymentLoader {...props} /> : null}
    </div>
  )
}

const title = {
  display: 'flex',
  justifyContent: 'space-between',
  backgroundColor: '#00263E',
  height: '4rem',
  alignItems: 'center',
  padding: '0 1rem',
  h2: {
    textTransform: 'uppercase',
    backgroundColor: '#00263E',
    color: 'white',
    letterSpacing: '2px',
    fontSize: '1.35rem',
    marginLeft: '2%',
  },
}
