/** @jsx jsx */
import { jsx } from '@emotion/react'
import { PaymentElement, useStripe, useElements } from '@stripe/react-stripe-js'
import axios from 'axios'
import React, { useState, useContext } from 'react'
import ReactLoading from 'react-loading'
import ReactModal from 'react-modal'
import { Redirect } from 'react-router-dom'
import { ToastContainer, toast } from 'react-toastify'
import facepaint from 'facepaint'

import 'react-toastify/dist/ReactToastify.css'

import { URL } from '../../../../config'
import UserContext from '../../../../contexts/userContext'

import { sendConfirmationEmail } from '../../../../services/sendEmail'
import { estimatedTime } from '../../helpers/EstimatedTime'

// RESPONSIVENESS SETTINGS
const breakpoints = [600]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const main = mq({
  boxShadow: '0px 3px 6px #00000029',
  padding: ['2rem 0', '2rem'],
  display: 'flex',
  flexDirection: 'column',
  marginBottom: '1.2rem',
  button: {
    backgroundColor: '#FA5402',
    outline: 'none',
    color: 'white',
    border: 'none',
    padding: '0.83rem 1.7rem',
    cursor: 'pointer',
    textTransform: 'uppercase',
    fontSize: '0.8rem',
    fontWeight: '500',
    fontFamily: 'Roboto, sans-serif',
    width: 'fit-content',
    margin: '1rem 0.75rem',
    alignSelf: 'flex-end',
  },
  u: {
    cursor: 'pointer',
  },
})

export default function PaymentDisplay(props) {
  const stripe = useStripe()
  const elements = useElements()
  const context = useContext(UserContext)

  const [loading, setLoading] = useState(false)
  const [redirect, setRedirect] = useState(false)
  const [message, setMessage] = useState(false)
  const [order, setOrder] = useState(null)

  const handleRequest = async () => {
    setLoading(true)
    try {
      const orderRequest = await createOrder()
      if (!orderRequest.ok)
        return stopProcess(
          "Couldn't create the order. Please, contact support or try again."
        )
      setOrder(orderRequest.order)
      // once order request sent, create payment
      const result = await createPayment()
      if (!result)
        return stopProcess(
          "Couldn't process payment. Please, contact support or try again."
        )
      const updated = await updateOrder(orderRequest.order._id)
      if (!updated.data.ok)
        return stopProcess(
          "Couldn't update payment status on your order. Please, contact support and send them your order ID: " +
            orderRequest.order_id
        )
      const sent = await sendEmail(orderRequest.order)
      if (!sent) {
        // setTimeout(() => {
        //   localStorage.removeItem('shipmentData')
        //   setLoading(false)
        //   setRedirect(true)
        // }, 2000)
        return stopProcess(
          "Couldn't send the confirmation email. Contact support with your order ID:  " +
            orderRequest.order_id
        )
      } else {
        localStorage.removeItem('shipmentData')
        setLoading(false)
        setRedirect(true)
      }
    } catch (err) {
      console.error(err)
      notify(err)
    }
  }

  const stopProcess = (message) => {
    setLoading(false)
    notify(message)
  }

  const createOrder = async () => {
    // prepare object to send minimmum info
    // safely duplicate array to manipulate it
    // very doubtful of method
    setMessage('Placing temporary order')
    // let onlyIdsSampleCart = JSON.parse(JSON.stringify(context.user.cart));
    // onlyIdsSampleCart.forEach(item => {
    //   item.range = item.range._id;
    //   delete item['_id'];
    // });
    const res = await axios.post(`${URL}/orders/request/${context.user._id}`, {
      cart: context.user.cart,
      shipmentAddress: props.shipmentData.shipmentData,
      shippingMethod: props.shipmentData.shippingMethod,
      billingAddress: props.useBilling
        ? props.billingData
        : props.shipmentData.shipmentData,
      shipmentCost:
        props.shipmentData.shippingMethod === 'Collect'
          ? 0
          : props.region.shippingRate,
    })
    return res.data
  }

  const createPayment = async () => {
    setMessage('Creating payment')
    if (!stripe || !elements) {
      notify('Wait for stripe to load')
      return false
    }
    const res = await stripe.confirmPayment({
      elements,
      redirect: 'if_required',
    })
    if (res?.paymentIntent?.status !== 'succeeded') {
      notify(res.error.message)
      return false
    }
    return true
  }

  // HERE
  const updateOrder = async (order_id) => {
    setMessage('Payment confirmed. Updating order to status: completed')
    const res = await axios.put(`${URL}/orders/changestatus/${order_id}`)
    return res.data.ok ? res : false
  }

  const sendEmail = async (tempOrder) => {
    setMessage('Sending email to you and administration')
    let someDate = new Date()
    let numberOfDaysToAdd = estimatedTime(
      tempOrder.shipmentAddress.region.situation
    )
    let result = someDate.setDate(someDate.getDate() + numberOfDaysToAdd)
    const date = new Date(result).toUTCString().slice(0, 11)
    const data = {
      name: props.shipmentData.name + ' ' + (props.shipmentData.lastName || ''),
      email:
        props.shipmentData.shippingMethod === 'Ship'
          ? props.shipmentData.shipmentData.email
          : props.billingData.email,
      subject: 'Order Confirmation',
      message: 'YOUR ORDER WAS SUCCESSFUL',
      orderNumber: tempOrder.orderNumber,
      orderDate: date,
      shippingAddress: tempOrder.shipmentAddress,
      eta: estimatedTime(tempOrder.shipmentAddress.region.situation),
      billingAddress: tempOrder.billingAddress,
      shipped: tempOrder.shippingMethod === 'Ship',
      productsBought: tempOrder.productsBought,
    }
    if (data.email) {
      return await sendConfirmationEmail(data)
    } else return true
  }

  const notify = (message) => toast.error(message)

  if (redirect) {
    const url = `/checkout/success?payment_intent=${order._id}`
    return <Redirect push to={url} />
  } else
    return (
      <React.Fragment>
        <div>
          <div css={main}>
            <CreditCardOptions />
            {stripe && elements && (
              <button onClick={() => handleRequest()}>Make an order</button>
            )}
          </div>
          <span css={termsStyle}>
            By clicking the Place Order button, you conﬁrm that you have read
            and agreed to our{' '}
            <u onClick={() => window.open('/more/')}>Terms and Conditions</u>{' '}
            and our{' '}
            <u onClick={() => window.open('/more/privacy')}>Returns Policy</u>{' '}
            and acknowledge that you have read Mad About Fabric's{' '}
            <u onClick={() => window.open('/more/privacy')}>Privacy Policy</u>.
          </span>
        </div>
        <ToastContainer />
        <Modal loading={loading} message={message} />
      </React.Fragment>
    )
}

const CreditCardOptions = () => {
  return (
    <div css={flexCenterGrey}>
      <h3>ADD CARD</h3>
      <div css={cardInputsTextWrapper}>
        <PaymentElement />
      </div>
    </div>
  )
}

const flexCenterGrey = {
  display: 'flex',
  width: '100%',
  flexDirection: 'column',
  backgroundColor: 'rgba(235,235,235,1)',
  marginBottom: '1rem',
  padding: '1.5rem 2rem',
  h3: {
    letterSpacing: '2px',
    fontSize: '1.4rem',
    marginBottom: '1rem',
  },
}

const cardInputsTextWrapper = {
  gridTemplateColumns: '2fr 1fr 1fr',
  gap: '1vw',
  p: {
    fontWeight: 300,
    paddingLeft: '2px',
    fontSize: '0.9rem',
  },
}

const termsStyle = {
  fontSize: '0.85rem',
  lineHeight: '1.3rem',
  color: '#898989',
  fontWeight: '300',
}

const Modal = ({ message, loading }) => {
  return (
    <ReactModal
      ariaHideApp={false}
      isOpen={loading}
      style={{
        overlay: {
          backgroundColor: '#ffffffdd',
        },
        content: {
          position: 'relative',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
          backgroundColor: 'transparent',
          border: 'none',
          fontFamily: 'Roboto, sans-serif',
          fontSize: '0.85rem',
          letterSpacing: '2px',
          textTransform: 'uppercase',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
          width: '80vw',
          textAlign: 'center',
        },
      }}
    >
      <ReactLoading type={'spin'} color='#030303' />
      <h3 style={{ marginTop: '2rem' }}>Order being processed</h3>
      <h3>Do not go back or refresh the page until it's complete.</h3>
      <p style={{ marginTop: '2rem' }}>{message}</p>
    </ReactModal>
  )
}
