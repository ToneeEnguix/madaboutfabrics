import React from 'react'
import { Elements } from '@stripe/react-stripe-js'
import { loadStripe } from '@stripe/stripe-js'

import { STRIPE_PUBLIC_KEY } from '../../../../config'

import PaymentDisplay from './PaymentDisplay'

export default function Payment(props) {
  const stripePromise = loadStripe(STRIPE_PUBLIC_KEY)

  const appearance = {
    theme: 'stripe',
    variables: {
      colorPrimary: '#f95302',
      colorText: '#03031f',
    },
  }

  const options = {
    clientSecret: props.stripeSecret,
    appearance,
  }

  return (
    <Elements stripe={stripePromise} options={options}>
      <PaymentDisplay {...props} />
    </Elements>
  )
}
