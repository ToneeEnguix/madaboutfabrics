/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useState } from 'react'
import ShippingForm from './ShippingForm'
import ShippingDisplay from './ShippingDisplay'

const show = {
  transition: 'height 500ms',
  marginBottom: '2rem',
  boxShadow: '0px 3px 6px #00000029',
  h2: {
    textTransform: 'uppercase',
    backgroundColor: '#00263E',
    color: 'white',
    letterSpacing: '2px',
    fontSize: '1.35rem',
    marginLeft: '2%',
  },
}

const title = {
  display: 'flex',
  justifyContent: 'space-between',
  backgroundColor: '#00263E',
  height: '4rem',
  alignItems: 'center',
  padding: '0 1rem',
  span: {
    color: 'white',
    textDecoration: 'underline',
    cursor: 'pointer',
  },
  i: {
    color: 'green',
  },
}

export default function Shipping(props) {
  const [showingForm, setShowingForm] = useState(true)
  const [shipmentData, setShipmentData] = useState(props.shipmentData)

  const receiveShipmentData = (data) => {
    setShowingForm(false)
    setShipmentData(data)
    props.sendShipmentData(data)
  }

  const showForm = () => {
    setShowingForm(true)
    setShipmentData({ ...shipmentData, shippingMethod: 'Ship' })
    props.toggleShipment(true)
    props.togglePayment(false)
    props.toggleConfirm(false)
  }

  return (
    <div key={props.shipmentData.name} css={show}>
      {showingForm === true ? (
        <React.Fragment>
          <div css={title}>
            <h2>1. Shipping</h2>
          </div>
          <ShippingForm
            shipmentData={shipmentData}
            sendShipmentData={receiveShipmentData}
            toggleRegion={(region) => props.toggleRegion(region)}
            setShipped={props.setShipped}
            context={props.context}
            availableForPickup={props.availableForPickup}
          />
        </React.Fragment>
      ) : (
        <React.Fragment>
          <div css={title}>
            <div css={{ display: 'flex', alignItems: 'flex-end' }}>
              <i className='material-icons #15D032'>done</i>
              <h2>1.Shipping</h2>
            </div>
            <span
              onClick={() => {
                showForm()
              }}
            >
              Edit
            </span>
          </div>
          <ShippingDisplay shipmentData={shipmentData} />
        </React.Fragment>
      )}
    </div>
  )
}
