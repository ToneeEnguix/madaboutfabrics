/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useState, useEffect } from 'react'
import facepaint from 'facepaint'

import StyledSelect from '../../generalPurpose/StyledInput/StyledSelect'
import tick from '../../../admin/pictures/tick.svg'

// RESPONSIVENESS SETTINGS
const breakpoints = [500, 800]
const mq = facepaint(breakpoints.map((bp) => `@media (min-width: ${bp}px)`))

const formData = {
  display: 'flex',
  justifyContent: 'space-around',
  flexWrap: 'wrap',
  flexDirection: 'column',
  padding: '0 1.5rem',
  marginTop: '0.5rem',
  marginBottom: '1.5rem',
}

const shipmentMethods = {
  display: 'flex',
  alignItems: 'center',
  fontSize: '0.95rem',
  margin: '1rem 0.75rem 1.8rem 1rem',
  "input[type='radio']": {
    WebkitAppearance: 'none',
    border: '1px solid lightgray',
    borderRadius: '100px',
    width: '20px',
    height: '20px',
  },
  input: {
    color: 'black',
    margin: '0 0.5rem 0 0',
    padding: '0.5rem',
  },
  label: {
    padding: '0 1rem 0 0',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
  },
}

const buttonFlex = {
  display: 'flex',
  justifyContent: 'flex-end',
  width: '100%',
}

const inputWrapper = mq({
  position: 'relative',
  width: '100%',
  margin: ['0 0 1.3rem', '0 0 1.3rem', '0 0.75rem 1.3rem'],
  img: {
    position: 'absolute',
    top: '16px',
    right: '15px',
    filter:
      'invert(51%) sepia(99%) saturate(310%) hue-rotate(58deg) brightness(90%) contrast(90%)',
    display: 'none',
  },
})

const inputWrapper2 = mq({
  position: 'relative',
  width: '100%',
  img: {
    position: 'absolute',
    top: '16px',
    right: '15px',
    filter:
      'invert(51%) sepia(99%) saturate(310%) hue-rotate(58deg) brightness(90%) contrast(90%)',
  },
  margin: [0, 0, '0 0.75rem 1rem 0.75rem'],
  boxSizing: 'border-box',
})

const doubleWrapper = mq({
  flexDirection: ['column', 'column', 'row'],
})

const tripleWrapper = mq({
  display: 'grid',
  gridTemplateColumns: ['1fr', '1fr', '1fr 1fr 1fr'],
  gap: [0, 0, '1.3rem'],
  boxSizing: 'border-box',
  padding: [0, 0, '0 0.75rem'],
  '.postalCodeInput': {
    marginRight: 0,
    marginLeft: 0,
  },
  '.cityInput': {
    margin: 0,
    marginBottom: ['1.3rem', '1.3rem', 0],
  },
  '.regionInput': {
    marginLeft: 0,
    marginBottom: ['1.3rem', '1.3rem', 0],
  },
})

const checkoutButton = {
  backgroundColor: '#FA5402',
  outline: 'none',
  color: 'white',
  border: 'none',
  padding: '0.83rem 1.7rem',
  cursor: 'pointer',
  textTransform: 'uppercase',
  fontSize: '0.8rem',
  fontWeight: '500',
  marginRight: '0.75rem',
  fontFamily: 'Roboto, sans-serif',
}

export default function ShippingForm(props) {
  const [shipmentData, setShipmentData] = useState({
    name: '',
    lastName: '',
    direction: '',
    postalCode: '',
    town: '',
    region: '',
    email: '',
    phoneNumber: '',
  })

  useEffect(() => {
    const tempShipmentData = JSON.parse(localStorage.getItem('shipmentData'))
    tempShipmentData
      ? setShipmentData(tempShipmentData)
      : setShipmentData({
          name: props.context.user.address?.name,
          lastName: props.context.user.address?.lastName,
          direction: props.context.user.address?.direction,
          postalCode: props.context.user.address?.postalCode,
          town: props.context.user.address?.town,
          region: {
            _id: 'NIRE',
            name: 'Northern Ireland',
            coin: 'GBP',
            shippingRate: 10,
            situation: 'Ireland',
          },
          email: props.context.user.email,
          phoneNumber: props.context.user.address?.phoneNumber,
        })
  }, [props.context])

  const onRadioChange = (e) => {
    props.setShipped(false)
    let tempShipmentData = {
      name: 'Mad About Fabrics',
      lastName: 'SL',
      direction: '16-18 Dargan Crescent, Duncrue Industrial Estate',
      postalCode: 'BT3 9JP',
      town: 'Belfast',
      region: {
        _id: 'NIRE',
        name: 'Northern Ireland',
        coin: 'GBP',
        shippingRate: 10,
        situation: 'Ireland',
      },
      email: 'hello@madaboutfabrics.com',
      phoneNumber: '+44 2890 370 390',
    }
    handleSubmit(e, tempShipmentData)
  }

  const handleSubmit = (e, tempShipmentData) => {
    e && e.preventDefault()
    !tempShipmentData &&
      localStorage.setItem('shipmentData', JSON.stringify(shipmentData))
    props.sendShipmentData({
      shippingMethod: tempShipmentData ? 'Collect' : 'Ship',
      shipmentData: tempShipmentData ? tempShipmentData : shipmentData,
    })
  }

  return (
    <form css={formData} onSubmit={handleSubmit}>
      {props.availableForPickup ? (
        <div css={shipmentMethods}>
          <label>
            <input
              type='radio'
              style={{ backgroundColor: 'black' }}
              className='pointer'
            />
            Home / Office
          </label>
          <label>
            <input type='radio' onChange={onRadioChange} className='pointer' />
            Click & Collect
          </label>
        </div>
      ) : (
        <div style={{ margin: '1rem auto' }} />
      )}
      <div css={doubleWrapper} className='inputsWrapper' id='form_validation'>
        <div css={inputWrapper}>
          <input
            defaultValue={shipmentData.name}
            className='styledInput2'
            name='name'
            placeholder='Name'
            autoFocus
            type='text'
            required
            onChange={(e) =>
              setShipmentData({ ...shipmentData, name: e.target.value })
            }
          />
          <img src={tick} alt='tick' />
        </div>
        <div css={inputWrapper}>
          <input
            defaultValue={shipmentData.lastName}
            className='styledInput2'
            name='lastName'
            placeholder='Surname/s'
            onChange={(e) =>
              setShipmentData({ ...shipmentData, lastName: e.target.value })
            }
            required
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
      </div>
      <div className='inputsWrapper' id='form_validation'>
        <div css={inputWrapper}>
          <input
            required
            defaultValue={shipmentData.direction}
            className='styledInput2'
            name='direction'
            placeholder='Address'
            onChange={(e) =>
              setShipmentData({ ...shipmentData, direction: e.target.value })
            }
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
      </div>
      <div className='inputsWrapper' css={tripleWrapper} id='form_validation'>
        <div className='postalCodeInput' css={inputWrapper}>
          <input
            required
            defaultValue={shipmentData.postalCode}
            className='styledInput2'
            name='postalCode'
            placeholder='Postal Code'
            onChange={(e) =>
              setShipmentData({ ...shipmentData, postalCode: e.target.value })
            }
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
        <div className='cityInput' css={inputWrapper}>
          <input
            required
            defaultValue={shipmentData.town}
            className='styledInput2'
            name='town'
            placeholder='City'
            onChange={(e) =>
              setShipmentData({ ...shipmentData, town: e.target.value })
            }
            type='text'
          />
          <img src={tick} alt='tick' />
        </div>
        <div className='regionInput' css={inputWrapper2}>
          <StyledSelect
            innerName={'region'}
            toggleRegion={(region) => {
              props.toggleRegion(region)
              setShipmentData({ ...shipmentData, region: region })
            }}
            region={shipmentData.region}
          />
        </div>
      </div>
      <div css={doubleWrapper} className='inputsWrapper' id='form_validation'>
        <div css={inputWrapper}>
          <input
            required
            defaultValue={shipmentData.email}
            className='styledInput2'
            name='email'
            placeholder='Email'
            onChange={(e) =>
              setShipmentData({ ...shipmentData, email: e.target.value })
            }
            type='email'
          />
          <img src={tick} alt='tick' />
        </div>
        <div css={inputWrapper}>
          <input
            required
            defaultValue={shipmentData.phoneNumber}
            type='number'
            className='styledInput2'
            name='phoneNumber'
            placeholder='Phone Number'
            onChange={(e) =>
              setShipmentData({
                ...shipmentData,
                phoneNumber: e.target.value,
              })
            }
          />
          <img src={tick} alt='tick' />
        </div>
      </div>
      <div css={buttonFlex}>
        <button type='submit' css={checkoutButton}>
          Continue
        </button>
      </div>
    </form>
  )
}
