/** @jsx jsx */
import { jsx } from '@emotion/react'
import React from 'react'

import { estimatedTime } from '../../helpers/EstimatedTime'
import { calculatePickupTime } from '../../helpers/getTime'

const contentWrapper = {
  display: 'flex',
  flexDirection: 'column',
  padding: '1.5rem 2rem',
  span: { lineHeight: '1.65rem' },
  fontSize: '1rem',
  color: 'gray',
  fontWeight: '300',
  '.titleStyle': {
    color: 'black',
    fontWeight: '400',
  },
}

export default function ShippingDisplay(props) {
  return (
    <div css={contentWrapper}>
      <span className='titleStyle'>Shipping Address</span>
      <span>
        {props.shipmentData.shipmentData.name}{' '}
        {props.shipmentData.shipmentData.lastName}
      </span>
      <span>{props.shipmentData.shipmentData.direction}</span>
      <span>
        {props.shipmentData.shipmentData.town},{'  '}
        {props.shipmentData.shipmentData.postalCode}
      </span>
      <span>{props.shipmentData.shipmentData.region.name}</span>
      <span>{props.shipmentData.shipmentData.email}</span>
      <span>{props.shipmentData.shipmentData.phoneNumber}</span>
      {props.shipmentData.shippingMethod === 'Collect' ? (
        <React.Fragment>
          <span className='titleStyle' style={{ marginTop: '1rem' }}>
            Pick-Up Time
          </span>
          <span>Your samples will be ready for collection by:</span>
          <span>{calculatePickupTime()}</span>
        </React.Fragment>
      ) : (
        <React.Fragment>
          <span className='titleStyle' style={{ marginTop: '1rem' }}>
            Shipping Time
          </span>
          <span>
            Postal Delivery: £
            {props.shipmentData.shipmentData.region.shippingRate}
          </span>
          <span className='titleStyle'>
            Get it in{' '}
            {estimatedTime(props.shipmentData.shipmentData.region.situation)}{' '}
            {estimatedTime(props.shipmentData.shipmentData.region.situation) ===
            1 ? (
              <React.Fragment> day </React.Fragment>
            ) : (
              <React.Fragment> days </React.Fragment>
            )}
            after shipment confirmation.
          </span>
        </React.Fragment>
      )}
    </div>
  )
}
