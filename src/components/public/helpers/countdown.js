/** @jsx jsx */
import { jsx } from '@emotion/react'
import { useEffect, useState } from 'react'

const countdownStyle = {
  fontFamily: 'Montserrat, sans-serif',
  fontWeight: '700',
  width: 'fit-content !important',
  height: '6rem',
  minWidth: '20vw',
  fontSize: '4rem',
  padding: '0 2rem',
  pre: {
    fontSize: 'inherit',
    fontFamily: 'inherit',
    fontWeight: 'inherit',
    letterSpacing: 'inherit',
  },
}

export default function CountDown(props) {
  const [countdown, setCountdown] = useState({
    days: 0,
    hours: 0,
    minutes: 0,
    seconds: 0,
  })

  useEffect(() => {
    const intervalId = setInterval(() => {
      // get total seconds between the times
      let delta = Math.abs(
        (new Date(props.futureDate) - new Date(Date.now())) / 1000
      )
      // calculate (and subtract) whole days
      let days = Math.floor(delta / 86400)
      delta -= days * 86400

      // calculate (and subtract) whole hours
      let hours = (Math.floor(delta / 3600) % 24) - 2
      delta -= hours * 3600

      // calculate (and subtract) whole minutes
      let minutes = Math.floor(delta / 60) % 60
      delta -= minutes * 60

      // what's left is seconds
      let seconds = Math.floor(delta % 60) // in theory the modulus is not required
      setCountdown({ days, hours, minutes, seconds })
    }, 1000)
    return () => clearInterval(intervalId)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <div css={countdownStyle} className='flexCenter'>
      <span>{countdown.days}</span>
      <Dots />
      <span>
        {countdown.hours < 10 ? '0' + countdown.hours : countdown.hours}
      </span>
      <Dots />
      <span>
        {countdown.minutes < 10 ? '0' + countdown.minutes : countdown.minutes}
      </span>
      <Dots />
      <span>
        {countdown.seconds < 10 ? '0' + countdown.seconds : countdown.seconds}
      </span>
    </div>
  )
}

const Dots = (props) => {
  return (
    <span>
      <pre> : </pre>
    </span>
  )
}
