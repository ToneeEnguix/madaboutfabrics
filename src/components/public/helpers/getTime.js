export const getTime = (date, days = 0, time) => {
  let result = new Date(date);
  result.setDate(result.getDate() + days);

  let formattedDate =
    new Date(result).toLocaleDateString() +
    " @" +
    (time
      ? time
      : new Date(result).getHours() +
        ":" +
        new Date(result).getMinutes().toLocaleString("en-US", {
          minimumIntegerDigits: 2,
        }));

  return formattedDate;
};

export const calculatePickupTime = () => {
  if (
    new Date(Date.now()).getDay() === 1 ||
    new Date(Date.now()).getDay() === 2 ||
    new Date(Date.now()).getDay() === 3 ||
    new Date(Date.now()).getDay() === 7
  ) {
    return getTime(Date.now(), 1, "15:00");
  } else if (new Date(Date.now()).getDay() === 4) {
    return getTime(Date.now(), 4, "15:00");
  } else if (new Date(Date.now()).getDay() === 5) {
    return getTime(Date.now(), 3, "15:00");
  } else if (new Date(Date.now()).getDay() === 6) {
    return getTime(Date.now(), 2, "15:00");
  }
};
