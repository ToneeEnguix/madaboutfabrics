export const estimatedTime = region => {
  if (region === 'Ireland') {
    return 1;
  } else if (region === 'Britain') {
    return 3;
  } else if (region === 'Mainland') {
    return 7;
  } else return 2;
};
