/** @jsx jsx */
import { jsx } from '@emotion/react'
import { Route, Redirect } from 'react-router-dom'
import { isLogin } from '../../services/login.js'

const PublicRoute = ({ component: Component, restricted, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) =>
        isLogin() && restricted ? (
          <Redirect to='/home' />
        ) : (
          <Component {...props} />
        )
      }
    />
  )
}

export default PublicRoute
