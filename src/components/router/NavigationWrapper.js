/** @jsx jsx */
import { jsx } from '@emotion/react'

import Display from '../public/generalPurpose/display/desktop/Display'
import Footer from '../public/generalPurpose/footer/Footer'
import NavBar from '../public/generalPurpose/navBar/NavBar'

export default function NavigationWrapper(props) {
  if (
    !props.location.pathname.includes('adminlogin') &&
    !props.location.pathname.includes('checkout')
  ) {
    return (
      <div>
        <div css={navbar}>
          <Display {...props} />
        </div>
        <NavBar />
        {props.children}
        <Footer />
      </div>
    )
  } else return props.children
}

const navbar = {
  position: 'sticky',
  top: 0,
  backgroundColor: 'white',
  zIndex: 10,
  boxShadow: 'inset 1px 1px 2px #00000029',
  minWidth: '100vw',
}
